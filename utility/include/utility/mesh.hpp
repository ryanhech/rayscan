#ifndef INX_UTILITY_MESH_HPP_INCLUDED
#define INX_UTILITY_MESH_HPP_INCLUDED

#include <polyanyalib/Mesh.hpp>
#include <rayscanlib/scen/PolygonEnvironment.hpp>
#include <rayscanlib/geometry/Box.hpp>
#include <vector>

namespace utility
{

class SaveData : public std::vector<std::vector<bool>>
{
public:
	using point = ::rayscanlib::geometry::Point<double>;
	using box_type = ::rayscanlib::geometry::Box<double>;
	using env_type = ::rayscanlib::scen::PolygonEnvironment;
	using mesh_type = ::polyanyalib::Mesh;
	static constexpr ssize_t Padding = 4;
	double scale;
	double iscale;
	box_type box;
	ssize_t width;
	ssize_t height;
	std::variant<env_type*, mesh_type*> env;
	point cellToEnv(ssize_t x, ssize_t y);
	std::pair<ssize_t,ssize_t> envToCell(point pt);
	void loadEnvironment(env_type& e);
	void loadMesh(mesh_type& m);
	void populateVars(double scale, bool setupData);
};

struct Component
{
	int id;
	double area_percent;
	std::vector<int> faces;
};
struct Face
{
	int id;
	int vertices;
	::rayscanlib::geometry::Point<double> centre;
	double area;
	bool init;
	bool trav;
	int comp_id;
};
struct MeshS
{
	double total_area;
	double trav_area;
};

class MeshStats
{
public:
	double face_area(int i) const;
	::rayscanlib::geometry::Point<double> face_centre(int i) const;
	void init_face(int i);
	bool load_mesh(const ::polyanyalib::Mesh& mesh);
	void sort_components_by_size();

	const MeshS& getMeshStats() const noexcept { return m_meshS; }
	const std::vector<Face>& getFaces() const noexcept { return m_faces; }
	const std::vector<const Face*>& getOrderedFaces() const noexcept { return m_orderedFaces; }
	const std::vector<Component>& getComponents() const noexcept { return m_comps; }

private:
	const ::polyanyalib::Mesh* m_mesh;
	MeshS m_meshS;
	std::vector<Face> m_faces;
	std::vector<const Face*> m_orderedFaces;
	std::vector<Component> m_comps;
};

} // namespace utility

#endif // INX_UTILITY_MESH_HPP_INCLUDED
