#include <utility/mesh.hpp>
#include <ranges>

namespace utility
{
	
auto SaveData::cellToEnv(ssize_t x, ssize_t y) -> point
{
	y = height - y - 1;
	return point((x - Padding) * iscale + box.lower().x, (y - Padding) * iscale + box.lower().y);
}
auto SaveData::envToCell(point pt) -> std::pair<ssize_t,ssize_t>
{
	pt -= box.lower();
	pt = scale * pt;
	pt.x += Padding; pt.y += Padding;
	pt.y = height - pt.y - 1;
	return {static_cast<ssize_t>(std::floor(pt.x + inx::high_epsilon<double>)),
			static_cast<ssize_t>(std::floor(pt.y + inx::high_epsilon<double>))};
}

void SaveData::loadEnvironment(env_type& e)
{
	box = e.getEnclosureBox();
	env.emplace<0>(&e);
}
void SaveData::loadMesh(mesh_type& m)
{
	box = m.getBounds();
	env.emplace<1>(&m);
}

void SaveData::populateVars(double scale, bool setupData)
{
	this->scale = scale;
	iscale = 1.0 / scale;
	box.lower().x = std::floor(box.lower().x);
	box.lower().y = std::floor(box.lower().y);
	box.upper().x = std::ceil(box.upper().x);
	box.upper().y = std::ceil(box.upper().y);
	width = static_cast<ssize_t>(std::ceil(box.width() * scale)) + 2*SaveData::Padding;
	height = static_cast<ssize_t>(std::ceil(box.height() * scale)) + 2*SaveData::Padding;
	if (setupData)
		assign(height, std::vector<bool>(width, false));
}

double MeshStats::face_area(int i) const
{
	assert(m_mesh != nullptr);
	auto& F = m_mesh->mesh_polygons[i];
	if (!F.isInitalised())
		return -1;
	long double a = 0.0;
	auto p0 = m_mesh->mesh_vertices[F.vertices[0]].p;
	for (int j = 1, je = static_cast<int>(F.vertices.size()-1); j < je; ++j) {
		a += p0.cross(m_mesh->mesh_vertices[F.vertices[j]].p, m_mesh->mesh_vertices[F.vertices[j+1]].p);
	}
	return std::abs(static_cast<double>(0.5 * a));
}
::rayscanlib::geometry::Point<double> MeshStats::face_centre(int i) const
{
	auto& F = m_mesh->mesh_polygons[i];
	long double x = 0, y = 0;
	for (int j : F.vertices) {
		auto& p = m_mesh->mesh_vertices[j].p;
		x += p.x; y += p.y;
	}
	double s = 1.0 / F.vertices.size();
	return ::rayscanlib::geometry::Point<double>(static_cast<double>(x * s), static_cast<double>(y * s));
}
void MeshStats::init_face(int i)
{
	Face& face = m_faces[i];
	auto& F = m_mesh->mesh_polygons[i];
	face.id = i;
	if (F.isInitalised()) {
		face.vertices = F.vertices.size();
		face.centre = face_centre(i);
		face.area = face_area(i);
		face.init = true;
		face.trav = F.isTraversable();
	} else {
		face.vertices = 0;
		face.centre = ::rayscanlib::geometry::Point<double>::zero();
		face.area = 0;
		face.init = false;
		face.trav = false;
	}
	face.comp_id = -1;
}
bool MeshStats::load_mesh(const ::polyanyalib::Mesh& mesh)
{
	m_mesh = &mesh;
	int mesh_size = static_cast<int>(mesh.mesh_polygons.size());
	// std::vector<double> v_dbl;

	// setup face
	m_faces.assign(mesh_size, Face{});
	for (int i = 0; i < mesh_size; ++i) {
		init_face(i);
	}
	{
		auto Fptr = m_faces | std::views::transform([](const Face& a) noexcept { return &a; });
		m_orderedFaces.assign(Fptr.begin(), Fptr.end());
		auto Fpart = std::ranges::stable_partition(m_orderedFaces, std::identity(), &Face::init);
		auto Fsort = std::ranges::subrange(m_orderedFaces.begin(), Fpart.begin());
		std::ranges::stable_sort(Fsort, {}, &Face::area);
		auto Farea = Fsort | std::views::transform(&Face::area);
		m_meshS.total_area = static_cast<double>(std::accumulate(Farea.begin(), Farea.end(), static_cast<long double>(0)));
		auto Ftrav = Fsort | std::views::filter(&Face::trav) | std::views::transform(&Face::area);
		m_meshS.trav_area = static_cast<double>(std::accumulate(Ftrav.begin(), Ftrav.end(), static_cast<long double>(0)));
	}

	m_comps.clear();
	// generate
	std::stack<int> qu;
	for (Face& F : m_faces) {
		if (!F.trav || F.comp_id >= 0)
			continue;
		auto& P = mesh.mesh_polygons[F.id];
		auto& C = m_comps.emplace_back();
		C.id = static_cast<int>(m_comps.size()-1);
		qu.push(F.id);
		while (!qu.empty()) {
			int qx = qu.top(); qu.pop();
			Face& Fx = m_faces[qx];
			if (!Fx.trav) {
				assert(false);
				return false;
			}
			if (Fx.comp_id >= 0)
				continue;
			assert(Fx.comp_id < 0 || Fx.comp_id == C.id);
			auto& Px = mesh.mesh_polygons[qx];
			Fx.comp_id = C.id;
			C.faces.push_back(qx);
			for (int j : Px.polygons) {
				if (j > 0 && m_faces[j].comp_id < 0) {
					qu.push(j);
				}
			}
		}
		// sort and update face-id
		auto Farea = [&faces=m_faces](int i) noexcept { return faces[i].area; };
		std::ranges::sort(C.faces, {}, Farea);
		auto Carea = C.faces | std::views::transform(Farea);
		long double areaX = std::accumulate(Carea.begin(), Carea.end(), static_cast<long double>(0.0));
		C.area_percent = std::clamp(static_cast<double>(areaX / m_meshS.trav_area), 0.0, 1.0);
	}
	std::ranges::sort(m_comps, std::ranges::greater(), &Component::area_percent);
	std::vector<int> compTraslate(m_comps.size(), 0);
	for (int i = 0, ie = static_cast<int>(m_comps.size()); i < ie; ++i) {
		compTraslate[m_comps[i].id] = i;
		m_comps[i].id = i;
	}
	for (Face& F : m_faces) {
		if (F.comp_id >= 0) {
			F.comp_id = compTraslate[F.comp_id];
		}
	}
	return true;
}

} // namespace utility
