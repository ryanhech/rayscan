#include <rayscanlib/scen/PolygonEnvironment.hpp>
#include <rayscanlib/scen/StaticBenchmarkSingleTarget.hpp>
#include <polyanyalib/Mesh.hpp>
#include <polyanyalib/IntervalFacer.hpp>
#include <utility/mesh.hpp>
#include <inx/alg/bit_table.hpp>
#include <vector>
#include <string_view>
#include <string>
#include <filesystem>
#include <iostream>
#include <fstream>
#include <random>
#include <chrono>
#include <cmath>
#include <boost/lexical_cast.hpp>
#include <boost/container/small_vector.hpp>
#include <boost/container_hash/hash.hpp>

using args_type = std::vector<std::string_view>;

using polygon_type = rayscanlib::geometry::Polygon<double>;
using point_type = polygon_type::point_type;

namespace utility {

namespace {

struct PrintPolygon
{
	const polygon_type* m_P;
	std::string_view m_groupSplit;

	PrintPolygon(const polygon_type& P, std::string_view groupSplit = "\n\t") noexcept
		: m_P(&P), m_groupSplit(groupSplit)
	{ }
};
std::ostream& operator<<(std::ostream& out, const PrintPolygon& PP)
{
	const polygon_type& P = *PP.m_P;
	const std::string_view groupSplit = PP.m_groupSplit;
	out << '[' << P.size() << "] {";
	unsigned i = 0;
	for (auto it = P.begin(), ite = P.end(); it != ite; ++it) {
		if (i != 0)
			out << ',';
		if ((i & 0b111) == 0b111)
			out << groupSplit;
		out << " (" << it->x << ',' << it->y << ')';
	}
	out << " }";
	return out;
}

struct Map : inx::alg::bit_table<1, 1>
{
	uint32_t width, height;

	constexpr static bool passable(char c) noexcept
	{
		switch (c)
		{
			case '.':
			case 'G':
				return true;
			default:
				return false;
		}
	}

	int read(std::istream& rd)
	{
		std::string ign;
		if ( !(rd >> ign >> ign >> ign >> height >> ign >> width >> ign) )
			return 2;
		if (width == 0 || width > 100'000 || height == 0 || height > 100'000)
			return 3;
		// read map now
		setup(width, height);
		for (uint32_t y = 0; y < height; ++y) {
			if ( !(rd >> ign) )
				return 2;
			for (uint32_t x = 0; x < width; ++x) {
				if (passable(ign[x])) {
					bit_or(x, y, 1);
				}
			}
		}
		return 0;
	}
};
std::istream& operator>>(std::istream& in, Map& map)
{
	map.read(in);
	return in;
}

int mesh_stats(const args_type& args)
{
	if (args.size() != 1)
		return 1;

	// load map
	using namespace std::string_view_literals;
	using mesh_type = polyanyalib::Mesh;
	using env_type = rayscanlib::scen::PolygonEnvironment;
	using ptd = rayscanlib::geometry::Point<double>;
	bool check_env = false;
	mesh_type mesh;
	env_type env;
	{
		rayscanlib::scen::Archiver archive;
		archive.load(args[0]);
		mesh.load(archive);
		if (archive.has("polygonEnv"sv)) {
			check_env = true;
			env.load(archive);
		}
	}
	if (!mesh.isMeshValid())
		return 2;

	// print out
	std::ostream& out = std::cout;
	out.precision(12);
	
	std::vector<const env_type::polygon*> envPolys;
	std::vector<env_type::point> envPoints;
	if (check_env) {
		// PolygonEnvironment data
		size_t obsts = env.countPolygons();
		size_t obstsVerts = env.countVertices();

		// Mesh and PolygonEnvironment point info
		for (const auto& ply : env.getEnclosures()) {
			envPolys.emplace_back(&ply);
		}
		for (const auto& ply : env.getObstacles()) {
			envPolys.emplace_back(&ply);
		}
		// find all points
		for (size_t i = 0; i < envPolys.size(); ++i) {
			auto* ply = envPolys[i];
			for (auto& pt : *ply) {
				envPoints.emplace_back(pt);
			}
			for (size_t j = i+1; j < envPolys.size(); ++j) {
				auto* ply2 = envPolys[j];
				env_type::point a, b = ply->back(), c, d;
				for (auto it = ply->begin(), ite = ply->end(); it != ite; ++it) {
					a = b; b = *it;
					d = ply2->back();
					for (auto jt = ply2->begin(), jte = ply2->end(); jt != jte; ++jt) {
						c = d; d = *jt;
						if (auto I = rayscanlib::geometry::segment_intersect(a, b-a, c, d-c); I.intersect() && I.rangeAexc() && I.rangeBexc()) {
							envPoints.emplace_back(a + I.scaleA() * (b-a));
						}
					}
				}
			}
		}
		size_t obstIntersectVertices = envPoints.size();
		// get obstacle point info
		double obstMinEdgeLength = rayscanlib::inf<double>;
		double obstClosestPointDist = rayscanlib::inf<double>;
		double obstClostestIntersectPointDist = rayscanlib::inf<double>;
		double obstAngMax = 0;
		double obstAngMin = rayscanlib::inf<double>;
		for (size_t i = 0; i < envPolys.size(); ++i) {
			auto* ply = envPolys[i];
			auto pv = ply->back();
			for (size_t pi = 0; pi < ply->size(); ++pi) {
				auto [at,nx] = ply->segment(pi);
				obstMinEdgeLength = std::min(obstMinEdgeLength, at.distance(nx));
				double ang = at.cross(pv, nx);
				if (ptd::isColin(ang)) {
					ang = at.isBack(pv, nx) ? 180.0 : 0.0;
				} else {
					ang /= at.distance(nx);
					ang /= at.distance(pv);
					ang = std::acos(std::clamp(ang, -1.0, 1.0));
					ang = ang / rayscanlib::pi<double> * 180.0;
				}
				obstAngMax = std::max(obstAngMax, ang);
				obstAngMin = std::min(obstAngMin, ang);
				for (size_t pj = 0; pj < ply->size(); ++pj) {
					if (pi == pj)
						continue;
					obstClosestPointDist = std::min(obstClosestPointDist, at.distance(ply->point(pj)));
				}
				for (size_t j = i+1; j < envPolys.size(); ++j) {
					auto* ply2 = envPolys[j];
					for (auto& pt : *ply2) {
						obstClosestPointDist = std::min(obstClosestPointDist, at.distance(pt));
					}
				}
			}
		}
		for (size_t i = 0; i < envPoints.size(); ++i) {
			for (size_t j = i+1; j < envPoints.size(); ++j) {
				obstClostestIntersectPointDist = std::min(obstClostestIntersectPointDist, envPoints[i].distance(envPoints[j]));
			}
		}
		out << "obstacles = " << obsts << std::endl
			<< "obstacles-vertices = " << obstsVerts << std::endl
			<< "obstacles-vertices-with-intersections = " << obstIntersectVertices << std::endl
			<< "obstacles-min-edge-length = " << obstMinEdgeLength << std::endl
			<< "obstacles-colin-angle-min = " << obstAngMin << std::endl
			<< "obstacles-colin-angle-max = " << obstAngMax << std::endl
			<< "obstacles-clostest-points-dist = " << obstClosestPointDist << std::endl
			<< "obstacles-clostest-points-dist-with-intersections = " << obstClostestIntersectPointDist << std::endl;
	}
	// Mesh data
	utility::MeshStats mstats;
	mstats.load_mesh(mesh);
	size_t faces = 0;
	size_t travFaces = 0;
	size_t travEdgeDeg = 0;
	ptd lowerBound = mesh.getBounds().lower();
	ptd upperBound = mesh.getBounds().upper();
	double faceMinEdgeLength = rayscanlib::inf<double>;
	double faceMinEdgeTravLength = rayscanlib::inf<double>;
	// Mesh face info
	for (auto& P : mesh.mesh_polygons) {
		if (!P.isInitalised())
			continue;
		faces += 1;
		for (size_t i = 0, ie = P.vertices.size(); i < ie; ++i) {
			auto p1 = mesh.mesh_vertices[P.vertices[i]].p;
			auto p2 = mesh.mesh_vertices[P.vertices[(i+P.vertices.size()-1) % P.vertices.size()]].p;
			double len = p1.distance(p2);
			faceMinEdgeLength = std::min(faceMinEdgeLength, len);
			if (P.isTraversable() && P.polygons[i] > 0) {
				faceMinEdgeTravLength = std::min(faceMinEdgeTravLength, len);
			}
		}
		if (P.isTraversable()) {
			travFaces += 1;
			for (int i : P.polygons) {
				if (i > 0)
					travEdgeDeg += 1;
			}
		}
	}
	// Mesh verticies info
	size_t verts = 0;
	size_t vertsDeg = 0;
	std::vector<int> nei;
	for (auto& V : mesh.mesh_vertices) {
		if (!V.isInitalised())
			continue;
		verts += 1;
		nei.clear();
		for (int i : V.polygons) {
			if (i <= 0)
				continue;
			auto& P = mesh.mesh_polygons[i];
			uint32_t vid = static_cast<uint32_t>(P.findVertex(V.id));
			nei.push_back(P.vertices[(vid + 1) % P.vertices.size()]);
			nei.push_back(P.vertices[(vid + P.vertices.size() + 1) % P.vertices.size()]);
		}
		std::sort(nei.begin(), nei.end());
		vertsDeg += std::distance(nei.begin(), std::unique(nei.begin(), nei.end()));
	}

	// Mesh connected components

	// get mesh point info
	double maxAdjDistance = 0;
	for (auto& V : mesh.mesh_vertices) {
		if (!V.isInitalised())
			continue;
		double len = rayscanlib::inf<double>;
		for (auto& pt : envPoints) {
			len = std::min(len, pt.distance(V.p));
		}
		if (len < 1)
			maxAdjDistance = std::max(maxAdjDistance, len);
	}

	out << "mesh-bounds-x1 = " << lowerBound.x << std::endl
		<< "mesh-bounds-y1 = " << lowerBound.y << std::endl
		<< "mesh-bounds-x2 = " << upperBound.x << std::endl
		<< "mesh-bounds-y2 = " << upperBound.y << std::endl
		<< "mesh-bounds-width = " << (upperBound.x - lowerBound.x) << std::endl
		<< "mesh-bounds-height = " << (upperBound.y - lowerBound.y) << std::endl
		<< "faces = " << faces << std::endl
		<< "trav-faces = " << travFaces << std::endl
		<< "trav-faces-avg-deg = " << (static_cast<long double>(travEdgeDeg) / travFaces) << std::endl
		<< "faces-area = " << mstats.getMeshStats().total_area << std::endl
		<< "trav-faces-area = " << mstats.getMeshStats().trav_area << std::endl
		<< "faces-min-edge-length = " << faceMinEdgeLength << std::endl
		<< "faces-min-trav-edge-length = " << faceMinEdgeTravLength << std::endl
		<< "vertices = " << verts << std::endl
		<< "vertices-avg-deg = " << (static_cast<long double>(vertsDeg) / verts) << std::endl;
	
	if (check_env) {
		// get mesh point info
		double maxAdjDistance = 0;
		for (auto& V : mesh.mesh_vertices) {
			if (!V.isInitalised())
				continue;
			double len = rayscanlib::inf<double>;
			for (auto& pt : envPoints) {
				len = std::min(len, pt.distance(V.p));
			}
			if (len < 1)
				maxAdjDistance = std::max(maxAdjDistance, len);
		}
		out << "vertices-max-adjustment-from-obstacles = " << maxAdjDistance << std::endl;
	}

	// components
	std::vector<int> comps(mstats.getComponents().size(), 0);
	std::iota(comps.begin(), comps.end(), 0);
	std::sort(comps.begin(), comps.end(), [&C=mstats.getComponents()](int a, int b){
		return C[a].area_percent > C[b].area_percent;
	});
	out	<< "components-count = " << comps.size() << std::endl;
	for (int i = 0; i < comps.size(); i++) {
		const auto& C = mstats.getComponents()[comps[i]];
		out << "components-" << i << "-faces = " << C.faces.size() << std::endl
			<< "components-" << i << "-point = " << mstats.getFaces()[C.faces.front()].centre << std::endl
			<< "components-" << i << "-trav-area-percent = " << C.area_percent << std::endl;
	}

	return 0;
}

int random_queries(const args_type& args)
{
	if (args.size() < 2 || args.size() > 4)
		return 1;

	// load map
	using namespace std::string_view_literals;
	using mesh_type = polyanyalib::Mesh;
	using ptd = rayscanlib::geometry::Point<double>;
	mesh_type mesh;
	rayscanlib::scen::Archiver archive;
	archive.load(args[0]);
	mesh.load(archive);
	if (!mesh.isMeshValid())
		return 2;

	size_t N = boost::lexical_cast<size_t>(args[1]);
	double min_dist = 0.0;
	if (args.size() > 2) {
		min_dist = boost::lexical_cast<double>(args[2]);
		if (!std::isfinite(min_dist) || min_dist < 0 || min_dist > 1e9)
			return 1;
	}
	bool output_points = true;
	std::string outFile;
	if (args.size() > 3) {
		output_points = false;
		outFile = args[3];
	}

	// print out
	std::ostream& out = std::cout;
	out.precision(12);

	MeshStats mstats;
	mstats.load_mesh(mesh);

	struct Comp
	{
		const utility::Component* comp;
		std::vector<double> acc_faces;
	};
	std::vector<Comp> C(mstats.getComponents().size(), Comp{});
	std::vector<double> acc_comps(C.size(), 0);
	{
		long double area = 0;
		for (int i = 0, ie = static_cast<int>(C.size()); i < ie; ++i) {
			const utility::Component& Cq = mstats.getComponents()[i];
			Comp& Cx = C[i];
			Cx.comp = &Cq;
			area += Cq.area_percent;
			acc_comps[i] = static_cast<double>(area);
			long double farea = 0;
			long double trav = static_cast<long double>(Cq.area_percent) * mstats.getMeshStats().trav_area;
			trav = 1.0 / trav;
			Cx.acc_faces.assign(Cq.faces.size(), 0);
			for (int j = 0, je = static_cast<int>(Cq.faces.size()); j < je; ++j) {
				farea += mstats.getFaces()[Cq.faces[j]].area;
				Cx.acc_faces[j] = static_cast<double>(farea * trav);
			}
			Cx.acc_faces.back() = 2;
		}
	}
	acc_comps.back() = 2;

	struct PointSpace {
		using point = ::rayscanlib::geometry::Point<double>;
		using type = int64_t;
		using loc = std::pair<type, type>;
		
		PointSpace(double l_dist) {
			if (l_dist < 1e-9) {
				dist = 0.0;
				iscale = 1.0;
			}
			dist = l_dist * l_dist;
			iscale = 1.0 / l_dist;
		}

		type cast(double q) const
		{
			return static_cast<type>(std::round(q * iscale));
		}
		loc cast(point q) const
		{
			return loc(cast(q.x), cast(q.y));
		}
		void insert(point q)
		{
			if (!init())
				return;
			loc P = cast(q);
			for (int x = -1; x < 2; ++x)
			for (int y = -1; y < 2; ++y) {
				space[loc(P.first + x, P.second + y)].push_back(q);
			}
		}
		bool nearby(point q) const
		{
			if (!init())
				return false;
			loc P = cast(q);
			for (int x = -1; x < 2; ++x)
			for (int y = -1; y < 2; ++y) {
				auto it = space.find(loc(P.first + x, P.second + y));
				if (it != space.end()) {
					for (const point& p : it->second) {
						if (near(q, p))
							return true;
					}
				}
			}
			return false;
		}
		bool near(point q, point p) const
		{
			return q.square(p) < dist;
		}

		bool init() const noexcept { return dist != 0.0; }
		double dist;
		double iscale;
		struct H
		{
			size_t operator()(const loc& q) const { return boost::hash_value(q); }
		};
		std::unordered_map<loc, boost::container::small_vector<point, 2>, H> space;
	} ps(min_dist);

	std::random_device rd;
	std::mt19937_64 rand(rd());
	std::uniform_real_distribution<double> rand1(0.0, 1.0);
	int randcomp = 0;
	auto&& random_point = [&](bool prevComp, int repeat = 0) -> std::pair<PointSpace::point, bool> {
		PointSpace::point q;
		do {
			double rv, rv2;
			if (!prevComp) {
				rv = rand1(rand);
				randcomp = std::lower_bound(acc_comps.cbegin(), acc_comps.cend(), rv) - acc_comps.cbegin();
			}
			const Comp& Cx = C.at(randcomp);
			rv = rand1(rand);
			int randface = std::lower_bound(Cx.acc_faces.cbegin(), Cx.acc_faces.cend(), rv) - Cx.acc_faces.cbegin();
			const utility::Face& Fx = mstats.getFaces()[Cx.comp->faces.at(randface)];
			rv = 2.0 * rand1(rand) * Fx.area;
			auto& poly = mesh.mesh_polygons[Fx.id];
			PointSpace::point p0 = mesh.mesh_vertices[poly.vertices.front()].p;
			PointSpace::point p1 = mesh.mesh_vertices[poly.vertices.at(1)].p - p0;
			double parea = 0.0;
			for (int pi = 0, pie = static_cast<int>(poly.vertices.size()) - 1; pi <= pie; ++pi) {
				PointSpace::point p2 = mesh.mesh_vertices[poly.vertices.at(pi)].p - p0;
				if (pi == pie || (parea += std::abs(p1.cross(p2))) >= rv) {
					// this face
					rv = rand1(rand); rv2 = rand1(rand);
					if (rv + rv2 > 1)
						rv = 1.0 - rv, rv2 = 1.0 - rv2;
					q = p0 + (rv * p1 + rv2 * p2);
					break;
				}
				p1 = p2;
			}
			if (repeat == std::numeric_limits<int>::min()) {// for inf loop, assume not checking result so terminate
				repeat = 0;
			}
		} while (--repeat != 0 && ps.nearby(q));
		return {q, repeat != 0};
	};

	// output
	bool useFile = false;
	if (!output_points && !outFile.empty()) {
		useFile = true;
	}
	out << std::setprecision(12);
	if (output_points) {
		for (int i = 0; i < N; i += 1) {
			PointSpace::point P = random_point(false).first;
			std::cout << P.x << ' ' << P.y << '\n';
			ps.insert(P);
		}
		out << std::flush;
	} else {
		::rayscanlib::scen::StaticBenchmarkSingleTarget bench;
		bench.beginInserting();
		for (int i = 0; i < N; ) {
			PointSpace::point P, Q;
			bool suc = false;
			while (!suc) {
				P = random_point(false).first;
				for (int i = 0; i < 100; ++i) {
					auto res = random_point(true, 1000);
					if (!res.second)
						continue;
					Q = res.first;
					if (!ps.near(Q, P)) {
						suc = true;
						bench.addInstanceNoCheck(P, Q);
						ps.insert(P);
						ps.insert(Q);
						break;
					}
				}
				if (suc) {
					bench.addInstanceNoCheck(Q, P);
					ps.insert(Q);
					ps.insert(P);
					i += 1;
				}
			}
		}
		bench.endInserting();
		bench.save(archive, false);
		archive.save(std::filesystem::path(outFile));
	}

	return 0;
}

int check_mesh(const args_type& args)
{
	if (args.size() > 2)
		return 2;
	// load mesh
	namespace geo = ::rayscanlib::geometry;
	std::filesystem::path meshName;
	if (args.size() >= 1) {
		meshName = args[0];
	}
	::polyanyalib::Mesh mesh;
	int ret = 0;
	{
		rayscanlib::scen::Archiver archive;
		if (meshName == "-")
			archive.load(std::cin, "");
		else
			archive.load(meshName);
		mesh.load(archive);
		if (!mesh.isMeshValid()) {
			std::cout << "Mesh invalid\n";
			ret = 3;
		}
	}

	// test mesh area
	std::deque<polygon_type> faces;
	std::vector<point_type> allPoints;
	for (auto& face : mesh.mesh_polygons) {
		if (face.isInitalised()) {
			auto& P = faces.emplace_back();
			for (int vid : face.vertices) {
				auto& V = mesh.mesh_vertices.at(vid);
				if (!V.isInitalised()) {
					std::cout << "Invalid vertex in mesh\n";
					return 3;
				}
				P.addPoint(V.p);
				allPoints.push_back(V.p);
			}
			if (!P.finalise(geo::PolyType::SimplePolygon)) {
				std::cout << "Invalid vertex in mesh\n";
				return 3;
			}
			if (!geo::is_convex_polygon<true>(P)) {
				std::cout << "Polygon is not convex\n"
					<< PrintPolygon(P) << std::endl;
				return 3;
			}
		}
	}
	// generate convex hull of mesh
	polygon_type hull;
	geo::convex_hull<false>(allPoints.begin(), allPoints.end(), ::rayscanlib::Dir::CW, [&hull] (auto it) {
		hull.addPoint(*it);
	});
	if (!hull.finalise(geo::PolyType::SimplePolygon)) {
		std::cout << "Invalid hull\n";
		return 3;
	}
	if (!geo::is_convex_polygon<true>(hull)) {
		std::cout << "Hull is not convex\n";
		return 3;
	}

	// check for polygon inside and area
	long double hullArea = geo::convex_polygon_area2(hull);
	long double polyArea = 0;
	for (auto it = faces.begin(), ite = faces.end(); it != ite; ++it) {
		polyArea += geo::convex_polygon_area2(*it);
		for (auto jt = std::next(it); jt != ite; ++jt) {
			if (int r = geo::polygon_inside_polygon(*it, *jt); r >= 0) {
				std::cout << "Polygon " << PrintPolygon(*it)
					<< (r == 0 ? "\nCONTAINED WITHIN\n" : r == 1 ? "\nINSIDE OF\n" : "\nOUTSIDE OF\n")
					<< "Polygon " << PrintPolygon(*jt) << std::endl;
				return 4;
			}
		}
	}
	std::cout << "Faces area: ";
	if (hullArea > 0.1)
		std::cout << (100 * (polyArea / hullArea));
	else
		std::cout << "-";
	std::cout << std::endl;
	if (!std::isfinite(hullArea) || !std::isfinite(polyArea) || polyArea - 1 > hullArea) {
		std::cout << "Faces area is larger than the hull of the mesh\n";
		return 4;
	}

	if (args.size() > 1) {
		std::filesystem::path arcName = args[1];
		rayscanlib::scen::PolygonEnvironment env;
		{
			rayscanlib::scen::Archiver archive;
			archive.load(arcName);
			env.load(archive);
		}

		std::cout.precision(12);
		struct VertexInfo
		{
			const polyanyalib::Vertex* v;
			rayscanlib::geometry::Pos pos;
			const polygon_type* I;
		};
		auto&& printPly = [](std::ostream& out, const polygon_type& ply) {
			bool first = true;
			for (const auto& p : ply) {
				if (first)
					first = false;
				else
					out << " -- ";
				out << p;
			}
		};
		auto&& printVinfo = [&printPly](std::ostream& out, const VertexInfo& vi) {
			out << (rayscanlib::geometry::pos_is_inside(vi.pos) ? "Inside" : rayscanlib::geometry::pos_is_outside(vi.pos) ? "Outside" : "OnEdge");
			if (vi.I) {
				out << " on ";
				printPly(out, *vi.I);
			}
		};
		auto&& checkInt = [&env](polygon_type& ply, auto&& out4fn) -> int {
			for (const auto& enc : env.getEnclosures()) {
				for (size_t i = 0, ie = ply.size(); i < ie; ++i) {
					auto [a,b] = ply.segment(i);
					auto I = rayscanlib::geometry::line_intersect_polygon_exc(a, b-a, enc);
					if (I.first != point_type::scale_inf())
						return out4fn(i, enc, I.second, I.first);
				}
			}
			for (const auto& enc : env.getObstacles()) {
				for (size_t i = 0, ie = ply.size(); i < ie; ++i) {
					auto [a,b] = ply.segment(i);
					auto I = rayscanlib::geometry::line_intersect_polygon_exc(a, b-a, enc);
					if (I.first != point_type::scale_inf())
						return out4fn(i, enc, I.second, I.first);
				}
			}
			return 0;
		};
		std::vector<VertexInfo> vinfo(mesh.mesh_vertices.size());
		// decern mesh vertices positions in env
		for (auto& v : mesh.mesh_vertices) {
			if (!v.isInitalised())
				continue;
			auto& vi = vinfo.at(v.id);
			vi.v = &v;
			std::tie(vi.pos, vi.I) = env.pointLocation(v.p);
		}
		// check all polygon faces are in correct positions
		for (auto& p : mesh.mesh_polygons) {
			if (!p.isInitalised())
				continue;
			bool trav = p.isTraversable();
			auto* is_inside = trav ? &rayscanlib::geometry::pos_is_inside : &rayscanlib::geometry::pos_is_outside;
			auto* is_outside = trav ? &rayscanlib::geometry::pos_is_outside : &rayscanlib::geometry::pos_is_inside;
			polygon_type ply;
			for (auto i : p.vertices) {
				ply.addPoint(mesh.mesh_vertices[i].p);
			}
			ply.finalise(rayscanlib::geometry::PolyType::Auto);
			point_type mid(0,0);
			for (auto i : p.vertices) {
				auto& vi = vinfo[i];
				if (is_outside(vi.pos)) {
					std::cout << "invalid point " << vi.v->p << " ";
					printVinfo(std::cout, vi);
					std::cout << " on " << (trav ? "traversable" : "non-traversable") << " face ";
					printPly(std::cout, ply); 
					std::cout << std::endl;
					return 3;
				}
				mid += vi.v->p;
			}
			mid = (1.0 / p.vertices.size()) * mid;
			if (auto r = env.pointLocation(mid); !is_inside(r.first)) {
				std::cout << "mid point " << mid << " of face ";
				printPly(std::cout, ply);
				std::cout << " failure ";
				printVinfo(std::cout, VertexInfo{nullptr,r.first,r.second});
				std::cout << std::endl;
				return 3;
			}
			if (int ret = checkInt(ply, [&ply,&printPly](size_t i, const polygon_type& obst, size_t seg, point_type::scale_type st) {
				auto [a,b] = ply.segment(i);
				auto [c,d] = obst.segment(seg);
				std::cout << "intersection of segments " << a << "-" << b << " and " << c << "-" << d << " at " << st << "(" << (a + st*(b-a)) << ") on face ";
				printPly(std::cout, ply);
				std::cout << std::endl;
				return 3;
			}); ret != 0)
				return ret;
		}
	}

	return 0;
}

int adj_mesh(const args_type& args)
{
	constexpr double PrecE = 1e-8;
	constexpr double RangeE = 1e-4;
	if (args.size() > 3)
		return 2;
	std::filesystem::path arcName;
	if (args.size() > 0 && !args[0].empty()) {
		arcName = args[0];
	}
	std::filesystem::path meshName;
	if (args.size() > 1 && !args[1].empty()) {
		meshName = args[1];
	}
	std::filesystem::path outName;
	if (args.size() > 2 && !args[2].empty()) {
		outName = args[2];
	}
	rayscanlib::scen::PolygonEnvironment env;
	polyanyalib::Mesh mesh;
	{
		rayscanlib::scen::Archiver archive;
		if (arcName.empty())
			archive.load(std::cin, "");
		else
			archive.load(arcName);
		if (!meshName.empty()) {
			archive.load(meshName);
		}
		env.load(archive);
		mesh.load(archive);
		if (!mesh.isMeshValid()) {
			std::cout << "Mesh invalid\n";
			return 3;
		}
	}

	std::vector<const polygon_type*> envPolys;
	std::vector<point_type> envPoints;
	for (const polygon_type& ply : env.getEnclosures()) {
		envPolys.emplace_back(&ply);
	}
	for (const polygon_type& ply : env.getObstacles()) {
		envPolys.emplace_back(&ply);
	}
	// find all points
	for (auto* ply : envPolys) {
		for (auto& pt : *ply) {
			envPoints.emplace_back(pt);
		}
		for (auto* ply2 : envPolys) {
			if (ply == ply2)
				continue;
			point_type a, b = ply->back(), c, d;
			for (auto it = ply->begin(), ite = ply->end(); it != ite; ++it) {
				a = b; b = *it;
				d = ply2->back();
				for (auto jt = ply2->begin(), jte = ply2->end(); jt != jte; ++jt) {
					c = d; d = *jt;
					if (auto I = rayscanlib::geometry::segment_intersect(a, b-a, c, d-c); I.intersect() && I.rangeAexc() && I.rangeBexc()) {
						envPoints.emplace_back(a + I.scaleA() * (b-a));
					}
				}
			}
		}
	}

	// update mesh
	for (auto& v : mesh.mesh_vertices) {
		if (!v.isInitalised())
			continue;
		point_type p = v.p;
		bool exists = false;
		bool inRange = false;
		point_type np;
		for (auto& q : envPoints) {
			if (auto pq = q-p; std::abs(pq.x) < PrecE && std::abs(pq.y) < PrecE) {
				exists = true;
				break;
			} else if (std::abs(pq.x) < RangeE && std::abs(pq.y) < RangeE) {
				if (!inRange) {
					inRange = true;
					np = q;
				} else if (pq.square() < (np-p).square()) {
					np = q;
				}
			}
		}
		if (!exists && inRange) {
			v.p = np;
		}
	}

	if (!mesh.isMeshValid()) {
		std::cerr << "mesh not valid" << std::endl;
		return 3;
	}

	rayscanlib::scen::Archiver archive;
	env.save(archive, "//poly");
	mesh.save(archive, "//mesh");
	if (outName.empty())
		archive.save(std::cout, outName, rayscanlib::scen::Archiver::NoLink);
	else
		archive.save(outName, rayscanlib::scen::Archiver::NoLink);

	return 0;
}

int grid2mesh(const args_type& args)
{
	std::string_view mapFile = args.at(0);
	std::string_view meshFile = args.at(1);
	std::string_view scaleArg = args.at(2);
	std::string_view sepsArgs;
	if (args.size() > 3)
		sepsArgs = args.at(3);
	else
		sepsArgs = "2";

	Map map;
	{
		std::ifstream in{std::string(mapFile)};
		in >> map;
	}
	::polyanyalib::Mesh mesh;
	{
		::rayscanlib::scen::Archiver archive;
		archive.load(meshFile);
		mesh.load(archive);
		if (!mesh.isMeshValid()) {
			std::cerr << "Mesh invalid\n";
			return 3;
		}
		mesh.emplace_facer<::polyanyalib::IntervalFacer>();
		mesh.facerInitalise();
	}
	SaveData sd;
	sd.loadMesh(mesh);
	sd.populateVars(boost::lexical_cast<double>(scaleArg), false);
	int seps = boost::lexical_cast<int>(sepsArgs);
	if (seps < 0 || seps > 1'000) {
		return 3;
	}

	double split;
	if (seps == 0)
		split = 0;
	else
		split = 1.0 / seps;
	for (size_t y = 0; y < map.height; ++y)
	for (size_t x = 0; x < map.width;  ++x) {
		if (map.bit_test<0>(x, y)) {
			auto pt = sd.cellToEnv(x, y);
			for (int i = 0; i <= seps; ++i)
			for (int j = 0; j <= seps; ++j) {
				auto ptx = pt;
				ptx.x += (j * split) * sd.iscale;
				ptx.y += (i * split) * sd.iscale;
				if (mesh.findFaceFromPoint(ptx).type == ::polyanyalib::PointLocation::NOT_ON_MESH) {
					std::cerr << "Mesh invalid point (" << (x + i*0.5) << "," << (y + i*0.5) << ")\n";
					return 1;
				}
			}
		}
	}

	return 0;
}

} // namespace

} // namespace utility

int main(int argc, char* argv[])
{
	args_type args;
	std::string_view exec;
	if (argc < 2)
		return 1;
	exec = argv[1];
	for (int i = 2; i < argc; ++i) {
		args.push_back(argv[i]);
	}

	using namespace std::string_view_literals;
	if (exec == "stats"sv)
		return utility::mesh_stats(args);
	else if (exec == "sample-points"sv)
		return utility::random_queries(args);
	else if (exec == "check-mesh"sv)
		return utility::check_mesh(args);
	else if (exec == "adj-mesh"sv)
		return utility::adj_mesh(args);
	else if (exec == "grid-mesh-check"sv)
		return utility::grid2mesh(args);
	else if (exec == "--help"sv || exec == "help"sv || exec == "?"sv) {
		std::cout << "stats [mesh]\n"
		          << "sample-points [mesh] [number] ([min-dist])? ([output-archive])?\n"
		          << "check-mesh [mesh] ([poly-env])?\n"
		          << "adj-mesh [mesh]\n"
		          << "grid-mesh-check [grid] [mesh] [scale]\n"
				  << std::flush;
		return 0;
	}
		return 1;
}
