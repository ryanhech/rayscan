#ifndef RAYSCAN_EXT_POLYANYA_FADE2DMESH_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYA_FADE2DMESH_HPP_INCLUDED

#include <polyanyalib/fwd.hpp>
#include <polyanyalib/Mesh.hpp>

namespace polyanyalib
{

class Fade2dMesh : public Mesh
{
public:
	virtual void loadEnvironment(const environment& env);
};

}

#endif // RAYSCAN_EXT_POLYANYA_FADE2DMESH_HPP_INCLUDED
