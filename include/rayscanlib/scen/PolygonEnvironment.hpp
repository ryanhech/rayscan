#ifndef RAYSCAN_SCEN_POLYGONENVIRONMENT_HPP_INCLUDED
#define RAYSCAN_SCEN_POLYGONENVIRONMENT_HPP_INCLUDED

#include "fwd.hpp"
#include "Archiver.hpp"
#include <rayscanlib/geometry/Polygon.hpp>
#include <rayscanlib/geometry/Box.hpp>
#include <inx/functions.hpp>
#include <list>
#include <unordered_map>
#include <deque>
#include <ostream>
#include <filesystem>
#include <chrono>
#include <random>

namespace rayscanlib::scen
{

class PolygonHierarchy
{
public:
	using coord = double;
	using point = geometry::Point<coord>;
	using polygon = geometry::Polygon<coord>;
	using box = geometry::Box<coord>;

	struct Enclosure;
	struct Obstacle;
	using Obstacles = std::pmr::vector<Obstacle*>;
	using Enclosures = std::pmr::vector<Enclosure*>;
	struct Obstacle
	{
		const polygon* P;
		box boxed;
		// point within;
		Enclosure* enclosure;
		Obstacle() noexcept : P{}, boxed{}, enclosure{}
		{ }

		bool point_within(point p, bool strict = false) const;
	};
	struct Enclosure : Obstacle
	{
		Obstacle* enclosed_within;
		Enclosures children;
		Obstacles obstructions;
		Enclosure(std::pmr::memory_resource* mr) : enclosed_within{}
			,children(std::pmr::polymorphic_allocator<Enclosure*>(mr))
			,obstructions(std::pmr::polymorphic_allocator<Obstacle*>(mr))
		{ }
	};

	static int is_obstacle_within(const Obstacle& O, const Obstacle& E);
	const Enclosure* find_point_within(point p, bool strict = false) const;
	Enclosure* find_point_within(point p, bool strict = false) { return const_cast<Enclosure*>(std::as_const(*this).find_point_within(p, strict)); }

	PolygonHierarchy() : m_built(false), m_enclosurePool(&m_poolRes), m_obstaclePool(&m_poolRes)
	{ }
	PolygonHierarchy(PolygonHierarchy&&) = delete;
	PolygonHierarchy(const PolygonHierarchy&) = delete;

	void insert_obstacle(const polygon& p);
	void insert_enclosure(const polygon& p);

	void build();

	operator bool() const noexcept { return m_built; }

	const Enclosures& getRoots() const noexcept { return m_roots; }
	const Enclosures& getTopological() const noexcept { return m_topological; }
	const Obstacles& getLoseObstacles() const noexcept { return m_loseObstacles; }

protected:
	static int is_obstacle_within_impl_(const Obstacle& O, const Obstacle& E);
	void init_obstacle(Obstacle& O, const polygon& p);

	void build_recurse(Enclosures& E, Enclosures::iterator from, Enclosures::iterator to);
	Enclosures::iterator build_partition_roots(Enclosures::iterator from, Enclosures::iterator to);
	Enclosures::iterator build_partition_within(Obstacle& O, Enclosures::iterator from, Enclosures::iterator to);
	void build_obstacles();
	void build_enclosed_within();

private:
	bool m_built;
	std::pmr::unsynchronized_pool_resource m_poolRes;
	std::pmr::unsynchronized_pool_resource m_encRes;
	std::pmr::deque<Enclosure> m_enclosurePool;
	std::pmr::deque<Obstacle> m_obstaclePool;
	Enclosures m_roots;
	Enclosures m_topological;
	Obstacles m_loseObstacles;
};

class PolygonEnvironment
{
public:
	constexpr static uint32 VERSION = 1;
	using coord = double;
	using point = geometry::Point<coord>;
	using polygon = geometry::Polygon<coord>;
	using box = geometry::Box<coord>;

	PolygonEnvironment();

	constexpr static Dir enclsoure_ori = Dir::CCW;
	constexpr static Dir obstacle_ori = Dir::CW;

	void clear();

	bool pushEnclosure(const polygon& poly);
	bool pushEnclosure(polygon&& poly);
	template <typename It>
	bool pushEnclosure(It&& a, It&& b)
	{
		polygon p(a, b);
		if (!p.finalise(geometry::PolyType::SimplePolygon))
			throw std::runtime_error("bad polygon");
		if (p.orientation() == enclsoure_ori)
			return pushEnclosure(std::move(p));
		else
			return pushEnclosure(polygon(p.rbegin(), p.rend(), geometry::PolyType::SimplePolygon));
	}
	bool pushEnclosureNoCheck(const polygon& poly);
	bool pushEnclosureNoCheck(polygon&& poly);

	std::list<polygon>::iterator pushObstacle(const polygon& poly);
	std::list<polygon>::iterator pushObstacle(polygon&& poly);
	template <typename It>
	std::list<polygon>::iterator pushObstacle(It&& a, It&& b)
	{
		polygon p(a, b);
		if (!p.finalise(geometry::PolyType::SimplePolygon))
			throw std::runtime_error("bad polygon");
		if (p.orientation() == obstacle_ori || p.orientation() == Dir::COLIN)
			return pushObstacle(std::move(p));
		else
			return pushObstacle(polygon(p.rbegin(), p.rend(), geometry::PolyType::SimplePolygon));
	}
	std::list<polygon>::iterator pushObstacleNoCheck(const polygon& poly);
	std::list<polygon>::iterator pushObstacleNoCheck(polygon&& poly);
	void eraseObstacleNoCheck(std::list<polygon>::iterator it);

	void finalise();

	void reboxObstacles();

	const box& getEnclosureBox() const noexcept { return m_enclosureBox; }
	box getEnclosureBox(coord enlarge) const noexcept { return box(m_enclosureBox.lower() - point(enlarge, enlarge), m_enclosureBox.upper() + point(enlarge, enlarge)); }
	const box& getObstaclesBox() const noexcept { return m_obstaclesBox; }
	box getObstaclesBox(coord enlarge) const noexcept { return box(m_obstaclesBox.lower() - point(enlarge, enlarge), m_obstaclesBox.upper() + point(enlarge, enlarge)); }

	polygon& getEnclosure() { if (auto it = m_enclosures.begin(); it == m_enclosures.end()) throw std::runtime_error("invalid"); else return *it; }
	const polygon& getEnclosure() const { if (auto it = m_enclosures.begin(); it == m_enclosures.end()) throw std::runtime_error("invalid"); else return *it; }
	std::list<polygon>& getEnclosures() noexcept { return m_enclosures; }
	const std::list<polygon>& getEnclosures() const noexcept { return m_enclosures; }
	std::list<polygon>& getObstacles() noexcept { return m_obstacles; }
	const std::list<polygon>& getObstacles() const noexcept { return m_obstacles; }

	void save(std::ostream& file) const;
	void save(Archiver& archive, std::filesystem::path filename = std::filesystem::path()) const;
	void load(const Archiver& archive);

	std::pair<geometry::Pos, const polygon*> pointLocation(point pt) const;
	bool polygonValid(const polygon& poly) const;
	// will find a valid point within, trying to restrict within a scale, will repeat a number of times randomly before just selecting by nearest polygon
	point findPoint(std::mt19937_64& rng, const geometry::Box<double>& withinScale, int repeat = 0, bool allowEdge = true, int permutate = 0) const;

	size_t countPolygons() const noexcept { return m_obstacles.size() + 1; }
	size_t countVertices() noexcept { return m_countVertices ? m_countVertices : (m_countVertices = countVertices_()); }
	size_t countVertices() const noexcept { return m_countVertices ? m_countVertices : countVertices_(); }

	bool isInteger() const noexcept { return m_integer; }
	bool isConvex() const noexcept { return m_convex; }
	bool isOverlap() const noexcept { return m_overlap; }

	void makeOverlap() noexcept { m_overlap = true; }

	const PolygonHierarchy& getHierarchy() const
	{
		if (!m_hierarchy)
			throw std::logic_error("Hierarchy not created, call makeHierarchy()");
		return *m_hierarchy;
	}
	PolygonHierarchy& getHierarchy()
	{
		return const_cast<PolygonHierarchy&>(std::as_const(*this).getHierarchy());
	}
	void makeHierarchy();

#if 0
	class string_manager
	{
	public:
		static constexpr size_t Max = 32;
		static constexpr size_t Block = 512;
		static constexpr size_t BlockPtr = 512 / sizeof(void*);
	private:
		using array_block = std::array<void*, BlockPtr>;
		array_block* m_block;
		void** m_blockAt;
		size_t m_rem;
	
	public:
		string_manager() : m_block(nullptr), m_blockAt(nullptr), m_rem(0)
		{ }
		string_manager(const string_manager&) = delete;
		~string_manager()
		{
			while (m_block != nullptr) {
				array_block* old = m_block;
				m_block = static_cast<array_block*>((*m_block)[0]);
				delete old;
			}
		}

		char* alloc(size_t n)
		{
			if (n > Max)
				throw std::runtime_error("");
			if (n < m_rem) {
				array_block* ar = new array_block();
				(*ar)[0] = m_block;
				m_block = ar;
				m_blockAt = ar->data() + 1;
				m_rem = (BlockPtr-1) * sizeof(void*);
			}
			size_t words = (n + (sizeof(void*) - 1)) / sizeof(void*);
			char* res = reinterpret_cast<char*>(m_blockAt);
			m_blockAt += words;
			m_rem -= words * sizeof(void*);
			return res;
		}
		std::string_view construct(std::string_view str)
		{
			char* res = alloc(str.size());
			std::copy(str.begin(), str.end(), res);
			return std::string_view(res, str.size());
		}
	};
#endif

private:
	void removeObstaclesFilter(const polygon& enclosure, geometry::Pos keepIfTrue);
	size_t countVertices_() const noexcept;
	void updateFlags(const polygon& poly);

protected:
	std::list<polygon> m_enclosures;
	std::list<polygon> m_obstacles;
	// string_manager m_shapesStr;
	// std::unordered_map<std::string_view, polygon> m_shapes;
	box m_enclosureBox;
	box m_obstaclesBox;
	size_t m_countVertices;
	bool m_overlap;
	bool m_convex;
	bool m_integer;
	inx::unique_clear_ptr<PolygonHierarchy> m_hierarchy;
};

std::ostream& operator<<(std::ostream& s, const PolygonEnvironment& poly);
std::istream& operator>>(std::istream& s, PolygonEnvironment& poly);

template <typename RayScan, typename Benchmark>
struct rayscan_environment_exec
{
	std::chrono::nanoseconds operator()(RayScan& rayscan, const Benchmark& benchmark)
	{
		auto dur = rayscan.loadEnvironment(*benchmark.getEnvironment());
		rayscan.getStats_().pushSetupTime(dur);
		return dur;
	}
};

template <typename RayScan, typename Benchmark>
std::chrono::nanoseconds rayscan_environment(RayScan& rayscan, const Benchmark& benchmark)
{
	rayscan_environment_exec<RayScan, Benchmark> exec;
	return exec(rayscan, benchmark);
}

} // namespace rayscan::scen

#endif // RAYSCAN_SCEN_POLYGONENVIRONMENT_HPP_INCLUDED
