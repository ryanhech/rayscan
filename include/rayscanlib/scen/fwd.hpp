#ifndef RAYSCANLIB_SCEN_FWD_HPP_INCLUDED
#define RAYSCANLIB_SCEN_FWD_HPP_INCLUDED

#include <rayscanlib/inx.hpp>

namespace rayscanlib::scen
{

class PolygonEnvironment;
class StaticBenchmarkSingleTarget;
class StaticBenchmarkMultiTarget;
class DynamicBenchmarkSingleTarget;
class DynamicBenchmarkMultiTarget;
class MovingAiBenchmarks;

}

#endif // RAYSCANLIB_SCEN_FWD_HPP_INCLUDED
