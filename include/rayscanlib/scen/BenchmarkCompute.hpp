#ifndef RAYSCANLIB_SCEN_BENCHMARKCOMPUTE_HPP_INCLUDED
#define RAYSCANLIB_SCEN_BENCHMARKCOMPUTE_HPP_INCLUDED

#include "fwd.hpp"

namespace rayscanlib::scen
{

// will autodecuce the correct benchmark type, returns true if type is supported, false otherwise
bool benchmarkComputeDistancesAuto(StaticBenchmarkSingleTarget& suite);

void benchmarkComputeDistances(StaticBenchmarkSingleTarget& suite);
void benchmarkComputeDistances(StaticBenchmarkMultiTarget& suite);
void benchmarkComputeDistances(DynamicBenchmarkSingleTarget& suite);
void benchmarkComputeDistances(DynamicBenchmarkMultiTarget& suite);

} // namespace rayscanlib::scen

#endif // RAYSCANLIB_SCEN_BENCHMARKCOMPUTE_HPP_INCLUDED
