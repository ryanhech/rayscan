#ifndef RAYSCAN_SCEN_STATICBENCHMARKSINGLETARGET_HPP_INCLUDED
#define RAYSCAN_SCEN_STATICBENCHMARKSINGLETARGET_HPP_INCLUDED

#include "fwd.hpp"
#include "PolygonEnvironment.hpp"
#include "Archiver.hpp"
#include <vector>
#include <istream>
#include <ostream>
#include <optional>
#include <filesystem>
#include <optional>

namespace rayscanlib::scen
{

class StaticBenchmarkSingleTarget
{
public:
	constexpr static uint32 VERSION = 1;

	using environment = PolygonEnvironment;
	struct Instance
	{
		int32 validId;
		int32 filterId;
		environment::point start;
		environment::point target;
		std::optional<double> distance;
		std::optional<double> dynamic;
		Instance() noexcept : validId(0), filterId(0), start{}, target{}
		{ }
		bool isFilteredOut() const noexcept { return filterId < 0; }
	};

	StaticBenchmarkSingleTarget() : m_instanceAt(-1), m_ioLevel(0), m_inserting(false)
	{ }
	StaticBenchmarkSingleTarget(const std::shared_ptr<environment>& env) : StaticBenchmarkSingleTarget()
	{
		setEnvironment(env);
	}
	virtual ~StaticBenchmarkSingleTarget() = default;

	virtual void clear();

	void setEnvironment(const std::shared_ptr<environment>& env) { setEnvironment(std::shared_ptr<environment>(env)); }
	virtual void setEnvironment(std::shared_ptr<environment>&& env);
	const std::shared_ptr<environment>& getEnvironment() const noexcept { return m_environment; }

	virtual const std::shared_ptr<environment>& getCurrentEnvironment() { return m_environment; }
	virtual const std::shared_ptr<environment>& getCurrentEnvironment() const { return m_environment; }

	virtual void beginInserting();
	virtual void endInserting();
	virtual bool isValidScenario(environment::point start, environment::point target);
	virtual bool addInstance(environment::point start, environment::point target);
	virtual bool addInstanceNoCheck(environment::point start, environment::point target);

	void applyFilter(std::string_view filter, bool remove = false);
	virtual void applyFilter(const std::vector<bool>& filter);
	void computeFilter(std::vector<bool>& scenarios, std::string_view filter, bool remove = false) const;

	const std::vector<Instance>& instances() const noexcept { return m_instances; }
	std::vector<Instance>& instancesMutable() noexcept { return m_instances; }
	size_t size() const noexcept { return m_instances.size(); }
	size_t filter_size() const noexcept { return std::count_if(m_instances.begin(), m_instances.end(), [](const Instance& inst) noexcept { return !inst.isFilteredOut(); }); }
	virtual size_t size_major() const noexcept { return size(); }
	virtual void selectInstance(size_t i);
	virtual void resetInstance();
	Instance& currentInstance() { return m_instances.at(static_cast<size_t>(m_instanceAt)); }
	const Instance& currentInstance() const { return m_instances.at(static_cast<size_t>(m_instanceAt)); }

	void computeDistances();
	void orderByDistances();

	void save(std::ostream& file) const;
	void save(std::ostream& file, bool env) const;
	void save(Archiver& archive, bool env = false) const;

	void load(const Archiver& archive, bool env = true);

	virtual std::shared_ptr<StaticBenchmarkSingleTarget> refineBenchmarkByAppliedFilter() const;

protected:
	void addInstance_(environment::point start, environment::point target);

//	virtual void computeDistances_();
	void refineBenchmarkByAppliedFilter_(StaticBenchmarkSingleTarget& reff, size_t num) const;

	virtual void save_(std::ostream& file, bool env) const;
	virtual void saveEnv_(std::ostream& file, bool env) const;
	virtual void saveInst_(std::ostream& file) const;

	virtual void load_(const Archiver& archive, bool env);
	virtual void loadEnv_(const Archiver& archive);
	virtual void loadInst_(const Archiver& archive);

protected:
	int32 m_instanceAt;
	uint32 m_ioLevel;
	bool m_inserting;
	std::shared_ptr<environment> m_environment;
	std::vector<Instance> m_instances;
};

std::ostream& operator<<(std::ostream& s, const StaticBenchmarkSingleTarget& poly);
std::istream& operator>>(std::istream& s, StaticBenchmarkSingleTarget& poly);

// returns pair opt double, first is the distance (if found), second is the shortest path (if available)
template <typename RayScan>
inline std::pair<std::optional<double>, std::optional<double>> rayscan_search(RayScan& rayscan, StaticBenchmarkSingleTarget& suite, uint32 id)
{
	suite.selectInstance(id);
	auto& I = suite.currentInstance();
	auto& stats = rayscan.getStats_();
	stats.pushDynSetupTime(stats.setupTime);
	stats.setupTime = std::chrono::nanoseconds(0);
	std::optional<double> sp = I.distance;
	return { rayscan.search(static_cast<typename RayScan::point>(I.start), static_cast<typename RayScan::point>(I.target)), sp };
}

} // namespace rayscanlib::scen

#endif // RAYSCAN_SCEN_STATICBENCHMARKSINGLETARGET_HPP_INCLUDED
