#ifndef RAYSCAN_SCEN_DYNAMICBENCHMARKSINGLETARGET_HPP_INCLUDED
#define RAYSCAN_SCEN_DYNAMICBENCHMARKSINGLETARGET_HPP_INCLUDED

#include "StaticBenchmarkSingleTarget.hpp"
#include <unordered_map>
#include <chrono>
#include <optional>

namespace rayscanlib::scen
{

class DynamicBenchmarkSingleTarget : public virtual StaticBenchmarkSingleTarget
{
public:
	constexpr static uint32 VERSION = 2;

	struct DynamicPolygon
	{
		environment::polygon P;
		std::list<environment::polygon>::iterator envIt;
		bool inEnv;
		bool isOrigional;
		DynamicPolygon(const environment::polygon& p) : P(p, geometry::PolyType::Auto), inEnv(false), isOrigional(false) { }
	};

	DynamicBenchmarkSingleTarget() : m_dynamicChangesAt(0)
	{ }
	
	void clear() override;
	void setEnvironment(std::shared_ptr<environment>&& env) override;
	using StaticBenchmarkSingleTarget::setEnvironment;
	void selectInstance(size_t i) override;
	void resetInstance() override;
	
	void beginInserting() override;
	void endInserting() override;
	bool addInstance(environment::point start, environment::point target) override;
	// returns polyid on success, 0 on failure
	uint32 addDynamicPolygon(const environment::polygon& p, uint32 id = 0);
	bool addDynamicInsertion(uint32 id);
	bool addDynamicDeletion(uint32 id);
	const std::shared_ptr<environment>& getCurrentEnvironment() override;
	const std::shared_ptr<environment>& getCurrentEnvironment() const override;
	std::shared_ptr<StaticBenchmarkSingleTarget> refineBenchmarkByAppliedFilter() const override;

protected:
	void clear_();
	void addDynamicChanges_();
	void setupEnvironmentObstacles();
	void syncEnvironmentObstacles();
	void refineBenchmarkByAppliedFilter_(DynamicBenchmarkSingleTarget& reff, size_t num) const;
	
	void save_(std::ostream& file, bool env) const override;
	virtual void saveDyn_(std::ostream& file) const;
	
	void load_(const Archiver& archive, bool env) override;
	virtual void loadDyn_(const Archiver& archive);

public:
	const std::unordered_map<uint32, DynamicPolygon>& getDynamicPolygons() const noexcept { return m_dynamicPolygons; }
	const auto& getSelectDeletions() const noexcept { return m_selectDeletions; }
	const auto& getSelectInsertions() const noexcept { return m_selectInsertions; }

protected:
	void forwardInstances(size_t count);
	void reverseInstances(size_t count);
	void applyDynamicChanges();

protected:
	std::shared_ptr<environment> m_dynEnvironment;
	std::unordered_map<uint32, DynamicPolygon> m_dynamicPolygons; // the polygon list that will be added or removed
	std::vector<int32> m_dynamicChanges; // the structure that holds all possible changes, every instance starts with a number n stating how many changes occur, followed by n -id deletions and +id insertions, deletions must come first, then followed by the number n again to allow reverse traversal
	uint32 m_dynamicChangesAt;
	std::vector<uint32> m_selectDeletionsId;
	std::vector<uint32> m_selectInsertionsId;
	std::vector<const environment::polygon*> m_selectDeletions;
	std::vector<const environment::polygon*> m_selectInsertions;
};

// returns pair opt double, first is the distance (if found), second is the shortest path (if available)
template <typename RayScan>
inline std::chrono::nanoseconds rayscan_apply_dynamic_changes(RayScan& rayscan, DynamicBenchmarkSingleTarget& suite)
{
	auto& D = suite.getSelectDeletions();
	auto& I = suite.getSelectInsertions();
	const auto& env = suite.getCurrentEnvironment();
	if (D.empty() && I.empty()) {
		rayscan.envFinaliseNull(env.get());
		return std::chrono::nanoseconds(0);
	}
	auto start = std::chrono::steady_clock::now();
	rayscan.envInitalise(env.get());
	for (auto* d : D) {
		rayscan.envRemoveObstacle(d);
	}
	for (auto* i : I) {
		rayscan.envAddObstacle(i, *i);
	}
	rayscan.envFinalise(env.get());
	std::chrono::nanoseconds dur = std::chrono::steady_clock::now() - start;
	return dur;
}

// returns pair opt double, first is the distance (if found), second is the shortest path (if available)
template <typename RayScan>
inline std::pair<std::optional<double>, std::optional<double>> rayscan_search(RayScan& rayscan, DynamicBenchmarkSingleTarget& suite, uint32 id)
{
	suite.selectInstance(id);
	auto& I = suite.currentInstance();
	auto setup = rayscan_apply_dynamic_changes(rayscan, suite);
	auto& stats = rayscan.getStats_();
	stats.pushDynSetupTime(stats.setupTime + setup);
	stats.setupTime = std::chrono::nanoseconds(0);
	std::optional<double> sp = I.dynamic;
	return { rayscan.search(static_cast<typename RayScan::point>(I.start), static_cast<typename RayScan::point>(I.target)), sp };
}

} // namespace rayscanlib::scen

#endif // RAYSCAN_SCEN_DYNAMICBENCHMARKSINGLETARGET_HPP_INCLUDED
