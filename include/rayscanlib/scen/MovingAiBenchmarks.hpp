#ifndef RAYSCAN_SCEN_MOVINGAI_BENCHMARKS_HPP_INCLUDED
#define RAYSCAN_SCEN_MOVINGAI_BENCHMARKS_HPP_INCLUDED

#include "fwd.hpp"
#include "StaticBenchmarkSingleTarget.hpp"
#include <inx/alg/bit_table.hpp>
#include <istream>
#include <filesystem>

namespace rayscanlib::scen
{

class MovingAiBenchmarks
{
public:
	using suite = StaticBenchmarkSingleTarget;
	using environment = suite::environment;
	using point = environment::point;

	MovingAiBenchmarks();

	void loadMap(std::istream& file);
	void loadScenario(std::istream& file, bool loadMapFromScenario = true, const std::filesystem::path& fname = std::filesystem::path());

	const std::shared_ptr<environment>& getEnvironment() const noexcept { return m_environment; }
	const std::shared_ptr<suite>& getSuite() const noexcept { return m_suite; }

	point translatePoint(const point& a) const noexcept { assert(m_height != 0); return point(a.x, static_cast<point::coord_type>(m_height-1) - a.y); }
	point translatePointScale(const point& a) const noexcept { assert(m_height != 0); return 2 * translatePoint(a) + point(1, 1); }

protected:
	std::shared_ptr<environment> traceGrid();

protected:
	uint32 m_width;
	uint32 m_height;
	inx::alg::bit_table<1, 2> m_grid;
	std::shared_ptr<environment> m_environment;
	std::shared_ptr<suite> m_suite;
};

} // namespace rayscan::scen

#endif // RAYSCAN_SCEN_MOVINGAI_BENCHMARKS_HPP_INCLUDED
