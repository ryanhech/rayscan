#ifndef RAYSCAN_SCEN_STATICBENCHMARKMULTITARGET_HPP_INCLUDED
#define RAYSCAN_SCEN_STATICBENCHMARKMULTITARGET_HPP_INCLUDED

#include "fwd.hpp"
#include "StaticBenchmarkSingleTarget.hpp"
#include <vector>
#include <istream>
#include <ostream>
#include <filesystem>
#include <cmath>
#include <optional>

namespace rayscanlib::scen
{

class StaticBenchmarkMultiTarget : public virtual StaticBenchmarkSingleTarget
{
public:
	struct MultiInstance
	{
		int32 validId;
		int32 filterId;
		environment::point start;
		uint32 indexBegin;
		uint32 indexCount;
		MultiInstance() noexcept : validId(0), filterId(0), start{}, indexBegin(0), indexCount(0)
		{ }
		bool isFilteredOut() const noexcept { return filterId < 0; }
	};

	StaticBenchmarkMultiTarget() : m_selectInstanceOther(false), m_multiInstanceAt(-1)
	{ }
	StaticBenchmarkMultiTarget(const std::shared_ptr<environment>& env) : StaticBenchmarkMultiTarget()
	{
		setEnvironment(env);
	}

	void clear() override;
	size_t size_multi() const noexcept { return m_multiInstance.size(); }
	size_t size_major() const noexcept override { return size_multi(); }
	size_t filter_size_multi() const noexcept { return std::count_if(m_multiInstance.begin(), m_multiInstance.end(), [](const MultiInstance& inst) noexcept { return !inst.isFilteredOut(); }); }

	MultiInstance& currentInstance() { return m_multiInstance.at(static_cast<size_t>(m_multiInstanceAt)); }
	const MultiInstance& currentInstance() const { return m_multiInstance.at(static_cast<size_t>(m_multiInstanceAt)); }
	Instance& currentTarget(size_t i) { return m_instances.at(currentInstance().indexBegin + i); }
	const Instance& currentTarget(size_t i) const { return m_instances.at(currentInstance().indexBegin + i); }

	bool isValidScenario(environment::point start, environment::point target) override;
	bool addInstance(environment::point start, environment::point target) override;
	virtual void selectMultiInstance(size_t i);
	void selectInstance(size_t i) override;
	void resetInstance() override;
	virtual bool addMultiInstance(environment::point start);
	virtual bool addTarget(environment::point target);

	size_t singleToMultiId(size_t i);

	const std::vector<MultiInstance>& multiInstances() const noexcept { return m_multiInstance; }

	void applyFilter(const std::vector<bool>& filter) override;
	using StaticBenchmarkSingleTarget::applyFilter;

protected:
	void clear_();
	void save_(std::ostream& file, bool env) const override;
	virtual void saveMulti_(std::ostream& file) const;

	void load_(const Archiver& archive, bool env) override;
	virtual void loadMulti_(const Archiver& archive);

protected:
	bool m_selectInstanceOther;
	int32 m_multiInstanceAt;
	std::vector<MultiInstance> m_multiInstance;
};

template <typename RayScan>
inline std::pair<std::optional<double>, std::optional<double>> rayscan_search(RayScan& rayscan, StaticBenchmarkMultiTarget& suite, uint32 id)
{
	suite.selectMultiInstance(id);
	auto& I = suite.currentInstance();
	auto pts = std::make_unique<typename RayScan::point[]>(I.indexCount);
	auto& stats = rayscan.getStats_();
	stats.pushDynSetupTime(stats.setupTime);
	stats.setupTime = std::chrono::nanoseconds(0);
	std::optional<double> total = 0.0;
	for (uint32 i = 0; i < I.indexCount; i++) {
		auto& J = suite.currentTarget(i);
		pts[i] = J.target;
		if (total.has_value()) {
			if (J.distance.has_value())
				*total += J.distance.value();
			else
				total.reset();
		}
	}
	return { rayscan.search(static_cast<typename RayScan::point>(I.start), pts.get(), I.indexCount), total };
}

template <typename RayScan>
inline std::pair<std::optional<double>, std::optional<double>> rayscan_mst_search(RayScan& rayscan, StaticBenchmarkMultiTarget& suite, uint32 id)
{
	suite.selectInstance(id);
	std::chrono::nanoseconds totaltime(0);
	auto& I = suite.currentInstance();
	std::optional<double> ans = 0.0;
	std::optional<double> total = 0.0;
	for (uint32 i = 0; i < I.indexCount; i++) {
		auto& J = suite.currentTarget(i);
		auto x = rayscan.search(static_cast<typename RayScan::point>(I.start), static_cast<typename RayScan::point>(J.target), i != 0);
		totaltime += rayscan.getStats().searchTime;
		if (total.has_value()) {
			if (J.distance.has_value())
				*total += J.distance.value();
			else
				total.reset();
		}
		if (ans.has_value()) {
			if (x.has_value())
				*ans += *x;
			else
				ans.reset();
		}
	}
	auto& stats = rayscan.getStats_();
	stats.pushDynSetupTime(stats.setupTime);
	stats.setupTime = std::chrono::nanoseconds(0);
	stats.searchTime = totaltime;
	return { ans, total };
}

} // namespace rayscanlib::scen

#endif // RAYSCAN_SCEN_STATICBENCHMARKMULTITARGET_HPP_INCLUDED
