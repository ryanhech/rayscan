#ifndef RAYSCAN_SCEN_DYNAMICBENCHMARKMULTITARGET_HPP_INCLUDED
#define RAYSCAN_SCEN_DYNAMICBENCHMARKMULTITARGET_HPP_INCLUDED

#include "DynamicBenchmarkSingleTarget.hpp"
#include "StaticBenchmarkMultiTarget.hpp"
#include <unordered_map>
#include <chrono>
#include <optional>

namespace rayscanlib::scen
{

class DynamicBenchmarkMultiTarget : public StaticBenchmarkMultiTarget, public DynamicBenchmarkSingleTarget
{
public:
	using StaticBenchmarkSingleTarget::environment;
	void clear() override;
	bool addInstance(environment::point start, environment::point target) override;
	void selectInstance(size_t i) override;
	void resetInstance() override;
	using StaticBenchmarkMultiTarget::currentInstance;
	using DynamicBenchmarkSingleTarget::getCurrentEnvironment;

protected:
	void save_(std::ostream& file, bool env) const override;
	void load_(const Archiver& archive, bool env) override;
};

// returns pair opt double, first is the distance (if found), second is the shortest path (if available)
template <typename RayScan>
inline std::chrono::nanoseconds rayscan_apply_dynamic_changes(RayScan& rayscan, DynamicBenchmarkMultiTarget& suite)
{
	return rayscan_apply_dynamic_changes(rayscan, dynamic_cast<DynamicBenchmarkSingleTarget&>(suite));
}

// returns pair opt double, first is the distance (if found), second is the shortest path (if available)
template <typename RayScan>
inline std::pair<std::optional<double>, std::optional<double>> rayscan_search(RayScan& rayscan, DynamicBenchmarkMultiTarget& suite, uint32 id)
{
	suite.selectMultiInstance(id);
	auto& I = suite.currentInstance();
	auto pts = std::make_unique<typename RayScan::point[]>(I.indexCount);
	auto setup = rayscan_apply_dynamic_changes(rayscan, suite);
	auto& stats = rayscan.getStats_();
	stats.pushDynSetupTime(stats.setupTime + setup);
	stats.setupTime = std::chrono::nanoseconds(0);
	std::optional<double> total = 0.0;
	for (uint32 i = 0; i < I.indexCount; i++) {
		auto& J = suite.currentTarget(i);
		pts[i] = J.target;
		if (total.has_value()) {
			if (J.dynamic.has_value())
				*total += J.dynamic.value();
			else
				total.reset();
		}
	}
	return { rayscan.search(static_cast<typename RayScan::point>(I.start), pts.get(), I.indexCount), total };
}

template <typename RayScan>
inline std::pair<std::optional<double>, std::optional<double>> rayscan_mst_search(RayScan& rayscan, DynamicBenchmarkMultiTarget& suite, uint32 id)
{
	suite.selectInstance(id);
	std::chrono::nanoseconds totaltime(0);
	auto& I = suite.currentInstance();
	// setup dynamic changes
	auto setup = rayscan_apply_dynamic_changes(rayscan, suite);
	auto& stats = rayscan.getStats_();
	stats.pushDynSetupTime(stats.setupTime);
	stats.setupTime = std::chrono::nanoseconds(0);
	stats.searchTime = totaltime;
	// perfrom the search
	std::optional<double> ans = 0.0;
	std::optional<double> total = 0.0;
	for (uint32 i = 0; i < I.indexCount; i++) {
		auto& J = suite.currentTarget(i);
		auto x = rayscan.search(static_cast<typename RayScan::point>(I.start), static_cast<typename RayScan::point>(J.target), i != 0);
		totaltime += rayscan.getStats().searchTime;
		if (total.has_value()) {
			if (J.distance.has_value())
				*total += J.distance.value();
			else
				total.reset();
		}
		if (ans.has_value()) {
			if (x.has_value())
				*ans += *x;
			else
				ans.reset();
		}
	}
	return { ans, total };
}

} // namespace rayscanlib::scen

#endif // RAYSCAN_SCEN_DYNAMICBENCHMARKMULTITARGET_HPP_INCLUDED
