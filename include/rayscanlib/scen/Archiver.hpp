#ifndef RAYSCAN_SCEN_ARCHIVER_HPP_INCLUDED
#define RAYSCAN_SCEN_ARCHIVER_HPP_INCLUDED

#include "fwd.hpp"
#include <unordered_map>
#include <inx/io/chunked_array_device.hpp>
#include <istream>
#include <ostream>
#include <filesystem>

namespace rayscanlib::scen
{

class Archiver;

class FileBlock
{
public:
	FileBlock(std::pmr::unsynchronized_pool_resource& buff) : m_version(0), m_data(&buff)
	{ }

	std::string_view getName() const noexcept { return m_name; }
	size_t getVersion() const noexcept { return m_version; }
	std::string_view getFilename() const noexcept { return m_filename; }

	inx::io::chunked_array_source openRead() const { return inx::io::chunked_array_source(m_data); }

	void clearFilename() { m_filename = std::string_view(); }

protected:
	std::string_view m_name;
	std::string_view m_filename;
	std::size_t m_version;
	bool m_fileLoaded;
	inx::io::chunked_array m_data;

	friend class Archiver;
};

class Archiver
{
public:
	enum Link {
		NoLink,
		LinkRel,
		LinkAbs
	};
	Archiver();
	Archiver(const Archiver&) = delete;
	Archiver(Archiver&&) = delete;
	
	void load(const std::filesystem::path& filename, bool raw = false);
	void load(std::istream& input, const std::filesystem::path& filename, bool raw = false);
	void save(const std::filesystem::path& filename, Link link = LinkRel) const;
	void save(std::ostream& output, const std::filesystem::path& filename, Link link = LinkRel) const;

	bool has(const std::string_view type) const { return m_blocks.count(std::string(type)) > 0; }
	const FileBlock& at(const std::string_view type) const { return m_blocks.at(std::string(type)); }

	bool hasLoaded(const std::filesystem::path& filename) const { return m_loadedFiles.count(std::filesystem::absolute(filename)) != 0; }

	void eraseBlock(const std::string& block)
	{
		m_blocks.erase(block);
	}

	void copy(Archiver& other, bool override = true) const;
	void copy(Archiver& other, std::string_view type, bool override = true) const;

	struct LoadedFile
	{
		std::string canonical;
		std::string absolute; // absolute filename
		LoadedFile* parent;
		LoadedFile(std::string&& l_canonical, std::string&& l_absolute, LoadedFile* p) : canonical(l_canonical), absolute(std::move(l_absolute)), parent(p)
		{ }
		LoadedFile(std::string&& l_canonical, std::string&& l_absolute) : LoadedFile(std::move(l_canonical), std::move(l_absolute), nullptr)
		{ }
		LoadedFile() : parent(nullptr)
		{ }
	};

	const auto& getBlocks() const noexcept { return m_blocks; }
	auto& getBlocksMutable() noexcept { return m_blocks; }

protected:
	void copy_(Archiver& other, std::string&& stype) const;

protected:
	bool hasLoaded_(const std::filesystem::path& filename) const { assert(filename.empty() || filename.is_absolute()); return m_loadedFiles.count(filename) != 0; }

protected:
	std::pmr::unsynchronized_pool_resource m_resource;
	std::pmr::unordered_map<std::string, LoadedFile> m_loadedFiles; // first: canonical filename, second: absolute filename
	std::pmr::unordered_map<std::string, FileBlock> m_blocks;
	LoadedFile* m_parent;
};

} // namespace rayscan::scen

#endif // RAYSCAN_SCEN_ARCHIVER_HPP_INCLUDED
