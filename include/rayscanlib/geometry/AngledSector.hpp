#ifndef RAYSCANLIB_GEOMETRY_ANGLEDSECTOR_HPP_INCLUDED
#define RAYSCANLIB_GEOMETRY_ANGLEDSECTOR_HPP_INCLUDED

#include "fwd.hpp"
#include "Point.hpp"

namespace rayscanlib::geometry
{

template <typename CoordType, Between Type, Dir TurnD>
struct AngledSector
{
	static_assert(TurnD == Dir::CW || TurnD == Dir::CCW, "TurnD must be CW or CCW");
	using self = AngledSector<CoordType, Type, TurnD>;
	using self_inv = AngledSector<CoordType, Type, inv_dir(TurnD)>;
	using point = Point<CoordType>;

	constexpr static Dir turn_dir() noexcept { return TurnD; };
	constexpr static Between between_type() noexcept { return Type; }

	point pivot;
	point turn;

	AngledSector() noexcept = default;
	constexpr AngledSector(const self&) noexcept = default;
	constexpr AngledSector(self&&) noexcept = default;
	constexpr AngledSector(point l_pt) noexcept : pivot(TurnD == Dir::CW ? l_pt : point::zero()), turn(TurnD == Dir::CW ? point::zero() : l_pt)
	{ }
	constexpr AngledSector(point l_pivot, point l_turn) noexcept : pivot(l_pivot), turn(l_turn)
	{ }
	constexpr AngledSector(const self_inv& cp) noexcept : pivot(cp.turn), turn(cp.pivot) { }
	constexpr AngledSector& operator=(const self&) noexcept = default;
	constexpr AngledSector& operator=(self&&) noexcept = default;

	constexpr bool isWhole() const noexcept
	{
		if constexpr (TurnD == Dir::CW) {
			return turn.isZero();
		} else {
			return pivot.isZero();
		}
	}

	constexpr bool isClose() const noexcept
	{
		return turn.isColinFwd(pivot);
	}

	template <Dir D>
	constexpr const point& get() const noexcept
	{
		static_assert(D == Dir::CW || D == Dir::CCW, "D must be CW or CCW");
		if constexpr (D != TurnD)
			return pivot;
		else
			return turn;
	}
	template <Dir D>
	constexpr point& get() noexcept
	{
		static_assert(D == Dir::CW || D == Dir::CCW, "D must be CW or CCW");
		if constexpr (D != TurnD)
			return pivot;
		else
			return turn;
	}

#if 0
	template <Dir D>
	constexpr void set(arc_point value) noexcept
	{
		assert(!value.isZero());
		if constexpr (D != TurnD)
			pivot = value;
		else
			turn = value;
	}
	constexpr void set(arc_point ccw, arc_point cw) noexcept
	{
		assert(!ccw.isZero() && !cw.isZero());
		if constexpr (TurnD == Dir::CCW) {
			pivot = cw;
			turn = ccw;
		} else {
			pivot = ccw;
			turn = cw;
		}
	}
#endif

	template <Dir D>
	constexpr AngledSector<CoordType, Between::WHOLE, D> makeDir() const noexcept
	{
		if constexpr (D == TurnD)
			return *this;
		else
			return AngledSector<CoordType, Between::WHOLE, D>(turn, pivot);
	}

	// SplitWhole: if angle sector is whole and pt is align with angle, close the sector if true, otherwise keep as whole
	template <Dir D, Dir SplitD = D>
	constexpr AngledSector<CoordType, D==SplitD?Between::WIDE:Between::WHOLE, D> split(point pt) const noexcept
	{
		static_assert(D == Dir::CW || D == Dir::CCW, "D must be CW or CCW");
		assert(within(pt));
		if (isWhole()) {
			if constexpr (D!=SplitD) {
				if (pt.isColinFwd(get<Dir::CCW>()))
					return *this;
				else
					return AngledSector<CoordType, Between::WHOLE, D>(pt, get<Dir::CCW>());
			} else {
				return AngledSector<CoordType, Between::WIDE, D>(pt, get<Dir::CCW>());
			}
		} else {
			if constexpr (D == TurnD) {
				return AngledSector<CoordType, D==SplitD?Between::WIDE:Between::WHOLE, D>(pt, turn);
			} else {
				return AngledSector<CoordType, D==SplitD?Between::WIDE:Between::WHOLE, D>(pt, pivot);
			}
		}
	}
	template <Dir D, Dir SplitD = D>
	constexpr AngledSector<CoordType, D==SplitD?Between::WIDE:Between::WHOLE, inv_dir(D)> splitInv(point pt) const noexcept
	{
		return split<D, SplitD>(pt);
	}

	constexpr bool within(point pt) const noexcept
	{
		assert(!pt.isZero());
		if (auto pvtr = pivot.cross(turn); point::template isDir<TurnD>(pvtr)) {
			return !pt.template isDir<TurnD>(pivot) && !turn.template isDir<TurnD>(pt); // narrow
		} else if (point::template isDir<inv_dir(TurnD)>(pvtr)) {
			return !pt.template isDir<TurnD>(pivot) || !turn.template isDir<TurnD>(pt); // wide
		} else { // collinear
			if (auto pvtrm = pivot.pair_mult(turn); point::isBack(pvtrm.x, pvtrm.y))
				return !pt.template isDir<TurnD>(pivot); // oposite collinear
			else if (point::isFwd(pvtrm.x, pvtrm.y))
				return pt.isColinFwd(pivot); // forward collinear
			else
				return true; // whole
		}
	}
	template <Dir D>
	constexpr bool within(point from, point to) const noexcept
	{
		static_assert(D == Dir::CW || D == Dir::CCW, "must be CW or CCW");
		assert(!from.isZero() && !to.isZero());
		assert(within(from));
		assert(!to.template isDir<D>(from));

		if (isWhole())
			return !get<Dir::CCW>().template isBetween<Between::NARROW, D>(from, to);
		if constexpr (D == TurnD) {
			return !from.isColinFwd(this->turn) ? !this->turn.template isBetween<Between::NARROW, D>(from, to) // turn is not aligned with from
			                                                   : from.isColinFwd(to); // algined, thus only to not turning keeps true
		} else {
			return !from.isColinFwd(this->pivot) ? !this->pivot.template isBetween<Between::NARROW, D>(from, to) // turn is not aligned with from
			                                                   : from.isColinFwd(to); // algined, thus only to not turning keeps true
		}
	}
};

template <typename CoordType, Dir TurnD>
struct AngledSector<CoordType, Between::WIDE, TurnD> : AngledSector<CoordType, Between::WHOLE, TurnD>
{
	using super = AngledSector<CoordType, Between::WHOLE, TurnD>;
	using self = AngledSector<CoordType, Between::WIDE, TurnD>;
	using self_inv = AngledSector<CoordType, Between::WIDE, inv_dir(TurnD)>;
	using point = Point<CoordType>;

	constexpr static Between between_type() noexcept { return Between::WIDE; }

	AngledSector() noexcept = default;
	constexpr AngledSector(const self&) noexcept = default;
	constexpr AngledSector(self&&) noexcept = default;
	constexpr AngledSector(point l_pivot, point l_turn) noexcept : super(l_pivot, l_turn)
	{
		assert(!this->pivot.isZero() && !this->turn.isZero());
	}
	constexpr AngledSector(const self_inv& cp) noexcept : super(cp)
	{
		assert(!this->pivot.isZero() && !this->turn.isZero());
	}
	constexpr explicit AngledSector(const super& s) noexcept : AngledSector(s.pivot, s.turn)
	{ }
	constexpr AngledSector& operator=(const self&) noexcept = default;
	constexpr AngledSector& operator=(self&&) noexcept = default;

	constexpr bool isWhole() const noexcept
	{
		return false;
	}

	template <Dir D>
	constexpr AngledSector<CoordType, Between::WIDE, D> makeDir() const noexcept
	{
		if constexpr (D == TurnD)
			return *this;
		else
			return AngledSector<CoordType, Between::WIDE, D>(this->turn, this->pivot);
	}

	template <Dir D, Dir SplitD[[maybe_unused]] = D>
	constexpr AngledSector<CoordType, Between::WIDE, D> split(point pt) const noexcept
	{
		static_assert(D == Dir::CW || D == Dir::CCW, "D must be CW or CCW");
		assert(within(pt));
		if constexpr (D == TurnD) {
			return AngledSector<CoordType, Between::WIDE, D>(pt, this->turn);
		} else {
			return AngledSector<CoordType, Between::WIDE, D>(pt, this->pivot);
		}
	}
	template <Dir D, Dir SplitD[[maybe_unused]] = D>
	constexpr AngledSector<CoordType, Between::WIDE, inv_dir(D)> splitInv(point pt) const noexcept
	{
		return split<D>(pt);
	}

	constexpr bool within(point pt) const noexcept
	{
		assert(!pt.isZero());
		if (auto pvtr = this->pivot.cross(this->turn); point::template isDir<TurnD>(pvtr)) {
			return !pt.template isDir<TurnD>(this->pivot) && !this->turn.template isDir<TurnD>(pt); // narrow
		} else if (point::template isDir<inv_dir(TurnD)>(pvtr)) {
			return !pt.template isDir<TurnD>(this->pivot) || !this->turn.template isDir<TurnD>(pt); // wide
		} else { // collinear
			if (this->pivot.isBack(this->turn))
				return !pt.template isDir<TurnD>(this->pivot); // oposite collinear
			else
				return pt.isColinFwd(this->pivot); // forward collinear
		}
	}
	template <Dir D>
	constexpr bool within(point from, point to) const noexcept
	{
		static_assert(D == Dir::CW || D == Dir::CCW, "must be CW or CCW");
		assert(!from.isZero() && !to.isZero());
		assert(within(from));
		assert(!to.template isDir<D>(from));

		if constexpr (D == TurnD) {
			return !from.isColinFwd(this->turn) ? !this->turn.template isBetween<Between::NARROW, D>(from, to) // turn is not aligned with from
			                                                   : from.isColinFwd(to); // algined, thus only to not turning keeps true
		} else {
			return !from.isColinFwd(this->pivot) ? !this->pivot.template isBetween<Between::NARROW, D>(from, to) // turn is not aligned with from
			                                                   : from.isColinFwd(to); // algined, thus only to not turning keeps true
		}
	}
};

template <typename CoordType, Dir TurnD>
struct AngledSector<CoordType, Between::NARROW, TurnD> : AngledSector<CoordType, Between::WIDE, TurnD>
{
	using super = AngledSector<CoordType, Between::WIDE, TurnD>;
	using self = AngledSector<CoordType, Between::NARROW, TurnD>;
	using self_inv = AngledSector<CoordType, Between::NARROW, inv_dir(TurnD)>;
	using point = Point<CoordType>;

	constexpr static Between between_type() noexcept { return Between::NARROW; }

	AngledSector() noexcept = default;
	constexpr AngledSector(const self&) noexcept = default;
	constexpr AngledSector(self&&) noexcept = default;
	constexpr AngledSector(point l_pivot, point l_turn) noexcept : super(l_pivot, l_turn)
	{
		assert(!this->pivot.template isDir<inv_dir(TurnD)>(this->turn));
	}
	constexpr AngledSector(const self_inv& cp) noexcept : super(cp)
	{
		assert(!this->pivot.template isDir<inv_dir(TurnD)>(this->turn));
	}
	constexpr explicit AngledSector(const super& s) noexcept : AngledSector(s.pivot, s.turn)
	{ }
	constexpr AngledSector& operator=(const self&) noexcept = default;
	constexpr AngledSector& operator=(self&&) noexcept = default;

	template <Dir D>
	constexpr AngledSector<CoordType, Between::NARROW, D> makeDir() const noexcept
	{
		if constexpr (D == TurnD)
			return *this;
		else
			return AngledSector<CoordType, Between::NARROW, D>(this->turn, this->pivot);
	}

	template <Dir D, Dir SplitD[[maybe_unused]] = D>
	constexpr AngledSector<CoordType, Between::NARROW, D> split(point pt) const noexcept
	{
		static_assert(D == Dir::CW || D == Dir::CCW, "D must be CW or CCW");
		assert(within(pt));
		if constexpr (D == TurnD) {
			return AngledSector<CoordType, Between::NARROW, D>(pt, this->turn);
		} else {
			return AngledSector<CoordType, Between::NARROW, D>(pt, this->pivot);
		}
	}
	template <Dir D, Dir SplitD[[maybe_unused]] = D>
	constexpr AngledSector<CoordType, Between::NARROW, inv_dir(D)> splitInv(point pt) const noexcept
	{
		return split<D>(pt);
	}

	constexpr bool within(point pt) const noexcept
	{
		assert(!pt.isZero());
		if (auto pvpt = this->pivot.cross(pt), trpt = this->turn.cross(pt); point::template isDir<TurnD>(pvpt)) {
			return !point::template isDir<TurnD>(trpt);
		} else if (point::isColin(pvpt)) {
			return point::template isDir<inv_dir(TurnD)>(trpt) || (point::isColin(trpt) && this->turn.isColinFwd(pt));
		} else {
			return false;
		}
	}
	template <Dir D>
	constexpr bool within(point from, point to) const noexcept
	{
		static_assert(D == Dir::CW || D == Dir::CCW, "must be CW or CCW");
		assert(!from.isZero() && !to.isZero());
		assert(within(from));
		assert(!to.template isDir<D>(from));

		if constexpr (D == TurnD) {
			return !from.isColin(this->turn) ? !this->turn.template isBetween<Between::NARROW, D>(from, to) // turn is not aligned with from
			                                 : from.isColinFwd(to); // algined, thus only to not turning keeps true
		} else {
			return !from.isColin(this->pivot) ? !this->pivot.template isBetween<Between::NARROW, D>(from, to) // turn is not aligned with from
			                                 : from.isColinFwd(to); // algined, thus only to not turning keeps true
		}
	}
};

} // namespace rayscanlib::geometry

#endif // RAYSCANLIB_GEOMETRY_ANGLEDSECTOR_HPP_INCLUDED
