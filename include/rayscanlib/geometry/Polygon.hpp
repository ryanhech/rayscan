#ifndef RAYSCAN_GEOMETRY_POLYGON_HPP_INCLUDED
#define RAYSCAN_GEOMETRY_POLYGON_HPP_INCLUDED

#include "fwd.hpp"
#include "Point.hpp"
#include <inx/value_iterator.hpp>
#include <boost/iterator/counting_iterator.hpp>
// #include <boost/range/iterator_range.hpp>
// #include <boost/range/adaptor/transformed.hpp>
#include <vector>
#include <boost/geometry.hpp>
#include <iostream>
#include <cmath>

namespace rayscanlib::geometry {

template <typename CoordType>
class Polygon;
template <typename T>
class PolygonIterator;
template <typename T, bool Rev>
class CircPolygonIterator;
template <typename PolyType>
class ConvexPolygon;

template <typename It>
struct is_circ_iterator_ : std::false_type
{ };
template <typename Poly, bool Rev>
struct is_circ_iterator_<CircPolygonIterator<Poly, Rev>> : std::true_type
{ };
template <typename It>
struct is_circ_iterator : is_circ_iterator_<inx::remove_cvref_t<It>>
{ };
template <typename It>
inline constexpr bool is_circ_iterator_v = is_circ_iterator<It>::value;

enum class PolyType : int8
{
	Auto = -1,
	None = 0,
	SimplePolygon,
	SimpleColinPolygon,
	SimpleWithHull, // convex hull excludes collinear points, but ack it
	SimpleWithHullColin, // convex hull includes collinear points
	SimpleColinWithHullColin, // convex hull includes collinear points
};

template <typename CoordType>
class Polygon
{
public:
	using coord_type = CoordType;
	using point_type = Point<CoordType>;
	using segment_type = std::pair<point_type, point_type>;
	using edge_type = std::tuple<point_type, point_type, point_type>;
	using iterator = PolygonIterator<CoordType>;
	using reverse_iterator = std::reverse_iterator<iterator>;
	using circ_iterator = CircPolygonIterator<CoordType, false>;
	using reverse_circ_iterator = CircPolygonIterator<CoordType, true>;
	using container_type = std::vector<point_type>;
	using self = Polygon<CoordType>;

protected:
	point_type* mData;
	PolyType mType;
	Dir mOrientation;
	uint32 mSize;
	std::vector<point_type> mPoints;

public:
	Polygon() : mData(nullptr), mType(PolyType::None), mOrientation(Dir::COLIN), mSize(0) { }
	template <typename It>
	Polygon(It&& a, It&& b, PolyType type = PolyType::Auto) : Polygon()
	{
		if constexpr (is_circ_iterator_v<It>) {
			mPoints.emplace_back();
			mPoints.reserve(std::distance(a, b) + 3);
			mPoints.insert(mPoints.end(), a, b);
			mPoints.emplace_back(*b);
		} else {
			if constexpr (std::is_base_of_v<std::random_access_iterator_tag, std::iterator_traits<std::remove_reference_t<It>>>) {
				mPoints.reserve(std::distance(a, b) + 2);
			}
			mPoints.emplace_back();
			mPoints.insert(mPoints.end(), a, b);
		}
		finalise(type);
	}
	Polygon(const self& poly, PolyType type) : Polygon(poly.begin(), poly.end(), type)
	{ }
	Polygon(const self& poly)
	{
		mPoints = poly.mPoints;
		switch (poly.getType()) {
		case PolyType::None:
			mData = nullptr;
			mType = PolyType::None;
			mOrientation = Dir::COLIN;
			mSize = 0;
			break;
		default:
			mData = mPoints.data() + 1;
			mType = PolyType::SimplePolygon;
			mOrientation = poly.mOrientation;
			mSize = poly.mSize;
		}
	}
	Polygon(self&& poly)
	{
		mPoints = std::move(poly.mPoints);
		switch (poly.getType()) {
		case PolyType::None:
			mData = nullptr;
			mType = PolyType::None;
			mOrientation = Dir::COLIN;
			mSize = 0;
			break;
		default:
			mData = mPoints.data() + 1;
			mType = PolyType::SimplePolygon;
			mOrientation = poly.mOrientation;
			mSize = poly.mSize;
		}
	}
	virtual ~Polygon() = default;

	self& operator=(const self&) = delete;
	self& operator=(self&&) = delete;

	virtual void assign(const self& poly)
	{
		mPoints = poly.mPoints;
		switch (poly.getType()) {
		case PolyType::None:
			mData = nullptr;
			mType = PolyType::None;
			mOrientation = Dir::COLIN;
			mSize = 0;
			break;
		default:
			mData = mPoints.data() + 1;
			mType = PolyType::SimplePolygon;
			mOrientation = poly.mOrientation;
			mSize = poly.mSize;
		}
	}

	virtual void assign(self&& poly)
	{
		mPoints = std::move(poly.mPoints);
		switch (poly.getType()) {
		case PolyType::None:
			mData = nullptr;
			mType = PolyType::None;
			mOrientation = Dir::COLIN;
			mSize = 0;
			break;
		default:
			mData = mPoints.data() + 1;
			mType = PolyType::SimplePolygon;
			mOrientation = poly.mOrientation;
			mSize = poly.mSize;
		}
	}

	void assign(const self& poly, PolyType type)
	{
		clear();
		mPoints = poly.mPoints;
		finalise(type);
	}

	void assign(self&& poly, PolyType type)
	{
		clear();
		mPoints = std::move(poly.mPoints);
		finalise(type);
	}

	bool isFinalised() const noexcept { return mType != PolyType::None; }
	PolyType getType() const noexcept { return mType; }

	ssize_t addPoint(point_type p)
	{
		assert(mPoints.empty() || mPoints.back() != p);
		if (mPoints.size() == 0)
			mPoints.emplace_back();
		mPoints.emplace_back(p);
		return mPoints.size() - 2;
	}
	ssize_t addPointFree(point_type p)
	{
		switch (auto s = mPoints.size(); s) {
		case 0:
			mPoints.emplace_back();
		[[fallthrough]];
		case 1:
		case 2:
			mPoints.emplace_back(p);
			break;
		default:
			auto pivot = mPoints[s-1];
			auto pv = (mPoints[s-2] - pivot).normalise();
			auto nx = (p - pivot).normalise();
			if (pr_op<pr_loweps(PRop::eqZero)>(nx.cross(pv))) {
				auto pm = nx.pair_mult(pv);
				if (point_type::isBack(pm.x, pm.y))
					mPoints[s-1] = p;
				else if (point_type::isFwd(pm.x, pm.y))
					mPoints.emplace_back(p);
				else
					return -1;
			} else {
				mPoints.emplace_back(p);
			}
		}
		return mPoints.size() - 2;
	}
	size_t popPoint()
	{
		mPoints.pop_back();
		return mPoints.size()-2;
	}

	bool normaliseRotate()
	{
		if (auto n = mPoints.size(); mPoints[n-1].dirx(mPoints[n-2], mPoints[1]) == Dir::BACK)
			mPoints.pop_back();
		std::rotate(mPoints.begin()+1, std::min_element(mPoints.begin()+1, mPoints.end()), mPoints.end());
		return true;
	}
	bool normaliseOctile(Dir ori)
	{
		if (mPoints.size() <= 4)
			return false;
		// remove inner corner points
		mPoints.push_back(mPoints[1]);
		std::vector<point_type> pts;
		pts.reserve(3 * mPoints.size() / 2);
		pts.emplace_back();
		uint32 i = 2, n = static_cast<uint32>(mPoints.size());
		for (point_type pv = mPoints[n-2], at = mPoints[1], nx = mPoints[2]; ; pv = at, at = nx, nx = mPoints[i]) {
			if (at.dir(nx, pv) != ori) {
				pts.push_back(2 * at);
			} else {
				pts.push_back(2 * at + (pv - at));
				pts.push_back(2 * at + (nx - at));
			}
			if (++i == n)
				break;
		}
		mPoints = std::move(pts);
		return true;
	}
	bool normaliseCollinear()
	{
		if (mPoints.size() <= 4)
			return false;
		// remove inner corner points
		mPoints.push_back(mPoints[1]);
		uint32 i = 2, j = 1, n = static_cast<uint32>(mPoints.size());
		for (point_type pv = mPoints[n-2], at = mPoints[1], nx = mPoints[2]; ; at = nx, nx = mPoints[i]) {
			if (!at.isColin(pv, nx)) {
				mPoints[j++] = at;
				pv = at;
			}
			if (++i == n)
				break;
		}
		mPoints.erase(mPoints.begin() + j, mPoints.end());
		return true;
	}
	bool normaliseDia()
	{
		// remove inner corner points
		mPoints.push_back(mPoints[1]);
		mPoints.push_back(mPoints[2]);
		uint32 i = 3, j = 1, n = static_cast<uint32>(mPoints.size());
		for (point_type pv = mPoints[n-2], at = mPoints[1], nx = mPoints[2], nx2 = mPoints[3]; ; pv = at, at = nx, nx = nx2, nx2 = mPoints[i]) {
			point_type an = nx - at;
			if (pr_op<PRop::neZero>(an.x * an.y) && pr_op<PRop::eqOne>(at.square(pv)) && pr_op<PRop::eqOne>(nx.square(nx2))) {
				// remove corner
				// skip this and next edge
				++i;
				pv = at, at = nx, nx = nx2, nx2 = mPoints[i];
			} else {
				mPoints[j++] = at;
			}
			if (++i == n)
				break;
		}
		mPoints.erase(mPoints.begin() + j, mPoints.end());
		return true;
	}

	virtual bool finalise(PolyType type)
	{
		if (type == PolyType::None || mPoints.size() < 3)
			return false;
		if (isFinalised())
			return true;
		if (type == PolyType::Auto)
			type = PolyType::SimplePolygon;
		if (type != PolyType::SimplePolygon && type != PolyType::SimpleColinPolygon)
			return false;

		this->mType = type;
		auto n = mPoints.size();
		if (n == 3) { // explicit line
			if (mPoints[1] == mPoints[2])
				return false;
		} else {
			switch (mPoints[n-1].dirx(mPoints[n-2], mPoints[1])) {
			case Dir::BACK:
				if (type == PolyType::SimpleColinPolygon)
					break;
				[[fallthrough]];
			case Dir::INV:
				mPoints.pop_back();
				break;
			case Dir::FWD:
				return false;
			default:
				break;
			}
		}
		mPoints[0] = mPoints.back();
		mPoints.push_back(mPoints[1]);
		mPoints.shrink_to_fit();
		mSize = static_cast<uint32>(mPoints.size()-2);
		mData = mPoints.data() + 1;

		// orientation
		if (mSize == 2) { // line
			mOrientation = Dir::COLIN;
		} else {
			typename point_type::long_result_type s = 0;
			for (int i = 1, ie = mSize; i <= ie; i++) {
				s += (mPoints[i+1].x - mPoints[i].x) * (mPoints[i+1].y + mPoints[i].y);
			}
			mOrientation = s < point_type::neg_epsilon() ? Dir::CCW : s > point_type::pos_epsilon() ? Dir::CW : Dir::COLIN;
		}
		return true;
	}

	virtual void clear()
	{
		mData = nullptr;
		mType = PolyType::None;
		mOrientation = Dir::COLIN;
		mSize = 0;
		mPoints.clear(); mPoints.shrink_to_fit();
	}

	const point_type& point(size_t i) const noexcept
	{
		assert(i < static_cast<size_t>(mSize));
		return mData[i];
	}
	segment_type segment(size_t i) const noexcept(noexcept(std::make_pair(std::declval<point_type>(), std::declval<point_type>())))
	{
		assert(i < static_cast<size_t>(mSize));
		return std::make_pair(mData[i], mData[i+1]);
	}
	segment_type segment_rev(size_t i) const noexcept(noexcept(std::make_pair(std::declval<point_type>(), std::declval<point_type>())))
	{
		assert(i < static_cast<size_t>(mSize));
		return std::make_pair(mData[i], mData[i-1]);
	}
	template <bool Forward>
	segment_type segment_fwd(size_t i) const noexcept(noexcept(std::make_pair(std::declval<point_type>(), std::declval<point_type>())))
	{
		if constexpr (Forward)
			return segment(i);
		else
			return segment_rev(i);
	}
	edge_type edge(size_t i) const noexcept(noexcept(std::make_tuple(std::declval<point_type>(), std::declval<point_type>(), std::declval<point_type>())))
	{
		assert(i < static_cast<size_t>(mSize));
		return std::make_tuple(mData[static_cast<ssize_t>(i)-1], mData[i], mData[i+1]);
	}
	edge_type edge_rev(size_t i) const noexcept(noexcept(std::make_tuple(std::declval<point_type>(), std::declval<point_type>(), std::declval<point_type>())))
	{
		assert(i < static_cast<size_t>(mSize));
		return std::make_tuple(mData[i+1], mData[i], mData[static_cast<ssize_t>(i)-1]);
	}
	template <bool Forward>
	edge_type edge_fwd(size_t i) const noexcept(noexcept(std::make_tuple(std::declval<point_type>(), std::declval<point_type>(), std::declval<point_type>())))
	{
		if constexpr (Forward)
			return edge(i);
		else
			return edge_rev(i);
	}

	const point_type& operator[](ssize_t i) const noexcept
	{
		assert(-1 <= i && i <= static_cast<ssize_t>(mSize));
		return mData[i];
	}

	container_type& container() noexcept { return mPoints; }
	const container_type& container() const noexcept { return mPoints; }

	const point_type& front() const noexcept { return mData[0]; }
	const point_type& back() const noexcept { return mData[mSize-1]; }
	
	size_t size() const noexcept { return mSize; }
	point_type* data() noexcept { return mData; }
	const point_type* data() const noexcept { return mData; }

	iterator begin() const noexcept { return make_iterator(0); }
	iterator end() const noexcept { return make_iterator(mSize); }
	reverse_iterator rbegin() const noexcept { return reverse_iterator(end()); }
	reverse_iterator rend() const noexcept { return reverse_iterator(begin()); }
	iterator cbegin() const noexcept { return begin(); }
	iterator cend() const noexcept { return end(); }
	reverse_iterator rcbegin() const noexcept { return rbegin(); }
	reverse_iterator rcend() const noexcept { return rend(); }

	circ_iterator circ_first() const noexcept { return make_circ_iterator(0); }
	circ_iterator circ_last() const noexcept { return make_circ_iterator(this->mSize-1); }
	reverse_circ_iterator circ_rfirst() const noexcept { return make_circ_riterator(0); }
	reverse_circ_iterator circ_rlast() const noexcept { return make_circ_riterator(this->mSize-1); }

	iterator make_iterator(size_t point_id) const noexcept
	{
		assert(point_id <= mSize);
		return iterator(*this, point_id);
	}
	reverse_iterator make_riterator(size_t point_id) const noexcept
	{
		return reverse_iterator(make_iterator(point_id));
	}
	std::pair<iterator,reverse_iterator> make_iterators(size_t point_id) const noexcept
	{
		return std::make_pair(make_iterator(point_id), make_riterator(point_id));
	}

	circ_iterator make_circ_iterator(size_t point_id) const noexcept
	{
		assert(point_id < mSize);
		return circ_iterator(*this, point_id);
	}
	reverse_circ_iterator make_circ_riterator(size_t point_id) const noexcept
	{
		assert(point_id < mSize);
		return reverse_circ_iterator(*this, point_id);
	}
	std::pair<circ_iterator,reverse_circ_iterator> make_circ_iterators(size_t point_id) const noexcept
	{
		return std::make_pair(make_circ_iterator(point_id), make_circ_riterator(point_id));
	}

	size_t next_index(size_t point_id, size_t adj = 1) const noexcept
	{
		return (point_id + adj) % this->mSize;
	}
	size_t prev_index(size_t point_id, size_t adj = 1) const noexcept
	{
		return (point_id + this->mSize - adj) % this->mSize;
	}

	Dir orientation() const noexcept { return mOrientation; }
};

template <bool AmbigPoints, typename Poly>
bool is_simple_polygon(const Poly& poly)
{
	if (!poly.isFinalised() || poly.size() < 2)
		return false;
	if (poly.size() == 2)
		return true;
	for (auto it = poly.begin(), ite = poly.end(); it != ite; ++it) {
		auto [a, ax] = it.segment(); ax -= a;
		for (auto jt = std::next(it); jt != ite; ++jt) {
			auto [b, bx] = jt.segment(); bx -= b;
			if constexpr (!AmbigPoints) {
				if (a == b)
					return false;
			}
			if (Intersect I = segment_intersect(a, ax, b, bx); I.colin()) {
				if constexpr (!AmbigPoints) {
					if (!I.parallel() && collinear_segment_overlap<false>(a, ax, b, bx)) {
						return false;
					}
				}
			} else if (pr_op<pr_vloweps(PRop::rangeExc)>(I.a, I.scale) && pr_op<pr_vloweps(PRop::rangeExc)>(I.b, I.scale)) {
				return false;
			}
		}
	}
	return true;
}

/// T: coordinate type
template <typename T>
class PolygonIterator
{
public:
	using self = PolygonIterator<T>;
	using size_type = uint32;
	using difference_type = int32;
	using value_type = Point<T>;
	using pointer = const value_type*;
	using reference = const value_type&;
	using iterator_category = std::random_access_iterator_tag;

protected:
	pointer pts;

public:
	PolygonIterator() noexcept : pts(nullptr) { }
	PolygonIterator(pointer lpts) noexcept : pts(lpts) { }
	template <typename Poly>
	PolygonIterator(const Poly& lpoly, size_type lid) noexcept : pts(lpoly.data() + lid) { }
	PolygonIterator(const self&) = default;
	PolygonIterator(self&&) = default;
	~PolygonIterator() = default;

	self& operator=(const self&) = default;
	self& operator=(self&&) = default;

	auto data() const noexcept { return pts; }

	reference point() const noexcept { return *pts; }
	std::array<value_type, 2> segment() const noexcept { return {pts[0], pts[1]}; }
	std::array<value_type, 3> edge() const noexcept { return {pts[-1], pts[0], pts[1]}; }
	bool operator==(const self& b) const noexcept { return pts == b.pts; }
	bool operator!=(const self& b) const noexcept { return !(*this == b); }
	bool operator<(const self& b) const noexcept { return pts < b.pts; }
	bool operator>(const self& b) const noexcept { return b < *this; }
	bool operator<=(const self& b) const noexcept { return !(b < *this); }
	bool operator>=(const self& b) const noexcept { return !(*this < b); }
	reference operator*() const noexcept { return *pts; }
	pointer operator->() const noexcept { return pts; }
	reference operator[](difference_type i) const noexcept { return pts[i]; }
	self& operator++() noexcept { pts++; return *this; }
	self operator++(int) noexcept { auto c = *this; ++(*this); return c; }
	self& operator--() noexcept { pts--; return *this; }
	self operator--(int) noexcept { auto c = *this; --(*this); return c; }
	self& operator+=(difference_type b) noexcept { pts += b; return *this; }
	self operator+(difference_type b) const noexcept { return self(*this) += b; }
	self& operator-=(difference_type b) noexcept { pts -= b; return *this; }
	self operator-(difference_type b) const noexcept { return self(*this) -= b; }
	difference_type operator-(const self& b) const noexcept { return pts - b.pts; }
};

template <typename T, bool Rev>
class CircPolygonIterator
{
public:
	using self = CircPolygonIterator<T, Rev>;
	using inverted = CircPolygonIterator<T, !Rev>;
	using size_type = uint32;
	using difference_type = int32;
	using value_type = Point<T>;
	using pointer = const value_type*;
	using reference = const value_type&;
	using iterator_category = std::random_access_iterator_tag;

protected:
	pointer pts;
	inx::circular_iterator<size_type, Rev> id;

public:
	CircPolygonIterator() noexcept : pts(nullptr) { }
	CircPolygonIterator(pointer lpts, size_type lid, size_type lsize) noexcept : pts(lpts), id(lid, lsize) { }
	template <typename Poly>
	CircPolygonIterator(const Poly& lpoly, size_type lid) noexcept : pts(lpoly.data()), id(lid, lpoly.size()) { }
//	CircPolygonIterator(const T& lpoly, const PolygonIterator<T>& it) noexcept : pts(lpoly.data()), id(it.data() - pts), size(lpoly.size()) { }
	CircPolygonIterator(const self&) = default;
	CircPolygonIterator(self&&) = default;
	~CircPolygonIterator() = default;

	self& operator=(const self&) = default;
	self& operator=(self&&) = default;

	auto data() const noexcept { return pts; }
	auto data_point() const noexcept { return pts + *id; }
	size_type index() const noexcept { return *id; }
	size_type size() const noexcept { return id.size(); }

	template <bool MRev>
	auto make_rev() const noexcept
	{
		if constexpr (Rev == MRev) {
			return *this;
		} else {
			return inverted(pts, *id, id.size());
		}
	}
	inverted invert() const noexcept { return inverted(pts, *id, id.size()); }
	reference point() const noexcept { return pts[*id]; }
	std::array<value_type, 2> segment() const noexcept { return {pts[*id], pts[*id+1]}; }
	std::array<value_type, 2> segment_fwd() const noexcept { if constexpr (Rev) return {pts[*id], pts[static_cast<int32>(*id)-1]}; else return segment(); }
	std::array<value_type, 3> edge() const noexcept { return {pts[static_cast<int32>(*id)-1], pts[*id], pts[*id+1]}; }
	std::array<value_type, 3> edge_fwd() const noexcept { if constexpr (Rev) return {pts[*id+1], pts[*id], pts[static_cast<int32>(*id)-1]}; else return edge(); }
	bool operator==(const self& b) const noexcept { return id == b.id; }
	bool operator!=(const self& b) const noexcept { return !(*this == b); }
	bool operator<(const self& b) const noexcept { return id < b.id; }
	bool operator>(const self& b) const noexcept { return b < *this; }
	bool operator<=(const self& b) const noexcept { return !(b < *this); }
	bool operator>=(const self& b) const noexcept { return !(*this < b); }
	reference operator*() const noexcept { return pts[*id]; }
	pointer operator->() const noexcept { return pts + *id; }
	reference operator[](difference_type i) const noexcept { return pts[id[i]]; }
	self& operator++() noexcept { ++id; return *this; }
	self operator++(int) noexcept { auto c = *this; ++(*this); return c; }
	self& operator--() noexcept { --id; return *this; }
	self operator--(int) noexcept { auto c = *this; --(*this); return c; }
	self& operator+=(difference_type i) noexcept { id += i; return *this; }
	self operator+(difference_type i) const noexcept { return self(*this) += i; }
	self& operator-=(difference_type i) noexcept { id -= i; return *this; }
	self operator-(difference_type i) const noexcept { return self(*this) -= i; }
	difference_type operator-(const self& i) const noexcept { return i.id.circ_distance_to(id); }

//	operator PolygonIterator<T>() const noexcept { return PolygonIterator<T>(pts + id); }
};

template <typename T, bool Rev>
CircPolygonIterator<T, Rev> operator+(typename CircPolygonIterator<T, Rev>::difference_type a, const CircPolygonIterator<T, Rev>& b) noexcept
{
	return CircPolygonIterator<T, Rev>(b) += a;
}

template <bool Colin, typename It, typename Functor>
void convex_hull(It a, It b, Dir ori, Functor&& fn)
{
	uint32 siz = static_cast<uint32>(std::distance(a, b));
	switch (int c = 0; siz) {
	case 2:
		fn(a + c++);
	[[fallthrough]];
	case 1:
		fn(a + c);
	[[fallthrough]];
	case 0:
		return;
	default:
		break;
	}
	std::vector<uint32> pts(boost::counting_iterator<uint32>(0), boost::counting_iterator<uint32>(siz));
	std::sort(pts.begin(), pts.end(), [a](uint32 lhs, uint32 rhs) { return a[lhs] < a[rhs]; });
	std::vector<uint32> hull; hull.reserve(16);
	hull.emplace_back(pts[0]); hull.emplace_back(pts[1]);
	uint32 zero = 2;
	auto push_hull = [&](uint32 i) {
		while (static_cast<uint32>(hull.size()) >= zero) {
			if (uint32 p1 = hull[hull.size()-2], p2 = hull[hull.size()-1];
				Colin ? a[p1].isCW(a[p2], a[i]) : !a[p1].isCCW(a[p2], a[i])) {
				hull.pop_back();
			} else {
				break;
			}
		}
		hull.emplace_back(i);
	};
	for (uint32 i = 2; i < siz; ++i) {
		push_hull(pts[i]);
	}
	hull.emplace_back(pts.back());
	zero = static_cast<uint32>(hull.size());
	for (uint32 i = siz - 2; ; --i) {
		push_hull(pts[i]);
		if (i == 0) // i is unsigned, thus underflow with create infinite loop otherwise
			break;
	}
	assert(hull.front() == hull.back());
	hull.pop_back();
	auto itm = std::min_element(hull.begin(), hull.end());
	if (ori == Dir::CCW) {
		for (auto it = itm, ite = hull.end(); it != ite; ++it) {
			fn(a + *it);
		}
		for (auto it = hull.begin(); it != itm; ++it) {
			fn(a + *it);
		}
	} else {
		auto ritm = std::make_reverse_iterator(std::next(itm));
		for (auto it = ritm, ite = hull.rend(); it != ite; ++it) {
			fn(a + *it);
		}
		for (auto it = hull.rbegin(); it != ritm; ++it) {
			fn(a + *it);
		}
	}
}

template <bool Colin, typename Poly, typename Functor>
void convex_hull(const Poly& poly, Functor&& fn)
{
	convex_hull<Colin>(poly.begin(), poly.end(), poly.orientation(), std::forward<Functor>(fn));
}

/// will split a polygon into sub-polygons along its convex hull
template <bool IgnoreLines, typename Poly, typename Functor>
void convex_splitter(const Poly& poly, Functor&& fn)
{
	using it_type = typename Poly::iterator;
	it_type pv;
	convex_hull<true>(poly, [&](it_type at) {
		if (pv != it_type()) {
			if constexpr (IgnoreLines) {
				if (pv < at ? std::distance(pv, at) != 1 : at != poly.begin() || pv != std::prev(poly.end()))
					fn(poly.make_circ_iterator(std::distance(poly.begin(), pv)), poly.make_circ_iterator(std::distance(poly.begin(), at)));
			} else {
				fn(poly.make_circ_iterator(std::distance(poly.begin(), pv)), poly.make_circ_iterator(std::distance(poly.begin(), at)));
			}
		}
		pv = at;
	});
}

template <typename CoordType>
class ConvexPolygon final : public Polygon<CoordType>
{
public:
	using self = ConvexPolygon<CoordType>;
	using polygon = Polygon<CoordType>;
	using iterator = PolygonIterator<CoordType>;
	using reverse_iterator = std::reverse_iterator<iterator>;
	using circ_iterator = CircPolygonIterator<CoordType, false>;
	using reverse_circ_iterator = CircPolygonIterator<CoordType, true>;

	enum PointOnHull : int8 {
		PointNotOnHull = 0,
		PointOnHullCorner = 1,
		PointOnHullSegment = -1
	};

	struct SimpleTo
	{
		int32 outer; /// only directly on convex hull (i.e. only PointOnHullCorner)
		int32 inner; /// either outer (PointOnHullCorner) or the collinear point on hull (PointOnHullSegment)
	};
	
protected:
	polygon mConvexHull;
	std::vector<SimpleTo> mSimpleToConvex;
	std::vector<int32> mConvexToSimple;
	std::vector<PointOnHull> mPointOnHull;
public:
	ConvexPolygon() : polygon()
	{ }
	template <typename It>
	ConvexPolygon(It&& a, It&& b, PolyType type = PolyType::Auto) : polygon(a, b, PolyType::None)
	{
		finalise(type);
	}
	ConvexPolygon(const polygon& poly, PolyType type) : ConvexPolygon(poly.begin(), poly.end(), type)
	{ }
	ConvexPolygon(const polygon& poly) : ConvexPolygon(poly.begin(), poly.end(), poly.isFinalised() ? PolyType::Auto : PolyType::None)
	{ }
	ConvexPolygon(const self& poly) : polygon(poly)
	{
		switch (poly.getType()) {
		case PolyType::None:
		case PolyType::SimplePolygon:
			break;
		default: // convex hull
			assert(poly.mConvexHull.isFinalised());
			this->mType = PolyType::SimpleWithHull;
			mConvexHull = poly.mConvexHull;
			mSimpleToConvex = poly.mSimpleToConvex;
			mConvexToSimple = poly.mConvexToSimple;
			mPointOnHull = poly.mPointOnHull;
		}
	}
	ConvexPolygon(self&& poly) : polygon(std::move(poly))
	{
		switch (poly.getType()) {
		case PolyType::None:
		case PolyType::SimplePolygon:
			break;
		default: // convex hull
			assert(poly.mConvexHull.isFinalised());
			this->mType = PolyType::SimpleWithHull;
			mConvexHull = std::move(poly.mConvexHull);
			mSimpleToConvex = std::move(poly.mSimpleToConvex);
			mConvexToSimple = std::move(poly.mConvexToSimple);
			mPointOnHull = std::move(poly.mPointOnHull);
		}
	}

	self& operator=(const self&) = delete;
	self& operator=(self&&) = delete;

	void assign(const polygon& poly) override
	{
		polygon::assign(poly);
		switch (poly.getType()) {
		case PolyType::None:
		case PolyType::SimplePolygon:
			mConvexHull.clear();
			mSimpleToConvex.clear(); mSimpleToConvex.shrink_to_fit();
			mConvexToSimple.clear(); mConvexToSimple.shrink_to_fit();
			mPointOnHull.clear(); mPointOnHull.shrink_to_fit();
			break;
		default: // convex hull
			const self& cp = static_cast<const self&>(poly);
			assert(cp.mConvexHull.isFinalised());
			this->mType = PolyType::SimpleWithHull;
			mConvexHull.assign(cp.mConvexHull);
			mSimpleToConvex = cp.mSimpleToConvex;
			mConvexToSimple = cp.mConvexToSimple;
			mPointOnHull = cp.mPointOnHull;
		}
	}
	void assign(polygon&& poly) override
	{
		polygon::assign(poly);
		switch (poly.getType()) {
		case PolyType::None:
		case PolyType::SimplePolygon:
			mConvexHull.clear();
			mSimpleToConvex.clear(); mSimpleToConvex.shrink_to_fit();
			mConvexToSimple.clear(); mConvexToSimple.shrink_to_fit();
			mPointOnHull.clear(); mPointOnHull.shrink_to_fit();
			break;
		default: // convex hull
			const self& cp = static_cast<const self&>(poly);
			assert(cp.mConvexHull.isFinalised());
			this->mType = PolyType::SimpleWithHull;
			mConvexHull.assign(std::move(cp.mConvexHull));
			mSimpleToConvex = std::move(cp.mSimpleToConvex);
			mConvexToSimple = std::move(cp.mConvexToSimple);
			mPointOnHull = std::move(cp.mPointOnHull);
		}
	}
	using polygon::assign;

	const polygon& convex() const noexcept { return mConvexHull; }
	const std::vector<SimpleTo>& getSimpleToConvex() const noexcept { return mSimpleToConvex; }
	SimpleTo simpleToConvex(int32 i) const noexcept { return mSimpleToConvex[i]; }
	const std::vector<int32>& getConvexToSimple() const noexcept { return mConvexToSimple; }
	int32 convexToSimple(int32 i) const noexcept { return mConvexToSimple[i]; }
	const std::vector<PointOnHull>& getPointOnHull() const noexcept { return mPointOnHull; }
	int32 caveLowerBound(int32 i) const noexcept
	{
		if (int32 x = mSimpleToConvex[i].inner; x < 0)
			return -x - 1;
		else
			return i;
	}
	int32 caveUpperBound(int32 i) const noexcept
	{
		if (int32 x = mSimpleToConvex[i].inner; x < 0)
			return mSimpleToConvex[-x - 1].inner;
		else
			return x;
	}
	bool simplePointIsCaveEnterance(int32 i) const noexcept
	{
		assert(static_cast<size_t>(i) < this->size() && this->simpleToConvex(i).inner >= 0);
		return mPointOnHull[this->next_index(i)] == PointNotOnHull;
	}
	bool convexPointIsCaveEnterance(int32 i) const noexcept
	{
		assert(static_cast<size_t>(i) < mConvexHull.size());
		return mPointOnHull[this->next_index(mConvexToSimple[i])] == PointNotOnHull;
	}

	iterator begin() const noexcept { return make_iterator(0); }
	iterator end() const noexcept { return make_iterator(this->mSize); }
	reverse_iterator rbegin() const noexcept { return reverse_iterator(end()); }
	reverse_iterator rend() const noexcept { return reverse_iterator(begin()); }
	iterator cbegin() const noexcept { return begin(); }
	iterator cend() const noexcept { return end(); }
	reverse_iterator rcbegin() const noexcept { return rbegin(); }
	reverse_iterator rcend() const noexcept { return rend(); }

	circ_iterator circ_first() const noexcept { return make_circ_iterator(0); }
	circ_iterator circ_last() const noexcept { return make_circ_iterator(this->mSize-1); }
	reverse_circ_iterator circ_rfirst() const noexcept { return make_circ_riterator(0); }
	reverse_circ_iterator circ_rlast() const noexcept { return make_circ_riterator(this->mSize-1); }

	iterator make_iterator(size_t point_id) const noexcept
	{
		assert(point_id <= this->mSize);
		return iterator(*this, point_id);
	}
	reverse_iterator make_riterator(size_t point_id) const noexcept
	{
		return reverse_iterator(make_iterator(point_id));
	}
	std::pair<iterator,reverse_iterator> make_iterators(size_t point_id) const noexcept
	{
		return std::make_pair(make_iterator(point_id), make_riterator(point_id));
	}

	circ_iterator make_circ_iterator(size_t point_id) const noexcept
	{
		assert(point_id < this->mSize);
		return circ_iterator(*this, point_id);
	}
	reverse_circ_iterator make_circ_riterator(size_t point_id) const noexcept
	{
		assert(point_id < this->mSize);
		return reverse_circ_iterator(*this, point_id);
	}
	std::pair<circ_iterator,reverse_circ_iterator> make_circ_iterators(size_t point_id) const noexcept
	{
		return std::make_pair(make_circ_iterator(point_id), make_circ_riterator(point_id));
	}

	iterator convex_to_simple_iterator(iterator it) const noexcept
	{
		return make_iterator(convexToSimple(it - begin()));
	}
	reverse_iterator convex_to_simple_iterator(reverse_iterator it) const noexcept
	{
		return make_riterator(convexToSimple(it - rbegin()));
	}
	circ_iterator convex_to_simple_iterator(circ_iterator it) const noexcept
	{
		return make_circ_iterator(convexToSimple(it.index()));
	}
	reverse_circ_iterator convex_to_simple_iterator(reverse_circ_iterator it) const noexcept
	{
		return make_circ_riterator(convexToSimple(it.index()));
	}

	bool pointOnHull(size_t id) const noexcept
	{
		return mPointOnHull[id] != PointNotOnHull;
	}
	template <typename It, typename Enable = std::enable_if_t<!std::is_integral_v<std::remove_reference_t<It>>>>
	bool pointOnHull(const It& it) const noexcept
	{
		size_t id;
		if constexpr (is_circ_iterator_v<It>) {
			id = it.index();
		} else {
			id = static_cast<size_t>(inx::normalise_iterator(it).data() - this->mData);
		}
		return pointOnHull(id);
	}

	bool segmentOnHull(size_t id) const noexcept
	{
		return mPointOnHull[id] != PointNotOnHull && mPointOnHull[id+1] != PointNotOnHull;
	}
	template <typename It, typename Enable = std::enable_if_t<!std::is_integral_v<std::remove_reference_t<It>>>>
	bool segmentOnHull(const It& it) const noexcept
	{
		size_t id;
		if constexpr (is_circ_iterator_v<It>) {
			id = it.index();
		} else {
			id = static_cast<size_t>(inx::normalise_iterator(it).data() - this->mData);
		}
		return segmentOnHull(id);
	}

	bool edgeOnHull(size_t id) const noexcept
	{
		return mPointOnHull[id] != PointNotOnHull && mPointOnHull[id+1] != PointNotOnHull && mPointOnHull[id != 0 ? id-1 : this->mSize-1] != PointNotOnHull;
	}
	template <typename It, typename Enable = std::enable_if_t<!std::is_integral_v<std::remove_reference_t<It>>>>
	bool edgeOnHull(const It& it) const noexcept
	{
		size_t id;
		if constexpr (is_circ_iterator_v<It>) {
			id = it.index();
		} else {
			id = static_cast<size_t>(inx::normalise_iterator(it).data() - this->mData);
		}
		return edgeOnHull(id);
	}

	bool finalise(PolyType type) override
	{
		if (type == PolyType::None)
			return false;
		if (type == PolyType::Auto)
			type = PolyType::SimpleWithHull;
		bool simpleColin = type == PolyType::SimpleColinPolygon || type == PolyType::SimpleColinWithHullColin;
		bool hullColin = type == PolyType::SimpleWithHullColin || type == PolyType::SimpleColinWithHullColin;
		if (!polygon::finalise(!simpleColin ? PolyType::SimplePolygon : PolyType::SimpleColinPolygon))
			return false;
		if (this->mType == type)
			return true;

		this->mType = type;
		mSimpleToConvex.assign(this->mSize + 1, SimpleTo{-1, -1});
		mPointOnHull.assign(this->mSize + 1, PointNotOnHull);
		std::vector<uint32> pts; pts.reserve(this->mSize + 1);
		convex_hull<true>(static_cast<const polygon&>(*this), [&pts,bg=std::begin(static_cast<const polygon&>(*this))] (auto it) {
			pts.push_back(static_cast<uint32>(std::distance(bg, it)));
		});
		pts.push_back(pts.front());
		assert(mConvexToSimple.empty());
		mConvexToSimple.reserve(pts.size());
		SimpleTo pti;
		constexpr int32 INNERMAX = std::numeric_limits<int32>::max();
		pti.outer = mConvexHull.addPoint(this->mData[pts[0]]);
		pti.inner = INNERMAX;
		mConvexToSimple.push_back(pts[0]);
		mSimpleToConvex[pts[0]] = pti;
		mPointOnHull[pts[0]] = PointOnHullCorner;
		typename polygon::point_type pt = this->mData[pts[0]], at, nx = this->mData[pts[1]];
		for (size_t i = 2; i < pts.size(); ++i) {
			if (pts[i-1] == pts[i])
				continue;
			at = nx;
			nx = this->mData[pts[i]];
			if (hullColin || !pt.isColin(at, nx)) {
				pt = at;
				pti.outer = mConvexHull.addPoint(at);
				mConvexToSimple.push_back(pts[i-1]);
				mPointOnHull[pts[i-1]] = PointOnHullCorner;
			} else {
				mPointOnHull[pts[i-1]] = PointOnHullSegment;
			}
			mSimpleToConvex[pts[i-1]] = pti;
		}
		pti = {-1,0};
		for (uint32 i = 0; i < this->mSize; i++) {
			if (pti.outer >= 0) {
				if (mSimpleToConvex[i].inner == INNERMAX) {
					mSimpleToConvex[-pti.inner - 1].inner = i;
					pti.inner = -i - 1;
				}
				mSimpleToConvex[i].inner = pti.inner;
			}
			if (mSimpleToConvex[i].outer >= 0) {
				pti.outer = mSimpleToConvex[i].outer;
				pti.inner = -i - 1;
			} else {
				mSimpleToConvex[i].outer = pti.outer;
			}
		}
		assert(pti.outer >= 0 && pti.inner != INNERMAX);
		for (uint32 i = 0; ; i++) {
			if (mSimpleToConvex[i].inner == INNERMAX) {
				mSimpleToConvex[-pti.inner - 1].inner = i;
				pti.inner = -i - 1;
			}
			if (mSimpleToConvex[i].outer < 0)
				mSimpleToConvex[i].inner = pti.inner;
			else {
				mSimpleToConvex[-pti.inner - 1].inner = i;
				break;
			}
			mSimpleToConvex[i].outer = pti.outer;
		}
		mSimpleToConvex.back() = mSimpleToConvex.front();
		mConvexToSimple.push_back(mConvexToSimple.front());
		mPointOnHull.back() = mPointOnHull.front();
		
		return mConvexHull.finalise(!hullColin ? PolyType::SimplePolygon : PolyType::SimpleColinPolygon);
	}

	void clear() override
	{
		polygon::clear();
		mConvexHull.clear();
		mSimpleToConvex.clear(); mSimpleToConvex.shrink_to_fit();
		mConvexToSimple.clear(); mConvexToSimple.shrink_to_fit();
		mPointOnHull.clear(); mPointOnHull.shrink_to_fit();
	}

	template <bool IgnoreLines, typename Functor>
	void convex_splitter(Functor&& fn)
	{
		int32 pv = -1;
		for (int32 i = 0; i < static_cast<int32>(this->mSize); i++) {
			if (mPointOnHull[i] != PointNotOnHull) {
				if (pv >= 0) {
					if (!IgnoreLines || i-pv > 1) {
						fn(make_circ_iterator(pv), make_circ_iterator(i));
					}
				}
				pv = i;
			}
		}
		if (pv >= 0) {
			for (int32 i = 0; i < pv; i++) {
				if (mPointOnHull[i] != PointNotOnHull) {
					if (!IgnoreLines || (i != 0 || pv != static_cast<int32>(this->mSize-1))) {
						fn(make_circ_iterator(pv), make_circ_iterator(i));
					}
					break;
				}
			}
		}
	}
};

enum class Pos : int8
{
	Outside = -1,
	OnEdge = 0,
	Inside = 1
};

inline constexpr bool pos_is_outside(Pos p) noexcept { return static_cast<int8>(p) < 0; }
inline constexpr bool pos_is_onedge(Pos p) noexcept { return static_cast<int8>(p) == 0; }
inline constexpr bool pos_is_inside(Pos p) noexcept { return static_cast<int8>(p) > 0; }
inline constexpr Pos pos_invert(Pos p) noexcept { return static_cast<Pos>(-static_cast<int8>(p)); }

template <bool AllowColin, typename Poly>
bool is_convex_polygon(const Poly& poly)
{
	if (!poly.isFinalised())
		return false;
	if (poly.orientation() == Dir::CCW) {
		for (auto dat = poly.data(), date = dat + poly.size(); dat != date; ++dat) {
			if constexpr (AllowColin) {
				if (auto ua = dat[-1] - dat[0], ub = dat[1] - dat[0]; ua.isCCW(ub) || (ua.isColin(ub) && !ua.isBack(ub)))
					return false;
			} else {
				if (!dat[0].isCW(dat[-1], dat[1]))
					return false;
			}
		}
		return true;
	} else if (poly.orientation() == Dir::CW) {
		for (auto dat = poly.data(), date = dat + poly.size(); dat != date; ++dat) {
			if constexpr (AllowColin) {
				if (auto ua = dat[-1] - dat[0], ub = dat[1] - dat[0]; ua.isCW(ub) || (ua.isColin(ub) && !ua.isBack(ub)))
					return false;
			} else {
				if (!dat[0].isCCW(dat[-1], dat[1]))
					return false;
			}
		}
		return true;
	} else {
		return false;
	}
}

template <Dir Ori, bool AllowColin, typename ItA, typename ItB, typename Transform>
bool is_convex_polygon(ItA&& it1, ItB&& it2, Transform&& transform = [] (const auto& pt) noexcept { return pt; })
{
	if (it1 == it2)
		return false;
	const auto p0 = transform(*it1);
	++it1;
	if (it1 == it2) // point considered convex hull
		return true;
	const auto p1 = transform(*it1);
	++it1;
	if (it1 == it2) // line considered convex hull
		return p0 != p1;
	auto pv = p0;
	auto at = p1;
	while (it1 != it2) {
		auto nx = transform(*it1);
		++it1;
		if constexpr (AllowColin) {
			if (auto ua = pv - at, ub = nx - at; ua.template isDir<Ori>(ub) || (ua.isColin(ub) && !ua.isBack(ub))) {
				return false;
			}
		} else {
			if (!at.template isDir<inv_dir(Ori)>(pv, at)) {
				return false;
			}
		}
		pv = at;
		at = nx;
	}
	if constexpr (AllowColin) {
		if (auto ua = at - p0, ub = p1 - p0; ua.template isDir<Ori>(ub) || (ua.isColin(ub) && !ua.isBack(ub))) {
			return false;
		}
	} else {
		if (!p0.template isDir<inv_dir(Ori)>(at, p1)) {
			return false;
		}
	}
	return true;
}

template <Dir Ori, bool Colin, typename ItBegin, typename ItEnd, typename Vector, typename GetPoint>
void convex_hull_adv(ItBegin&& itbegin, ItEnd&& itend, Vector& out, GetPoint&& getter = [] (const auto& pt) noexcept -> const auto& { return pt; })
{
	assert(out.empty());
	std::copy(itbegin, itend, std::back_inserter(out));
	convex_hull_adv(out, std::forward<GetPoint>(getter));
}

template <Dir Ori, bool Colin, typename Vector, typename GetPoint>
void convex_hull_adv(Vector& out, GetPoint&& getter = [] (const auto& pt) noexcept -> const auto& { return pt; })
{
	static_assert(Ori == Dir::CW || Ori == Dir::CCW, "Ori must be CW or CCW");
	using point_type = std::remove_cv_t<std::remove_reference_t<decltype(getter(out.front()))>>;
	if (out.size() < 3)
		return;
	std::swap(out[0], *std::min_element(out.begin(), out.end(),
		[&getter](const auto& a, const auto& b) noexcept(noexcept(getter(a))) {
			return strict_order<false, false>(getter(a), getter(b));
		}
	));
	auto pivot = getter(out[0]);
	// check for a stright line
	if (std::all_of(out.begin() + 2, out.end(),
		[&getter,pivot,ang=getter(out[1])-pivot](const auto& a) noexcept(noexcept(getter(a))) {
			return ang.isColin(getter(a) - pivot);
		}
	)) {
		// stright line
		if constexpr (Colin) {
			std::sort(out.begin()+1, out.end(),
				[&getter,pivot](const auto& a, const auto& b) noexcept(noexcept(getter(a))) {
					return pivot.square(getter(a)) < pivot.square(getter(b));
				}
			);
		} else {
			if (auto it = max_element(out.begin()+1, out.end(),
				[&getter,pivot](const auto& a, const auto& b) noexcept(noexcept(getter(a))) {
					return pivot.square(getter(a)) < pivot.square(getter(b));
				}
			); it != out.begin()+1) {
				out[1] = std::move(*it);
			}
			out.resize(2);
		}
		return;
	}
	// sort Ori, then Colin by furthest distance from pivot first
	std::sort(out.begin() + 1, out.end(),
		[&getter,pivot](const auto& a, const auto& b) noexcept(noexcept(getter(a))) {
			auto pa = getter(a) - pivot;
			auto pb = getter(b) - pivot;
			auto crs = pa.cross(pb);
			if constexpr (Ori == Dir::CW) {
				return crs < point_type::neg_epsilon() ? true :
					crs > point_type::pos_epsilon() ? false :
					pa.square() > pb.square();
			} else {
				return crs > point_type::pos_epsilon() ? true :
					crs < point_type::neg_epsilon() ? false :
					pa.square() > pb.square();
			}
		}
	);
	if constexpr (Colin) { // if Colin, remove duplicate points
		out.erase(std::unique(out.begin(), out.end(), [&getter](const auto& a, const auto& b) noexcept(noexcept(getter(a))) {
			return getter(a) == getter(b);
		}), out.end());
	}
	while (getter(out.back()) == pivot) {
		out.pop_back();
		assert(!out.empty());
	}
	auto start = out.begin();
	auto end = out.end();
	auto at = std::next(start);
	auto it = std::next(at);
	// move end to first of collinear points, for Colin: done as points are in reverse order, for !Colin: only wants the furthest one
	while (pivot.isColin(getter(end[-1]), getter(end[-2]))) {
		--end;
		assert(end >= std::next(out.begin()));
	}
	while (pivot.isColin(getter(*at), getter(*it))) {
		++it;
		assert(it < out.end());
	}
	if constexpr (Colin) { // move all colin points
		std::reverse(at, it); // reverse so closest is first
		at = std::prev(it);
	}
	for ( ; it != end; ++it) {
		if constexpr (Colin) {
			while (getter(*at).template isDir<Ori>(getter(at[-1]), getter(*it))) {
				--at;
			}
		} else {
			while (!getter(*at).template isDir<Ori>(getter(*it), getter(at[-1]))) {
				--at;
			}
		}
		*++at = std::move(*it);
	}
	if constexpr (Colin) { // add points colin to the end
		auto ite = out.end();
		if (end != ite) {
			for ( ; end != ite; ++end) {
				*++at = std::move(*end);
			}
		}
	}
	++at;
	out.erase(at, out.end());
	assert( is_convex_polygon<Ori INX_COMMA Colin> (out.begin(), out.end(), getter) );
}

template <typename Pt, typename Poly>
Pos point_in_convex_polygon(Pt pt, const Poly& poly)
{
	// handle small sizes
	switch (poly.size()) {
	case 0:
	case 1:
		assert(false);
		return Pos::Outside;
	case 2: {
		auto [a, b] = poly.segment(1);
		switch (pt.dirx(a, b)) {
		case Dir::BACK:
		case Dir::INV:
			return Pos::OnEdge;
		default:
			return pt != b ? Pos::Outside : Pos::OnEdge;
		}
	}
	case 3: {
		Pt pv, at = poly[-1];
		for (int i = 0; i < 3; ++i) {
			pv = at; at = poly[i];
			if (auto d = pt.dirx(pv, at); d == Dir::BACK || d == Dir::INV) {
				return Pos::OnEdge;
			} else if (d != poly.orientation()) {
				return Pos::Outside;
			}
		}
		return Pos::Inside;
	}
	default:
		break;
	}
	//  check for point in poly
	auto* data = poly.data();
	size_t size = poly.size();
	using ct = typename Pt::coord_type;
	const ct mod = poly.orientation() == Dir::CCW ? 1 : -1;
	for (size_t a = 0, b = size; a < b; ) {
		size_t c = b-a;
		if (c < 8) { // if small, naive check
			for (size_t i = 0; i < c-1; i++) {
				if (ct x = pt.cross(data[a+i], data[a+i+1]) * mod; Pt::isCW(x))
					return Pos::Outside;
				else if (Pt::isColin(x))
					return Pos::OnEdge;
			}
			if (ct x = pt.cross(data[b-1], data[a]) * mod; Pt::isCW(x))
				return Pos::Outside;
			else if (Pt::isColin(x))
				return Pos::OnEdge;
			else
				return Pos::Inside;
		}
		c >>= 1; // mid point
		Pt ptx(pt - data[a]);
		Pt ptd(data[a+c] - data[a]);
		if (ct x = ptd.cross(ptx) * mod; Pt::isCW(x)) { // if point on first half
			b = a+c;
		} else if (Pt::isCCW(x)) { // if point on second half
			a = a+c;
		} else if (auto y = collinear_point_on_segment(ptx, ptd); pr_op<PRop::eqZero>(y) || pr_op<PRop::eqOne>(y)) { // point is collinear, check if on corner
			return Pos::OnEdge;
		} else if (pr_op<PRop::rangeInc>(y)) { // check if inside
			return Pos::Inside;
		} else { // point is outside convex polygon
			return Pos::Outside;
		}
	}
	return Pos::Inside;
}

// @return area of convex polygon * 2
template <typename Poly>
typename Poly::point_type::result_type convex_polygon_area2(const Poly& poly)
{
	assert(poly.isFinalised());
	typename Poly::point_type::result_type A = 0;
	auto [pivot,nx] = poly.segment(0);
	nx -= pivot;
	typename Poly::point_type at;
	for (auto it = std::next(poly.begin(), 2), ite = poly.end(); it != ite; ++it) {
		at = nx; nx = *it - pivot;
		A += at.cross(nx);
	}
	return std::abs(A);
}

template <typename Pt, typename Poly>
Pos point_in_polygon(Pt pt, const Poly& poly)
{
	// handle small sizes
	if (poly.size() < 4)
		return point_in_convex_polygon(pt, poly);
#if 0
	//  check for point in poly
	uint32 intersections = 0;
	auto [pv, at, nx] = poly.edge(0);
	pv -= pt; at -= pt; nx -= pt;
	if (is_all_zero(pv.y, at.y)) {
		if (static_cast<typename Pt::result_type>(pv.x) * static_cast<typename Pt::result_type>(at.x) <= Pt::pos_epsilon())
			return Pos::OnEdge;
		pv = poly.end()[-2] - pt;
		assert( !is_all_zero(pv.y, at.y) );
	}
	for (auto it = poly.begin(), ite = poly.end(); ; ) {
		if (is_all_zero(at.y, nx.y)) {
			if (static_cast<typename Pt::result_type>(at.x) * static_cast<typename Pt::result_type>(nx.x) <= Pt::pos_epsilon())
				return Pos::OnEdge;
			if (++it == ite) break;
			at = nx;
			nx = it[1] - pt;
			assert( !is_all_zero(pv.y, at.y) );
		}
		auto d = at.cross(nx);
	//	int64 dx = static_cast<int64>(at.x) * static_cast<int64>(nx.x);
		auto dy = static_cast<typename Pt::result_type>(at.y) * static_cast<typename Pt::result_type>(nx.y);
		if (Pt::isColin(d)) {
			if (dy <= Pt::pos_epsilon())
				return Pos::OnEdge;
		} else if (dy < Pt::neg_epsilon()) {
			if (static_cast<typename Pt::long_result_type>(d) * (nx.y - at.y) >= Pt::neg_epsilon()) // intersection TODO: update for integer
				intersections += 1;
		} else if (dy <= Pt::pos_epsilon() && is_zero(at.y)) {
			if (at.x > Pt::pos_epsilon()) {
				if (static_cast<typename Pt::result_type>(pv.y) * static_cast<typename Pt::result_type>(nx.y) < Pt::neg_epsilon())
					intersections += 1;
			} else if (at.x >= Pt::neg_epsilon()) {
				return Pos::OnEdge;
			}
		}
		// dy > 0 means no intersection
		if (++it == ite) break;
		pv = at;
		at = nx;
		nx = it[1] - pt;
	}
	return (intersections & 1) == 0 ? Pos::Outside : Pos::Inside;
#else
	// winding number
	Pt pv, at = poly.back() - pt;
	double ang = 0;
	double pvLen, atLen = at.length();
	for (auto it = poly.begin(), ite = poly.end(); it != ite; ++it) {
		pv = at;
		at = *it - pt;
		pvLen = atLen;
		atLen = at.length();
		if (pv.isZero()) pt = Pt::zero();
		if (at.isZero()) at = Pt::zero();
		if (auto dx = pv.cross(at); Pt::isColin(dx)) {
			if (!pv.isFwd(at)) // forward means line is in same direction, if not than it is either Bck (opposite) or equal to pv or at
				return Pos::OnEdge;
		} else {
			assert(pvLen > Pt::pos_epsilon() || atLen > Pt::pos_epsilon());
			double a = std::acos(std::clamp((pv * at) / (pvLen*atLen), -1.0, 1.0));
			if (Pt::isCCW(dx)) {
				ang += a;
			} else {
				ang -= a;
			}
		}
	}
	return std::abs(ang) < 1 ? Pos::Outside : Pos::Inside;
#endif
}

template <typename Pt, typename Poly>
std::pair<typename Pt::scale_type, size_t> line_intersect_polygon(Pt a, Pt ab, const Poly& poly)
{
	for (size_t i = 0, ie = static_cast<size_t>(poly.size()); i < ie; i++) {
		auto [p1, p2] = poly.segment(i);
		if (auto I = segment_intersect(a, ab, p1, p2-p1); I.intersect())
			return {I.scaleA(), i};
		else if (I.colin() && !I.parallel()) {
			if (auto d = collinear_point_on_segment(p1-a, ab); pr_op<PRop::rangeInc>(d))
				return {d, i};
		}
	}
	return {Pt::scale_inf(), 0};
}

template <typename Pt, typename Poly>
std::pair<typename Pt::scale_type, size_t> line_intersect_polygon_exc(Pt a, Pt ab, const Poly& poly)
{
	for (size_t i = 0, ie = static_cast<size_t>(poly.size()); i < ie; i++) {
		auto [p1, p2] = poly.segment(i);
		if (auto I = segment_intersect(a, ab, p1, p2-p1); I.intersect() && I.rangeAexc())
			return {I.scaleA(), i};
		else if (I.colin() && !I.parallel()) {
			if (auto d = collinear_point_on_segment(p1-a, ab); pr_op<PRop::rangeExc>(d))
				return {d, i};
		}
	}
	return {Pt::scale_inf(), 0};
}

template <typename Pt, typename Poly>
std::pair<typename Pt::scale_type, size_t> line_intersect_polygon_incexc(Pt a, Pt ab, const Poly& poly)
{
	for (size_t i = 0, ie = static_cast<size_t>(poly.size()); i < ie; i++) {
		auto [p1, p2] = poly.segment(i);
		if (auto I = segment_intersect(a, ab, p1, p2-p1); I.intersect() && pr_op<PRop::ltOne>(I.scaleA()))
			return {I.scaleA(), i};
		else if (I.colin() && !I.parallel()) {
			if (auto d = collinear_point_on_segment(p1-a, ab); pr_op<PRop::geZero>(d) && pr_op<PRop::ltOne>(d))
				return {d, i};
		}
	}
	return {Pt::scale_inf(), 0};
}

// return: -1: seperate, 0: overlap, 1: poly1, 2: poly2
template <typename Poly1, typename Poly2>
int polygon_inside_polygon(const Poly1& poly1, const Poly2& poly2)
{
	using point_type = Point<std::common_type_t<typename Poly1::coord_type, typename Poly1::coord_type>>;
	int res = -1;
	point_type a, b, c, d, b2, d2;
	for (auto it = poly1.begin(), ite = poly1.end(); it < ite; ++it) {
		if (poly1.orientation() == Dir::CCW)
			std::tie(b2, a, b) = inx::to_tuple(it.edge());
		else
			std::tie(b, a, b2) = inx::to_tuple(it.edge());
		b -= a; b2 -= a;
		for (auto jt = poly2.begin(), jte = poly2.end(); jt < jte; ++jt) {
			if (poly2.orientation() == Dir::CCW)
				std::tie(d2, c, d) = inx::to_tuple(jt.edge());
			else
				std::tie(d, c, d2) = inx::to_tuple(jt.edge());
			d -= c; d2 -= c;
			if (a == c) { // check is range
				if (b.isColinFwd(d)) {
					if (b2.isBetweenCCW(d, d2)) { if (res == 2) return 0; res = 1; }
					else if (d2.isBetweenCCW(b, b2)) { if (res == 1) return 0; res = 2; }
					else { if (res < 0) res = 0; }
				} else if (b2.isColinFwd(d2)) {
					if (b.isBetweenCCW(d, d2)) { if (res == 2) return 0; res = 1; }
					else if (d.isBetweenCCW(b, b2)) { if (res == 1) return 0; res = 2; }
					else { if (res < 0) res = 0; }
				} else {
					if (b.isBetweenCCW(d, d2)) {
						if (b2.isBetweenCCW(d, d2)) { if (res == 2) return 0; res = 1; }
						else return 0;
					} else if (d.isBetweenCCW(b, b2)) {
						if (d2.isBetweenCCW(b, b2)) { if (res == 1) return 0; res = 2; }
						else return 0;
					}
				}
			} else {
				auto I = segment_intersect(a, b, c, d);
				if (I.colin()) {
					if (!I.parallel()) {
						if (pr_op<PRop::rangeExc>(collinear_point_on_segment(a-c, d))) {
							if (d.dir(b) == poly2.orientation()) { if (res == 2) return 0; res = 1; }
						} else if (pr_op<PRop::rangeExc>(collinear_point_on_segment(c-a, b))) {
							if (b.dir(d) == poly1.orientation()) { if (res == 1) return 0; res = 2; }
						}
					}
				} else if (I.rangeAexc() && I.rangeBexc())
					return 0;
			}
		}
	}
	return res;
}

} // namespace rayscanlib::geometry

namespace boost
{

namespace geometry::traits
{

template <typename T> struct tag<rayscanlib::geometry::Polygon<T>>
{ using type = ring_tag; };

template <typename T> struct point_order<rayscanlib::geometry::Polygon<T>> : boost::mpl::int_<counterclockwise> {};

template <typename T> struct closure<rayscanlib::geometry::Polygon<T>> : boost::mpl::int_<open> {};

} // namespace geometry::traits

template< class T >
struct range_iterator< const rayscanlib::geometry::Polygon<T> >
{
	using type = typename rayscanlib::geometry::Polygon<T>::iterator;
};

namespace geometry::traits
{

template <typename T> struct tag<rayscanlib::geometry::ConvexPolygon<T>>
{ using type = ring_tag; };

template <typename T> struct point_order<rayscanlib::geometry::ConvexPolygon<T>> : boost::mpl::int_<counterclockwise> {};

template <typename T> struct closure<rayscanlib::geometry::ConvexPolygon<T>> : boost::mpl::int_<open> {};

} // namespace geometry::traits

template< class T >
struct range_iterator< const rayscanlib::geometry::ConvexPolygon<T> >
{
	using type = typename rayscanlib::geometry::ConvexPolygon<T>::iterator;
};

} // namespace boost

#endif // RAYSCAN_GEOMETRY_POLYGON_HPP_INCLUDED
