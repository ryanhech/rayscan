#ifndef RAYSCANLIB_GEOMETRY_FWD_HPP_INCLUDED
#define RAYSCANLIB_GEOMETRY_FWD_HPP_INCLUDED

#include <rayscanlib/inx.hpp>

namespace rayscanlib
{

enum class Dir : int8
{
	CW = -1, // clockwise
	COLIN = 0,
	CCW = 1, // counter-clockwise
	FWD, // forward/same direction/0 deg
	BACK, // backward/opposide direction/180 deg
	INV // invalid, point on origion, has no angle
};

enum class Between : int8
{
	NARROW = -1,
	WHOLE = 0,
	WIDE = 1
};

enum class Compass : uint8
{
	NorthEast,
	NorthWest,
	SouthWest,
	SouthEast,
	East,
	North,
	West,
	South,
};

inline constexpr bool is_compass_dia(Compass c) noexcept
{
	switch (c) {
	case Compass::NorthEast:
	case Compass::NorthWest:
	case Compass::SouthWest:
	case Compass::SouthEast:
		return true;
	default:
		return false;
	}
}

inline constexpr Dir inv_dir(Dir d) noexcept
{
	assert(d == Dir::CW || d == Dir::CCW);
	return d == Dir::CW ? Dir::CCW : Dir::CW;
}

namespace geometry
{

inline constexpr double epsilon = inx::epsilon<double>;

}

}

#endif // RAYSCANLIB_GEOMETRY_FWD_HPP_INCLUDED
