#ifndef RAYSCANLIB_GEOMETRY_UTIL_HILBERTRTREE_HPP_INCLUDED
#define RAYSCANLIB_GEOMETRY_UTIL_HILBERTRTREE_HPP_INCLUDED

#include "../fwd.hpp"
#include "../Box.hpp"
#include <inx/alg/mary_tree.hpp>
#include <boost/endian/conversion.hpp>
#include <vector>
#include <forward_list>
#include <memory_resource>
#include <cmath>

namespace rayscanlib::geometry::util
{

template <typename Coord>
struct HilbertRtreeBox
{
	using coord = Coord;
	using box_coord = std::conditional_t<std::is_integral_v<coord>, coord, int32>;

	auto& get_mbr() noexcept { return m_rtree.mbr; }
	const auto& get_mbr() const noexcept { return m_rtree.mbr; }

	size_t get_hilbert() const noexcept { return m_rtree.hilbert; }

	struct {
		Box<box_coord> mbr;
		size_t hilbert;
	} m_rtree;
};

template <typename Coord, size_t Fanout>
struct HilbertRtreeNode : HilbertRtreeBox<Coord>, inx::alg::mary_tree_node<Fanout>
{
	static constexpr bool fanout() noexcept { return Fanout; }
	bool is_leaf() const noexcept { return m_rtreeNode.index >= 0; }

	struct {
		int32 index;
		int32 size;
	} m_rtreeNode;
};

template <typename ValueType, size_t Fanout, size_t LeafSize>
class HilbertRtree : public inx::alg::mary_tree_base<Fanout>
{
public:
	static constexpr size_t array_size = 64;
	using coord = typename ValueType::coord;
	static_assert(std::is_floating_point_v<coord> || std::is_signed_v<coord>, "Must be signed value");
	using box_coord = typename ValueType::box_coord;
	static constexpr size_t fanout() noexcept { return Fanout; }
	static constexpr size_t leaf_size() noexcept { return LeafSize; }
	
	using value_type = ValueType;
	using box_type = HilbertRtreeBox<coord>;
	using node_box_type = HilbertRtreeBox<coord>;
	using node_type = HilbertRtreeNode<coord, Fanout>;
	using node_ptr = node_type*;
	using node_array = std::array<node_type, array_size>;

	static Box<box_coord> boxCoord(const Point<coord>& pt) noexcept
	{
		if constexpr (std::is_integral_v<coord>) {
			return {{pt.x, pt.y}, {pt.x, pt.y}};
		} else {
			Point<box_coord> bc{static_cast<box_coord>(std::floor(pt.x)), static_cast<box_coord>(std::floor(pt.y))};
			return {pt, {pt.x+1, pt.y+1}};
		}
	}

	HilbertRtree() : m_nodes(&m_nodeBuffer), m_nodesIt(m_nodes.before_begin()), m_nodesSize(array_size), m_factor(0)
	{
		this->m_root = nullptr;
		this->m_size = 0;
	}

	void initalise(size_t count)
	{
		m_values.resize(count);
		this->m_size = count;
		if (m_nodes.empty()) {
			m_nodesSize = array_size;
		} else {
			m_nodesSize = 0;
			m_nodesIt = m_nodes.begin();
		}
	}

	void finalise()
	{
		// setup mbr and hilbert of all values
		assert(!m_values.empty());
		Box<box_coord> mbr = static_cast<node_box_type*>(m_values.front())->m_rtree.mbr;
		assert(m_values.size() == this->m_size);
		size_t s = this->m_size;
		for (size_t i = 1; i < s; ++i) {
			mbr <<  static_cast<node_box_type*>(m_values[i])->m_rtree.mbr;
		}
		m_mbr = mbr;
		m_factor = inx::clz_index(std::max(mbr.width(), mbr.height()));
		for (node_box_type* va : m_values) {
			va->m_rtree.hilbert = hilbertIndex(va->m_rtree.mbr);
		}
		// sort values by hilbert for ordering in tree
		std::sort(m_values.begin(), m_values.end(), [](const node_box_type* a, const node_box_type* b) noexcept { return a->m_rtree.hilbert < b->m_rtree.hilbert; });
		assert(std::adjacent_find(m_values.begin(), m_values.end(), [](const node_box_type* a, const node_box_type* b) { return a->get_hilbert() == b->get_hilbert(); }) == m_values.end());
		// construct the leaf nodes
		std::vector<node_type*> nodes;
		nodes.reserve(s / LeafSize + 2);
		for (size_t i = 0; i < s; i += LeafSize) {
			// setup leaf node values
			node_type* node = newNode();
			int32 ns = static_cast<int32>(std::min(s - i, LeafSize));
			node->m_rtreeNode.index = static_cast<int32>(i);
			node->m_rtreeNode.size = static_cast<int32>(ns);
			// init mbr and hilbert value
			mbr = m_values[i]->m_rtree.mbr;
			for (int32 j = static_cast<int32>(i+1), je = static_cast<int32>(i) + ns; j < je; ++j) {
				mbr << m_values[j]->m_rtree.mbr;
			}
			node->m_rtree.mbr = mbr;
			node->m_rtree.hilbert = m_values[static_cast<int32>(i)+ns-1]->m_rtree.hilbert; // since they are ordered, the last index is largest hilbert value
			// push leaf to node queue
			nodes.push_back(node);
		}
		// construct intermedian nodes
		while (nodes.size() >= 2) { // reduce nodes size to 1, which will be the root node
			size_t idx = 0;
			for (size_t i = 0; i < nodes.size(); i += fanout(), ++idx) {
				node_type* node = newNode();
				int32 ns = static_cast<int32>(std::min(nodes.size() - i, fanout()));
				node->m_rtreeNode.index = -1; // not a leaf node
				node->m_rtreeNode.size = ns;
				// handle child nodes
				mbr = nodes[i]->m_rtree.mbr;
				for (int32 j = 0; j < ns; ++j) {
					node_type* n = nodes[static_cast<int32>(i)+j];
					mbr << n->m_rtree.mbr;
					node->connect_child(*n, j);
				}
				for (int32 j = ns; j < static_cast<int32>(fanout()); ++j) { // set all empty child to null
					node->connect_none(j);
				}
				node->m_rtree.mbr = mbr;
				node->m_rtree.hilbert = nodes[static_cast<int32>(i)+ns-1]->m_rtree.hilbert; // since they are ordered, the last index is largest hilbert value
				// push to node
				nodes[idx] = node;
			}
			// resize nodes to reflect new size
			assert(idx > 0);
			nodes.erase(nodes.begin() + idx, nodes.end());
		}
		assert(nodes.size() == 1);
		nodes.front()->make_root();
		this->m_root = nodes.front();
	}

	node_type* findLeafByHilbert(size_t hilbert)
	{
		node_type* ans = static_cast<node_type*>(this->m_root);
		while (ans != nullptr) {
			if (ans->is_leaf())
				break;
			if (auto it = std::partition_point(ans->children().begin(), ans->children().end(), [hilbert](const auto* g) {
					return g != nullptr && static_cast<const node_type*>(g)->m_rtree.hilbert < hilbert;
				}); it != ans->children().end())
				ans = static_cast<node_type*>(*it);
			else
				ans = nullptr;
		}
		return ans;
	}
	void eraseValue(node_type& node, size_t index)
	{
		this->m_size -= 1;
		auto [leaf, count] = getLeafValues(node);
		assert(leaf != nullptr && count != 0);
		uint8 op; // bit-0 = delete, bit-1 = rebox
		node_type* nod;
		if (count != 1) {
			op = static_cast<uint8>(!leaf[index]->get_mbr().strictly_within(node.get_mbr())) << 1; // if not strictly_within, than it's on the edge, should recalc box
			std::move(leaf + index + 1, leaf + count, leaf + index); // remove element
			leaf[count-1] = nullptr;
			node.m_rtreeNode.size = --count;
			assert(std::all_of(leaf, leaf+node.m_rtreeNode.size, [](auto* g) { return g != nullptr; }));
			if (op) { // rebox
				Box<box_coord> mbr = (*leaf)->get_mbr();
				while (--count > 0) {
					++leaf;
					mbr << (*leaf)->get_mbr();
				}
				node.get_mbr() = mbr;
			}
			nod = static_cast<node_type*>(node.parent());
			if (nod == nullptr)
				op = 0; // exit
		} else {
			op = 0b01; // delete
			nod = &node;
		}
		while (op) {
			if ( (op&0b01) ) { // if delete, move to parent, erase and check for rebox
				node_type* par = static_cast<node_type*>(nod->parent());
				if (par == nullptr) { // corner case, empty root = no tree
					this->m_root = nullptr;
					return;
				}
				assert(par->m_rtreeNode.size != 0);
				if (par->m_rtreeNode.size == 1) { // if 1, just delete, op is already set to delete
					nod = par;
					continue;
				}
				// delete node
				auto ite = par->children().begin() + par->m_rtreeNode.size--;
				auto it = std::find(par->children().begin(), ite, nod);
				std::move(std::next(it), ite, it);
				*std::prev(ite) = nullptr;
				assert(std::all_of(par->children().begin(), par->children().begin() + par->m_rtreeNode.size, [](auto* g) { return g != nullptr; }));
				// check to see if rebox is needed
				op = static_cast<uint8>(!nod->get_mbr().strictly_within(par->get_mbr())) << 1;
				// update nod to parent
				nod = par;
			} else { // other op, rebox
				auto it = nod->children().cbegin(), ite = nod->children().cbegin() + nod->m_rtreeNode.size;
				assert(*it != nullptr);
				Box<box_coord> mbr = static_cast<node_type*>(*it)->get_mbr();
				for (++it; it != ite; ++it) {
					assert(*it != nullptr);
					mbr << static_cast<node_type*>(*it)->get_mbr();
				}
				nod->get_mbr() = mbr; // update mbr
				// check parent for update
				if (node_type* par = static_cast<node_type*>(nod->parent()); par == nullptr) {
					break; // updated mbr of root node, exit
				} else {
					op = static_cast<uint8>(!nod->get_mbr().strictly_within(par->get_mbr())) << 1;
					nod = par; // move up a level
				}
			}
		}
	}

	size_t hilbertIndex(const Box<box_coord>& mbr) const noexcept
	{
		return hilbertIndex(Point<box_coord>(mbr.lower().x - m_mbr.lower().x + (mbr.width() >> 1), mbr.lower().y - m_mbr.lower().y + (mbr.height() >> 1)), m_factor);
	}
	static size_t hilbertIndex(Point<box_coord> p, size_t factor) noexcept
	{
		size_t ans = 0;
		union {
			std::array<uint8, 4> a;
			uint32 i;
		} sectors{0,3,1,2};
		for (box_coord i = (1 << factor); ; ) {
			uint8 sec = 0;
			if (p.x >= i) { p.x -= i; sec |= 1; }
			if (p.y >= i) { p.y -= i; sec |= 2; }
			sec = sectors.a[sec];
			ans += static_cast<size_t>(i) * static_cast<size_t>(i) * static_cast<size_t>(sec);
			i >>= 1;
			assert(i >= 0);
			if (i == 0) // put end condition here to avoid adjusting sectors on final instance
				break;
			assert(sec < 4);
			switch (sec) {
			case 0:
				sectors.i ^= boost::endian::little_to_native(static_cast<uint32>(0x00020200)); // flip odd numbers on array index 1 & 2
				break;
			case 1:
			case 2:
				break;
			case 3:
				sectors.i ^= boost::endian::little_to_native(static_cast<uint32>(0x02000002)); // flip even numbers on array index 0 & 3
				break;
			}
		}
		return ans;
	}

	auto& values() noexcept { return m_values; }
	const auto& values() const noexcept { return m_values; }

	std::pair<value_type**, size_t> getLeafValues(const node_type& node) noexcept
	{
		assert(node.m_rtreeNode.index >= 0 && node.m_rtreeNode.index < static_cast<int32>(m_values.size()) && node.m_rtreeNode.size >= 0 && node.m_rtreeNode.index + node.m_rtreeNode.size <= static_cast<int32>(m_values.size()));
		return std::pair<value_type**, size_t>(m_values.data() + node.m_rtreeNode.index, static_cast<size_t>(node.m_rtreeNode.size));
	}
	std::pair<const value_type**, size_t> getLeafValues(const node_type& node) const noexcept
	{
		assert(node.m_rtreeNode.index >= 0 && node.m_rtreeNode.index < static_cast<int32>(m_values.size()) && node.m_rtreeNode.size >= 0 && node.m_rtreeNode.index + node.m_rtreeNode.size <= static_cast<int32>(m_values.size()));
		return std::pair<const value_type**, size_t>(m_values.data() + node.m_rtreeNode.index, static_cast<size_t>(node.m_rtreeNode.size));
	}

protected:
	node_type* newNode()
	{
		if (m_nodesSize >= array_size) {
			m_nodesSize = 0;
			m_nodesIt = m_nodes.emplace_after(m_nodesIt);
		}
		return &(*m_nodesIt)[m_nodesSize++];
	}

private:
	std::vector<value_type*> m_values;
	std::pmr::monotonic_buffer_resource m_nodeBuffer;
	std::pmr::forward_list<node_array> m_nodes;
	typename decltype(m_nodes)::iterator m_nodesIt;
	size_t m_nodesSize;
	size_t m_factor;
	Box<box_coord> m_mbr;
};

} // namespace warthog

#endif // RAYSCANLIB_GEOMETRY_TREE_HILBERTRTREE_HPP_INCLUDED
