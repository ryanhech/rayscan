#ifndef RAYSCANLIB_GEOMETRY_UTIL_RAYSCANGEOMETRY_HPP_INCLUDED
#define RAYSCANLIB_GEOMETRY_UTIL_RAYSCANGEOMETRY_HPP_INCLUDED

#include "../fwd.hpp"
#include "../Point.hpp"

namespace rayscanlib::geometry::util
{

template <bool EdgeInt, bool CCW = true>
inline Intersect<int16> intersect_corner_segment(Point<int16> a, Point<int16> ab, Point<int16> x, Point<int16> xy, Point<int16> xz)
{
	using point = Point<int16>;
	if constexpr (EdgeInt) {
		Intersect<int16> I = segment_intersect(a, ab, x, xy);
		if (I.colin()) {
			if (!I.parallel()) {
				auto f = collinear_point_on_segment(x, a, ab);
				if (f.a >= 0) { // a == x does not intersect in this case unless entering polygon
					if (ab.template isBetween<CCW ? Dir::CCW : Dir::CW>(xy, xz)) {
						I = Intersect<int16>(f.b, f.a, 0);
					}
				} else if constexpr (EdgeInt) {
					if ((f.a < 0) != (ab * xy < 0)) { // ab heading to x, intersect at x
						I = Intersect<int16>(f.b, f.a < 0 ? -f.a : f.a, 0);
					}
				}
			}
		} else {
			if constexpr (EdgeInt) {
				if (I.b < 0 || I.b >= I.scale ||
					I.a < 0 || (I.a == 0 && !xy.template isDir<CCW ? Dir::CCW : Dir::CW>(ab)) ||
					(I.b == 0 && I.a == 0 && !ab.template isBetween<CCW ? Dir::CW : Dir::CCW>(xy, xz)))
				{
					I.scale = 0;
				}
			} else {
				if (I.b < 0 || I.b >= I.scale || I.a < 0) {
					I.scale = 0;
				} else if (I.b == 0) {
					if (!ab.template isBetween<CCW ? Dir::CCW : Dir::CW>(xy, xz)) {// not entering polygon
						I.scale = 0;
					}
				} else if (I.a == 0) {
					if (!xy.template isDir<CCW ? Dir::CCW : Dir::CW>(ab) )  {
						I.scale = 0;
					}
				}
			}
		}
		return I;
	} else { // newer and faster
		point ax = x - a;
		int32 abXxy = static_cast<int32>(ab.cross(xy));
		int32 den;
		if (abXxy < 0) {
			ax = -ax;
			den = -abXxy;
		} else {
			den = abXxy;
		}
		int32 ray = static_cast<int32>(ax.cross(xy));
		int32 seg = static_cast<int32>(ax.cross(ab));
		// calc distance, handle corner case
		frac<int32> dist;
		if (abXxy == 0) { // colin
			if (ray == 0) {
				dist = static_cast<frac<int32>>(collinear_point_on_segment(ax, ab));
				if (dist.b < 0) { dist.a = -dist.a; dist.b = -dist.b; }
			} else
				return Intersect<int16>(0);
		} else { // not colin, default
			dist.a = ray; dist.b = den;
		}

		// check if intersecting edge correctly
		if (dist.a >= 0) {
			if ( point::isDisjunctiveColin(abXxy, seg) ) { // abXxy == 0 || seg == 0
				int32 xzXab = static_cast<int32>(xz.cross(ab));
				int32 xzXxy = static_cast<int32>(xz.cross(xy));
				if (   point::template isDir<CCW ? Dir::CCW : Dir::CW>(xzXxy) ? // inner or outer edge
					point::template isConjunctiveDir<CCW ? Dir::CCW : Dir::CW>(xzXab, abXxy) // inner corner
					: point::template isDisjunctiveDir<CCW ? Dir::CCW : Dir::CW>(xzXab, abXxy) ) // outer corner
					return Intersect<int16>(dist.b, dist.a, 0);
			} else {
				if (seg > 0 && seg < den && point::template isDir<CCW ? Dir::CCW : Dir::CW>(abXxy))
					return Intersect<int16>(dist.b, dist.a, seg);
			}
		}

		// no intersection
		return Intersect<int16>(0);
	}
}

template <bool CCW = true>
inline std::pair<double,double> intersect_dbl_corner_segment(Point<double> a, Point<double> ab, Point<double> x, Point<double> ccw, Point<double> cw)
{
	using point = Point<double>;
	point ax = x - a;
	double abXccw = ab.cross(ccw);
	constexpr Dir D = CCW ? Dir::CCW : Dir::CW;
	if (pr_op<PRop::eqZero>(abXccw)) { // colin
		if (pr_op<PRop::neZero>(ax.cross(ab)) || !ab.isBetween<Between::WHOLE, D>(cw, ccw)) // parrael or not intersecting this line or not blocked by corner
			return {-1, -1};
		auto ray = collinear_point_on_segment(ax, ab);
		if (pr_op<PRop::geZero>(ray))
			return {ray, 0};
		else
			return {-1, -1};
	} else {
		double ray = ax.cross(ccw) / abXccw;
		if (pr_op<PRop::geZero>(ray)) {
			double seg = ax.cross(ab) / abXccw;
			if (pr_op<PRop::geOne>(seg) || pr_op<PRop::ltZero>(seg)) // not intersecting segment
				return {-1, -1};
			if (pr_op<PRop::gtZero>(ray)) { // ray not on segment
				if (pr_op<PRop::gtZero>(seg)) {
					if (ab.isDir<D>(ccw))
						return {ray, seg};
					else
						return {-1, -1};
				}
				else { // ray intersects corner, check
					if (ab.isBetween<Between::WHOLE, D>(cw, ccw))
						return {ray, 0};
					else
						return {-1, -1};
				}
			} else { // on intersecting segment
				if (pr_op<PRop::gtZero>(seg)) {
					if (ab.isDir<D>(ccw))
						return {0, seg};
					else
						return {-1, -1};
				} else {
					if (ab.isBetween<Between::WHOLE, D>(cw, ccw))
						return {0, 0};
					else
						return {-1, -1};
				}
			}
		} else {
			return {-1, -1};
		}
	}
}

template <bool CCW>
inline Intersect<int16> segment_intersect_corner_segment(Point<int16> a, Point<int16> ab, Point<int16> x, Point<int16> xy, Point<int16> ccw, Point<int16> cw)
{
	using res = Intersect<int16>;
	constexpr Dir D = CCW ? Dir::CCW : Dir::CW;
	auto I = segment_intersect(a, ab, x, xy);
	if (static_cast<uint32>(I.scale) == 0) {
		if (ab.isColin(x-a)) {
			auto CI = collinear_point_on_segment(x-a, ab);
			I.scale = CI.b;
			I.a = CI.a;
			I.b = 0;
		} else {
			return res(0);
		}
	}
	if (static_cast<uint32>(I.a) == 0) {
		if (static_cast<uint32>(I.b) == 0) {
			if (!ab.isBetween<Between::WHOLE, D>(ccw, cw))
				return res(0);
		} else if (static_cast<uint32>(I.b) < static_cast<uint32>(I.scale)) {
			if (!xy.isDir<D>(ab))
				return res(0);
		} else {
			return res(0);
		}
	} else if (static_cast<uint32>(I.a) < static_cast<uint32>(I.scale)) {
		if (static_cast<uint32>(I.b) >= static_cast<uint32>(I.scale))
			return res(0);
	} else if (static_cast<uint32>(I.a) == static_cast<uint32>(I.scale)) {
		if (static_cast<uint32>(I.b) == 0) {
			if (!(-ab).isBetween<Between::WHOLE, D>(ccw, cw))
				return res(0);
		} else if (static_cast<uint32>(I.b) < static_cast<uint32>(I.scale)) {
			if (!xy.isDir<D>(-ab))
				return res(0);
		} else {
			return res(0);
		}
	} else {
		return res(0);
	}
	return I;
}

} // namespace warthog

#endif // RAYSCANLIB_GEOMETRY_UTIL_RAYSCANGEOMETRY_HPP_INCLUDED
