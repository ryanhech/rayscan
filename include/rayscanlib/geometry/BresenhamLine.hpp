#ifndef RAYSCANLIB_GEOMETRY_BRESENHAMLINE_HPP_INCLUDED
#define RAYSCANLIB_GEOMETRY_BRESENHAMLINE_HPP_INCLUDED

#include "fwd.hpp"
#include "Point.hpp"

namespace rayscanlib::geometry
{

namespace details
{

template <bool Sealed>
struct BresenhamLineVars;

template <>
struct BresenhamLineVars<false>
{
	uint8 m_axis;
	int8 m_axisMod;
	int8 m_invMod;
};

template <>
struct BresenhamLineVars<true>
{
	uint8 m_axis;
	int8 m_axisMod;
	int8 m_invMod;
	int8 m_sealMod;
};

} // namespace details

template <typename Crd, bool Sealed, bool StartBehind = false>
struct BresenhamLine : details::BresenhamLineVars<Sealed>
{
	using point_type = Point<Crd>;
	static_assert(point_type::is_integral(), "Must be integer");

	BresenhamLine(point_type a, point_type ab) noexcept : m_at(a), m_part(0)
	{
		assert(ab.x != 0 || ab.y != 0);
		Crd gcd = std::gcd(ab.x, ab.y);
		ab.x /= gcd; ab.y /= gcd;
		if (ab.x == 0 || ab.y == 0) {
			if (ab.y > 0) { m_at.x -= 1; if constexpr (Sealed) { this->m_sealMod = 1; } }
			else if (ab.y < 0) { m_at.y -= 1; if constexpr (Sealed) { this->m_sealMod = -1; } }
			else if (ab.x < 0) { m_at.x -= 1; m_at.y -= 1; if constexpr (Sealed) { this->m_sealMod = 1; } }
			else if constexpr (Sealed) { this->m_sealMod = -1; }
			this->m_axis = static_cast<uint8>(ab.x == 0);
			if (ab[this->m_axis] > 0) {
				this->m_axisMod = 1;
			} else {
				this->m_axisMod = -1;
			}
			this->m_invMod = 0;
			m_partScale = 1;
			m_partMod = 1;
		} else {
			if (ab.x < 0) m_at.x -= 1;
			if (ab.y < 0) m_at.y -= 1;
			this->m_axis = std::abs(ab.x) < std::abs(ab.y);
			if (ab[this->m_axis] > 0) {
				this->m_axisMod = 1;
				m_partScale = ab[this->m_axis];
			} else {
				this->m_axisMod = -1;
				m_partScale = -ab[this->m_axis];
			}
			if (ab[this->m_axis^1] > 0) {
				this->m_invMod = 1;
				m_partMod = ab[this->m_axis^1];
			} else {
				this->m_invMod = -1;
				m_partMod = -ab[this->m_axis^1];
			}
			if constexpr (Sealed) {
				this->m_sealMod = this->m_invMod;
			}
		}
		m_len.a = 0;
		m_len.b = m_partScale * gcd;
		if constexpr (StartBehind) {
			m_len.a = -1;
			m_part = m_partScale - m_partMod;
			m_at[this->m_axis] -= this->m_axisMod;
			m_at[this->m_axis^1] -= this->m_invMod;
		}
	}

	template <typename Fn>
	void iterate(Fn&& fn) noexcept(noexcept(fn(std::as_const(this->m_at), std::as_const(this->m_len))))
	{
		bool con = true;
		do {
			assert(m_part >= 0 && m_part < m_partScale);
			con &= fn(std::as_const(m_at), std::as_const(m_len));
			m_part += m_partMod;
			if (m_part == m_partScale) {
				if constexpr (Sealed) {
					m_at[this->m_axis^1] += static_cast<Crd>(this->m_sealMod);
					con &= fn(std::as_const(m_at), std::as_const(m_len));
					m_at[this->m_axis^1] -= static_cast<Crd>(this->m_sealMod);
				}
				m_part = 0;
				m_at[this->m_axis^1] += this->m_invMod;
			} else if (m_part > m_partScale) {
				m_part -= m_partScale;
				m_at[this->m_axis^1] += this->m_invMod;
				con &= fn(std::as_const(m_at), std::as_const(m_len));
			}
			m_at[this->m_axis] += this->m_axisMod;
			m_len.a += 1;
		} while (con);
	}

	point_type m_at;
	Crd m_part;
	Crd m_partScale;
	Crd m_partMod;
	inx::frac<Crd> m_len;
};



// how far behind long-axis should the line start
struct BresenhamDblLine
{
	using point = geometry::Point<double>;
	using crd = geometry::Point<int32>;
	constexpr static double axis_eps = 1e-8;
	constexpr static double axis_int_eps = 1e-6;

	double axisProg;
	double axisProgInc;
	uint32 axis; // 0 = x, y = 1; represents longer axis
	int32 axisMod;
	int32 axisIMod;
	inx::frac<int32> prog;
	crd pos;
	point start;
	point adj;

	/// @param a The starting point
	/// @param ab The line segment
	/// @param adjStartUnits Adjusts a by number of major-axis units, pos is forward of ab, neg is backwards of ab (i.e. starts behind a)
	void setup(point a, point ab, int adjStartUnits = 0) noexcept
	{
		assert(!ab.isZero());
		// setup axis and axis mod
		if (std::abs(ab.y) < std::abs(ab.x)) {
			axis = 0;
			axisMod = ab.x < 0 ? -1 : 1;
			std::swap(a.x, a.y);
			std::swap(ab.x, ab.y);
		} else {
			axis = 1;
			axisMod = ab.y < 0 ? -1 : 1;
		}
		assert(std::abs(ab.y) >= std::abs(ab.x) && std::abs(ab.y) > axis_eps);
		prog.a = static_cast<int32>(adjStartUnits);
		prog.b = static_cast<int32>(std::ceil(std::abs(ab.y)));
		ab.x /= std::abs(ab.y);
		ab.y = axisMod;
		if (ab.x < -axis_eps)
			axisIMod = -1;
		else if (ab.x > axis_eps)
			axisIMod = 1;
		else
			axisIMod = 0;
		a += static_cast<double>(adjStartUnits) * ab;
		// align to edge of axis
		double axisF, axisI;
		if (axisMod >= 0) {
			axisF = std::floor(a.y);
			assert(axisF <= a.y);
			if (a.y - axisF > 1-axis_int_eps) {
				axisF += 1;
				axisI = a.x;
			} else {
				axisI = a.x - ab.x * (a.y - axisF);
			}
			pos[axis] = static_cast<int32>(axisF);
		} else {
			axisF = std::ceil(a.y);
			assert(axisF >= a.y);
			if (axisF - a.y > 1-axis_int_eps) {
				axisF -= 1;
				axisI = a.x;
			} else {
				axisI = a.x - ab.x * (axisF - a.y);
			}
			pos[axis] = static_cast<int32>(axisF)-1;
		}
		start[axis] = axisF;
		start[axis^1] = axisI;
		adj[axis^1] = ab.x;
		adj[axis] = ab.y;
		double modp, modi;
		modp = std::modf(axisI, &modi);
		assert(axisI >= 0);
		if (axisIMod > 0) {
			if (modp < axis_eps) {
				modi -= axisIMod;
				axisProg = 1+modp;
			} else if (modp > 1-axis_eps) {
			//	modi -= axisIMod;
				axisProg = modp;
			} else {
				axisProg = modp;
			}
			axisProgInc = ab.x;
		} else if (axisIMod < 0) {
			if (modp < axis_eps) {
			//	modi -= axisIMod;
				axisProg = 1 - modp;
			} else if (modp > 1-axis_eps) {
				modi -= axisIMod;
				axisProg = modp;
			} else {
				axisProg = 1 - modp;
			}
			axisProgInc = -ab.x;
		} else { // hori/vert line
			if (modp > 1-axis_eps) {
				modi += 1;
			}
			axisProg = 0.5;
			axisProgInc = 0;
		}
		pos[axis^1] = static_cast<int32>(std::floor(modi));
	}

	// increment one axis-sep
	// if returns true, than pos[axis^1] += axisIMod
	// afterwards, pos[axis] += axisMod
	// prog and pos remain unchanged, up to user to track changes
	bool increment() noexcept
	{
		axisProg += axisProgInc;
		if (axisProg >= 1-axis_eps) {
			axisProg -= 1;
			return true;
		}
		return false;
	}

	bool intersection() const noexcept
	{
		return axisProg < axis_eps;
	}

	void make_ray() noexcept
	{
		prog.b = std::numeric_limits<int32>::max();
	}
};

} // namespace rayscanlib::geometry

#endif // RAYSCANLIB_GEOMETRY_BRESENHAMLINE_HPP_INCLUDED
