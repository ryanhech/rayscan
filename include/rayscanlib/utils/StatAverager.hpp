#ifndef RAYSCANLIB_UTILS_STATAVERAGER_HPP_INCLUDED
#define RAYSCANLIB_UTILS_STATAVERAGER_HPP_INCLUDED

#include "fwd.hpp"
#include <vector>
#include "Stats.hpp"

namespace rayscanlib::utils
{

struct StatAverager
{
	using value_type = StatValue::value_type;

	StatAverager()
	{
		values.reserve(16);
	}

	void operator()(StatValue& stat, const value_type& val, StatValue::Operator op)
	{
		switch (op) {
		case StatValue::OpAssign:
			values.emplace_back(val);
			break;
		case StatValue::OpPlus:
			if (values.empty() == 0) throw std::runtime_error("plus requires a stat");
			std::visit([&val](auto& value) { value += std::get<std::decay_t<decltype(value)>>(val); }, values.back());
			break;
		case StatValue::OpMinus:
			if (values.empty() == 0) throw std::runtime_error("minus requires a stat");
			std::visit([&val](auto& value) { value -= std::get<std::decay_t<decltype(value)>>(val); }, values.back());
			break;
		case StatValue::OpReset:
			values.clear();
			break;
		default:
			break;
		}
		avg(stat);
	}

	void avg(StatValue& stat)
	{
		if (values.empty()) {
			stat.assigned = false;
			stat.value = std::visit([](const auto& value) {
				return value_type(std::decay_t<decltype(value)>{});
			}, stat.value);
		} else {
			stat.assigned = true;
			std::sort(values.begin(), values.end(), [](const value_type& a, const value_type& b) {
				return std::visit([&b](const auto& v) { return v < std::get<std::decay_t<decltype(v)>>(b); }, a);
			});
			size_t ign_slow = getSlow(), ign_fast = getFast();
			value_type avg = std::visit([&values=values,ign_slow,ign_fast](auto init) {
				for (size_t i = ign_slow+1, ie = values.size()-ign_fast; i < ie; ++i) {
					init += std::get<std::decay_t<decltype(init)>>(values[i]);
				}
				init /= values.size();
				return value_type(init);
			}, values[ign_slow]);
			stat.value = avg;
		}
	}

	size_t getSlow() { return values.size() < 5 ? 0 : 1; }
	size_t getFast() { return values.size() < 5 ? 0 : 1; }

	std::vector<value_type> values;
};

} // namespace rayscanlib::utils

#endif // RAYSCANLIB_UTILS_STATAVERAGER_HPP_INCLUDED
