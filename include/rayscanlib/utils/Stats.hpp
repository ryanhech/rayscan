#ifndef RAYSCANLIB_UTILS_STATS_HPP_INCLUDED
#define RAYSCANLIB_UTILS_STATS_HPP_INCLUDED

#include "fwd.hpp"
#include <variant>
#include <chrono>
#include <map>
#include <memory_resource>
#include <string>
#include <string_view>

namespace rayscanlib::utils
{

namespace details {

using stat_variant = std::variant<int64, double, std::chrono::nanoseconds>;

template <typename T, typename Enable = void>
struct is_valid_stat_type : std::false_type
{ };
template <>
struct is_valid_stat_type<stat_variant, void> : std::true_type
{ };
template <typename T>
struct is_valid_stat_type<T, std::enable_if_t<std::is_integral_v<T>>> : std::true_type
{ };
template <typename T>
struct is_valid_stat_type<T, std::enable_if_t<std::is_floating_point_v<T>>> : std::true_type
{ };
template <typename... Args>
struct is_valid_stat_type<std::chrono::duration<Args...>, void> : std::true_type
{ };

template <typename T, typename Enable = std::enable_if_t< is_valid_stat_type<std::decay_t<T>>::value >>
auto stat_type_cast(T&& val)
{
	using type = std::decay_t<T>;
	if constexpr (std::is_same_v<type, stat_variant>)
		return val;
	else if constexpr (std::is_integral_v<type>)
		return static_cast<int64>(val);
	else if constexpr (std::is_floating_point_v<type>)
		return static_cast<double>(val);
	else
		return std::chrono::duration_cast<std::chrono::nanoseconds>(val);
}

}

struct StatValue
{
	enum Operator {
		OpAssign,
		OpPlus,
		OpMinus,
		OpClear,
		OpReset
	};
	using value_type = details::stat_variant;
	using operator_type = std::function<void (StatValue& stat, const value_type& val, Operator op)>;
	using op_type = void (StatValue& stat, const value_type& val, Operator op);
	std::string_view name;
	bool assigned;
	value_type value;
	std::function<op_type> operators;
	
	StatValue() : assigned(false)
	{ }

	void assign(const StatValue::value_type& val)
	{
		operators(*this, val, StatValue::OpAssign);
	}
	void plus(const StatValue::value_type& val)
	{
		operators(*this, val, StatValue::OpPlus);
	}
	void minus(const StatValue::value_type& val)
	{
		operators(*this, val, StatValue::OpMinus);
	}
	void clear()
	{
		operators(*this, {}, StatValue::OpClear);
	}
	void reset()
	{
		operators(*this, {}, StatValue::OpReset);
	}

	template <typename T>
	StatValue& operator=(T&& val)
	{
		assign(details::stat_type_cast(val));
		return *this;
	}
};

template <typename T>
inline StatValue& operator+=(StatValue& stat, T&& val)
{
	stat.plus(details::stat_type_cast(val));
	return stat;
}

template <typename T>
inline StatValue& operator-=(StatValue& stat, T&& val)
{
	stat.minus(details::stat_type_cast(val));
	return stat;
}

void statop_generic(StatValue& stat, const StatValue::value_type& val, StatValue::Operator op);
void statop_ign_clear(StatValue& stat, const StatValue::value_type& val, StatValue::Operator op);
void statop_ign_clear_ign_reset(StatValue& stat, const StatValue::value_type& val, StatValue::Operator op);

struct statop_single_op
{
	StatValue::op_type* statop;
	statop_single_op (StatValue::op_type* fn) noexcept : statop(fn)
	{ }

	void operator()(StatValue& stat, const StatValue::value_type& val, StatValue::Operator op);
};

class Stats
{
public:
	using key_type = std::string;
	using mapped_type = StatValue;
	using map_type = std::pmr::map<key_type, mapped_type>;
	using value_type = map_type::value_type;
	using iterator = map_type::iterator;
	using const_iterator = map_type::const_iterator;

	Stats() = default;
	Stats(std::pmr::memory_resource* mr) : m_stats(mr)
	{ }
	Stats(const Stats&) = default;
	Stats(Stats&&) = default;

	Stats& operator=(const Stats& rhs);
	Stats& operator=(Stats&&) = delete;

	Stats& operator+=(const Stats& rhs);
	Stats& operator-=(const Stats& rhs);

	void clearOp();
	void resetOp();

	template <typename T, typename Op = decltype(&statop_generic)>
	std::pair<iterator, bool> try_emplace(std::string_view name, Op&& op = &statop_generic, T value = T{})
	{
		auto ins = m_stats.try_emplace(std::string(name.begin(), name.end()));
		if (ins.second) {
			ins.first->second.name = ins.first->first;
			ins.first->second.value = value;
			ins.first->second.operators = std::forward<Op>(op);
		}
		return ins;
	}

	const mapped_type& at(const std::string& value) const
	{
		return m_stats.at(value);
	}
	mapped_type& at(const std::string& value)
	{
		return m_stats.at(value);
	}

protected:
	template <typename Zipper>
	void match(const Stats& second, Zipper&& zipper = Zipper());

protected:
	map_type m_stats;
};

template <typename Zipper>
void Stats::match(const Stats& second, Zipper&& zipper)
{
	auto it1 = m_stats.begin();
	auto it2 = second.m_stats.begin();
	auto ite1 = m_stats.end();
	auto ite2 = second.m_stats.end();
	while (it1 != ite1 && it2 != ite2) {
		int cmp = it1->first.compare(it2->first);
		if (cmp == 0)
			zipper(it1->second, it2->second);
		if (cmp <= 0)
			++it1;
		if (cmp >= 0)
			++it2;
	}
}

} // namespace rayscanlib::utils

#endif // RAYSCANLIB_UTILS_STATS_HPP_INCLUDED
