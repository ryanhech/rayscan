#ifndef RAYSCANLIB_UTILS_STATSTABLE_HPP_INCLUDED
#define RAYSCANLIB_UTILS_STATSTABLE_HPP_INCLUDED

#include "fwd.hpp"
#include "Stats.hpp"
#include <fstream>
#include <unordered_map>
#include <vector>
#include <filesystem>

namespace rayscanlib::utils
{

class StatsTable
{
public:
	StatsTable() = default;
	~StatsTable();

	void emplace_column(std::string&& col, int64 order);
	void emplace_column(std::string_view col, int64 order);
	
	bool is_open() { return getStream() != nullptr; }
	void open(std::string_view filename);
	void open(const std::filesystem::path& filename);
	void open(std::ostream& stream);

	void close() noexcept;
	void flush();

	void push(const Stats& stats);

protected:
	void initColumns();
	std::ostream* getStream();

protected:
	std::unordered_map< std::string, std::pair<int64, size_t> > m_order;
	std::vector<decltype(m_order)::const_iterator> m_columns;
	std::variant<std::ostream*, std::ofstream> m_stream;
};

} // namespace rayscanlib::utils

#endif // RAYSCANLIB_UTILS_STATSTABLE_HPP_INCLUDED
