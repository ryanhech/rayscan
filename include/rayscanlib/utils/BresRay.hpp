#ifndef RAYSCANLIB_UTILS_BRESRAY_HPP_INCLUDED
#define RAYSCANLIB_UTILS_BRESRAY_HPP_INCLUDED

#include "fwd.hpp"
#include <rayscanlib/geometry/Polygon.hpp>
#include <rayscanlib/geometry/Box.hpp>
#include <rayscanlib/geometry/BresenhamLine.hpp>
#include <inx/alg/bit_table.hpp>
#include <inx/data/factory.hpp>
#include <inx/data/vector_table.hpp>
#include <boost/container/small_vector.hpp>
#include <boost/range/iterator_range.hpp>

#if 0
#define BRES_MAP_OUTPUT_FILE "map.bres"
#define BRES_RAY_OUTPUT_FILE "ray.bres"
#define BRES_RAY_ID 159429
#endif

#if defined(BRES_MAP_OUTPUT_FILE) || defined(BRES_RAY_OUTPUT_FILE)
#include <fstream>
#endif

//#define BRES_DISABLE_VECTOR_TABLE

#ifdef BRES_DISABLE_VECTOR_TABLE
#include <scoped_allocator>
#endif

namespace rayscanlib::utils
{

class BresRay
{
public:
	using coord = int16;
	using polygon = geometry::Polygon<coord>;
	using point = polygon::point_type;
	using box = geometry::Box<int16>;
	static constexpr bool SEALED_RAY = true;
	static constexpr size_t BRES_PADDING = 4;
	static constexpr size_t BRES_WALL = 2;

	BresRay() : m_dataPool(2048)
	{ }

	struct Data
	{
		point at, nx, pv;
		int32 id;
		uint64 lastCheck;
		void* data;
		Data() = default;
		Data(const Data&) = default;
		Data(Data&&) = default;
		Data& operator=(const Data&) = default;
		Data& operator=(Data&&) = default;
	};
	using data_set = boost::container::small_vector<Data*, 4>;
	
	void setBox(const box& b);

	template <typename Obstacle>
	void addObstacle(Obstacle& obst)
	{
		int32 id = 0;
		for (auto it = obst.shape.begin(), ite = obst.shape.end(); it != ite; ++it, ++id) {
			auto [pv, at, nx] = it.edge();
			pv -= at; nx -= at; at -= m_lowerBound;
			Data* dat = m_dataPool.construct();
			obst.vertices[id].bresDat = dat;
			dat->at = at;
			dat->nx = nx;
			dat->pv = pv;
			dat->id = id;
			dat->data = &obst;
			dat->lastCheck = 0;
			addObstacleData(*dat);
		}
	}
	void addObstacleData(Data& dat);

	template <typename Obstacle>
	void removeObstacle(Obstacle& obst)
	{
		for (const auto& v : boost::make_iterator_range(obst.vertices.get(), obst.vertices.get() + obst.size())) {
			Data* dat = static_cast<Data*>(v.bresDat);
			removeObstacleData(*dat);
			m_dataPool.destruct(dat);
		}
	}
	void removeObstacleData(Data& dat);
	
	std::pair<Data*, geometry::Intersect<coord>> shootRay(point a, point ab);

private:
	data_set* try_get(coord x, coord y)
	{
		return !m_rasterEnv.bit_test(x, y) ? nullptr : &m_rasterData[y][x];
	}
	bool test(coord x, coord y) const
	{
		return m_rasterEnv.bit_test(x, y);
	}
	data_set& get_fill(coord x, coord y)
	{
		m_rasterEnv.bit_or(x, y, 1);
		return m_rasterData[y][x];
	}
	data_set& get(coord x, coord y)
	{
		return m_rasterData[y][x];
	}

private:
	point m_lowerBound;
	uint64 m_rayId;
	inx::alg::bit_table<1, 0> m_rasterEnv;
	inx::data::ReuseFactory<Data> m_dataPool;
	std::vector<std::vector<data_set>> m_rasterData;
};

class BresDblRay
{
public:
	using coord = double;
	using polygon = geometry::Polygon<coord>;
	using point = polygon::point_type;
	using box = geometry::Box<coord>;
	static constexpr size_t Padding = 8;

	BresDblRay() : m_iscale(1), m_dataPool(2048)
#ifdef BRES_DISABLE_VECTOR_TABLE
	,m_gridData(std::scoped_allocator_adaptor<vgrid::allocator_type, vchild::allocator_type>(vgrid::allocator_type(), vchild::allocator_type(&m_gridPool)))
#endif
	{ }

	struct Data
	{
		point at, nx, pv;
		int32 id;
		uint64 lastCheck;
		void* data;
		Data() = default;
		Data(const Data&) = default;
		Data(Data&&) = default;
		Data& operator=(const Data&) = default;
		Data& operator=(Data&&) = default;
	};
	
	void setBox(const box& b);

	template <typename Obstacle>
	void addObstacle(Obstacle& obst)
	{
		int32 id = 0;
		for (auto it = obst.shape.begin(), ite = obst.shape.end(); it != ite; ++it, ++id) {
			point pv, at, nx;
			std::tie(pv, at, nx) = inx::to_tuple(it.edge());
			pv = m_iscale * pv; at = m_iscale * at; nx = m_iscale * nx;
			pv -= at; nx -= at; at -= m_lowerBound;
			Data* dat = m_dataPool.construct();
			obst.vertices[id].bresDat = dat;
			dat->at = at;
			dat->nx = nx;
			dat->pv = pv;
			dat->id = id;
			dat->data = &obst;
			dat->lastCheck = 0;
			addObstacleData(*dat);
		}
	}
	void addObstacleData(Data& dat);

	template <typename Obstacle>
	void removeObstacle(Obstacle& obst)
	{
		for (const auto& v : boost::make_iterator_range(obst.vertices.get(), obst.vertices.get() + obst.size())) {
			Data* dat = static_cast<Data*>(v.bresDat);
			removeObstacleData(*dat);
			m_dataPool.destruct(dat);
		}
	}
	void removeObstacleData(Data& dat);

	void setScale(double scale) noexcept
	{
		assert(scale > 1e-4);
		m_iscale = 1.0 / scale;
	}

	std::pair<Data*, std::pair<double, double>> shootRay(point a, point ab);

	auto getRayId() const noexcept { return m_rayId; }

private:
	double m_iscale;
	point m_lowerBound;
	uint64 m_rayId;
	std::pmr::unsynchronized_pool_resource m_gridPool;
	inx::data::ReuseFactory<Data> m_dataPool;
	inx::alg::bit_table<1, 0> m_grid;
#ifndef BRES_DISABLE_VECTOR_TABLE
	inx::data::vector_table<Data*, 4, std::pmr::polymorphic_allocator<Data*>> m_gridData;
#else
	using vchild = std::pmr::vector<Data*>;
	using vgrid = std::pmr::vector<vchild>;
	vgrid m_gridData;
#endif
};

namespace details {

#if defined(BRES_MAP_OUTPUT_FILE) || defined(BRES_RAY_OUTPUT_FILE)
template <typename Tab>
void BresOutput(const Tab& tab, std::string filename)
{
	std::ofstream file(filename);
	size_t width = tab.getWidth();
	size_t height = tab.getHeight();
	std::string line(width+1, '.');
	line.back() = '\n';
	for (size_t y = height; y > 0; --y) {
		for (size_t x = 0; x < width; ++x) {
			line[x] = tab.template bit_test<0>(x, y-1) ? '#' : ',';
		}
		file << line;
	}
}
#endif

template <bool Fill, typename BitTable, typename Table, typename Func>
void bres_ray_loop(geometry::BresenhamDblLine& line, BitTable& bt, Table& tab, Func&& fn
#ifdef BRES_RAY_OUTPUT_FILE
, bool output = false
#endif
)
{
	// setup line detail
#ifdef BRES_RAY_OUTPUT_FILE
	BitTable outTab;
	if (output) {
		outTab.setup(bt.getWidth(), bt.getHeight());
	}
#endif
	auto bitId = bt.bit_index(line.pos.x, line.pos.y);
#ifndef BRES_DISABLE_VECTOR_TABLE
	auto tabId = tab.index(line.pos.x, line.pos.y);
#else
	size_t tabId = static_cast<size_t>(line.pos.y) * bt.getWidth() + line.pos.x;
#endif
	// setup adjustment detail
	int32 bitRowAdj = static_cast<int32>(bt.getRowWords());
	int32 bitColAdj;
	int32 bitColReset;
	int32 tabRowAdj = static_cast<int32>(bt.getWidth());
	bool xpos = line.axisMod >= 0, ypos = line.axisIMod >= 0;
	if (line.axis != 0)
		std::swap(xpos, ypos);
	if (xpos) {
		bitColAdj = 1;
		bitColReset = 0;
	} else {
		bitColAdj = -1;
		bitColReset = BitTable::bit_id_end - BitTable::bit_id_step;
	}
	if (!ypos) {
		bitRowAdj = -bitRowAdj;
		tabRowAdj = -tabRowAdj;
	}
	// loop through
	auto&& action = [&]() {
		assert(tabId < tab.size());
		assert(bitId.word / std::abs(bitRowAdj) == tabId / std::abs(tabRowAdj));
		assert(bitId.word % std::abs(bitRowAdj) * BitTable::bit_id_end + bitId.bit == tabId % std::abs(tabRowAdj));
#ifdef BRES_RAY_OUTPUT_FILE
		if (output) {
			outTab.bit_or(bitId, 1);
			BresOutput(outTab, BRES_RAY_OUTPUT_FILE);
		}
#endif
		if constexpr (Fill) {
			bt.bit_or(bitId, 1);
			fn(tab[tabId]);
		} else {
			if (bt.template bit_test<0>(bitId))
				fn(tab[tabId]);
		}
	};
	if (line.axis == 0) {
		// x-axis is longer
		do {
			action();
			if (line.increment()) { // y-axis inc
				if (line.intersection()) { // on corner, fill whole square
					auto tmpBitId = bitId;
					auto tmpTabId = tabId;
					bitId.bit = static_cast<uint32>(static_cast<int32>(bitId.bit) + bitColAdj);
					if (bitId.bit >= BitTable::bit_id_end) {
						bitId.bit = bitColReset;
						bitId.word = static_cast<uint32>(static_cast<int32>(bitId.word) + bitColAdj);
					}
					tabId = static_cast<uint64>(static_cast<int64>(tabId) + bitColAdj);
					action();
					bitId = tmpBitId;
					tabId = tmpTabId;
				}
				bitId.word = static_cast<uint32>(static_cast<int32>(bitId.word) + bitRowAdj);
				tabId = static_cast<uint64>(static_cast<int64>(tabId) + tabRowAdj);
				action();
			}
			bitId.bit = static_cast<uint32>(static_cast<int32>(bitId.bit) + bitColAdj);
			if (bitId.bit >= BitTable::bit_id_end) {
				bitId.bit = bitColReset;
				bitId.word = static_cast<uint32>(static_cast<int32>(bitId.word) + bitColAdj);
			}
			tabId = static_cast<uint64>(static_cast<int64>(tabId) + bitColAdj);
		}
		while (++line.prog.a <= line.prog.b);
	} else {
		// y-axis is longer
		do {
			action();
			if (line.increment()) { // y-axis inc
				if (line.intersection()) { // on corner, fill whole square
					auto tmpBitId = bitId;
					auto tmpTabId = tabId;
					bitId.word = static_cast<uint32>(static_cast<int32>(bitId.word) + bitRowAdj);
					tabId = static_cast<uint64>(static_cast<int64>(tabId) + tabRowAdj);
					action();
					bitId = tmpBitId;
					tabId = tmpTabId;
				}
				bitId.bit = static_cast<uint32>(static_cast<int32>(bitId.bit) + bitColAdj);
				if (bitId.bit >= BitTable::bit_id_end) {
					bitId.bit = bitColReset;
					bitId.word = static_cast<uint32>(static_cast<int32>(bitId.word) + bitColAdj);
				}
				tabId = static_cast<uint64>(static_cast<int64>(tabId) + bitColAdj);
				action();
			}
			bitId.word = static_cast<uint32>(static_cast<int32>(bitId.word) + bitRowAdj);
			tabId = static_cast<uint64>(static_cast<int64>(tabId) + tabRowAdj);
		}
		while (++line.prog.a <= line.prog.b);
	}

}

} // namespace details

} // namespace rayscanlib::utils

#endif // RAYSCANLIB_UTILS_BRESRAY_HPP_INCLUDED
