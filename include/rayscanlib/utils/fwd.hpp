#ifndef RAYSCANLIB_UTILS_FWD_HPP_INCLUDED
#define RAYSCANLIB_UTILS_FWD_HPP_INCLUDED

#include <rayscanlib/inx.hpp>

namespace rayscanlib::utils
{

template <typename T>
class Factory;
class Stats;
class StatAverager;

}

#endif // RAYSCANLIB_UTILS_FWD_HPP_INCLUDED
