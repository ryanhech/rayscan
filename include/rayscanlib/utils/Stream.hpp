#ifndef RAYSCANLIB_UTILS_STREAM_HPP_INCLUDED
#define RAYSCANLIB_UTILS_STREAM_HPP_INCLUDED

#include "fwd.hpp"
#include <istream>
#include <ostream>
#include <sstream>

namespace rayscanlib::utils
{

class stream_error : public std::runtime_error
{
public:
	using std::runtime_error::runtime_error;
};
class bad_read_error : public stream_error
{
public:
	bad_read_error() : stream_error("bad read")
	{ }
};
class invalid_syntax_error : public stream_error
{
public:
	using stream_error::stream_error;
};

struct Stream
{
	static bool endOfStream(std::istream& is)
	{
		is >> std::ws;
		if (is.bad())
			throw bad_read_error();
		return is.eof();
	}
	static bool getline(std::istream& is, std::istringstream& iss, std::string& line)
	{
		if (!std::getline(is, line)) {
			if (is.eof())
				return false;
			throw bad_read_error();
		}
		iss.str(line);
		iss.clear();
		if (endOfStream(iss))
			throw invalid_syntax_error("empty line");
		return true;
	}
};

} // namespace rayscanlib::utils

#endif // RAYSCANLIB_UTILS_STREAM_HPP_INCLUDED
