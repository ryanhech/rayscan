#ifndef RAYSCANLIB_SEARCH_RAYCAST_BRESENHAMDBLRAYCAST_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_RAYCAST_BRESENHAMDBLRAYCAST_HPP_INCLUDED

#include "../RayScanBase.hpp"
#include <rayscanlib/utils/BresRay.hpp>
#include <rayscanlib/geometry/util/RayScanGeometry.hpp>
#include <rsapp/conf/Config.hpp>
#include <boost/lexical_cast.hpp>
#include <chrono>

namespace rayscanlib::search
{

namespace modules {
template <bool Oracle = false>
struct BresenhamDblRayCastModule { };
} // namespace modules

namespace raycast {

template <bool Oracle, typename... Modules>
class BresenhamDblRayCast : public search_next_module<modules::BresenhamDblRayCastModule<Oracle>, Modules...>::type
{
public:
	using self = BresenhamDblRayCast<Oracle, Modules...>;
	using super = typename search_next_module<modules::BresenhamDblRayCastModule<Oracle>, Modules...>::type;
	RAYSCANLIB_BASE_CHILD

	BresenhamDblRayCast()
	{ }

	void envSetBox_(const box& b) override
	{
		super::envSetBox_(b);
		if (auto conf = ::rsapp::conf::Config::instance(); conf != nullptr) {
			auto scale = conf->param("descale");
			if (scale) {
				m_bresRay.setScale(boost::lexical_cast<double>(*scale));
			}
		}
		m_bresRay.setBox(b);
	}

	obstacle& envAddObstacle_(const void* uid, polygon&& poly) override
	{
		obstacle& obst = super::envAddObstacle_(uid, std::move(poly));
		m_bresRay.addObstacle(obst);
		return obst;
	}

	void envRemoveObstacle_(const void* uid, obstacle& obst) override
	{
		m_bresRay.removeObstacle(obst);
		super::envRemoveObstacle_(uid, obst);
	}
	
protected:
	typename super::cast_ray_result castRayShoot_(point a, point ab) override final
	{
		inx::optional_type_t<Oracle, std::chrono::steady_clock::time_point> start;
		if constexpr (Oracle) {
			start = std::chrono::steady_clock::now();
		}
		
		auto ans = m_bresRay.shootRay(a, ab);

		if constexpr (Oracle) {
			this->getStats_().rayCastTime += std::chrono::steady_clock::now() - start;
		}

		assert(ans.first != nullptr);
		if constexpr (super::is_floating_point()) {
			return {static_cast<obstacle*>(ans.first->data), ans.first->id, ans.second.first, ans.second.second};
		} else {
			if (ans.first->data != nullptr) {
				auto [pv, at, nx] = static_cast<obstacle*>(ans.first->data)->shape.edge(ans.first->id);
				auto I = geometry::util::intersect_corner_segment<false>(a, ab, at, nx - at, pv - at);
				assert(!I.colin());
				return {static_cast<obstacle*>(ans.first->data), ans.first->id, I.scaleA(), I.scaleB()};
			} else {
				return {nullptr, ans.first->id, inx::frac<int32>(1,0), inx::frac<int32>(1,0)};
			}
		}
	}

private:
	utils::BresDblRay m_bresRay;
};

} // namespace expansion

namespace details
{

template <bool Oracle, typename... Modules>
struct search_data<RayScanVertex, modules::BresenhamDblRayCastModule<Oracle>, Modules...>
	: search_next_data<RayScanVertex, modules::BresenhamDblRayCastModule<Oracle>, Modules...>::type
{
	using self = search_data<RayScanVertex, modules::BresenhamDblRayCastModule<Oracle>, Modules...>;
	using super = typename search_next_data<RayScanVertex, modules::BresenhamDblRayCastModule<Oracle>, Modules...>::type;
	RAYSCANLIB_STRUCT_CHILD

	search_data() : bresDat(nullptr)
	{ }

	void* bresDat;
};

template <bool Oracle, typename... Modules>
struct search_module<modules::BresenhamDblRayCastModule<Oracle>, Modules...>
{
	using type = raycast::BresenhamDblRayCast<Oracle, Modules...>;
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_RAYCAST_BRESENHAMDBLRAYCAST_HPP_INCLUDED
