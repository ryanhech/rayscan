#ifndef RAYSCANLIB_SEARCH_RAYCAST_BRESENHAMRAYCAST_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_RAYCAST_BRESENHAMRAYCAST_HPP_INCLUDED

#include "../RayScanBase.hpp"
#include <rayscanlib/utils/BresRay.hpp>
#include <chrono>

namespace rayscanlib::search
{

namespace modules {
template <bool SealedRay = true, bool Oracle = false>
struct BresenhamRayCastModule : DynamicTag { };
} // namespace modules

namespace raycast {

template <bool SealedRay, bool Oracle, typename... Modules>
class BresenhamRayCast : public search_next_module<modules::BresenhamRayCastModule<SealedRay, Oracle>, Modules...>::type
{
public:
	using self = BresenhamRayCast<SealedRay, Oracle, Modules...>;
	using super = typename search_next_module<modules::BresenhamRayCastModule<SealedRay, Oracle>, Modules...>::type;
	RAYSCANLIB_BASE_CHILD

	BresenhamRayCast()
	{ }

	void envSetBox_(const box& b) override
	{
		super::envSetBox_(b);
		m_bresRay.setBox(b);
	}

	obstacle& envAddObstacle_(const void* uid, polygon&& poly) override
	{
		obstacle& obst = super::envAddObstacle_(uid, std::move(poly));
		m_bresRay.addObstacle(obst);
		return obst;
	}

	void envRemoveObstacle_(const void* uid, obstacle& obst) override
	{
		m_bresRay.removeObstacle(obst);
		super::envRemoveObstacle_(uid, obst);
	}
	
protected:
	typename super::cast_ray_result castRayShoot_(point a, point ab) override final
	{
		inx::optional_type_t<Oracle, std::chrono::steady_clock::time_point> start;
		if constexpr (Oracle) {
			start = std::chrono::steady_clock::now();
		}
		
		auto ans = m_bresRay.shootRay(a, ab);

		if constexpr (Oracle) {
			this->getStats_().rayCastTime += std::chrono::steady_clock::now() - start;
		}

		assert(ans.first != nullptr);
		return {static_cast<obstacle*>(ans.first->data), ans.first->id, ans.second.scaleA(), ans.second.scaleB()};
	}

private:
	utils::BresRay m_bresRay;
};

} // namespace expansion

namespace details
{

template <bool SealedRay, bool Oracle, typename... Modules>
struct search_data<RayScanVertex, modules::BresenhamRayCastModule<SealedRay, Oracle>, Modules...>
	: search_next_data<RayScanVertex, modules::BresenhamRayCastModule<SealedRay, Oracle>, Modules...>::type
{
	using self = search_data<RayScanVertex, modules::BresenhamRayCastModule<SealedRay, Oracle>, Modules...>;
	using super = typename search_next_data<RayScanVertex, modules::BresenhamRayCastModule<SealedRay, Oracle>, Modules...>::type;
	RAYSCANLIB_STRUCT_CHILD

	search_data() : bresDat(nullptr)
	{ }

	void* bresDat;
};

template <bool SealedRay, bool Oracle, typename... Modules>
struct search_module<modules::BresenhamRayCastModule<SealedRay, Oracle>, Modules...>
{
	using type = raycast::BresenhamRayCast<SealedRay, Oracle, Modules...>;
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_RAYCAST_BRESENHAMRAYCAST_HPP_INCLUDED
