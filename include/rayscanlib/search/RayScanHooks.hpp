#ifndef RAYSCANLIB_SEARCH_RAYSCANHOOKS_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_RAYSCANHOOKS_HPP_INCLUDED

#include "RayScanBase.hpp"
#include <rayscanlib/geometry/AngledSector.hpp>

namespace rayscanlib::search
{

namespace hooks {
struct BeginShortestPathSearchHook { };
struct EndShortestPathSearchHook { };
struct BeginMultiTargetSearchHook { };
struct EndMultiTargetSearchHook { };
struct BeginNodeExpansionHook { };
struct EndNodeExpansionHook { };
struct PushSuccessorHook { };
struct UpdateNodeHook { };
} // namespace hooks

namespace details {

SEARCH_HOOK_DEFINE(BeginShortestPathSearchHook,void(typename hook::point start, typename hook::point target))
SEARCH_HOOK_DEFINE(EndShortestPathSearchHook,void(double distance))
SEARCH_HOOK_DEFINE(BeginMultiTargetSearchHook,void(typename hook::point start, typename hook::point* targets, size_t targetsCount))
SEARCH_HOOK_DEFINE(EndMultiTargetSearchHook,void(double distance))
SEARCH_HOOK_DEFINE(BeginNodeExpansionHook,void(const typename hook::vertex& node, geometry::AngledSector<typename hook::coord, Between::WHOLE, Dir::CW> projectionField))
SEARCH_HOOK_DEFINE(EndNodeExpansionHook,void(const typename hook::vertex& node))
SEARCH_HOOK_DEFINE(PushSuccessorHook,void(const typename hook::vertex& from, const typename hook::vertex& to, bool added, bool pushed))
SEARCH_HOOK_DEFINE(UpdateNodeHook,void(const typename hook::vertex& node))

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_RAYSCANHOOKS_HPP_INCLUDED
