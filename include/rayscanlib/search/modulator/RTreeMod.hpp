#ifndef RAYSCANLIB_SEARCH_MODULATOR_RTREEMOD_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_MODULATOR_RTREEMOD_HPP_INCLUDED

#include "../RayScanBase.hpp"
#include <rayscanlib/geometry/util/HilbertRtree.hpp>
#include <rayscanlib/search/modulator/TargetReassignMod.hpp>
#include <algorithm>
#include <chrono>

namespace rayscanlib::search
{

namespace modules {
struct RTreeModules { };
template <size_t Fanout, size_t LeafSize>
struct RTreeModule : RTreeModules { };
} // namespace modules

namespace modulator {

template <size_t Fanout, size_t LeafSize, typename... Modules>
class RTreeMod : public search_next_module<modules::RTreeModule<Fanout, LeafSize>, Modules...>::type
{
public:
	using self = RTreeMod<Fanout, LeafSize, Modules...>;
	using super = typename search_next_module<modules::RTreeModule<Fanout, LeafSize>, Modules...>::type;
	RAYSCANLIB_BASE_CHILD
	static_assert(has_module<modules::MultiTargetTag>() && !has_module<modules::SingleTargetTag>(), "Must be used on multi target");

	struct RTreeTarget : ::rayscanlib::geometry::util::HilbertRtreeBox<coord>
	{
		point id;
		vertex* target;
	};
	using rtree = ::rayscanlib::geometry::util::HilbertRtree<RTreeTarget, Fanout, LeafSize>;

	struct RTreeMinHeap
	{
		using value_type = std::pair<double, const typename rtree::node_type*>;
		static bool heap_op(const value_type& a, const value_type& b) noexcept { return a.first > b.first; }

		void push(double value, const typename rtree::node_type* entry)
		{
			m_heap.emplace_back(value, entry);
			std::push_heap(m_heap.begin(), m_heap.end(), &heap_op);
		}
		const auto& top() const noexcept { return m_heap.front(); }
		const auto pop()
		{
			std::pop_heap(m_heap.begin(), m_heap.end(), &heap_op);
			m_heap.pop_back();
		}

		bool empty() const noexcept { return m_heap.empty(); }
		size_t size() const noexcept { return m_heap.size(); }
		void clear() noexcept { m_heap.clear(); }
		auto& heap() noexcept { return m_heap; }
		const auto& heap() const noexcept { return m_heap; }
		std::vector<value_type> m_heap;
	};

	RTreeMod() : m_rtreeNano(0)
	{ }

	void setupMultiTarget(point* pt, size_t count) override
	{
		super::setupMultiTarget(pt, count);
		auto tp = std::chrono::steady_clock::now();
		size_t c = this->getMultiTarget().size();
		m_rtreeTargets.resize(c);
		m_rtree.initalise(c);
		for (size_t i = 0; i < c; ++i) {
			auto& val = m_rtreeTargets[i];
			auto& t = this->getMultiTarget_()[i];
			val.get_mbr() = rtree::boxCoord(t.id);
			val.id = t.id;
			val.target = &t;
			m_rtree.values()[i] = &val;
		}
		m_rtree.finalise();
		m_rtreeNano = std::chrono::steady_clock::now() - tp;
	}

	void closeTarget_(vertex& target) override
	{
		super::closeTarget_(target);
		if constexpr (has_module<modules::TargetReassignModule>()) { // erase leaf from rtree only if target reassign is enabled
			auto& rTarget = m_rtreeTargets[target.target_id]; // select rtree target
			auto* leaf = m_rtree.findLeafByHilbert(rTarget.get_hilbert()); // find leaf
			assert(leaf != nullptr);
			auto [data, count] = m_rtree.getLeafValues(*leaf); // get leaf values for search
			assert(std::find(data, data+count, &rTarget) != data+count);
			m_rtree.eraseValue(*leaf, std::find(data, data+count, &rTarget) - data); // erase leaf from rtree
		}
	}

	auto& getRTree() noexcept { return m_rtree; }
	const auto& getRTree() const noexcept { return m_rtree; }

	void setupStats(rayscanlib::utils::StatsTable* table, rayscanlib::utils::Stats* stats) const override
	{
		super::setupStats(table, stats);
		using namespace std::literals::string_view_literals;
		if (table != nullptr) {
			table->emplace_column("rtreeNano"sv, 7000);
		}
		if (stats != nullptr) {
			stats->template try_emplace<int64>("rtreeNano"sv);
		}
	}

	void saveStats(rayscanlib::utils::Stats& stats) const override
	{
		super::saveStats(stats);
		using namespace std::literals::string_literals;
		stats.at("rtreeNano"s) = m_rtreeNano.count();
	}

private:
	rtree m_rtree;
	std::vector<RTreeTarget> m_rtreeTargets;
	std::chrono::nanoseconds m_rtreeNano;
};

} // namespace modulator

namespace details
{

template <size_t Fanout, size_t LeafSize, typename... Modules>
struct search_module<modules::RTreeModule<Fanout, LeafSize>, Modules...>
{
	using type = modulator::RTreeMod<Fanout, LeafSize, Modules...>;
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_MODULATOR_RSTARTREEMOD_HPP_INCLUDED
