#ifndef RAYSCANLIB_SEARCH_MODULATOR_BYPASSMOD_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_MODULATOR_BYPASSMOD_HPP_INCLUDED

#include "../RayScanBase.hpp"
#include <cmath>

namespace rayscanlib::search
{

namespace modules {
struct BypassModules { };
template <size_t ScanDistance, bool SkipEnable, bool BypassEnable>
struct BypassModule : BypassModules { };
} // namespace modules

namespace modulator {

template <size_t ScanDistance, bool SkipEnable, bool BypassEnable, typename... Modules>
class BypassMod : public search_next_module<modules::BypassModule<ScanDistance, SkipEnable, BypassEnable>, Modules...>::type
{
public:
	using self = BypassMod<ScanDistance, SkipEnable, BypassEnable, Modules...>;
	using super = typename search_next_module<modules::BypassModule<ScanDistance, SkipEnable, BypassEnable>, Modules...>::type;
	RAYSCANLIB_BASE_CHILD
	static_assert(ScanDistance > 0 && ScanDistance <= 128, "Scan distance must fall within 1 and 128");
	static_assert(SkipEnable != false || BypassEnable != false, "Skip and Bypass can't both be disabled");

	constexpr static bool has_skip() { return SkipEnable; }
	constexpr static bool has_bypass() { return BypassEnable; }

	struct SkipAmount
	{
		constexpr int32 skip() const noexcept
		{
			if constexpr (SkipEnable && BypassEnable) {
				return std::abs(amt);
			} else {
				return amt;
			}
		}
		constexpr bool can_skip() const noexcept
		{
			return amt != 0;
		}
		int32 amt;
	};

	template <Dir D, typename AS>
	SkipAmount canBypass(point u, point at, point nx, iter<D> it, AS field, bool skip)
	{
		if constexpr (SkipEnable && !BypassEnable) {
			if (!skip)
				return SkipAmount{0};
		}
		/*if constexpr (AS::between_type() != Between::NARROW) {
			// handle corner case where field extends past -at, restrict to 90-turn to prevent the colin case from occuring
			point turn = at.template turn90<inv_dir(D)>();
			if (!field.within(turn))
				turn = field.template get<inv_dir(D)>();
			return canBypass<D>(u, at, nx, it, geometry::AngledSector<coord, Between::NARROW, D>(turn, at), skip); // restrict to better field type
		} else*/ {
			if constexpr (BypassEnable) {
				point ut = this->getTarget().id - u;
				return field.within(ut) ? self::canBypassScan<D, true>(u, at, nx, it, field, skip, ut) : self::canBypassScan<D, false>(u, at, nx, it, field, skip, ut);
			} else {
				return self::canBypassScan<D, false>(u, at, nx, it, field, skip);
			}
		}
	}
	template <Dir D, bool CheckTarget, typename AS>
	static SkipAmount canBypassScan(point u, point at, point nx, iter<D> it, AS field, bool skip, point ut[[maybe_unused]] = point::zero())
	{
		point uu = at, Anx = nx;
		bool end = false, targetFound[[maybe_unused]] = false;
		for (int32 i = 0; ; ++i) {
			if (!at.template isDir<D>(nx)) { // is reverse rotating
				if (!field.template within<inv_dir(D)>(at, nx)) // leaves field
					return SkipAmount{0};
				if constexpr (CheckTarget) {
					if (!ut.template isBetween<Between::WIDE, D>(at, nx) && !at.template isDir<D>(Anx, ut)) {
						if (!skip)
							return SkipAmount{0};
						else
							targetFound = true; // skip is also enabled, so still try to skip
					}
				}
			} else {
				if (!field.template within<D>(at, nx))
					return SkipAmount{0};
				if constexpr (CheckTarget) {
					// check if target is passed by scan line
					if (!ut.template isBetween<Between::WIDE, inv_dir(D)>(at, nx) && !at.template isDir<inv_dir(D)>(Anx, ut)) {
						if (!skip)
							return SkipAmount{0};
						else
							targetFound = true; // skip is also enabled, so still try to skip
					}
				}
			}
			if (end) { // at leaves the upper line
				assert(i > 0);
				if constexpr (SkipEnable && BypassEnable)
					return SkipAmount{targetFound ? -i : i};
				else
					return SkipAmount{i};
			}
			if (i > static_cast<int32>(ScanDistance))
				return SkipAmount{0};
			at = nx, nx = *++it - u, Anx = nx;
			if (uu.template isDir<D>(nx)) { // nx has passed uu
				if (!at.template isDir<D>(nx)) // handle corner case where scanline crosses -uu
					return SkipAmount{0};
				end = true;
				nx = uu;
			}
		}
	}
};

} // namespace modulator

namespace details
{

template <size_t ScanDistance, bool SkipEnable, bool BypassEnable, typename... Modules>
struct search_module<modules::BypassModule<ScanDistance, SkipEnable, BypassEnable>, Modules...>
{
	using type = modulator::BypassMod<ScanDistance, SkipEnable, BypassEnable, Modules...>;
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_MODULATOR_BYPASSMOD_HPP_INCLUDED
