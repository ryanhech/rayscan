#ifndef RAYSCANLIB_SEARCH_MODULATOR_CACHEMOD_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_MODULATOR_CACHEMOD_HPP_INCLUDED

#include "../RayScanBase.hpp"
#include <algorithm>

namespace rayscanlib::search
{

namespace modules {
struct CacheModule { };
} // namespace modules

namespace modulator {

template <typename... Modules>
class CacheMod : public search_next_module<modules::CacheModule, Modules...>::type
{
public:
	using self = CacheMod<Modules...>;
	using super = typename search_next_module<modules::CacheModule, Modules...>::type;
	RAYSCANLIB_BASE_CHILD

	using raypoint = typename vertex::Cache::raypoint;
	using raypoint_data = typename vertex::Cache::raypoint_data;

	struct NodeCaching
	{
		raypoint_data* data;
		bool cache;
		bool set_cache;
		NodeCaching() noexcept : data(nullptr), cache(false), set_cache(false) { }
		void start_cache() noexcept { cache = set_cache; }
	};

	constexpr static bool cacheIsOrdered(const raypoint& a, const raypoint& b) noexcept
	{
		return a.first.isCCW(b.first);
	}
	constexpr static bool cacheIsColin(const raypoint& a, const raypoint& b) noexcept
	{
		return a.first.isColin(b.first);
	}

	CacheMod() { }

protected:
	void setupCacheNode(vertex& node)
	{
		assert(m_cachingData.data == nullptr);
		// clears cache if environment changes has occured
		node.cached.update(this->getEnvUpdateNo());
		// setup caching vertex data based on turn
		m_cachingData.data = &node.cached.data[vertex::Cache::idx(node.turn)];
		m_cachingData.set_cache = true;
		m_cachingInsert.clear();
	}

	void finaliseCacheNode()
	{
		if (auto* data = m_cachingData.data; data != nullptr) {
			m_cachingData.data = nullptr;
			m_cachingData.set_cache = false;
			if (!m_cachingInsert.empty()) {
				std::sort(m_cachingInsert.begin(), m_cachingInsert.end(), &self::cacheIsOrdered);
#if 1
				if (data->empty()) {
					data->resize(m_cachingInsert.size());
					data->erase(std::unique_copy(m_cachingInsert.begin(), m_cachingInsert.end(), data->begin(), &cacheIsColin), data->end());
				} else {
					m_cachingMerge.resize(m_cachingInsert.size() + data->size());
					auto itm = m_cachingMerge.begin();
					for (auto ita = data->begin(), itae = data->end(), itb = m_cachingInsert.begin(), itbe = m_cachingInsert.end(); ; ) {
						if (cacheIsOrdered(*ita, *itb)) {
							*itm++ = *ita++;
							if (ita == itae) { // data merge complete, copy rest of insert
								itm = std::copy(itb, itbe, itm);
								break;
							}
						} else {
							assert(!cacheIsColin(*ita, *itb));
							*itm++ = *itb++;
							while (itb != itbe && cacheIsColin(itb[-1], *itb)) {
								++itb; // skip colin entries
							}
							if (itb == itbe) { // insert merge complete, copy rest of data
								itm = std::copy(ita, itae, itm);
								break;
							}
						}
					}
					m_cachingMerge.erase(itm, m_cachingMerge.end());
					std::swap(*data, m_cachingMerge);
				}
#else
			m_cachingMerge.resize(data->size() + m_cachingInsert.size());
			std::merge(data->begin(), data->end(), m_cachingInsert.begin(), m_cachingInsert.end(), m_cachingMerge.begin(), &self::cacheIsOrdered);
			std::swap(*data, m_cachingMerge);
#endif
			}
		}
	}

	void expandNode_(vertex& node) override
	{
		setupCacheNode(node);
		super::expandNode_(node);
		finaliseCacheNode();
	}
	
	bool castRayNode_(const vertex& from, vertex& to) override
	{
		m_cachingData.start_cache();
		bool res = super::castRayNode_(from, to);
		m_cachingData.cache = false;
		return res;
	}
	
	intersection castRay_(point a, point ab) override
	{
		if (m_cachingData.cache) {
			assert(m_cachingData.data != nullptr);
			auto front = m_cachingData.data->begin(), back = m_cachingData.data->end();
			while (front != back) {
				auto mid = front + (std::distance(front, back) >> 1);
				if (auto crs = mid->first.cross(ab); point::isCW(crs)) { // before
					back = mid; // exclude mid
				} else if (point::isCCW(crs)) { // after
					front = std::next(mid);
				} else { // colin, result cached result
					if (mid->first != ab) { 
						// update RayInt if ray is different length
						intersection res = mid->second;
						// update RayInt as ab is different magnitude to store ray
						auto [at, nx] = res.getObstruction()->shape.segment(res.getIndex());
						nx -= at;
						if (!ab.isColin(nx)) { // line intersects
							std::get<3>(res) = nx.isOpposites(a-at, (a-at)+ab) ? RayInt::BLOCKED : RayInt::OVERSHOT;
						} else { // line intersects colin at a concave corner, check if blocked by overshooting at
							std::get<3>(res) = ab.isFwd((a-at)+ab) ? RayInt::BLOCKED : RayInt::OVERSHOT;
						}
						assert(res == super::castRay_(a, ab));
						return res;
					} else {
						assert(mid->second == super::castRay_(a, ab));
						return mid->second;
					}
				}
			}
		}
		intersection res = super::castRay_(a, ab);
		if (m_cachingData.cache) { // insert res into cache
			m_cachingInsert.emplace_back(ab, res);
		}
		return res;
	}

private:
	NodeCaching m_cachingData;
	raypoint_data m_cachingInsert;
	raypoint_data m_cachingMerge;
};

} // namespace modulator

namespace details
{

template <typename... Modules>
struct search_module<modules::CacheModule, Modules...>
{
	using type = modulator::CacheMod<Modules...>;
};

template <typename... Modules>
struct search_data<RayScanVertex, modules::CacheModule, Modules...>
	: search_next_data<RayScanVertex, modules::CacheModule, Modules...>::type
{
	using self = search_data<RayScanVertex, modules::CacheModule, Modules...>;
	using super = typename search_next_data<RayScanVertex, modules::CacheModule, Modules...>::type;
	RAYSCANLIB_STRUCT_CHILD

	struct Cache
	{
		using raypoint = std::pair<point, intersection>;
		using raypoint_data = std::vector<raypoint>;
		std::array<raypoint_data, 2> data;
		uint32 last_update;
		constexpr static int idx(Dir d) noexcept // Turn- CCW == 0, CW = 1
		{
			assert(d == Dir::CW || d == Dir::CCW);
			return ( (static_cast<int>(d) >> 1) & 1 );
		}

		void update(uint32 updateNo)
		{
			if (last_update != updateNo) {
				last_update = updateNo;
				data[0].clear();
				data[1].clear();
			}
		}

		Cache() : last_update(0)
		{ }
	};

	Cache cached;
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_MODULATOR_CACHEMOD_HPP_INCLUDED
