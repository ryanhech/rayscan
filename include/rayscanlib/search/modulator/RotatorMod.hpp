#ifndef RAYSCANLIB_SEARCH_MODULATOR_ROTATORMOD_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_MODULATOR_ROTATORMOD_HPP_INCLUDED

#include "../RayScanBase.hpp"
#include "RTreeMod.hpp"
#include <vector>
#ifndef NDEBUG
#include <unordered_set>
#endif

namespace rayscanlib::search
{

namespace modules {
struct RotatorModules { };
template <bool RTree>
struct RotatorModule : RotatorModules { };
} // namespace modules

namespace modulator {

template <bool RTree, typename... Modules>
class RotatorMod : public search_next_module<modules::RotatorModule<RTree>, Modules...>::type
{
public:
	using self = RotatorMod<RTree, Modules...>;
	using super = typename search_next_module<modules::RotatorModule<RTree>, Modules...>::type;
	RAYSCANLIB_BASE_CHILD
	static_assert(has_module<modules::MultiTargetTag>() && !has_module<modules::SingleTargetTag>(), "Must be used on multi target");
	static_assert(!RTree || has_module<modules::RTreeModules>(), "Must have RTreeModule");

	using rotator_type = std::vector<vertex*>;
	class RotatorList
	{
	public:
		RotatorList() : m_current(nullptr), m_front(nullptr), m_back(nullptr)
		{ }

		struct node
		{
			point uv;
			vertex* v;
			node* next;
			node* prev;
			template <Dir D>
			node* get() noexcept
			{
				static_assert(D == Dir::CW || D == Dir::CCW, "D must be CW or CCW");
				if constexpr (D == Dir::CW)
					return next;
				else
					return prev;
			}
			template <Dir D>
			void set(node* n) noexcept
			{
				static_assert(D == Dir::CW || D == Dir::CCW, "D must be CW or CCW");
				if constexpr (D == Dir::CW)
					next = n;
				else
					prev = n;
			}
		};

		void resize(size_t size)
		{
			m_nodes.resize(size + 2);
		}
		void setup(point center, vertex** vertices, size_t size, point uv1, point uvn)
		{
			assert(m_nodes.size() >= size+2);
			auto nat = m_nodes.data();
			m_front = nat;
			m_front->uv = uv1;
			m_front->v = nullptr;
			m_front->prev = m_front;
			node* pv = m_front;
			for ( ; size != 0; --size) {
				node* cr = ++nat;
				pv->next = cr;
				cr->v = *(vertices++);
				cr->uv = cr->v->id - center;
				cr->prev = pv;
				pv = cr;
			}
			m_back = ++nat;
			pv->next = m_back;
			m_back->uv = uvn;
			m_back->v = nullptr;
			m_back->next = m_back;
			m_back->prev = pv;
#ifndef NDEBUG
			if (!testListsState_())
				assert(false);
#endif
		}

		bool empty() const noexcept { return m_front->next->v == nullptr; }
		node* current() noexcept
		{
			assert(m_current != nullptr);
#ifndef NDEBUG
			assert(m_debug.count(m_current) > 0);
#endif
			return m_current;
		}
		node* front() noexcept { return m_front; }
		node* back() noexcept { return m_back; }
		[[nodiscard]] node* first_ccw() noexcept { return m_front->template get<Dir::CW>(); }
		[[nodiscard]] node* first_cw() noexcept { return m_back->template get<Dir::CCW>(); }
		template <Dir D>
		[[nodiscard]] node* first() noexcept
		{
			static_assert(D == Dir::CW || D == Dir::CCW, "D must be CW or CCW");
			if constexpr (D == Dir::CW)
				return first_cw();
			else
				return first_ccw();
		}

		void setCurrent(node* n) noexcept
		{
			m_current = n;
#ifndef NDEBUG
			assert(m_debug.count(m_current) > 0);
#endif
		}
		void clearCurrent() noexcept { m_current = nullptr; }
		template <Dir D>
		void turn() noexcept
		{
			m_current = m_current->template get<D>();
#ifndef NDEBUG
			assert(m_debug.count(m_current) > 0);
#endif
		}

		template <Dir D>
		void eraseCurrent() noexcept
		{
			assert(m_current != nullptr && m_current->v != nullptr);
			node* n = m_current;
			turn<D>();
			assert(m_current != n);
			erase(n);
			assert(m_current->prev != n && m_current->next != n);
#ifndef NDEBUG
			assert(m_debug.count(m_current) > 0);
#endif
		}
		void erase(node* n) noexcept
		{
			assert(n != nullptr && n->v != nullptr);
#ifndef NDEBUG
			if (m_debug.count(n) > 0)
				m_debug.erase(n);
			else
				assert(false);
#endif
			node* nx = n->next;
			node* pv = n->prev;
			assert(nx->prev == n && pv->next == n);
			nx->prev = pv;
			pv->next = nx;
			assert(m_current != n);
		}

#if 0
		template <bool StartNode, Dir D>
		node* save_state(point ray) noexcept
		{
			node* ans = m_current;
			while (m_current->v != nullptr && !m_current->uv.template isDir<D>(ray)) {
				turn<D>();
			}
			return ans;
		}
#endif
	
	private:
#ifndef NDEBUG
		bool testListsState_()
		{
			m_debug.clear();
			m_debug.emplace(m_front);
			m_debug.emplace(m_back);
			if (m_front == nullptr || m_back == nullptr || m_front == m_back)
				return false;
			size_t attempts = m_nodes.size() + 2;
			if (m_front->v != nullptr || m_front->prev != m_front)
				return false;
			node *pv, *at;
			for (pv = m_front, at = m_front->next; --attempts > 0 && at != m_back; pv = at, at = at->next) {
				if (at == nullptr || m_debug.count(at) > 0 || at->prev != pv || at->v == nullptr)
					return false;
				m_debug.emplace(at);
			}
			if (attempts == 0 || m_back->prev != pv || m_back->next != m_back || m_back->v != nullptr)
				return false;
			return true;
		}
#endif

	private:
		node* m_current;
		node* m_front;
		node* m_back;
		std::vector<node> m_nodes;
#ifndef NDEBUG
		std::unordered_set<node*> m_debug;
#endif
	};

	RotatorMod() : m_rotatorSize(0)
	{ }

	void setupMultiTarget(point* pt, size_t count) override
	{
		super::setupMultiTarget(pt, count);
		m_rotator.clear();
		m_rotator.reserve(count);
		for (vertex& v : this->getMultiTarget_()) {
			m_rotator.push_back(&v);
		}
		m_rotatorList.resize(count);
	}

protected:
	void rotatorSortStart_(point center)
	{
		typename rotator_type::iterator at = m_rotator.begin(), mid = at, end = m_rotator.end();
		while (at != end) {
			if ((*at)->close) {
				*at = *--end;
			} else if (point uv = (*at)->id - center; uv.isZero()) {
				std::swap(*at, *mid);
				std::swap(m_rotator.front(), *mid);
				++mid;
			} else {
				if (uv.y < 0 || (uv.y == 0 && uv.x > 0)) {
					std::swap(*at, *mid); ++mid;
				}
				++at;
			}
		}
		m_rotator.erase(end, m_rotator.end());
		if (!m_rotator.empty()) {
			if (m_rotator.front()->id == center) {
				vertex* same = m_rotator.front();
				m_rotator.front() = *mid;
				*mid = m_rotator.back();
				m_rotator.back() = same;
				--mid; --end;
			}
			m_rotatorSize = std::distance(m_rotator.begin(), end);
			if (m_rotatorSize > 0) {
				std::sort(m_rotator.begin(), mid, [center](const vertex* a, const vertex* b) noexcept { return center.isCW(a->id, b->id); });
				std::sort(mid, end, [center](const vertex* a, const vertex* b) noexcept { return center.isCW(a->id, b->id); });
			}
		} else {
			m_rotatorSize = 0;
		}
		m_rotatorList.setup(center, m_rotator.data(), m_rotatorSize, point(1, 0), point(1, 0));
	}

	void rotatorSort_(point center, geometry::AngledSector<coord, Between::NARROW, Dir::CW> as)
	{
		if constexpr (!RTree) {
			typename rotator_type::iterator at = m_rotator.begin(), mid = at, end = m_rotator.end();
			while (at != end) {
				if ((*at)->close) {
					*at = *--end;
				} else {
					if (point uv = (*at)->id - center; !uv.isZero() && as.within(uv)) {
						std::swap(*at, *mid); ++mid;
					}
					++at;
				}
			}
			m_rotator.erase(end, m_rotator.end());
			m_rotatorSize = std::distance(m_rotator.begin(), mid);
			if (m_rotatorSize > 0) {
				std::sort(m_rotator.begin(), mid, [center](const vertex* a, const vertex* b) noexcept { return center.isCW(a->id, b->id); });
			}
			m_rotatorList.setup(center, m_rotator.data(), m_rotatorSize, as.template get<Dir::CCW>(), as.template get<Dir::CW>());
		} else { // RTree
			using ang = geometry::AngledSector<coord, Between::NARROW, Dir::CW>;
			m_rotator.clear();
			auto& tree = this->getRTree();
			m_rtdata.queue.clear();
			m_rtdata.queue.push_back(static_cast<typename RTreeData::value_type>(&tree.root()));
			do {
				auto* val = m_rtdata.queue.back();
				m_rtdata.queue.pop_back();
				bool within = false;
				ang bas;
				box bound(val->get_mbr().lower() - center, val->get_mbr().upper() - center);
				if (bound.lower().x > point::pos_epsilon()) { // on left side
					if (bound.lower().y > point::pos_epsilon()) { // in bottom left octect
						bas = ang(bound.upperLeft(), bound.lowerRight());
					} else if (bound.upper().y < point::neg_epsilon()) { // in top left octect
						bas = ang(bound.upperRight(), bound.lowerLeft());
					} else { // in left octect
						bas = ang(bound.upperLeft(), bound.lowerLeft());
					}
				} else if (bound.upper().x < point::neg_epsilon()) { // on right side
					if (bound.lower().y > point::pos_epsilon()) { // in bottom right octect
						bas = ang(bound.lowerLeft(), bound.upperRight());
					} else if (bound.upper().y < point::neg_epsilon()) { // in top right octect
						bas = ang(bound.lowerRight(), bound.upperLeft());
					} else { // in right octect
						bas = ang(bound.lowerRight(), bound.upperRight());
					}
				} else if (bound.lower().y > point::pos_epsilon()) { // in bottom octect
					bas = ang(bound.lowerLeft(), bound.lowerRight());
				} else if (bound.upper().y < point::neg_epsilon()) { // in top octect
					bas = ang(bound.upperRight(), bound.upperLeft());
				} else {
					within = true;
				}
				if (within || as.within(bas.template get<Dir::CW>()) || as.within(bas.template get<Dir::CCW>()) || bas.within(as.template get<Dir::CW>())) { // angled sector intersects rectangle
					// process this box
					if (!val->is_leaf()) { // is not leaf node
						for (size_t i = 0; i < super::rtree::fanout(); ++i) {
							if (auto* child = val->child(i); child != nullptr)
								m_rtdata.queue.push_back(static_cast<typename RTreeData::value_type>(child));
							else
								break;
						}
					} else { // is leaf node, check all points for smallest
						auto [node, s] = tree.getLeafValues(*val);
						for ( ; s > 0; ++node, --s) {
							if (auto t = (*node)->target; !t->close) {
								if (auto uv = t->id - center; !uv.isZero() && as.within(uv))
									m_rotator.push_back(t);
							}
						}
					}
				}
			} while (!m_rtdata.queue.empty());
			// finalise rotator
			m_rotatorSize = m_rotator.size();
			if (m_rotatorSize > 0) {
				std::sort(m_rotator.begin(), m_rotator.end(), [center](const vertex* a, const vertex* b) noexcept { return center.isCW(a->id, b->id); });
			}
			m_rotatorList.setup(center, m_rotator.data(), m_rotatorSize, as.template get<Dir::CCW>(), as.template get<Dir::CW>());
		}
	}

public:
	const RotatorList& getRotatorList() const noexcept { return m_rotatorList; }
	size_t getRotatorSize() const noexcept { return m_rotatorSize; }
protected:
	RotatorList& getRotatorList_() noexcept { return m_rotatorList; }

private:
	RotatorList m_rotatorList;
	rotator_type m_rotator;
	size_t m_rotatorSize;
	struct RTreeData {
		using value_type = add_const_pointer_t<typename super::rtree::node_ptr, 0>;
		size_t rotatorId;
		std::vector<add_const_pointer_t<typename super::rtree::node_ptr, 0>> queue;
		RTreeData() : rotatorId(0) { }
	};
	inx::optional_type_t<RTree, RTreeData> m_rtdata;
};

} // namespace modulator

namespace details
{

template <bool RTree, typename... Modules>
struct search_module<modules::RotatorModule<RTree>, Modules...>
{
	using type = modulator::RotatorMod<RTree, Modules...>;
};

template <bool RTree, typename... Modules>
struct search_data<RayScanVertex, modules::RotatorModule<RTree>, Modules...>
	: search_next_data<RayScanVertex, modules::RotatorModule<RTree>, Modules...>::type
{
	using self = search_data<RayScanVertex, modules::RotatorModule<RTree>, Modules...>;
	using super = typename search_next_data<RayScanVertex, modules::RotatorModule<RTree>, Modules...>::type;
	RAYSCANLIB_STRUCT_CHILD
	
	size_t rotator_rtree;
	search_data() noexcept : rotator_rtree(0) { }
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_MODULATOR_ROTATORMOD_HPP_INCLUDED
