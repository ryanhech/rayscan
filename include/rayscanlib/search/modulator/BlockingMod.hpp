#ifndef RAYSCANLIB_SEARCH_MODULATOR_BLOCKINGMOD_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_MODULATOR_BLOCKINGMOD_HPP_INCLUDED

#include "../RayScanBase.hpp"
#include <rayscanlib/search/modulator/RotatorMod.hpp>
#ifndef NDEBUG
#include <rayscanlib/geometry/util/RayScanGeometry.hpp>
#endif

namespace rayscanlib::search
{

namespace modules {
struct BlockingModule { };
} // namespace modules

namespace modulator {

template <typename... Modules>
class BlockingMod : public search_next_module<modules::BlockingModule, Modules...>::type
{
public:
	using self = BlockingMod<Modules...>;
	using super = typename search_next_module<modules::BlockingModule, Modules...>::type;
	RAYSCANLIB_BASE_CHILD

	static_assert(has_module<modules::SingleTargetTag>() || (has_module<modules::MultiTargetTag>() && has_module<modules::RotatorModules>()), "MultiTarget requires RotatorModule");

	BlockingMod() : m_ut(point::zero()), m_targetBlocked(false) { }

protected:
	bool isTargetBlocked() { return m_targetBlocked; }
	void setupBlocking()
	{
		if constexpr (has_module<modules::SingleTargetTag>()) {
			m_ut = this->getTarget().id - this->getCurrent()->id;
			m_targetBlocked = false;
		} else {
		}
	}

	template <bool StartNode, Dir D, bool Output = false>
	std::conditional_t<Output, bool, void> checkTargetIsBlocked(point pv, point at, point nx)
	{
		assert(pv.template isDir<D>(at));
		if constexpr (has_module<modules::SingleTargetTag>()) {
			if constexpr (Output) {
				bool check = point::template isConjunctiveDir<D>(pv.cross(m_ut), at.cross(pv, m_ut)) && (at.template isDir<D>(nx) ? !at.template isDir<D>(m_ut) : at.template isDir<inv_dir(D)>(m_ut));
				m_targetBlocked |= check;
				return check;
			} else {
				if (!m_targetBlocked && point::template isConjunctiveDir<D>(pv.cross(m_ut), at.cross(pv, m_ut)) && (at.template isDir<D>(nx) ? !at.template isDir<D>(m_ut) : at.template isDir<inv_dir(D)>(m_ut)))
					m_targetBlocked = true;
			}
		} else {
			for (auto* node = this->getRotatorList_().current(); node->v != nullptr && !node->uv.template isDir<inv_dir(D)>(pv); node = this->getRotatorList_().current())
				this->getRotatorList_().template turn<D>(); // align to just past prev
			for (auto* node = this->getRotatorList_().current();
			     node->v != nullptr && (StartNode ? ( !at.template isDir<D>(node->uv) && pv.template isDir<D>(node->uv) ) : !at.template isDir<D>(node->uv));
				 node = this->getRotatorList_().current()) {
				if (at.template isDir<D>(pv, node->uv) && (at.template isDir<D>(nx) ? !at.template isDir<D>(node->uv) : at.template isDir<inv_dir(D)>(node->uv))) {
#ifndef NDEBUG
					auto I = geometry::util::intersect_corner_segment<false, D!=Dir::CCW>(point::zero(), node->uv, at, pv - at, nx - at);
					assert(!I.colin() && I.a < I.scale);
#endif
					this->getRotatorList_().template eraseCurrent<D>();
				} else
					this->getRotatorList_().template turn<D>();
			}
			if constexpr (Output)
				return false;
		}
	}

	template <bool StartNode, Dir D, bool Output = false>
	std::conditional_t<Output, bool, void> checkTargetIsBlockedInv(point pv[[maybe_unused]], point at[[maybe_unused]])
	{
		assert(pv.template isDir<inv_dir(D)>(at));
		if constexpr (has_module<modules::SingleTargetTag>()) {
			if constexpr (Output)
				return point::template isConjunctiveNotDir<D>(pv.cross(m_ut), m_ut.cross(at), at.cross(pv, m_ut));
		} else {
			auto* node = this->getRotatorList_().current();
			do {
				if (at.template isDir<D>(node->uv) && !pv.template isDir<D>(node->uv))
					this->getRotatorList_().template turn<inv_dir(D)>();
				else
					break;
				node = this->getRotatorList_().current();
			} while (node->v != nullptr);
			if (Output)
				return false;
		}
	}

private:
	point m_ut;
	bool m_targetBlocked;
};

} // namespace modulator

namespace details
{

template <typename... Modules>
struct search_module<modules::BlockingModule, Modules...>
{
	using type = modulator::BlockingMod<Modules...>;
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_MODULATOR_BLOCKINGMOD_HPP_INCLUDED
