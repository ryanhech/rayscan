#ifndef RAYSCANLIB_SEARCH_MODULATOR_TARGETREASSIGNMOD_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_MODULATOR_TARGETREASSIGNMOD_HPP_INCLUDED

#include "../RayScanBase.hpp"
#include <algorithm>

namespace rayscanlib::search
{

namespace modules {
struct TargetReassignModule { };
} // namespace modules

namespace modulator {

template <typename... Modules>
class TargetReassignMod : public search_next_module<modules::TargetReassignModule, Modules...>::type
{
public:
	using self = TargetReassignMod<Modules...>;
	using super = typename search_next_module<modules::TargetReassignModule, Modules...>::type;
	RAYSCANLIB_BASE_CHILD

	TargetReassignMod() { }

	void initNode_(vertex& node) override
	{
		super::initNode_(node);
		node.target_id_ra = -1;
	}
	
	void search_() override
	{
		uint32 targets = static_cast<uint32>(this->getMultiTarget().size());
		auto& queue = this->astarQueue_();
		this->expandStartNode_(this->getStart_());

		while (this->getTargetsFound() < targets && !queue.empty()) {
			assert(inx::alg::is_valid_pairing_heap(queue));
			vertex* expand = &queue.top();
			queue.pop();
			assert(!inx::alg::pairing_heap_node_within(queue, *expand));
			assert(inx::alg::is_valid_pairing_heap(queue));
			if (!queue.empty()) { // if other elements exist on the queue, check h-value to see if it should reassign
				while (true) { // loop until finding value not reassigned
					if (this->heuristic_(*expand)) {
						// heuristic is outdated, update and check
						expand->f = expand->g + expand->h;
						if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::UpdateNodeHook>(std::as_const(*expand)); }
						if (expand->f > queue.top().f) { // if updated heuristic f-value is worse the what is on queue, change f-value
							assert(inx::alg::is_valid_pairing_heap(queue));
							queue.push(*expand);
							assert(inx::alg::pairing_heap_node_within(queue, *expand));
							assert(inx::alg::is_valid_pairing_heap(queue));
							expand = &queue.top();
							queue.pop();
							assert(!inx::alg::pairing_heap_node_within(queue, *expand));
							assert(inx::alg::is_valid_pairing_heap(queue));
							continue;
						}
					}
					break; // best possible f-value
				}
			}
			if (expand->isTarget()) {
				this->closeTarget_(*expand);
				continue;
			}
			this->expandNode_(*expand);
		}
	}
};

} // namespace modulator

namespace details
{

template <typename... Modules>
struct search_module<modules::TargetReassignModule, Modules...>
{
	using type = modulator::TargetReassignMod<Modules...>;
};

template <typename... Modules>
struct search_data<RayScanVertex, modules::TargetReassignModule, Modules...>
	: search_next_data<RayScanVertex, modules::TargetReassignModule, Modules...>::type
{
	using self = search_data<RayScanVertex, modules::TargetReassignModule, Modules...>;
	using super = typename search_next_data<RayScanVertex, modules::TargetReassignModule, Modules...>::type;
	RAYSCANLIB_STRUCT_CHILD

	search_data() : target_id_ra(-1)
	{ }

	int32 target_id_ra;
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_MODULATOR_TARGETREASSIGNMOD_HPP_INCLUDED
