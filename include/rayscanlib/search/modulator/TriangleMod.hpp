#ifndef RAYSCANLIB_SEARCH_MODULATOR_TRIANGLEMOD_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_MODULATOR_TRIANGLEMOD_HPP_INCLUDED

#include "../RayScanBase.hpp"

namespace rayscanlib::search
{

namespace modules {
struct TriangleModule { };
} // namespace modules

namespace modulator {

template <typename... Modules>
class TriangleMod : public search_next_module<modules::TriangleModule, Modules...>::type
{
public:
	using self = TriangleMod<Modules...>;
	using super = typename search_next_module<modules::TriangleModule, Modules...>::type;
	RAYSCANLIB_BASE_CHILD

	static_assert(has_module<modules::SingleTargetTag>(), "Must be used with single target");

	TriangleMod() : m_triArea(-1), m_triDupFound(0), m_triDupTargets(0), m_triPointsPruned(0), m_triPoints{}
	{ }

protected:
	void pushSuccessor_(vertex& successor, double g, const intersection& inst, Dir turn) override
	{
		if (successor.open || successor.close) {
			m_triDupFound += 1;
			point u = successor.id;
			point a = successor.pred->id;
			point b = this->getCurrent()->id;
			auto c1 = u.cross(a, b);
			if (!point::isColin(c1)) {
				if (point::isCW(c1)) { // make it CCW
					c1 = -c1;
					std::swap(a, b);
				}
				c1 += a.cross(b, u) + b.cross(u, a);
				assert(c1 > 0);
				if (static_cast<uint64>(c1) < static_cast<uint64>(m_triArea)) { // area is smaller, check if can update
					// check target is within area
					point t = this->getTarget().id;
					if (!t.isCW(u, a) && !t.isCW(a, b) && !t.isCW(b, u)) {
						m_triDupTargets += 1;
						// update target triangle
						m_triArea = c1;
						m_triPoints = {u, a, b};
					}
				}
			}
		}
		super::pushSuccessor_(successor, g, inst, turn);
	}

	void expandNode_(vertex& node) override
	{
		// test triangle area
		if (m_triArea < 0 ||
		    (  !node.id.isCW(m_triPoints[0], m_triPoints[1])
		    && !node.id.isCW(m_triPoints[1], m_triPoints[2])
		    && !node.id.isCW(m_triPoints[2], m_triPoints[0])
			)
		) {
			super::expandNode_(node);
		} else {
			m_triPointsPruned += 1;
		}
	}

	void setupSearchST_() override
	{
		super::setupSearchST_();
		m_triArea = -1;
		m_triDupFound = 0;
		m_triDupTargets = 0;
		m_triPointsPruned = 0;
	}

public:
	void setupStats(rayscanlib::utils::StatsTable* table, rayscanlib::utils::Stats* stats) const override
	{
		super::setupStats(table, stats);
		using namespace std::literals::string_view_literals;
		if (table != nullptr) {
			table->emplace_column("triDuplicates"sv, 6000);
			table->emplace_column("triTargets"sv, 6000);
			table->emplace_column("triPruned"sv, 6100);
		}
		if (stats != nullptr) {
			stats->template try_emplace<int64>("triDuplicates"sv);
			stats->template try_emplace<int64>("triTargets"sv);
			stats->template try_emplace<int64>("triPruned"sv);
		}
	}

	void saveStats(rayscanlib::utils::Stats& stats) const override
	{
		super::saveStats(stats);
		using namespace std::literals::string_literals;
		stats.at("triDuplicates"s) = m_triDupFound;
		stats.at("triTargets"s) = m_triDupTargets;
		stats.at("triPruned"s) = m_triPointsPruned;
	}

private:
	int64 m_triArea;
	int64 m_triDupFound;
	int64 m_triDupTargets;
	int64 m_triPointsPruned;
	std::array<point, 3> m_triPoints; // will be CCW orientation
};

} // namespace modulator

namespace details
{

template <typename... Modules>
struct search_module<modules::TriangleModule, Modules...>
{
	using type = modulator::TriangleMod<Modules...>;
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_MODULATOR_TRIANGLEMOD_HPP_INCLUDED
