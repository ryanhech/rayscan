#ifndef RAYSCANLIB_SEARCH_RAYSCANBASE_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_RAYSCANBASE_HPP_INCLUDED

#include "SearchBase.hpp"
#include <rayscanlib/geometry/Polygon.hpp>
#include <rayscanlib/geometry/Box.hpp>
#include <rayscanlib/scen/PolygonEnvironment.hpp>
#include <unordered_map>
#include <memory_resource>
#include <boost/range/adaptor/indexed.hpp>
#include <deque>

namespace rayscanlib::search
{

namespace modules {
template <typename CoordType>
struct RayScanModule { };
} // namespace modules

namespace hooks {
struct RayCastHook { };
} // namespace hooks

namespace details
{

#define RAYSCANLIB_DEFINE_TYPES \
	using coord = typename search_types::coord; \
	using point = typename search_types::point; \
	using polygon = typename search_types::polygon; \
	using basic_polygon = typename search_types::basic_polygon; \
	using box = typename search_types::box; \
	using iter_cw = typename search_types::iter_cw; \
	using iter_ccw = typename search_types::iter_ccw; \
	template <Dir D> using iter = typename search_types::template iter<D>; \
	using vertex = typename search_types::vertex; \
	using obstacle = typename search_types::obstacle; \
	using vertex_ref = typename search_types::vertex_ref; \
	using intersection = typename search_types::intersection;

#define RAYSCANLIB_DEFINE_TYPES_MODULES \
	SEARCH_DEFINE_TYPES_MODULES \
	RAYSCANLIB_DEFINE_TYPES

#define RAYSCANLIB_BASE_TYPES(baseType) \
	using coord = baseType::coord; \
	using point = baseType::point; \
	using polygon = baseType::polygon; \
	using basic_polygon = baseType::basic_polygon; \
	using box = baseType::box; \
	using iter_cw = baseType::iter_cw; \
	using iter_ccw = baseType::iter_ccw; \
	template <Dir D> using iter = baseType::template iter<D>; \
	using vertex = baseType::vertex; \
	using obstacle = baseType::obstacle; \
	using vertex_ref = baseType::vertex_ref; \
	using intersection = baseType::intersection;

#define RAYSCANLIB_INHERIT_TYPES(superType) \
	using typename superType::coord; \
	using typename superType::point; \
	using typename superType::polygon; \
	using typename superType::basic_polygon; \
	using typename superType::box; \
	using typename superType::iter_cw; \
	using typename superType::iter_ccw; \
	template <Dir D> using iter = typename superType::template iter<D>; \
	using typename superType::vertex; \
	using typename superType::obstacle; \
	using typename superType::vertex_ref; \
	using typename superType::intersection;

#define RAYSCANLIB_INHERIT_BASE_TYPES(superType) \
	SEARCH_INHERIT_BASE_TYPES(superType) \
	RAYSCANLIB_INHERIT_TYPES(superType)

#define RAYSCANLIB_BASE_CHILD \
	SEARCH_BASE_CHILD \
	RAYSCANLIB_INHERIT_TYPES(super)

#define RAYSCANLIB_STRUCT_CHILD \
	SEARCH_STRUCT_CHILD \
	RAYSCANLIB_INHERIT_TYPES(super)

} // namespace details

enum RayInt : int8 { BLOCKED = 0, OVERSHOT = 1, SELF = -1 };
struct RayScanIntersection;

template <typename... Modules>
struct RayScanTypeIntersection;

template <typename... Modules>
struct RayScanRef;

namespace details {

struct RayScanVertex { };
struct RayScanObstacle { };

template <typename CoordType, typename... Modules>
struct SearchTypes<modules::RayScanModule<CoordType>, Modules...> : next_search_type<modules::RayScanModule<CoordType>, Modules...>::type
{
	using coord = CoordType;
	using point = geometry::Point<CoordType>;
	using polygon = geometry::ConvexPolygon<CoordType>;
	using basic_polygon = geometry::Polygon<CoordType>;
	using box = geometry::Box<CoordType>;
	using iter_cw = typename polygon::reverse_circ_iterator;
	using iter_ccw = typename polygon::circ_iterator;
	template <Dir D>
	using iter = std::conditional_t<D == Dir::CW, iter_cw, iter_ccw>;
	using vertex = search_data<RayScanVertex, void, Modules...>;
	using obstacle = search_data<RayScanObstacle, void, Modules...>;
	using vertex_ref = RayScanRef<Modules...>;
	using intersection = RayScanTypeIntersection<Modules...>;
};

}

template <typename CoordType, typename... Modules>
class RayScanBase : public search_next_module<modules::RayScanModule<CoordType>, Modules...>::type
{
public:
	using self = RayScanBase<CoordType, Modules...>;
	using super = typename search_next_module<modules::RayScanModule<CoordType>, Modules...>::type;
	SEARCH_BASE_CHILD
	RAYSCANLIB_DEFINE_TYPES

	constexpr static bool is_floating_point() noexcept { return std::is_floating_point_v<CoordType>; }

	RayScanBase() : m_obstacles(&m_obstaclesPool), m_envBox{}, m_envUpdateNo(0)
	{ }

	template <typename Env>
	std::chrono::nanoseconds loadEnvironment(const Env& env)
	{
		auto tp1 = std::chrono::steady_clock::now();
		envInitalise(nullptr);
		envSetBox(env.getEnclosureBox(1));
		envAddObstacle(&env.getEnclosure(), env.getEnclosure());
		for (auto& poly : env.getObstacles()) {
			envAddObstacle(&poly, poly);
		}
		envFinalise(nullptr);
		auto tp2 = std::chrono::steady_clock::now();
		auto dur = tp2 - tp1;
		return dur;
	}

	void envSetBox(const box& b)
	{
		envSetBox_(b);
	}

	void envInitalise(const rayscanlib::scen::PolygonEnvironment* env[[maybe_unused]])
	{
		envInitalise_(env);
	}

	void envFinalise(const rayscanlib::scen::PolygonEnvironment* env)
	{
		envFinalise_(env);
	}

	void envFinaliseNull(const rayscanlib::scen::PolygonEnvironment* env[[maybe_unused]])
	{ }

	template <typename Crd>
	obstacle& envAddObstacle(const void* uid, const geometry::Polygon<Crd>& poly)
	{
		return envAddObstacle_(uid, polygon(poly.begin(), poly.end(), geometry::PolyType::None));
	}
	obstacle& envAddObstacle(const void* uid, const basic_polygon& poly)
	{
		return envAddObstacle_(uid, polygon(poly, geometry::PolyType::None));
	}
	void envRemoveObstacle(const void* uid)
	{
		auto it = m_obstacles.find(uid);
		if (it != m_obstacles.end()) {
			return envRemoveObstacle_(uid, it->second);
		}
	}

protected:
	virtual void search_() = 0;
	
	using cast_ray_scale = std::conditional_t<is_floating_point(), double, inx::frac<int32>>;
	using cast_ray_result = std::tuple<obstacle*, int32, cast_ray_scale, cast_ray_scale>;
	virtual cast_ray_result castRayShoot_(point a, point ab) = 0;
	virtual intersection castRay_(point a, point ab)
	{
		auto [obst, id, I, Ib] = castRayShoot_(a, ab);
		setLastIntersection_(I);
		intersection inst;
		std::get<0>(inst) = obst;
		std::get<3>(inst) = geometry::pr_op<geometry::PRop::geOne>(I) ? OVERSHOT : geometry::pr_op<geometry::PRop::neZero>(I) ? BLOCKED : SELF;
		if (geometry::pr_op<geometry::PRop::neZero>(Ib)) {
			std::get<1>(inst) = id;
			std::get<2>(inst) = 1;
		} else {
			int32 s = obst->shape.size();
			std::get<1>(inst) = (id + s - 1) % s;
			std::get<2>(inst) = 2;
		}

		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::RayCastHook>(a, ab, inst); }

		return inst;
	}
	
	virtual void envSetBox_(const box& b)
	{
		m_envBox = b;
	}

	virtual void envInitalise_(const rayscanlib::scen::PolygonEnvironment* env[[maybe_unused]])
	{ }

	virtual void envFinalise_(const rayscanlib::scen::PolygonEnvironment* env[[maybe_unused]])
	{
		m_envUpdateNo += 1;
	}

	virtual obstacle& envAddObstacle_(const void* uid, polygon&& poly)
	{
		assert(!poly.isFinalised());
		auto [it, s] = m_obstacles.try_emplace(uid);
		assert(s);
		obstacle& obst = it->second;
		obst.shape.assign(std::move(poly), geometry::PolyType::SimplePolygon);
		assert(obst.shape.getType() == geometry::PolyType::SimplePolygon);
		obst.vertices = std::make_unique<vertex[]>(obst.shape.size());
		for (auto pt : obst.shape | boost::adaptors::indexed(0)) {
			auto& v = obst.vertices[pt.index()];
			v.id = pt.value();
			v.ref.obst = &obst;
			v.ref.id = pt.index();
		}
		if (obst.shape.orientation() == Dir::CCW) {
			obst.enclosure = true;
			obst.shape.finalise(geometry::PolyType::SimpleWithHull);
			assert(obst.shape.getType() == geometry::PolyType::SimpleWithHull);
		}
		return obst;
	}

	virtual void envRemoveObstacle_(const void* uid, obstacle& obst[[maybe_unused]])
	{
		m_obstacles.erase(uid);
	}

	const auto& getLastIntersection_() const noexcept { return m_lastIntersection; } 
	void setLastIntersection_(const cast_ray_scale& I) noexcept { m_lastIntersection = I; } 

public:
	const auto& getObstacles() const noexcept { return m_obstacles; }
	const box& getEnvBox() const noexcept { return m_envBox; }
	uint32 getEnvUpdateNo() const noexcept { return m_envUpdateNo; }
protected:
	auto& getObstacles_() noexcept { return m_obstacles; }

private:
	std::pmr::unsynchronized_pool_resource m_obstaclesPool;
	std::pmr::unordered_map<const void*, obstacle> m_obstacles;
	box m_envBox;
	uint32 m_envUpdateNo;
	cast_ray_scale m_lastIntersection;
};

struct RayScanIntersection : std::tuple<void*, int32, int8, RayInt>
{
	void* getObstruction() const noexcept { return std::get<0>(*this); }
	int32 getIndexCW() const noexcept { return std::get<1>(*this); }
	int32 getIndexCCW() const noexcept { return std::get<1>(*this) + std::get<2>(*this); }
	int32 getIndex() const noexcept { return std::get<1>(*this) + (std::get<2>(*this)-1); }
	RayInt getRayInt() const noexcept { return std::get<3>(*this); }
	bool isCorner() const noexcept
	{
		assert(std::get<2>(*this) == 1 || std::get<2>(*this) == 2);
		return std::get<2>(*this) == 2;
	}
	operator bool() const noexcept { return std::get<0>(*this) != nullptr; }
	bool operator!() const noexcept { return std::get<0>(*this) == nullptr; }
};

template <typename... Modules>
struct RayScanTypeIntersection : RayScanIntersection
{
	RAYSCANLIB_DEFINE_TYPES_MODULES
	obstacle* getObstruction() const noexcept { return static_cast<obstacle*>(RayScanIntersection::getObstruction()); }
	int32 getIndexCCW() const noexcept { return static_cast<int32>(static_cast<uint32>(RayScanIntersection::getIndexCCW()) % static_cast<uint32>(getObstruction()->shape.size())); }
	int32 getIndex() const noexcept { return static_cast<int32>(static_cast<uint32>(RayScanIntersection::getIndex()) % static_cast<uint32>(getObstruction()->shape.size())); }
	iter_cw getIterCW() const noexcept { return getObstruction()->shape.make_circ_riterator(static_cast<uint32>(getIndexCW())); }
	iter_ccw getIterCCW() const noexcept { return getObstruction()->shape.make_circ_iterator(static_cast<uint32>(getIndexCCW())); }
	iter_cw getIterAt() const noexcept { return getObstruction()->shape.make_circ_riterator(static_cast<uint32>(getIndex())); }
	template <Dir D>
	auto getIter() const noexcept { if constexpr (D == Dir::CW) return getIterCW(); else return getIterCCW(); }
	point getPointCW() const noexcept { return getObstruction()->shape[getIndexCW()]; }
	point getPointCCW() const noexcept { return getObstruction()->shape[getIndexCCW()]; }
	point getPoint() const noexcept { return getObstruction()->shape[getIndex()]; }
#if 0
	bool isScannable() const noexcept
	{
		assert(getObstruction() != nullptr);
		obstacle& o = *getObstruction();
		if (!o.enclosure)
			return true;
		else {
			int32 index = getIndex();
			return isCorner() ? !o.shape.pointOnHull(index) : !o.shape.segmentOnHull(index);
		}
	}
#endif
};

template <typename... Modules>
struct RayScanRef
{
	RAYSCANLIB_DEFINE_TYPES_MODULES
	obstacle* obst;
	int32 id;
	constexpr RayScanRef() noexcept : obst(nullptr), id(0) { }
	obstacle* getObstacle() const noexcept { return obst; }
	int32 getIndex() const noexcept { return id; }
	vertex& getVertex() const noexcept { assert(obst != nullptr); return obst->vertices[id]; }
	vertex& getVertexCW() const noexcept { assert(obst != nullptr); return obst->vertices[(id+obst->obstacle.size()-1) % obst->obstacle.size()]; }
	vertex& getVertexCCW() const noexcept { assert(obst != nullptr); return obst->vertices[(id+1) % obst->obstacle.size()]; }
	point getPoint() const noexcept { assert(obst != nullptr); return obst->vertices[id]->id; }
	point getPointCW() const noexcept { assert(obst != nullptr); return obst->shape[id-1]; }
	point getPointCCW() const noexcept { assert(obst != nullptr); return obst->shape[id+1]; }
	auto getIterCW() const noexcept { assert(obst != nullptr); return ++obst->shape.make_circ_riterator(id); }
	auto getIterCCW() const noexcept { assert(obst != nullptr); return ++obst->shape.make_circ_iterator(id); }
};

namespace details {

template <typename CoordType, typename... Modules>
struct search_data<RayScanVertex, modules::RayScanModule<CoordType>, Modules...>
	: search_next_data<RayScanVertex, modules::RayScanModule<CoordType>, Modules...>::type
{
	using self = search_data<RayScanVertex, modules::RayScanModule<CoordType>, Modules...>;
	using super = typename search_next_data<RayScanVertex, modules::RayScanModule<CoordType>, Modules...>::type;
	SEARCH_STRUCT_CHILD
	RAYSCANLIB_DEFINE_TYPES
	
	point id;
	vertex_ref ref;

	Dir calcTurn(point from) const noexcept
	{
		auto [pv,at,nx] = ref.obst->shape.edge(ref.id);
		assert(at.isCCW(pv, nx));
		return !(at - from).template isBetween<Between::WIDE, Dir::CCW>(at - pv, nx - at) ? Dir::CW : Dir::CCW;
	}
};

template <typename CoordType, typename... Modules>
struct search_data<RayScanObstacle, modules::RayScanModule<CoordType>, Modules...>
	: search_next_data<RayScanObstacle, modules::RayScanModule<CoordType>, Modules...>::type
{
	using self = search_data<RayScanObstacle, modules::RayScanModule<CoordType>, Modules...>;
	using super = typename search_next_data<RayScanObstacle, modules::RayScanModule<CoordType>, Modules...>::type;
	SEARCH_STRUCT_CHILD
	RAYSCANLIB_DEFINE_TYPES

	polygon shape;
	std::unique_ptr<vertex[]> vertices;
	bool enclosure;

	search_data() : enclosure(false) { }
	size_t size() const noexcept { return shape.size(); }
};

template <typename CoordType, typename... Modules>
struct search_module<modules::RayScanModule<CoordType>, Modules...>
{
	using type = RayScanBase<CoordType, Modules...>;
};

SEARCH_HOOK_DEFINE(RayCastHook,void(typename hook::point a, typename hook::point ab, typename hook::intersection inst))

} // namespace details

struct RayScanStats
{
	details::SearchExpansion search; /// search id and expansion
	uint32 successorsFound; /// how many edges were discovered
	uint32 successorsPushed; /// how many edges found were undiscovered
	uint32 successorsUpdated; /// how many edges found were improved
	uint32 rays; /// how many rays were casted
	std::chrono::nanoseconds setupTime; /// time setup
	std::chrono::nanoseconds dynSetupTime; /// time search used
	std::chrono::nanoseconds searchTime; /// time search used
	std::chrono::nanoseconds rayCastTime; /// time used for raycasts, leave zero if disabled
	RayScanStats() noexcept : search{}, setupTime{}, dynSetupTime{} { clear(); }
	void clear() noexcept
	{
		successorsFound = 0;
		successorsPushed = 0;
		successorsUpdated = 0;
		rays = 0;
		searchTime = std::chrono::nanoseconds(0);
		rayCastTime = std::chrono::nanoseconds(0);
	}

	void pushSetupTime(std::chrono::nanoseconds t) noexcept
	{
		setupTime = t;
	}
	void pushDynSetupTime(std::chrono::nanoseconds t) noexcept
	{
		dynSetupTime = t;
	}
};

template <bool Hooks, typename CoordType, typename... Modules>
using RayScan = details::SearchFinal<Hooks, Modules..., modules::RayScanModule<CoordType>>;

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_RAYSCANBASE_HPP_INCLUDED
