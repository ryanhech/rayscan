#ifndef RAYSCANLIB_SEARCH_HEURISTIC_EUCLIDEANRTREEHEURISTIC_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_HEURISTIC_EUCLIDEANRTREEHEURISTIC_HPP_INCLUDED

#include "../RayScanBase.hpp"
#include "../modulator/RTreeMod.hpp"
#include "../modulator/TargetReassignMod.hpp"

namespace rayscanlib::search
{

namespace modules {
struct EuclideanRTreeHeuristicModule { };
} // namespace modules

namespace heuristic {

template <typename... Modules>
class EuclideanRTreeHeuristic : public search_next_module<modules::EuclideanRTreeHeuristicModule, Modules...>::type
{
public:
	using self = EuclideanRTreeHeuristic<Modules...>;
	using super = typename search_next_module<modules::EuclideanRTreeHeuristicModule, Modules...>::type;
	RAYSCANLIB_BASE_CHILD
	static_assert(has_module<modules::RTreeModules>(), "Must have RTreeModules");

	EuclideanRTreeHeuristic() = default;

protected:
	bool heuristic_(vertex& node) override
	{
		if constexpr (has_module<modules::TargetReassignModule>()) {
			if (this->getMultiTarget()[node.target_id_ra].close) {
				heuristicValue(node);
				node.h = node.target_dist;
				return true;
			} else {
				node.h = node.target_dist;
				return false;
			}
		} else {
			node.h = node.target_dist;
			return false;
		}
	}

	void heuristicValue(vertex& node)
	{
		using rtree = typename super::rtree;
		auto& tree = this->getRTree();
		m_queryHeap.clear();
		m_queryHeap.push(0, static_cast<const typename rtree::node_type*>(&tree.root()));
		double ans = inf<double>;
		int32 idx [[maybe_unused]] = -1;
		auto box = rtree::boxCoord(node.id);
		do {
			auto* val = m_queryHeap.top().second;
			m_queryHeap.pop();
			if (val->is_leaf()) { // is leaf node
				auto [nd, s] = tree.getLeafValues(*val);
				for ( ; s > 0; ++nd, --s) {
					double d = ((*nd)->id - node.id).square();
					if (d < ans) { // if Euclidean distance to mbr of child is less than current answer, add to queue
						ans = d;
						if constexpr (has_module<modules::TargetReassignModule>()) {
							idx = (*nd)->target->target_id; // update idx to target id for reassign
						}
					}
				}
			} else { // is not leaf node, check all points for smallest
				for (int i = 0; i < static_cast<int>(rtree::fanout()); ++i) {
					auto* child = static_cast<const typename rtree::node_type*>(val->child(i));
					if (child == nullptr) break;
					int64 d;
					if (auto dx = box.lower().x - child->get_mbr().lower().x; dx < 0) { // on left side
						if (auto dy = box.lower().y - child->get_mbr().lower().y; dy < 0) { // on bottom left
							d = static_cast<int64>(dx) * dx + static_cast<int64>(dy) * dy;
						} else if (dy = box.upper().y - child->get_mbr().upper().y; dy > 0) { // on top center left
							d = static_cast<int64>(dx) * dx + static_cast<int64>(dy) * dy;
						} else { // on right side
							d = static_cast<int64>(dx) * dx;
						}
					} else if (dx = box.upper().x - child->get_mbr().upper().x; dx > 0) { // on right side
						if (auto dy = box.lower().y - child->get_mbr().lower().y; dy < 0) { // on bottom right
							d = static_cast<int64>(dx) * dx + static_cast<int64>(dy) * dy;
						} else if (dy = box.upper().y - child->get_mbr().upper().y; dy > 0) { // on top right
							d = static_cast<int64>(dx) * dx + static_cast<int64>(dy) * dy;
						} else { // on right center side
							d = static_cast<int64>(dx) * dx;
						}
					} else { // on hori center
						if (auto dy = box.lower().y - child->get_mbr().lower().y; dy < 0) { // on bottom right
							d = static_cast<int64>(dy) * dy;
						} else if (dy = box.upper().y - child->get_mbr().upper().y; dy > 0) { // on top right
							d = static_cast<int64>(dy) * dy;
						} else { // on right center side
							d = 0;
						}
					}
					if (d < ans)
						m_queryHeap.push(d, child);
				}
			}
		} while (!m_queryHeap.empty() && ans > m_queryHeap.top().first);
		node.target_dist = std::sqrt(ans);
		if constexpr (has_module<modules::TargetReassignModule>()) {
			node.target_id_ra = idx;
		}
	}

	void initNode_(vertex& node) override
	{
		super::initNode_(node);
		heuristicValue(node);
	}

private:
	typename super::RTreeMinHeap m_queryHeap;
};

} // namespace expansion

namespace details
{

template <typename... Modules>
struct search_module<modules::EuclideanRTreeHeuristicModule, Modules...>
{
	using type = heuristic::EuclideanRTreeHeuristic<Modules...>;
};

template <typename... Modules>
struct search_data<RayScanVertex, modules::EuclideanRTreeHeuristicModule, Modules...>
	: search_next_data<RayScanVertex, modules::EuclideanRTreeHeuristicModule, Modules...>::type
{
	using self = search_data<RayScanVertex, modules::EuclideanRTreeHeuristicModule, Modules...>;
	using super = typename search_next_data<RayScanVertex, modules::EuclideanRTreeHeuristicModule, Modules...>::type;
	RAYSCANLIB_STRUCT_CHILD
	
	double target_dist;
	search_data() noexcept : target_dist(0) { }
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_HEURISTIC_EUCLIDEANRTREEHEURISTIC_HPP_INCLUDED
