#ifndef RAYSCANLIB_SEARCH_HEURISTIC_ZEROHEURISTIC_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_HEURISTIC_ZEROHEURISTIC_HPP_INCLUDED

#include "../RayScanBase.hpp"

namespace rayscanlib::search
{

namespace modules {
struct ZeroHeuristicModule { };
} // namespace modules

namespace heuristic {

template <typename... Modules>
class ZeroHeuristic : public search_next_module<modules::ZeroHeuristicModule, Modules...>::type
{
public:
	using self = ZeroHeuristic<Modules...>;
	using super = typename search_next_module<modules::ZeroHeuristicModule, Modules...>::type;
	RAYSCANLIB_BASE_CHILD

	ZeroHeuristic() = default;

protected:
	bool heuristic_(vertex& node) override
	{
		node.h = 0;
		return false;
	}
};

} // namespace expansion

namespace details
{

template <typename... Modules>
struct search_module<modules::ZeroHeuristicModule, Modules...>
{
	using type = heuristic::ZeroHeuristic<Modules...>;
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_HEURISTIC_ZEROHEURISTIC_HPP_INCLUDED
