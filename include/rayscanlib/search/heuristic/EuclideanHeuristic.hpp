#ifndef RAYSCANLIB_SEARCH_HEURISTIC_EUCLIDEANHEURISTIC_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_HEURISTIC_EUCLIDEANHEURISTIC_HPP_INCLUDED

#include "../RayScanBase.hpp"
#include "../modulator/TargetReassignMod.hpp"

namespace rayscanlib::search
{

namespace modules {
struct EuclideanHeuristicModule { };
} // namespace modules

namespace heuristic {

template <typename... Modules>
class EuclideanHeuristic : public search_next_module<modules::EuclideanHeuristicModule, Modules...>::type
{
public:
	using self = EuclideanHeuristic<Modules...>;
	using super = typename search_next_module<modules::EuclideanHeuristicModule, Modules...>::type;
	RAYSCANLIB_BASE_CHILD

	EuclideanHeuristic() = default;

protected:
	bool heuristic_(vertex& node) override
	{
		if constexpr (has_module<modules::TargetReassignModule>()) {
			if (this->getMultiTarget()[node.target_id_ra].close) {
				heuristicValue(node);
				node.h = node.target_dist;
				return true;
			} else {
				node.h = node.target_dist;
				return false;
			}
		} else {
			node.h = node.target_dist;
			return false;
		}
	}

	void heuristicValue(vertex& node)
	{
		if constexpr (has_module<modules::MultiTargetTag>()) {
			auto pt = node.id;
			double dist2 = inf<double>;
			int32 idx [[maybe_unused]] = -1;
			auto& mt = this->getMultiTarget();
			for (uint32 i = 0, ie = static_cast<uint32>(mt.size()); i < ie; ++i) {
				if constexpr (has_module<modules::TargetReassignModule>()) {
					if (!mt[i].close) {
						if (double d = static_cast<double>( pt.square(mt[i].id) ); d < dist2) {
							dist2 = d;
							idx = i;
						}
					}
				} else {
					dist2 = std::min(dist2, static_cast<double>( pt.square(mt[i].id) ));
				}
			}
			node.target_dist = std::sqrt(dist2);
			if constexpr (has_module<modules::TargetReassignModule>()) {
				node.target_id_ra = idx;
			}
		}
	}

	void initNode_(vertex& node) override
	{
		super::initNode_(node);
		if constexpr (has_module<modules::SingleTargetTag>()) {
			node.target_dist = node.id.distance(this->getTarget().id);
		} else if constexpr (has_module<modules::MultiTargetTag>()) {
			heuristicValue(node);
		}
	}
};

} // namespace expansion

namespace details
{

template <typename... Modules>
struct search_module<modules::EuclideanHeuristicModule, Modules...>
{
	using type = heuristic::EuclideanHeuristic<Modules...>;
};

template <typename... Modules>
struct search_data<RayScanVertex, modules::EuclideanHeuristicModule, Modules...>
	: search_next_data<RayScanVertex, modules::EuclideanHeuristicModule, Modules...>::type
{
	using self = search_data<RayScanVertex, modules::EuclideanHeuristicModule, Modules...>;
	using super = typename search_next_data<RayScanVertex, modules::EuclideanHeuristicModule, Modules...>::type;
	RAYSCANLIB_STRUCT_CHILD
	
	double target_dist;
	search_data() noexcept : target_dist(0) { }
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_HEURISTIC_EUCLIDEANHEURISTIC_HPP_INCLUDED
