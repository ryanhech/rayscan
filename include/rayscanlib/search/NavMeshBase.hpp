#ifndef RAYSCANLIB_SEARCH_NAVMESHBASE_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_NAVMESHBASE_HPP_INCLUDED

#include "SearchBase.hpp"
#include <rayscanlib/geometry/Polygon.hpp>
#include <rayscanlib/geometry/Box.hpp>
#include <rayscanlib/geometry/AngledSector.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <deque>
#include <unordered_map>
#include <chrono>

namespace rayscanlib::search
{

namespace modules {
template <typename CoordType>
struct NavMeshModule { };
} // namespace modules

namespace details
{

#define NAVMESH_DEFINE_TYPES \
	using coord = typename search_types::coord; \
	using point = typename search_types::point; \
	using polygon = typename search_types::polygon; \
	using basic_polygon = typename search_types::basic_polygon; \
	using box = typename search_types::box; \
	using vertex = typename search_types::vertex; \
	using obstacle = typename search_types::obstacle; \
	using face = typename search_types::face;

#define NAVMESH_DEFINE_TYPES_MODULES \
	SEARCH_DEFINE_TYPES_MODULES \
	NAVMESH_DEFINE_TYPES

#define NAVMESH_BASE_TYPES(baseType) \
	using coord = baseType::coord; \
	using point = baseType::point; \
	using polygon = baseType::polygon; \
	using basic_polygon = baseType::basic_polygon; \
	using box = baseType::box; \
	using vertex = baseType::vertex; \
	using obstacle = baseType::obstacle; \
	using face = baseType::face;

#define NAVMESH_INHERIT_TYPES(superType) \
	using typename superType::coord; \
	using typename superType::point; \
	using typename superType::polygon; \
	using typename superType::basic_polygon; \
	using typename superType::box; \
	using typename superType::vertex; \
	using typename superType::obstacle; \
	using typename superType::face;

#define NAVMESH_INHERIT_BASE_TYPES(superType) \
	SEARCH_INHERIT_BASE_TYPES(superType) \
	NAVMESH_INHERIT_TYPES(superType)

#define NAVMESH_BASE_CHILD \
	SEARCH_BASE_CHILD \
	NAVMESH_INHERIT_TYPES(super)

#define NAVMESH_STRUCT_CHILD \
	SEARCH_STRUCT_CHILD \
	NAVMESH_INHERIT_TYPES(super)

struct NavMeshVertex { };
struct NavMeshObstacle { };
struct NavMeshFace { };

template <typename CoordType, typename... Modules>
struct SearchTypes<modules::NavMeshModule<CoordType>, Modules...> : next_search_type<modules::NavMeshModule<CoordType>, Modules...>::type
{
	using coord = CoordType;
	using point = geometry::Point<CoordType>;
	using polygon = geometry::Polygon<CoordType>;
	using basic_polygon = polygon;
	using box = geometry::Box<CoordType>;
	using vertex = search_data<NavMeshVertex, void, Modules...>;
	using obstacle = search_data<NavMeshObstacle, void, Modules...>;
	using face = search_data<NavMeshFace, void, Modules...>;
};

} // namespace details

template <typename CoordType, typename... Modules>
class NavMeshBase : public search_next_module<modules::NavMeshModule<CoordType>, Modules...>::type
{
public:
	using self = NavMeshBase<CoordType, Modules...>;
	using super = typename search_next_module<modules::NavMeshModule<CoordType>, Modules...>::type;
	SEARCH_BASE_CHILD
	NAVMESH_DEFINE_TYPES

	template <typename Env>
	std::chrono::nanoseconds loadEnvironment(const Env& env)
	{
		auto tp1 = std::chrono::steady_clock::now();
		envInitalise();
		envSetBox(env.getEnclosureBox(1));
		envAddObstacle(&env.getEnclosure(), env.getEnclosure());
		for (auto& poly : env.getObstacles()) {
			envAddObstacle(&poly, poly);
		}
		envFinalise();
		auto tp2 = std::chrono::steady_clock::now();
		auto dur = tp2 - tp1;
		return dur;
	}

	void envSetBox(const box& b)
	{
		envSetBox_(b);
	}

	void envInitalise()
	{ }

	void envFinalise()
	{
		envFinalise_();
	}

	template <typename Crd>
	obstacle& envAddObstacle(const void* uid[[maybe_unused]], const geometry::Polygon<Crd>& poly)
	{
		return envAddObstacle_(polygon(poly.begin(), poly.end(), geometry::PolyType::None));
	}
	obstacle& envAddObstacle(const void* uid[[maybe_unused]], const basic_polygon& poly)
	{
		return envAddObstacle_(polygon(poly, geometry::PolyType::None));
	}

	void envRemoveObstacle(const void* uid[[maybe_unused]])
	{
		throw std::runtime_error("not implemented");
	}

protected:
	virtual void search_() = 0;
	
	virtual void envSetBox_(const box& b)
	{
		m_envBox = b;
	}

	virtual void envFinalise_()
	{
		constructMesh_();
	}
	
	void envFinaliseNull()
	{ }

	virtual obstacle& envAddObstacle_(polygon&& poly)
	{
		assert(!poly.isFinalised());
		obstacle& obst = m_obstacles.emplace_back();
		obst.shape.assign(std::move(poly), geometry::PolyType::SimplePolygon);
		assert(obst.shape.getType() == geometry::PolyType::SimplePolygon);
		obst.vertices = std::make_unique<vertex[]>(obst.shape.size());
		for (auto pt : obst.shape | boost::adaptors::indexed(0)) {
			auto& v = obst.vertices[pt.index()];
			v.id = pt.value();
			v.obstacle_ref = &obst;
			v.obstacle_id = pt.index();
		}
		if (obst.shape.orientation() == Dir::CCW) {
			obst.enclosure = true;
		//	obst.shape.finalise(geometry::PolyType::SimplePolygon);
		//	assert(obst.shape.getType() == geometry::PolyType::SimplePolygon);
		}
		return obst;
	}

	virtual void constructMesh_() = 0;

public:
	const std::deque<obstacle>& getObstacles() const noexcept { return m_obstacles; }
	const box& getEnvBox() const noexcept { return m_envBox; }
protected:
	std::deque<obstacle>& getObstacles_() noexcept { return m_obstacles; }

private:
	std::deque<obstacle> m_obstacles;
	box m_envBox;
};

namespace details {

template <typename CoordType, typename... Modules>
struct search_data<NavMeshVertex, modules::NavMeshModule<CoordType>, Modules...>
	: search_next_data<NavMeshVertex, modules::NavMeshModule<CoordType>, Modules...>::type
{
	using self = search_data<NavMeshVertex, modules::NavMeshModule<CoordType>, Modules...>;
	using super = typename search_next_data<NavMeshVertex, modules::NavMeshModule<CoordType>, Modules...>::type;
	SEARCH_STRUCT_CHILD
	NAVMESH_DEFINE_TYPES

	using face_angle = geometry::AngledSector<coord, Between::NARROW, Dir::CW>;
	struct FaceIncident : std::pair<face_angle, face*>
	{
		using std::pair<face_angle, face*>::pair;
		face_angle& a() noexcept { return this->first; }
		const face_angle& a() const noexcept { return this->first; }
		face& f() noexcept { return *this->second; }
		const face& f() const noexcept { return *this->second; }
	};
	
	point id;
	int32 obstacle_id;
	obstacle* obstacle_ref;
	std::vector<FaceIncident> trav_faces;

	search_data() : id{}, obstacle_id{}, obstacle_ref{} { }

	bool is_traversible_incident(face_angle as) const
	{
		auto [pv, at, nx] = obstacle_ref->shape.edge(obstacle_id);
		if (auto pvx = (pv - at).cross(as.template get<Dir::CW>()); point::isColin(pvx) && (pv - at).template isDir<Dir::FWD>(as.template get<Dir::CW>()))
			return false;
		else if (auto nxx = as.template get<Dir::CW>().cross(nx - at); point::isColin(nxx) && as.template get<Dir::CW>().template isDir<Dir::FWD>(nx - at))
			return true;
		else
			return at.isCW(pv, nx) ? point::isConjunctiveCW(pvx, nxx) : point::isDisjunctiveCW(pvx, nxx);
	}
};

template <typename CoordType, typename... Modules>
struct search_data<NavMeshObstacle, modules::NavMeshModule<CoordType>, Modules...>
	: search_next_data<NavMeshObstacle, modules::NavMeshModule<CoordType>, Modules...>::type
{
	using self = search_data<NavMeshObstacle, modules::NavMeshModule<CoordType>, Modules...>;
	using super = typename search_next_data<NavMeshObstacle, modules::NavMeshModule<CoordType>, Modules...>::type;
	SEARCH_STRUCT_CHILD
	NAVMESH_DEFINE_TYPES

	polygon shape;
	std::unique_ptr<vertex[]> vertices;
	bool enclosure;

	search_data() : enclosure(false) { }
};

template <typename CoordType, typename... Modules>
struct search_data<NavMeshFace, modules::NavMeshModule<CoordType>, Modules...>
	: search_next_data<NavMeshFace, modules::NavMeshModule<CoordType>, Modules...>::type
{
	using self = search_data<NavMeshFace, modules::NavMeshModule<CoordType>, Modules...>;
	using super = typename search_next_data<NavMeshFace, modules::NavMeshModule<CoordType>, Modules...>::type;
	SEARCH_STRUCT_CHILD
	NAVMESH_DEFINE_TYPES

	struct FaceEdge : std::pair<vertex*, face*>
	{
		using std::pair<vertex*, face*>::pair;
		vertex& v() noexcept { return *std::get<0>(*this); }
		const vertex& v() const noexcept { return *std::get<0>(*this); }
		face& f() noexcept { return *std::get<1>(*this); }
		const face& f() const noexcept { return *std::get<1>(*this); }
		bool has_face() const noexcept { return std::get<1>(*this) != nullptr; }
	};

	search_data() : traversable(true)
	{ }

	bool traversable;
	std::vector<FaceEdge> points;
};

template <typename CoordType, typename... Modules>
struct search_module<modules::NavMeshModule<CoordType>, Modules...>
{
	using type = NavMeshBase<CoordType, Modules...>;
};

} // namespace details

struct NavMeshStats
{
	details::SearchExpansion search; /// search id and expansion
	uint32 successorsFound; /// how many edges were discovered
	uint32 successorsPushed; /// how many edges found were undiscovered
	uint32 successorsUpdated; /// how many edges found were improved
	std::chrono::nanoseconds setupTime; /// time setup
	std::chrono::nanoseconds dynSetupTime; /// time search used
	std::chrono::nanoseconds searchTime; /// time search used
	uint32 rays, raysCached;
	NavMeshStats() noexcept : search{}, setupTime{}, dynSetupTime{} { clear(); }

	void clear() noexcept
	{
		successorsFound = 0;
		successorsPushed = 0;
		successorsUpdated = 0;
		searchTime = std::chrono::nanoseconds(0);
	}

	void pushSetupTime(std::chrono::nanoseconds t) noexcept
	{
		setupTime = t;
	}
	void pushDynSetupTime(std::chrono::nanoseconds t) noexcept
	{
		dynSetupTime = t;
	}
};

template <bool Hooks, typename CoordType, typename... Modules>
using NavMesh = details::SearchFinal<Hooks, Modules..., modules::NavMeshModule<CoordType>>;

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_NAVMESHBASE_HPP_INCLUDED
