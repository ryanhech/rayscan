#ifndef RAYSCANLIB_SEARCH_RAYSCANMULTITARGETSINGLE_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_RAYSCANMULTITARGETSINGLE_HPP_INCLUDED

#include "fwd.hpp"
#include "RayScanMultiTarget.hpp"

namespace rayscanlib::search
{

namespace modules {
struct RayScanMultiSingleTargetModule : SingleTargetTag { };
} // namespace modules

template <typename... Modules>
class RayScanMultiSingleTargetBase : public search_next_module<modules::RayScanMultiSingleTargetModule, Modules...>::type
{
public:
	using self = RayScanMultiSingleTargetBase<Modules...>;
	using super = typename search_next_module<modules::RayScanMultiSingleTargetModule, Modules...>::type;
	RAYSCANLIB_BASE_CHILD

	RayScanMultiSingleTargetBase() : m_targetId(0), m_searchComplete(false)
	{ }

	virtual void setupTarget(point pt)
	{
		auto& target = this->getMultiTarget_()[m_targetId];
		target.id = pt;
		target.search.s = {this->getStats().search.s.id, 0};
		target.target = true;
		target.open = target.close = false;
		target.f = target.g = inf<double>;
		target.h = 0;
		target.pred = nullptr;
	}

protected:
	virtual void pushTarget_(double g)
	{
		auto tf = this->getTargetsFound();
		this->pushMultiTarget_(m_targetId, g);
		if (tf != this->getTargetsFound())
			m_searchComplete = true;
	}

	void search_() override
	{
		uint32 targets = static_cast<uint32>(this->getMultiTarget().size());

		for (m_targetId = 0; m_targetId < targets; m_targetId++) {
			setupSearchST_();
			m_searchComplete = false;
			this->astarQueue_().clear();
			this->setupStart(this->getStart_().id);
			setupTarget(this->getMultiTarget()[m_targetId].id);
			this->expandStartNode_(this->getStart_());

			while (!m_searchComplete && !this->astarQueue_().empty()) {
				vertex& expand = this->astarQueue_().top();
				expand.close = true;
				if (expand.target) {
					m_searchComplete = true;
					break;
				}
				this->astarQueue_().pop();
				this->expandNode_(expand);
			}
		}
	}
	virtual void setupSearchST_()
	{
		this->getStats_().search.s.id += 1;
	}
	void setupSearchMT_() override
	{
		super::setupSearchMT_();
		this->getStats_().search.s.id -= 1;
	}

public:
	uint32 getTargetID() const noexcept { return m_targetId; }
	const vertex& getTarget() const noexcept { return this->getMultiTarget()[m_targetId]; }
	bool getSearchComplete() const noexcept { return m_searchComplete; }

protected:
	void setTargetID_(uint32 targetId) noexcept { m_targetId = targetId; }
	vertex& getTarget_() noexcept { return this->getMultiTarget_()[m_targetId]; }
	void setSearchComplete_(bool searchComplete) noexcept { m_searchComplete = searchComplete; }

private:
	uint32 m_targetId;
	bool m_searchComplete;
};

namespace details
{

template <typename... Modules>
struct search_module<modules::RayScanMultiSingleTargetModule, Modules...>
{
	using type = RayScanMultiSingleTargetBase<Modules...>;
};

} // namespace details

template <bool Hooks, typename CoordType, typename... Modules>
using RayScanMultiSingleTarget = RayScan<Hooks, CoordType, Modules..., modules::RayScanMultiSingleTargetModule>;

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_RAYSCANMULTITARGETSINGLE_HPP_INCLUDED
