#ifndef RAYSCANLIB_SEARCH_SCAN_CONVEXSCANNER_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_SCAN_CONVEXSCANNER_HPP_INCLUDED

#include "Scanners.hpp"
#include <rayscanlib/search/modulator/BlockingMod.hpp>
#include <rayscanlib/search/modulator/BypassMod.hpp>
#include <rayscanlib/search/modulator/RotatorMod.hpp>

namespace rayscanlib::search
{

namespace modules {
template <size_t DepthLimit>
struct ConvexScannerModule { };
} // namespace modules

namespace scan {

template <size_t DepthLimit, typename... Modules>
class ConvexScanner : public search_next_module<modules::ConvexScannerModule<DepthLimit>, Modules...>::type
{
public:
	using self = ConvexScanner<DepthLimit, Modules...>;
	using super = typename search_next_module<modules::ConvexScannerModule<DepthLimit>, Modules...>::type;
	RAYSCANLIB_BASE_CHILD

	ConvexScanner() = default;

protected:
	template <bool TargetWithin, typename Iter, typename Field>
	void beginStartScanning_(obstacle* obstruction, Iter it, point ray, Field field)
	{
		assert(obstruction != nullptr);
		m_depth.reset();
		if constexpr (search_types::hooks_enabled()) {
			this->template use_hook_<hooks::BeginFullScanHook>(Field::turn_dir(), ray, std::as_const(*obstruction), it.index(), field);
			scan_<true, false, TargetWithin>(obstruction, it, ray, field);
			this->template use_hook_<hooks::EndFullScanHook>();
		} else {
			scan_<true, false, TargetWithin>(obstruction, it, ray, field);
		}
	}
	template <bool TargetWithin, typename Iter, typename Field>
	point beginStartScanningFrontier_(obstacle* obstruction, Iter it, point ray, Field field)
	{
		assert(obstruction != nullptr);
		m_depth.reset();
		if constexpr (search_types::hooks_enabled()) {
			this->template use_hook_<hooks::BeginFullScanHook>(Field::turn_dir(), ray, std::as_const(*obstruction), it.index(), field);
			point pt = scan_<true, true, TargetWithin>(obstruction, it, ray, field);
			this->template use_hook_<hooks::EndFullScanHook>();
			return pt;
		} else {
			return scan_<true, true, TargetWithin>(obstruction, it, ray, field);
		}
	}
	
	template <bool TargetWithin, typename Iter, typename Field>
	void beginScanning_(obstacle* obstruction, Iter it, point ray, Field field)
	{
		assert(obstruction != nullptr);
		m_depth.reset();
		if constexpr (search_types::hooks_enabled()) {
			this->template use_hook_<hooks::BeginFullScanHook>(Field::turn_dir(), ray, std::as_const(*obstruction), it.index(), field);
			scan_<false, false, TargetWithin>(obstruction, it, ray, field);
			this->template use_hook_<hooks::EndFullScanHook>();
		} else {
			scan_<false, false, TargetWithin>(obstruction, it, ray, field);
		}
	}
	template <bool TargetWithin, typename Iter, typename Field>
	point beginScanningFrontier_(obstacle* obstruction, Iter it, point ray, Field field)
	{
		assert(obstruction != nullptr);
		m_depth.reset();
		if constexpr (search_types::hooks_enabled()) {
			this->template use_hook_<hooks::BeginFullScanHook>(Field::turn_dir(), ray, std::as_const(*obstruction), it.index(), field);
			point pt = scan_<false, true, TargetWithin>(obstruction, it, ray, field);
			this->template use_hook_<hooks::EndFullScanHook>();
			return pt;
		} else {
			return scan_<false, true, TargetWithin>(obstruction, it, ray, field);
		}
	}

private:
	template <bool StartNode, bool Frontier, bool TargetWithin, Dir D, Between Bet>
	std::conditional_t<Frontier, point, void> scan_(obstacle* obstruction, iter<D> it, point scanline, geometry::AngledSector<coord, Bet, D> field)
	{
		constexpr bool enable_blocking = TargetWithin && has_module<modules::BlockingModule>();
		constexpr bool enable_bypass = has_module<modules::BypassModules>();
		struct Functor
		{
			static int32 repeat(const vertex& current[[maybe_unused]], const obstacle& obstruction, uint32 index)
			{
				uint32 ans = obstruction.shape.size();
				if (obstruction.enclosure) {
					if constexpr (D == Dir::CW) {
						ans = (ans + index - obstruction.shape.caveLowerBound(index)) % ans;
					} else {
						ans = (ans + obstruction.shape.caveUpperBound(index) - index) % ans;
					}
				}
				if constexpr (!StartNode) {
					if (current.ref.getObstacle() == &obstruction) {
						uint32 ans2 = obstruction.shape.size();
						if constexpr (D == Dir::CW) {
							ans2 = (ans2 + index - static_cast<uint32>(current.ref.getIndex())) % ans2;
						} else {
							ans2 = (ans2 + static_cast<uint32>(current.ref.getIndex()) - index) % ans2;
						}
						return static_cast<int32>(std::min(ans, ans2));
					}
				}
				return static_cast<int32>(ans);
			}
		};

		assert(obstruction != nullptr && !scanline.isZero());
		auto depthlck [[maybe_unused]] = m_depth.lock();
		if constexpr (DepthLimit != 0) {
			if (!m_depth) {
				throw std::runtime_error("max depth reached");
			}
		}

		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::BeginRecurseScanHook>(D, scanline, std::as_const(*obstruction), it.index(), field); }

		int32 repeat = Functor::repeat(*this->getCurrent(), *obstruction, it.index());
		point frontierRay[[maybe_unused]](point::zero());
		point origin(this->getCurrent()->id);
		auto [pv, at, nx] = it.edge_fwd(); ++it;
		pv -= origin; at -= origin; nx -= origin;

		bool reverse = false;
		while (repeat-- > 0) {
			assert(pv != at && at != nx);
			// skip target if enabled
			if constexpr (enable_blocking) {
				if (pv.template isDir<D>(at))
					this->template checkTargetIsBlocked<StartNode, D>(pv, at, nx);
			}

			if constexpr (StartNode) { // only the start node will arrive at origin point
				if (at.isZero()) { // corner case of at == origin
					assert(!nx.isZero());
					assert(!reverse); // reverse can not happen before passing origin
					if (!nx.template isDir<D>(scanline)) { // narrow
						if (!field.template within<D>(scanline, nx))
							break;
					} else { // wide
						if (!field.template within<D>(scanline, -scanline) || !field.template within<D>(-scanline, nx))
							break;
					}
					if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::ProgressScanLineHook>(*std::prev(it, 2), at + origin, nx + origin, scanline, at.isZero() ? nx : at); }
					scanline = nx; pv = point::zero(); at = nx; nx = *++it - origin;
					continue;
				}
			}
			assert(!at.isZero());
			assert(field.within(scanline));
			if constexpr (Bet == Between::NARROW) { // if NARROW, will never have a SELF intersection
				if (!reverse) {
					if (!field.template within<D>(scanline, at)) // at left field
						break;
				} else {
					assert(field.template within<inv_dir(D)>(scanline, at)); // we know it can not leave the field
				}
			} else {
				if (!reverse) {
					if (!at.template isDir<D>(scanline)) { // narrow
						if (!field.template within<D>(scanline, at))
							break;
					} else { // wide
						if (!field.template within<D>(scanline, -scanline) || !field.template within<D>(-scanline, at))
							break;
					}
				} else {
					assert(field.within(at)); // we know it can not leave the field
				}
			}
			if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::ProgressScanLineHook>(*std::prev(it, 2), at + origin, nx + origin, scanline, at.isZero() ? nx : at); }
			assert(field.within(at));

			if (at.template isDir<D>(nx, pv)) {
				reverse = nx.template isDir<D>(at); // concave point, pass over
			} else if (reverse != nx.template isDir<D>(at)) { // turning point
				// bypass check
				auto& node = obstruction->vertices[std::prev(it).index()];
				this->initNode(node);
				assert(node.id == origin + at);
				// skipper check
				if constexpr (enable_bypass) {
					if (this->doneRayNode(node)) // refine by ray
						break;
					bool skip = self::has_skip() && !StartNode ? this->getCurrent()->g + at.length() >= node.g : false;
					if (!reverse) {
						typename self::SkipAmount amt = this->template canBypass<D>(origin, at, nx, it, field, skip);
						if (amt.can_skip()) {
							assert(skip || amt.amt >= 0); // never allow no skip and negative amount
							if (skip) // the node is skipped
								this->storeRayNode_(node, intersection{});
							// otherwise the node is bypassed
							it += amt.skip();
							scanline = at;
							std::tie(pv, at, nx) = inx::to_tuple(it.edge_fwd());
							pv -= origin; at -= origin; nx -= origin;
							repeat = Functor::repeat(*this->getCurrent(), *obstruction, it.index()); // bypass can skip an enclosure, thus re-calc repeat
							++it;
							assert(!scanline.template isDir<inv_dir(D)>(at));
							continue;
						}
					} else {
						if (skip) {
							// if skip, no need to check as we know we can continue past node
							this->storeRayNode_(node, intersection{});
							scanline = at, pv = at, at = nx, nx = *++it - origin;
							reverse = false;
							continue;
						} else if constexpr (self::has_bypass()) {
							if (this->template canBypass<inv_dir(D)>(origin, at, pv, std::prev(it, 2).invert(), field.template makeDir<inv_dir(D)>(), false).can_skip())
							{
								// bypass still requires to check for target within, though if not then proper check for bypass
								scanline = at, pv = at, at = nx, nx = *++it - origin;
								reverse = false;
								continue;
							}
						}
					}
				}
				bool rayShot = this->castRayNode(*this->getCurrent(), node);
				intersection I = node.expansionIntersect;
				assert(Bet != Between::NARROW || I.getRayInt() >= 0); // will never self intersetion on NARROW
				if constexpr (Frontier)
					frontierRay = at;
				if (!rayShot)
					break;
				if (I.getRayInt() > 0) { // overshot, at is a potential successor, push it
					this->pushSuccessor_(node, this->getCurrent()->g + at.length(), I, reverse ? D : inv_dir(D));
				}
				if (I.getRayInt() <= 0 || reverse) { // blocked, at is blocked and thus a reverse scan is required
					if (!reverse)
						scan_<StartNode, false, TargetWithin>(I.getObstruction(), I.template getIter<inv_dir(D)>(), at, field.template split<inv_dir(D)>(at));
					else
						scan_<StartNode, false, TargetWithin>(I.getObstruction(), I.template getIter<inv_dir(D)>(), at, field.template split<inv_dir(D), D>(at));
				}
				// continue the scan from obstruction
				reverse &= (I.getRayInt() > 0);
				if constexpr (DepthLimit != 0 || Bet == Between::WHOLE || search_types::hooks_enabled()) { // if a depth limit is applied, recurse, otherwise say within same function
					point r[[maybe_unused]];
					if (Bet != Between::WHOLE || !reverse)
						r = scan_<StartNode, true, TargetWithin>(reverse ? obstruction : I.getObstruction(), reverse ? it : I.template getIter<D>(), at, field.template split<D>(at));
					else
						r = scan_<StartNode, true, TargetWithin>(reverse ? obstruction : I.getObstruction(), reverse ? it : I.template getIter<D>(), at, field.template split<D, inv_dir(D)>(at));
					if constexpr (Frontier) {
						if (!r.isZero())
							frontierRay = r;
					}
					break;
				} else {
					// update scan to reflect next recursion without needed to recurse
					field = field.template split<D>(at);
					if (!reverse) {
						obstruction = I.getObstruction();
						it = I.template getIter<D>();
						repeat = Functor::repeat(*this->getCurrent(), *obstruction, it.index());
						scanline = at;
						std::tie(pv, at, nx) = inx::to_tuple(it.edge_fwd()); ++it;
						pv -= origin; at -= origin; nx -= origin;
						assert(!reverse);
						continue;
					} else {
						reverse = false;
					}
					assert(obstruction != nullptr && !scanline.isZero());
				}
			}
			// no successor found, continue
			scanline = at, pv = at, at = nx, nx = *++it - origin;
		}
		
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::EndRecurseScanHook>(at); }

		if constexpr (Frontier)
			return frontierRay;
	}

protected:
	DepthLimiter<DepthLimit> m_depth;
};

} // namespace scan

namespace details
{

template <size_t DepthLimit, typename... Modules>
struct search_module<modules::ConvexScannerModule<DepthLimit>, Modules...>
{
	using type = scan::ConvexScanner<DepthLimit, Modules...>;
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_SCAN_CONVEXSCANNER_HPP_INCLUDED
