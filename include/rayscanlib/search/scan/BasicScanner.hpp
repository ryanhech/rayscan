#ifndef RAYSCANLIB_SEARCH_SCAN_BASICSCANNER_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_SCAN_BASICSCANNER_HPP_INCLUDED

#include "Scanners.hpp"

namespace rayscanlib::search
{

namespace modules {
template <size_t DepthLimit>
struct BasicScannerModule { };
} // namespace modules

namespace scan {

template <size_t DepthLimit, typename... Modules>
class BasicScanner : public search_next_module<modules::BasicScannerModule<DepthLimit>, Modules...>::type
{
public:
	using self = BasicScanner<DepthLimit, Modules...>;
	using super = typename search_next_module<modules::BasicScannerModule<DepthLimit>, Modules...>::type;
	RAYSCANLIB_BASE_CHILD

	BasicScanner() = default;

protected:
	template <bool TargetWithin[[mabye_unused]], Dir D>
	void beginStartScanning_(obstacle* obstruction, iter<D> it, point ray, geometry::AngledSector<coord, Between::WHOLE, D> field)
	{
		beginScanningImpl_(obstruction, it, ray, field);
	}
	template <bool TargetWithin[[mabye_unused]], Dir D>
	point beginStartScanningFrontier_(obstacle* obstruction, iter<D> it, point ray, geometry::AngledSector<coord, Between::WHOLE, D> field)
	{
		return beginScanningImpl_(obstruction, it, ray, field);
	}

	template <bool TargetWithin[[maybe_unused]], Dir D>
	void beginScanning_(obstacle* obstruction, iter<D> it, point ray, geometry::AngledSector<coord, Between::WHOLE, D> field)
	{
		beginScanningImpl_(obstruction, it, ray, field);
	}
	template <bool TargetWithin[[maybe_unused]], Dir D>
	point beginScanningFrontier_(obstacle* obstruction, iter<D> it, point ray, geometry::AngledSector<coord, Between::WHOLE, D> field)
	{
		return beginScanningImpl_(obstruction, it, ray, field);
	}

private:
	template <Dir D>
	point beginScanningImpl_(obstacle* obstruction, iter<D> it, point ray, geometry::AngledSector<coord, Between::WHOLE, D> field)
	{
		assert(obstruction != nullptr);
		m_depth.reset();
		if constexpr (search_types::hooks_enabled()) {
			this->template use_hook_<hooks::BeginFullScanHook>(D, ray, std::as_const(*obstruction), it.index(), field);
			point pt = scan_(obstruction, it, ray, field);
			this->template use_hook_<hooks::EndFullScanHook>();
			return pt;
		} else {
			return scan_(obstruction, it, ray, field);
		}
	}

	template <Dir D>
	point scan_(obstacle* obstruction, iter<D> it, point scanline, geometry::AngledSector<coord, Between::WHOLE, D> field)
	{
		struct Functor
		{
			static int32 repeat(const vertex& current[[maybe_unused]], const obstacle& obstruction, uint32 index)
			{
				uint32 ans = obstruction.shape.size();
				if (obstruction.enclosure) {
					if constexpr (D == Dir::CW) {
						ans = (ans + index - obstruction.shape.caveLowerBound(index)) % ans;
					} else {
						ans = (ans + obstruction.shape.caveUpperBound(index) - index) % ans;
					}
				}
				if (current.ref.getObstacle() == &obstruction) {
					uint32 ans2 = obstruction.shape.size();
					if constexpr (D == Dir::CW) {
						ans2 = (ans2 + index - static_cast<uint32>(current.ref.getIndex())) % ans2;
					} else {
						ans2 = (ans2 + static_cast<uint32>(current.ref.getIndex()) - index) % ans2;
					}
					return static_cast<int32>(std::min(ans, ans2));
				}
				return static_cast<int32>(ans);
			}
		};

		assert(obstruction != nullptr && !scanline.isZero());
		auto depthlck [[maybe_unused]] = m_depth.lock();
		if constexpr (DepthLimit != 0) {
			if (!m_depth) {
				throw std::runtime_error("max depth reached");
			}
		}

		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::BeginRecurseScanHook>(D, scanline, std::as_const(*obstruction), it.index(), field); }

		int32 repeat = Functor::repeat(*this->getCurrent(), *obstruction, it.index());
		point frontierRay = point::zero();
		point origin(this->getCurrent()->id), pv(*std::prev(it) - origin), at(*it - origin), nx(*++it - origin);
		point ut;
		if constexpr (has_module<modules::SingleTargetTag>())
			ut = this->getTarget().id - origin;
		while (repeat-- > 0) {
			if (at.isZero()) { // corner case of at == origin
				assert(!nx.isZero());
				if (!nx.template isDir<D>(scanline)) { // narrow
					if (!field.template within<D>(scanline, nx))
						break;
				} else { // wide
					if (!field.template within<D>(scanline, -scanline) || !field.template within<D>(-scanline, nx))
						break;
				}
				if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::ProgressScanLineHook>(*std::prev(it, 2), at + origin, nx + origin, scanline, at.isZero() ? nx : at); }
				scanline = nx; pv = at, at = nx; nx = *++it - origin;
				continue;
			}
			assert(!at.isZero());
			assert(field.within(scanline));
			if (!at.template isDir<D>(scanline)) { // narrow
				if (!field.template within<D>(scanline, at))
					break;
			} else { // wide
				if (!field.template within<D>(scanline, -scanline) || !field.template within<D>(-scanline, at))
					break;
			}
			if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::ProgressScanLineHook>(*std::prev(it, 2), at + origin, nx + origin, scanline, at.isZero() ? nx : at); }
			assert(field.within(at));
			if (nx.template isDir<D>(at)) { // turning point
				auto& node = obstruction->vertices[(--it).index()];
				this->initNode(node);
				assert(node.id == origin + at);
				bool rayShot = this->castRayNode(*this->getCurrent(), node);
				intersection I = node.expansionIntersect;
				frontierRay = at;
				if (!rayShot)
					break;
				if (I.getRayInt() > 0) { // overshot, at is a potential successor, push it
					this->pushSuccessor_(node, this->getCurrent()->g + at.length(), I, inv_dir(D));
				} else { // blocked, at is blocked and thus a reverse scan is required
					scan_(I.getObstruction(), I.template getIter<inv_dir(D)>(), at, field.template split<inv_dir(D)>(at));
				}
				// continue the scan
				if constexpr (DepthLimit != 0 || search_types::hooks_enabled()) { // if a depth limit is applied, recurse, otherwise say within same function
					point r = scan_(I.getObstruction(), I.template getIter<D>(), at, field.template split<D>(at));
					if (!r.isZero())
						frontierRay = r;
					break;
				} else {
					// update scan to reflect next recursion without needed to recurse
					obstruction = I.getObstruction();
					it = I.template getIter<D>();
					scanline = at;
					field = field.template split<D>(at);
					assert(obstruction != nullptr && !scanline.isZero());
					repeat = Functor::repeat(*this->getCurrent(), *obstruction, it.index());
					pv = *std::prev(it) - origin;
					at = *it - origin;
					nx = *++it - origin;
					continue;
				}
			}
			// no successor found, continue
			scanline = at, pv = at, at = nx, nx = *++it - origin;
		}
		
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::EndRecurseScanHook>(at); }

		return frontierRay;
	}

protected:
	DepthLimiter<DepthLimit> m_depth;
};

} // namespace scan

namespace details
{

template <size_t DepthLimit, typename... Modules>
struct search_module<modules::BasicScannerModule<DepthLimit>, Modules...>
{
	using type = scan::BasicScanner<DepthLimit, Modules...>;
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_SCAN_BASICSCANNER_HPP_INCLUDED
