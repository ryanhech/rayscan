#ifndef RAYSCANLIB_SEARCH_SCAN_SCANNERS_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_SCAN_SCANNERS_HPP_INCLUDED

#include "../RayScanBase.hpp"
#include <inx/tuple.hpp>

namespace rayscanlib::search
{

namespace hooks {
struct BeginFullScanHook { };
struct EndFullScanHook { };
struct BeginRecurseScanHook { };
struct ProgressScanLineHook { };
struct EndRecurseScanHook { };
} // namespace hooks

namespace details {

SEARCH_HOOK_DEFINE(BeginFullScanHook,void(Dir orientation, typename hook::point rayDir, const typename hook::obstacle& obsticle, size_t obstIndex, typename geometry::AngledSector<typename hook::coord, Between::WHOLE, Dir::CW> field))
SEARCH_HOOK_DEFINE(EndFullScanHook,void())
SEARCH_HOOK_DEFINE(BeginRecurseScanHook,void(Dir orientation, typename hook::point rayDir, const typename hook::obstacle& obsticle, size_t obstIndex, typename geometry::AngledSector<typename hook::coord, Between::WHOLE, Dir::CW> field))
SEARCH_HOOK_DEFINE(ProgressScanLineHook,void(typename hook::point pv, typename hook::point at, typename hook::point nx, typename hook::point line_from, typename hook::point line_to))
SEARCH_HOOK_DEFINE(EndRecurseScanHook,void(typename hook::point rayDir))

} // namespace details

namespace scan {
	
namespace details {

template <size_t Limit>
struct DepthLimiter
{
	constexpr static size_t limit() noexcept { return Limit; }
	struct autopop
	{
		DepthLimiter& limiter;
		autopop(DepthLimiter& l_limiter) noexcept : limiter(l_limiter) { }
		~autopop() noexcept { limiter.pop(); }
	};
	size_t depth;
	DepthLimiter() noexcept : depth(0)
	{ };
	void reset() noexcept { depth = 0; }
	void push() noexcept { depth += 1; }
	void pop() noexcept { depth -= 1; }
	autopop lock() noexcept { push(); return autopop(*this); }
	operator bool() const noexcept { return depth <= limit(); }
	bool operator!() const noexcept { return depth > limit(); }
};
struct DepthLimiterOff
{
	struct autopop
	{ };
	constexpr DepthLimiterOff() = default;
	constexpr void reset() noexcept { }
	constexpr void push() noexcept { }
	constexpr void pop() noexcept { }
	constexpr autopop lock() noexcept { return autopop{}; }
	constexpr operator bool() const noexcept { return true; }
	constexpr bool operator!() const noexcept { return false; }
};

} // namespace details

template <size_t Limit>
using DepthLimiter = std::conditional_t<Limit != 0, details::DepthLimiter<Limit>, details::DepthLimiterOff>;

} // namespace scan

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_SCAN_BASICSCANNER_HPP_INCLUDED
