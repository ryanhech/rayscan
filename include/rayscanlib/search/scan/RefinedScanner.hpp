#ifndef RAYSCANLIB_SEARCH_SCAN_REFINEDSCANNER_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_SCAN_REFINEDSCANNER_HPP_INCLUDED

#include "Scanners.hpp"
#include <rayscanlib/search/modulator/BlockingMod.hpp>

namespace rayscanlib::search
{

namespace modules {
template <size_t DepthLimit>
struct RefinedScannerModule { };
} // namespace modules

namespace scan {

template <size_t DepthLimit, typename... Modules>
class RefinedScanner : public search_next_module<modules::RefinedScannerModule<DepthLimit>, Modules...>::type
{
public:
	using self = RefinedScanner<DepthLimit, Modules...>;
	using super = typename search_next_module<modules::RefinedScannerModule<DepthLimit>, Modules...>::type;
	RAYSCANLIB_BASE_CHILD

	RefinedScanner() = default;

protected:
	template <bool TargetWithin, typename Iter, typename Field>
	void beginStartScanning_(obstacle* obstruction, Iter it, point ray, Field field)
	{
		assert(obstruction != nullptr);
		m_depth.reset();
		if constexpr (search_types::hooks_enabled()) {
			this->template use_hook_<hooks::BeginFullScanHook>(Field::turn_dir(), ray, std::as_const(*obstruction), it.index(), field);
			scan_<true, false, TargetWithin>(obstruction, it, ray, field);
			this->template use_hook_<hooks::EndFullScanHook>();
		} else {
			scan_<true, false, TargetWithin>(obstruction, it, ray, field);
		}
	}
	template <bool TargetWithin, typename Iter, typename Field>
	point beginStartScanningFrontier_(obstacle* obstruction, Iter it, point ray, Field field)
	{
		assert(obstruction != nullptr);
		m_depth.reset();
		if constexpr (search_types::hooks_enabled()) {
			this->template use_hook_<hooks::BeginFullScanHook>(Field::turn_dir(), ray, std::as_const(*obstruction), it.index(), field);
			point pt = scan_<true, true, TargetWithin>(obstruction, it, ray, field);
			this->template use_hook_<hooks::EndFullScanHook>();
			return pt;
		} else {
			return scan_<true, true, TargetWithin>(obstruction, it, ray, field);
		}
	}
	
	template <bool TargetWithin, typename Iter, typename Field>
	void beginScanning_(obstacle* obstruction, Iter it, point ray, Field field)
	{
		assert(obstruction != nullptr);
		m_depth.reset();
		if constexpr (search_types::hooks_enabled()) {
			this->template use_hook_<hooks::BeginFullScanHook>(Field::turn_dir(), ray, std::as_const(*obstruction), it.index(), field);
			scan_<false, false, TargetWithin>(obstruction, it, ray, field);
			this->template use_hook_<hooks::EndFullScanHook>();
		} else {
			scan_<false, false, TargetWithin>(obstruction, it, ray, field);
		}
	}
	template <bool TargetWithin, typename Iter, typename Field>
	point beginScanningFrontier_(obstacle* obstruction, Iter it, point ray, Field field)
	{
		assert(obstruction != nullptr);
		m_depth.reset();
		if constexpr (search_types::hooks_enabled()) {
			this->template use_hook_<hooks::BeginFullScanHook>(Field::turn_dir(), ray, std::as_const(*obstruction), it.index(), field);
			point pt = scan_<false, true, TargetWithin>(obstruction, it, ray, field);
			this->template use_hook_<hooks::EndFullScanHook>();
			return pt;
		} else {
			return scan_<false, true, TargetWithin>(obstruction, it, ray, field);
		}
	}

private:
	template <bool StartNode, bool Frontier, bool TargetWithin, Dir D, Between Bet>
	std::conditional_t<Frontier, point, void> scan_(obstacle* obstruction, iter<D> it, point scanline, geometry::AngledSector<coord, Bet, D> field)
	{
		constexpr bool enable_blocking = TargetWithin && has_module<modules::BlockingModule>();
		struct Functor
		{
			static int32 repeat(const vertex& current[[maybe_unused]], const obstacle& obstruction, uint32 index)
			{
				uint32 ans = obstruction.shape.size();
				if (obstruction.enclosure) {
					if constexpr (D == Dir::CW) {
						ans = (ans + index - obstruction.shape.caveLowerBound(index)) % ans;
					} else {
						ans = (ans + obstruction.shape.caveUpperBound(index) - index) % ans;
					}
				}
				if (!StartNode) {
					if (current.ref.getObstacle() == &obstruction) {
						uint32 ans2 = obstruction.shape.size();
						if constexpr (D == Dir::CW) {
							ans2 = (ans2 + index - static_cast<uint32>(current.ref.getIndex())) % ans2;
						} else {
							ans2 = (ans2 + static_cast<uint32>(current.ref.getIndex()) - index) % ans2;
						}
						return static_cast<int32>(std::min(ans, ans2));
					}
				}
				return static_cast<int32>(ans);
			}
		};

		assert(obstruction != nullptr && !scanline.isZero());
		auto depthlck [[maybe_unused]] = m_depth.lock();
		if constexpr (DepthLimit != 0) {
			if (!m_depth) {
				throw std::runtime_error("max depth reached");
			}
		}

		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::BeginRecurseScanHook>(D, scanline, std::as_const(*obstruction), it.index(), field); }

		int32 repeat = Functor::repeat(*this->getCurrent(), *obstruction, it.index());
		point frontierRay [[maybe_unused]] = point::zero();
		point origin(this->getCurrent()->id), pv(*std::prev(it) - origin), at(*it - origin), nx(*++it - origin);
		
		while (repeat-- > 0) {
			// skip target if enabled
			if constexpr (enable_blocking) {
				if (pv.template isDir<D>(at))
					this->template checkTargetIsBlocked<StartNode, D>(pv, at, nx);
			}

			if constexpr (StartNode) { // only the start node will arrive at origin point
				if (at.isZero()) { // corner case of at == origin
					assert(!nx.isZero());
					if (!nx.template isDir<D>(scanline)) { // narrow
						if (!field.template within<D>(scanline, nx))
							break;
					} else { // wide
						if (!field.template within<D>(scanline, -scanline) || !field.template within<D>(-scanline, nx))
							break;
					}
					if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::ProgressScanLineHook>(*std::prev(it, 2), at + origin, nx + origin, scanline, at.isZero() ? nx : at); }
					scanline = nx; pv = point::zero(); at = nx; nx = *++it - origin;
					continue;
				}
			}
			assert(!at.isZero());
			assert(field.within(scanline));
			if constexpr (Bet == Between::NARROW) { // if NARROW, will never have a SELF intersection
				if (!field.template within<D>(scanline, at)) { // at left field
					break;
				}
			} else {
				if (!at.template isDir<D>(scanline)) { // narrow
					if (!field.template within<D>(scanline, at))
						break;
				} else { // wide
					if (!field.template within<D>(scanline, -scanline) || !field.template within<D>(-scanline, at))
						break;
				}
			}
			if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::ProgressScanLineHook>(*std::prev(it, 2), at + origin, nx + origin, scanline, at.isZero() ? nx : at); }
			assert(field.within(at));

			if (nx.template isDir<D>(at)) { // turning point
				auto& node = obstruction->vertices[(--it).index()];
				this->initNode(node);
				assert(node.id == origin + at);
				bool rayShot = this->castRayNode(*this->getCurrent(), node);
				intersection I = node.expansionIntersect;
				assert(Bet != Between::NARROW || I.getRayInt() >= 0); // will never self intersetion on NARROW
				if constexpr (Frontier)
					frontierRay = at;
				if (!rayShot)
					break;
				if (I.getRayInt() > 0) { // overshot, at is a potential successor, push it
					this->pushSuccessor_(node, this->getCurrent()->g + at.length(), I, inv_dir(D));
				} else { // blocked, at is blocked and thus a reverse scan is required
					scan_<StartNode, false, TargetWithin>(I.getObstruction(), I.template getIter<inv_dir(D)>(), at, field.template split<inv_dir(D)>(at));
				}
				// continue the scan
				if constexpr (DepthLimit != 0 || search_types::hooks_enabled()) { // if a depth limit is applied, recurse, otherwise say within same function
					if constexpr (Frontier) {
						point r = scan_<StartNode, true, TargetWithin>(I.getObstruction(), I.template getIter<D>(), at, field.template split<D>(at));
						if (!r.isZero())
							frontierRay = r;
					} else
						scan_<StartNode, false, TargetWithin>(I.getObstruction(), I.template getIter<D>(), at, field.template split<D>(at));
					break;
				} else {
					// update scan to reflect next recursion without needed to recurse
					obstruction = I.getObstruction();
					it = I.template getIter<D>();
					scanline = at;
					field = field.template split<D>(at);
					assert(obstruction != nullptr && !scanline.isZero());
					repeat = Functor::repeat(*this->getCurrent(), *obstruction, it.index());
					pv = *std::prev(it) - origin;
					at = *it - origin;
					nx = *++it - origin;
					continue;
				}
			}
			// no successor found, continue
			scanline = at, pv = at, at = nx, nx = *++it - origin;
		}
		
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::EndRecurseScanHook>(at); }

		if constexpr (Frontier)
			return frontierRay;
	}

protected:
	DepthLimiter<DepthLimit> m_depth;
};

} // namespace scan

namespace details
{

template <size_t DepthLimit, typename... Modules>
struct search_module<modules::RefinedScannerModule<DepthLimit>, Modules...>
{
	using type = scan::RefinedScanner<DepthLimit, Modules...>;
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_SCAN_REFINEDSCANNER_HPP_INCLUDED
