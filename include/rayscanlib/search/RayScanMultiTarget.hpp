#ifndef RAYSCANLIB_SEARCH_RAYSCANMULTITARGET_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_RAYSCANMULTITARGET_HPP_INCLUDED

#include "fwd.hpp"
#include "RayScanBase.hpp"
#include "RayScanHooks.hpp"
#include <rayscanlib/geometry/AngledSector.hpp>
#include <rayscanlib/utils/StatAverager.hpp>
#include <inx/alg/paring_heap.hpp>
#include <vector>
#include <iterator>
#include <chrono>
#include <numeric>
#include <cmath>
#include <optional>

namespace rayscanlib::search
{

namespace modules {
struct RayScanMultiTargetModule : MultiTargetTag { };
} // namespace modules

namespace details {
struct RayScanMultiTargetStats : RayScanStats
{
	uint32 raysCached; /// how many rays were casted
	RayScanMultiTargetStats() noexcept { raysCached = 0; }
	void clear() noexcept
	{
		RayScanStats::clear();
		raysCached = 0;
	}
};
} // namespace details

template <typename... Modules>
class RayScanMultiTargetBase : public search_next_module<modules::RayScanMultiTargetModule, Modules...>::type
{
public:
	using self = RayScanMultiTargetBase<Modules...>;
	using super = typename search_next_module<modules::RayScanMultiTargetModule, Modules...>::type;
	RAYSCANLIB_BASE_CHILD

	struct HeapCmp
	{
		bool operator()(const vertex& a, const vertex& b) const noexcept
		{
			return a.f < b.f;
		}
	};
	using queue_type = inx::alg::pairing_heap<vertex, HeapCmp, modules::RayScanMultiTargetModule>;

	RayScanMultiTargetBase() : m_targetsFound(0), m_current(nullptr)
	{ }

	virtual void setupStart(point pt)
	{
		m_start.id = pt;
		m_start.search.s = {m_stats.search.s.id, 0};
		m_start.open = true; m_start.close = false;
		m_start.f = m_start.g = m_start.h = 0;
		m_start.pred = nullptr;
	}
	virtual void setupMultiTarget(point* pt, size_t count)
	{
		m_multiTarget.resize(count);
		for (size_t i = 0; i < count; i++) {
			auto& target = m_multiTarget[i];
			target.id = pt[i];
			target.search.s = {m_stats.search.s.id, 0};
			target.target_id = i;
			target.open = target.close = false;
			target.f = target.g = inf<double>;
			target.h = 0;
			target.pred = nullptr;
		}
	}

	virtual void setDynamicSearch(bool dyn)
	{
		if constexpr (!has_module<modules::DynamicTag>()) {
			if (dyn)
				throw std::logic_error("not implemented");
		}
	}

	std::optional<double> search(point start, point* targets, size_t targetsCount)
	{
		setupSearchMT_();
		auto tp = std::chrono::steady_clock::now();
		m_targetsFound = 0;
		m_astarQueue.clear();
		setupStart(start);
		setupMultiTarget(targets, targetsCount);
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::BeginMultiTargetSearchHook>(start, targets, targetsCount); }
		search_();
		m_stats.searchTime = std::chrono::steady_clock::now() - tp;
		std::optional<double> ans = 0;
		for (auto& x : m_multiTarget)
		{
			if (x.pred == nullptr) {
				ans = std::nullopt;
				break;
			}
			*ans += x.g;
		}
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::EndMultiTargetSearchHook>(ans.value_or(inf<double>)); }
		return ans;
	}

	void initNode(vertex& node)
	{
		if (node.search.s.id != m_stats.search.s.id) {
			initNode_(node);
		}
	}

	static geometry::AngledSector<coord, Between::NARROW, Dir::CW> getProjectionField(vertex& node) noexcept
	{
		assert(node.pred != nullptr);
		return node.turn == Dir::CW ? geometry::AngledSector<coord, Between::NARROW, Dir::CW>(node.id - node.pred->id, node.ref.getPointCCW() - node.id)
		                            : geometry::AngledSector<coord, Between::NARROW, Dir::CW>(node.ref.getPointCW() - node.id, node.id - node.pred->id);
	}

	void setupStats(rayscanlib::utils::StatsTable* table, rayscanlib::utils::Stats* stats) const override
	{
		using namespace std::literals::string_view_literals;
		super::setupStats(table, stats);
		if (table != nullptr) {
			table->emplace_column("sExp"sv, 1000);
			table->emplace_column("sPushed"sv, 1100);
			table->emplace_column("sUpdated"sv, 1200);
			table->emplace_column("rays"sv, 2000);
			table->emplace_column("raysCached"sv, 2100);
			table->emplace_column("setupNs"sv, 9000);
			table->emplace_column("searchNs"sv, 9100);
		}
		if (stats != nullptr) {
			stats->template try_emplace<int64>("sExp"sv);
			stats->template try_emplace<int64>("sPushed"sv);
			stats->template try_emplace<int64>("sUpdated"sv);
			stats->template try_emplace<int64>("rays"sv);
			stats->template try_emplace<int64>("raysCached"sv);
			stats->template try_emplace<std::chrono::nanoseconds>("setupNs"sv, rayscanlib::utils::StatAverager());
			stats->template try_emplace<std::chrono::nanoseconds>("searchNs"sv, rayscanlib::utils::StatAverager());
		}
	}

	void saveStats(rayscanlib::utils::Stats& stats) const override
	{
		using namespace std::literals::string_literals;
		super::saveStats(stats);
		stats.at("sExp"s) = m_stats.search.s.expansions;
		stats.at("sPushed"s) = m_stats.successorsPushed;
		stats.at("sUpdated"s) = m_stats.successorsUpdated;
		stats.at("rays"s) = m_stats.rays;
		stats.at("raysCached"s) = m_stats.raysCached;
		stats.at("setupNs"s) = m_stats.dynSetupTime;
		stats.at("searchNs"s) = m_stats.searchTime - m_stats.rayCastTime;
	}

protected:
	virtual void expandStartNode_(vertex& node)
	{
		assert(&node == &m_start && !node.close);
		node.close = true;
		m_stats.search.s.expansions += 1;
		m_current = &node;
	}
	virtual void expandNode_(vertex& node)
	{
		assert(!node.close);
		node.close = true;
		m_stats.search.s.expansions += 1;
		m_current = &node;
	}

	intersection castRay(point a, point ab)
	{
		m_stats.rays += 1;
		return this->castRay_(a, ab);
	}

	bool castRayNode(const vertex& from, vertex& to)
	{
		m_stats.rays += 1;
		return castRayNode_(from, to);
	}

	bool doneRayNode(const vertex& to)
	{
		return to.search.expansionId == m_stats.search.expansionId;
	}

	virtual bool castRayNode_(const vertex& from, vertex& to)
	{
		assert(to.search.s.id == m_stats.search.s.id);
		if (!doneRayNode(to)) {
			storeRayNode_(to, this->castRay_(from.id, to.id - from.id));
			return true;
		} else {
			m_stats.raysCached += 1;
			if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::RayCastHook>(from.id, to.id - from.id, to.expansionIntersect); }
			return false;
		}
	}

	virtual void storeRayNode_(vertex& to, intersection I)
	{
		to.search.expansionId = this->getStats().search.expansionId;
		to.expansionIntersect = I;
	}

	virtual void initNode_(vertex& node)
	{
		node.search.s = {m_stats.search.s.id, 0};
		node.open = node.close = false;
		node.f = node.g = inf<double>;
		node.h = 0;
		node.pred = nullptr;
	}

	virtual void pushSuccessor_(vertex& successor, double g, const intersection& inst, Dir turn)
	{
		assert(m_current != nullptr);
		assert(successor.search.s.id == m_stats.search.s.id);
		assert(m_current->id != successor.id);
		assert(std::abs(g - (m_current->g + m_current->id.distance(successor.id))) < 1e-4);
		m_stats.successorsFound += 1;
		if (g >= successor.g || successor.close) {
			assert(m_current->g + m_current->id.distance(successor.id) > successor.g - 1e-4); // should not find a shorter path to closed node
			if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::PushSuccessorHook>(std::as_const(*m_current), std::as_const(successor), false, false); }
			return;
		}
		successor.g = g;
		heuristic_(successor);
		successor.f = g + successor.h;
		successor.pred = m_current;
		successor.turn = turn;
		successor.predIntersect = inst;
		if (!successor.open) {
			successor.open = true;
			m_stats.successorsPushed += 1;
			assert(!inx::alg::pairing_heap_node_within(m_astarQueue, successor));
			assert(inx::alg::is_valid_pairing_heap(m_astarQueue));
			m_astarQueue.push(successor);
			assert(inx::alg::pairing_heap_node_within(m_astarQueue, successor));
			assert(inx::alg::is_valid_pairing_heap(m_astarQueue));
			if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::PushSuccessorHook>(std::as_const(*m_current), std::as_const(successor), true, true); }
		} else {
			m_stats.successorsUpdated += 1;
			assert(inx::alg::is_valid_pairing_heap(m_astarQueue, &successor));
			assert(inx::alg::pairing_heap_node_within(m_astarQueue, successor));
			m_astarQueue.adjust(successor); // will raise or lower automatically
			assert(inx::alg::pairing_heap_node_within(m_astarQueue, successor));
			assert(inx::alg::is_valid_pairing_heap(m_astarQueue));
			if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::PushSuccessorHook>(std::as_const(*m_current), std::as_const(successor), true, false); }
		}
	}

	virtual void pushMultiTarget_(vertex& target, double g)
	{
		assert(target.isTarget() && static_cast<size_t>(target.target_id) < m_multiTarget.size());
		assert(std::abs(g - (m_current->g + m_current->id.distance(target.id))) < 1e-4);
		if (g < target.g && !target.close) {
			if (m_astarQueue.empty() || m_astarQueue.top().f >= g) {
				m_stats.successorsFound += 1;
				if (target.open) {
					assert(inx::alg::is_valid_pairing_heap(m_astarQueue));
					assert(inx::alg::pairing_heap_node_within(m_astarQueue, target));
					m_astarQueue.erase(&target);
					assert(inx::alg::is_valid_pairing_heap(m_astarQueue));
					assert(!inx::alg::pairing_heap_node_within(m_astarQueue, target));
				} else
					target.open = true;
				target.g = g;
				target.close = true;
				target.pred = m_current;
				m_targetsFound += 1;
			} else {
				pushSuccessor_(target, g, intersection{}, Dir::COLIN);
			}
			if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::PushSuccessorHook>(std::as_const(*m_current), std::as_const(target), true, true); }
		} else {
			m_stats.successorsFound += 1;
			if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::PushSuccessorHook>(std::as_const(*m_current), std::as_const(target), false, false); }
		}
	}

	virtual void closeTarget_(vertex& target)
	{
		assert(target.isTarget() && !target.close && static_cast<size_t>(target.target_id) < m_multiTarget.size());
		assert(target.pred != nullptr);
#ifndef NDEBUG
		{
			// check target return path's g-values
			double g = target.g;
			for (auto* pred = &target; pred != nullptr; pred = pred->pred) {
				assert(std::abs(g - pred->g) < 1e-4);
				if (pred->pred == nullptr) {
					assert(g < 1e-4);
				} else {
					g -= pred->id.distance(pred->pred->id);
				}
			}
		}
#endif
		target.close = true;
		m_targetsFound += 1;
	}

	// @returns: if heuristic value is updated
	virtual bool heuristic_(vertex& node) = 0;

	void search_() override
	{
		uint32 targets = static_cast<uint32>(m_multiTarget.size());
		expandStartNode_(m_start);

		while (m_targetsFound < targets && !m_astarQueue.empty()) {
			assert(inx::alg::is_valid_pairing_heap(m_astarQueue));
			vertex& expand = m_astarQueue.top();
			m_astarQueue.pop();
			assert(inx::alg::is_valid_pairing_heap(m_astarQueue));
			if (expand.isTarget()) {
				closeTarget_(expand);
				continue;
			}
			expandNode_(expand);
		}
	}
	virtual void setupSearchMT_()
	{
		m_stats.clear();
		m_stats.search.s.id += 1;
		m_stats.search.s.expansions = 0;
	}

public:
	const vertex& getStart() const noexcept { return m_start; }
	const std::vector<vertex>& getMultiTarget() const noexcept { return m_multiTarget; }
	uint32 getTargetsFound() const noexcept { return m_targetsFound; }
	bool searchComplete() const noexcept { return m_targetsFound >= m_multiTarget.size(); }
	const auto& getStats() const noexcept { return m_stats; }
	auto& getStats_() noexcept { return m_stats; }
	const vertex* getCurrent() const noexcept { return m_current; }

protected:
	vertex& getStart_() noexcept { return m_start; }
	std::vector<vertex>& getMultiTarget_() noexcept { return m_multiTarget; }
	void setTargetsFound_(uint32 targetsFound) noexcept { m_targetsFound = targetsFound; }
	vertex* getCurrent_() noexcept { return m_current; }
	auto& astarQueue_() noexcept { return m_astarQueue; }

private:
	uint32 m_targetsFound;
	vertex* m_current;
	vertex m_start;
	std::vector<vertex> m_multiTarget;
	details::RayScanMultiTargetStats m_stats;
	queue_type m_astarQueue;
};

namespace details
{

template <typename... Modules>
struct search_module<modules::RayScanMultiTargetModule, Modules...>
{
	using type = RayScanMultiTargetBase<Modules...>;
};

template <typename... Modules>
struct search_data<RayScanVertex, modules::RayScanMultiTargetModule, Modules...>
	: search_next_data<RayScanVertex, modules::RayScanMultiTargetModule, Modules...>::type,
	  inx::alg::pairing_heap_tag<modules::RayScanMultiTargetModule>
{
	using self = search_data<RayScanVertex, modules::RayScanMultiTargetModule, Modules...>;
	using super = typename search_next_data<RayScanVertex, modules::RayScanMultiTargetModule, Modules...>::type;
	RAYSCANLIB_STRUCT_CHILD
	
	SearchExpansion search;
	int32 target_id;
	bool open;
	bool close;
	Dir turn; /// CW = forwards, CCW = backwards
	double g;
	double h;
	double f;
	vertex* pred;
	intersection expansionIntersect;
	intersection predIntersect;
	search_data() noexcept : search{}, target_id(-1), open(false), close(false), turn(Dir::COLIN), g(0), h(0), f(0), pred(nullptr), expansionIntersect{}, predIntersect{}
	{ }

	bool isTarget() const noexcept { return target_id >= 0; }
};

} // namespace details

template <bool Hooks, typename CoordType, typename... Modules>
using RayScanMultiTarget = RayScan<Hooks, CoordType, Modules..., modules::RayScanMultiTargetModule>;

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_RAYSCANSHORTESTPATH_HPP_INCLUDED
