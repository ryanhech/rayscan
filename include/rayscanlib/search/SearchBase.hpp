#ifndef RAYSCANLIB_SEARCH_SEARCHBASE_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_SEARCHBASE_HPP_INCLUDED

#include "fwd.hpp"
#include <rayscanlib/utils/StatsTable.hpp>
#include <rayscanlib/utils/Stats.hpp>
#include <inx/tuple.hpp>
#include <typeinfo>
#include <typeindex>
#include <any>
#include <random>
#include <chrono>

namespace rayscanlib::search
{

namespace modules {
template <bool Hooks>
struct SearchModule { };
} // namespace modules

namespace details
{

template <typename Module, typename... Modules>
struct SearchTypes;
template <typename Module, typename... Modules>
struct next_search_type
{
	constexpr static size_t index = inx::type_index_v<Module, Modules...>;
	using next_module = type_select_t<index+1, Modules...>;
	using type = SearchTypes<next_module, Modules...>;
};
template <typename Module, typename... Modules>
struct SearchTypes : next_search_type<Module, Modules...>::type
{ };
template <bool Hooks, typename... Modules>
struct SearchTypes<modules::SearchModule<Hooks>, Modules...>
{
	constexpr static bool hooks_enabled() noexcept { return Hooks; }
	using module_list = inx::type_list<Modules...>;
	template <typename Mod>
	constexpr static bool has_module() noexcept { return (std::is_base_of<Mod, Modules>::value || ...); }
};
template <typename Module1, typename... Modules>
struct SearchTypesRoot : SearchTypes<Module1, Module1, Modules...>
{ };

#define SEARCH_DEFINE_TYPES(modules) \
	using search_types = ::rayscanlib::search::details::SearchTypesRoot<modules...>; \
	using module_list = typename search_types::module_list; \
	template <typename Mod> \
	constexpr static bool has_module() noexcept { return search_types::template has_module<Mod>(); }

#define SEARCH_DEFINE_TYPES_MODULES \
	SEARCH_DEFINE_TYPES(Modules)

#define SEARCH_BASE_TYPES(baseType) \
	using search_types = typename baseType::search_types; \
	using module_list = typename search_types::module_list; \
	template <typename Mod> \
	constexpr static bool has_module() noexcept { return search_types::template has_module<Mod>(); }

#define SEARCH_INHERIT_TYPES(superType) \
	using typename superType::search_types; \
	using typename superType::module_list; \
	template <typename Mod> \
	constexpr static bool has_module() noexcept { return superType::template has_module<Mod>(); }

#define SEARCH_INHERIT_BASE_TYPES(superType) \
public: \
	SEARCH_INHERIT_TYPES(superType)

#define SEARCH_BASE_CHILD \
	SEARCH_INHERIT_BASE_TYPES(super)
#define SEARCH_STRUCT_CHILD \
	constexpr static bool enabled() noexcept { return true; } \
	SEARCH_INHERIT_TYPES(super)

#define SEARCH_HOOK_DEFINE(hookmod, fntype) \
template <typename... Modules> \
struct search_hook<hooks::hookmod, Modules...> : std::true_type \
{ \
	using hook = ::rayscanlib::search::details::SearchTypesRoot<Modules...>; \
	using type = std::function< fntype >; \
};

template <typename Module, typename... Modules>
struct search_module;
template <typename Module, typename TypeList>
struct search_module_typelist;

template <typename Tag, typename Module, typename... Modules>
struct search_data
{
	constexpr static bool enabled() noexcept { return false; }
};
template <typename Tag, typename Module, typename... Modules>
struct search_next_data;
template <typename Tag, typename... Modules>
struct search_first_data;

template <typename Module>
struct search_module_interface;

union SearchExpansion
{
	struct {
		uint32 id; /// the search id
		uint32 expansions; /// how many expansions were performed
	} s;
	uint64 expansionId; /// unique expansion id
};

} // namespace details

template <typename Module, typename... Modules>
struct search_next_module;

namespace details {

template <typename Hook, typename... Modules>
struct search_hook : std::false_type
{ };

} // namespace details

template <bool Hooks, typename... Modules>
class SearchBase
{
public:
	using self = SearchBase<Hooks, Modules...>;
	static_assert(Hooks == false, "Hooks must equal false");
	SEARCH_DEFINE_TYPES_MODULES
	static constexpr bool hooks_enabled() noexcept { return false; }
	static constexpr bool is_single_target_search() noexcept { return has_module<modules::SingleTargetTag>() && !has_module<modules::MultiTargetTag>(); }
	static constexpr bool is_multi_target_search() noexcept { return has_module<modules::MultiTargetTag>(); }
	static constexpr bool is_dynamic_search() noexcept { return has_module<modules::DynamicTag>(); }

	SearchBase()
	{
		m_rand.seed(std::chrono::high_resolution_clock::now().time_since_epoch().count());
	}
	virtual ~SearchBase() = default;

	virtual void setupStats(rayscanlib::utils::StatsTable* table[[maybe_unused]], rayscanlib::utils::Stats* stats[[maybe_unused]]) const { }
	virtual void saveStats(rayscanlib::utils::Stats& stats[[maybe_unused]]) const { }

	std::minstd_rand& rand() noexcept { return m_rand; }

protected:
	template <typename Hook, typename... Args>
	void use_hook_(Args&&... args) const
	{
		throw std::logic_error("invalid");
	}

protected:
	virtual void search_() = 0;

private:
	std::minstd_rand m_rand;
};

template <typename... Modules>
class SearchBase<true, Modules...> : public SearchBase<false, Modules...>
{
public:
	using self = SearchBase<true, Modules...>;
	using super = SearchBase<false, Modules...>;
	SEARCH_INHERIT_TYPES(super)
	constexpr static bool hooks_enabled() noexcept { return true; }

protected:
	template <typename Hook, typename... Args>
	void use_hook_(Args&&... args) const
	{
		static_assert(details::search_hook<Hook, Modules...>::value, "could not deduce hook type");
		using type = typename details::search_hook<Hook, Modules...>::type;
		if (auto it = m_hooks.find(typeid(Hook)); it != m_hooks.end()) {
			assert(it->second.has_value());
			auto& fn = std::any_cast<const type&>(it->second);
			assert(fn != nullptr);
			fn(std::forward<Args>(args)...);
		}
	}
	template <typename Hook, typename Value>
	void set_hook_(Value&& value)
	{
		static_assert(details::search_hook<Hook, Modules...>::value, "could not deduce hook type");
		using type = typename details::search_hook<Hook, Modules...>::type;
		m_hooks[typeid(Hook)] = std::make_any<type>(std::forward<Value>(value));
	}
	template <typename Hook>
	void clear_hook_()
	{
		static_assert(details::search_hook<Hook, Modules...>::value, "could not deduce hook type");
		m_hooks.erase(typeid(Hook));
	}

private:
	std::unordered_map<std::type_index, std::any> m_hooks;
};

namespace details {

template <bool Hooks, typename... Modules>
class SearchFinal;

template <typename Module1, typename... Modules>
class SearchFinal<false, Module1, Modules...> final
	: public search_module<Module1, Module1, Modules..., modules::SearchModule<false>>::type
{
public:
	using self = SearchFinal<false, Module1, Modules...>;
	using super = typename search_module<Module1, Module1, Modules..., modules::SearchModule<false>>::type;
	SEARCH_BASE_CHILD
};

template <typename Module1, typename... Modules>
class SearchFinal<true, Module1, Modules...> final
	: public search_module<Module1, Module1, Modules..., modules::SearchModule<true>>::type
{
public:
	using self = SearchFinal<true, Module1, Modules...>;
	using super = typename search_module<Module1, Module1, Modules..., modules::SearchModule<true>>::type;
	SEARCH_BASE_CHILD

	template <typename Hook, typename Value>
	void set_hook(Value&& value)
	{
		super::template set_hook_<Hook>(std::forward<Value>(value));
	}
	template <typename Hook>
	void clear_hook()
	{
		super::template clear_hook_<Hook>();
	}
};

template <typename Tag, typename Module, typename Enable, typename... Modules>
struct search_next_data_impl_;
template <typename Tag, typename Module, typename... Modules>
struct search_next_data_impl_<Tag, Module, std::enable_if_t<!search_data<Tag, Module, Modules...>::enabled()>, Modules...>
	: search_next_data<Tag, Module, Modules...>
{ };
template <typename Tag, typename Module, typename... Modules>
struct search_next_data_impl_<Tag, Module, std::enable_if_t<search_data<Tag, Module, Modules...>::enabled()>, Modules...>
{
	using type = search_data<Tag, Module, Modules...>;
};
template <typename Tag, typename Module, typename... Modules>
struct search_next_data
{
	constexpr static size_t index = inx::type_index_v<Module, Modules...>;
	using next_module = type_select_t<index+1, Modules...>;
	using type = typename search_next_data_impl_<Tag, type_select_t<index+1, Modules...>, void, Modules...>::type;
};
template <typename Tag, typename... Modules>
struct search_first_data
{
	using type = typename search_next_data_impl_<Tag, type_select_t<0, Modules...>, void, Modules...>::type;
};

template <typename Tag, typename... Modules>
struct search_data<Tag, void, Modules...> : search_first_data<Tag, Modules...>::type
{
	using self = search_data<Tag, void, Modules...>;
	using super = typename search_first_data<Tag, Modules...>::type;
	SEARCH_STRUCT_CHILD
};
template <typename Tag, bool Hooks, typename... Modules>
struct search_data<Tag, modules::SearchModule<Hooks>, Modules...>
{
	SEARCH_DEFINE_TYPES_MODULES
	constexpr static bool enabled() noexcept { return true; }
};

template <bool Hooks, typename... Modules>
struct search_module<modules::SearchModule<Hooks>, Modules...>
{
	using type = SearchBase<Hooks, Modules...>;
};
template <typename Module, typename... Modules>
struct search_module_typelist<Module, type_list<Modules...>>
{
	using type = typename search_module<Module, Modules...>::type;
};

} // namespace details

template <typename Module, typename... Modules>
struct search_next_module
{
	constexpr static size_t index = type_index_v<Module, Modules...>;
	using type = typename details::search_module<type_select_t<index+1, Modules...>, Modules...>::type;
};

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_SEARCHBASE_HPP_INCLUDED
