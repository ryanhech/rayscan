#ifndef RAYSCANLIB_SEARCH_POLYANYABASE_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_POLYANYABASE_HPP_INCLUDED

#include "NavMeshBase.hpp"
#include <inx/alg/paring_heap.hpp>
#include <vector>
#include <iostream>
#include <optional>

namespace rayscanlib::search
{

namespace modules {
struct PolyanyaModule : SingleTargetTag { };
} // namespace modules


template <typename... Modules>
class PolyanyaBase : public search_next_module<modules::PolyanyaModule, Modules...>::type
{
public:
	using self = PolyanyaBase<Modules...>;
	using super = typename search_next_module<modules::PolyanyaModule, Modules...>::type;
	NAVMESH_BASE_CHILD

	struct Interval : inx::alg::pairing_heap_tag<>
	{
		double g;
		double f;
		vertex* root;
		face* area;
		point int_cw;
		point int_ccw;
	};
	struct IntervalCmp
	{
		bool operator()(const Interval& a, const Interval& b) const noexcept
		{
			return a.f < b.f;
		}
	};

	PolyanyaBase() : m_searchComplete{}, m_start{}, m_target{}, m_startFace{}, m_targetFace{}, m_root{}, m_interval{}
	{ }

public:
	std::optional<double> search(point start, point target, bool retain[[maybe_unused]] = false)
	{
		m_stats.search.s.id += 1;
		m_stats.search.s.expansions = 0;
		setupStart(start);
		setupTarget(target);
		search_();
		if (m_searchComplete)
			return m_target.g;
		else
			return std::nullopt;
	}

	virtual void setupStart(point start)
	{
		m_start.id = start;
		m_start.open = m_start.close = false;
		m_start.g = 0;
		m_start.pred = nullptr;
		m_startFace = locateFace(start);
	}

	virtual void setupTarget(point target)
	{
		m_target.id = target;
		m_target.open = m_target.close = false;
		m_target.g = inf<double>;
		m_target.pred = nullptr;
		m_targetFace = locateFace(target);
	}

	void initVertex(vertex& v)
	{
		if (v.search.s.id != m_stats.search.s.id) {
			initVertex_(v);
		}
	}

	virtual face* locateFace(point pt) = 0;

protected:
	virtual double heuristic_(vertex& root, point Fa, point Fab, point cw, point ccw) = 0;

	void search_() override
	{
		m_searchComplete = false;
		expandStartVertex_();
		while (!m_astarQueue.empty()) {
			Interval& inv = m_astarQueue.top();
			if (m_target.g < inv.f)
				break;
			m_astarQueue.pop();
			if (inv.g == inv.root->g) { // do not expand longer intervals
				expandInterval_(inv);
			}
			deleteInterval_(&inv);
		}
		if (m_target.pred != nullptr)
			m_searchComplete = true;
	}

	virtual void initVertex_(vertex& v)
	{
		v.search.s = {m_stats.search.s.id, 0};
		v.open = v.close = false;
		v.g = inf<double>;
		v.pred = nullptr;
	}
	
	virtual void expandStartVertex_()
	{
		std::cerr << "expand start " << m_start.id << std::endl;
		m_stats.search.s.expansions += 1;
		m_root = &m_start;
		m_interval = nullptr;
	}
	virtual void expandInterval_(Interval& interval)
	{
		std::cerr << "expand interval " << interval.root->id << "; " << interval.int_cw << " -- " << interval.int_ccw << std::endl;
		m_stats.search.s.expansions += 1;
		m_root = interval.root;
		m_interval = &interval;
	}

	virtual Interval* pushInterval_(face& F, vertex& root, point cw, point ccw)
	{
		assert(!cw.isCW(ccw));
		Interval* inv = newInterval_();
		inv->g = root.g;
		inv->f = root.g + heuristic_(root, point::zero(), point::zero(), cw, ccw);
		inv->area = &F;
		inv->root = &root;
		inv->int_cw = cw;
		inv->int_ccw = ccw;
		m_astarQueue.push(*inv);
		std::cerr << "push interval: ";
		for (auto&& [a,b] : F.points)
			std::cerr << a->id << "--";
		std::cerr << "; " << root.id << "; " << cw << " -- " << ccw << std::endl;
		return inv;
	}

	virtual void connectSuccessor_(vertex& root, vertex& successor, double g)
	{
		successor.g = g;
		successor.pred = &root;
	}

	virtual void connectTarget_(vertex& root, double g)
	{
		m_target.g = g;
		m_target.pred = &root;
	}

	Interval* newInterval_()
	{
		Interval* inv;
		if (!m_intervalFree.empty()) {
			inv = m_intervalFree.back();
			m_intervalFree.pop_back();
		} else if (m_intervalAt < m_intervalData.size()) {
			inv = &m_intervalData[m_intervalAt++];
		} else {
			inv = &m_intervalData.emplace_back();
			m_intervalAt += 1;
		}
		return inv;
	}
	void deleteInterval_(Interval* inv)
	{
		m_intervalFree.emplace_back(inv);
	}
	void clearInterval_()
	{
		m_intervalFree.clear();
		m_intervalAt = 0;
	}

public:
	bool getSearchComplete() const noexcept { return m_searchComplete; }
	const vertex& getStart() const noexcept { return m_start; }
	const vertex& getTarget() const noexcept { return m_target; }
	const face& getStartFace() const noexcept { return *m_startFace; }
	const face& getTargetFace() const noexcept { return *m_targetFace; }
	const vertex* getRoot() const noexcept { return m_root; }
	const Interval* getInterval() const noexcept { return m_interval; }
	const NavMeshStats& getStats() const noexcept { return m_stats; }
	NavMeshStats& getStats_() noexcept { return m_stats; }

protected:
	void setSearchComplete_(bool searchComplete) noexcept { m_searchComplete = searchComplete; }
	vertex& getStart_() noexcept { return m_start; }
	vertex& getTarget_() noexcept { return m_target; }
	face& getStartFace_() noexcept { return *m_startFace; }
	face& getTargetFace_() noexcept { return *m_targetFace; }
	vertex* getRoot_() noexcept { return m_root; }
	Interval* getInterval_() noexcept { return m_interval; }

private:
	bool m_searchComplete;
	vertex m_start;
	vertex m_target;
	face* m_startFace;
	face* m_targetFace;
	vertex* m_root;
	Interval* m_interval;
	NavMeshStats m_stats;
	std::deque<Interval> m_intervalData;
	std::vector<Interval*> m_intervalFree;
	size_t m_intervalAt;
protected:
	inx::alg::pairing_heap<Interval, IntervalCmp> m_astarQueue;
};

namespace details {

template <typename... Modules>
struct search_data<NavMeshVertex, modules::PolyanyaModule, Modules...>
	: search_next_data<NavMeshVertex, modules::PolyanyaModule, Modules...>::type//,
//	  inx::alg::pairing_heap_tag<modules::PolyanyaModule>
{
	using self = search_data<NavMeshVertex, modules::PolyanyaModule, Modules...>;
	using super = typename search_next_data<NavMeshVertex, modules::PolyanyaModule, Modules...>::type;
	NAVMESH_STRUCT_CHILD

	SearchExpansion search;
	bool target;
	bool open;
	bool close;
	double g;
	vertex* pred;
	search_data() noexcept : search{}, target(false), open(false), close(false), g(0), pred(nullptr)
	{ }
	
	bool isTarget() const noexcept { return target; }
};

template <typename... Modules>
struct search_module<modules::PolyanyaModule, Modules...>
{
	using type = PolyanyaBase<Modules...>;
};

} // namespace details

template <bool Hooks, typename CoordType, typename... Modules>
using Polyanya = NavMesh<Hooks, CoordType, Modules..., modules::PolyanyaModule>;

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_POLYANYABASE_HPP_INCLUDED
