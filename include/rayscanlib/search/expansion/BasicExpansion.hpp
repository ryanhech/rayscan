#ifndef RAYSCANLIB_SEARCH_EXPANSION_BASICEXPANSION_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_EXPANSION_BASICEXPANSION_HPP_INCLUDED

#include "../RayScanBase.hpp"

namespace rayscanlib::search
{

namespace modules {
struct BasicStartExpansionModule { };
struct BasicExpansionModule { };
} // namespace modules

namespace expansion {

template <typename... Modules>
class BasicStartExpansion : public search_next_module<modules::BasicStartExpansionModule, Modules...>::type
{
public:
	using self = BasicStartExpansion<Modules...>;
	using super = typename search_next_module<modules::BasicStartExpansionModule, Modules...>::type;
	RAYSCANLIB_BASE_CHILD

	BasicStartExpansion() = default;

protected:
	void expandStartNode_(vertex& node) override
	{
		constexpr bool enable_single = has_module<modules::SingleTargetTag>();
		constexpr bool enable_multi = !enable_single && has_module<modules::MultiTargetTag>();
		static_assert(enable_single != enable_multi, "Either single or multi target must be enabled");

		super::expandStartNode_(node);
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::BeginNodeExpansionHook>(std::as_const(node), geometry::AngledSector<coord, Between::WHOLE, Dir::CW>(point(1,0))); }
		uint32 tide = 1;
		if constexpr (enable_multi) {
			tide = static_cast<uint32>(this->getMultiTarget().size());
		}
		for (uint32 tid = 0; tid < tide; ++tid) {
			point st;
			vertex* target [[maybe_unused]];
			if constexpr (enable_single) {
				st = this->getTarget().id - node.id;
			} else if constexpr (enable_multi) {
				target = &this->getMultiTarget_()[tid];
				if (target->close)
					continue;
				st = target->id - node.id;
			} else {
				throw std::runtime_error("invalid interface");
			}
			auto inst = this->castRay(node.id, st);
			geometry::AngledSector<coord, Between::WHOLE, Dir::CW> projectionField(st);
			if (inst.getRayInt() > 0) { // over shot
				double g = st.length();
				if constexpr (enable_single) {
					this->pushTarget_(g);
				} else if constexpr (enable_multi) {
					this->pushMultiTarget_(*target, g);
				}
			} else {
				point scanlineCW = st, scanlineCCW = st;
				if (inst.getRayInt() < 0) { // intersected self, limit projection field
					auto it = --inst.getIterCW();
					if (inst.isCorner()) {
						auto [pv, at, nx] = it.edge_fwd();
						scanlineCW = nx-at; scanlineCCW = pv-at;
					} else {
						auto [at, nx] = it.segment_fwd();
						scanlineCW = nx-at; scanlineCCW = at-nx;
					}
					projectionField = geometry::AngledSector<coord, Between::WHOLE, Dir::CW>(scanlineCW, scanlineCCW);
				}
				// scan dirs
				this->template beginStartScanning_<false>(inst.getObstruction(), inst.getIterCW(), scanlineCW, projectionField.template makeDir<Dir::CW>());
				this->template beginStartScanning_<false>(inst.getObstruction(), inst.getIterCCW(), scanlineCCW, projectionField.template makeDir<Dir::CCW>());
			}
		}
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::EndNodeExpansionHook>(std::as_const(node)); }
	}
};

template <typename... Modules>
class BasicExpansion : public search_next_module<modules::BasicExpansionModule, Modules...>::type
{
public:
	using self = BasicExpansion<Modules...>;
	using super = typename search_next_module<modules::BasicExpansionModule, Modules...>::type;
	RAYSCANLIB_BASE_CHILD

	BasicExpansion() = default;

protected:
	void expandNode_(vertex& node) override
	{
		constexpr bool enable_single = has_module<modules::SingleTargetTag>();
		constexpr bool enable_multi = !enable_single && has_module<modules::MultiTargetTag>();
		static_assert(enable_single != enable_multi, "Either single or multi target must be enabled");

		super::expandNode_(node);
		geometry::AngledSector<coord, Between::NARROW, Dir::CW> projectionField(this->getProjectionField(node));
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::BeginNodeExpansionHook>(std::as_const(node), projectionField); }
		point ub(node.id - node.pred->id);
		uint32 tide = 1;
		if constexpr (enable_multi) {
			tide = static_cast<uint32>(this->getMultiTarget().size());
		}
		for (uint32 tid = 0; tid < tide; ++tid) {
			vertex* target;
			if constexpr (enable_single) {
				target = &this->getTarget_();
			} else if constexpr (enable_multi) {
				target = &this->getMultiTarget_()[tid];
				if (target->close)
					continue;
			} else {
				throw std::runtime_error("invalid interface");
			}
			assert(!target->close);
			point ut = target->id - node.id;
			if (ut.isZero() || !projectionField.within(ut))
				continue; // skip target not within projectionfield
			double g = node.g + ut.length();
			if (g >= target->g)
				continue; // no need to shoot to target that is further away than what is already found
			intersection inst = this->castRay(node.id, ut);
			if (inst.getRayInt() > 0) { // over shot
				if constexpr (enable_single) {
					this->pushTarget_(g);
				} else if constexpr (enable_multi) {
					this->pushMultiTarget_(*target, g);
				}
			} else {
				// scan dirs
				this->template beginScanning_<false>(inst.getObstruction(), inst.getIterCW(), ut, projectionField.template makeDir<Dir::CW>());
				this->template beginScanning_<false>(inst.getObstruction(), inst.getIterCCW(), ut, projectionField.template makeDir<Dir::CCW>());
			}
		}
		// shoot along the projection field
		intersection inst = node.predIntersect;
		const auto& ref = node.ref;
		if (node.turn == Dir::CW) { // turn is CW
			if (auto it = ref.getIterCCW(); !ref.getObstacle()->enclosure || !ref.getObstacle()->shape.pointOnHull(it.index()))
				this->template beginScanning_<false>(ref.getObstacle(), it, *it - node.id, projectionField.template makeDir<Dir::CCW>());
			this->template beginScanning_<false>(inst.getObstruction(), inst.getIterCW(), ub, projectionField.template makeDir<Dir::CW>());
		} else {
			if (auto it = ref.getIterCW(); !ref.getObstacle()->enclosure || !ref.getObstacle()->shape.pointOnHull(it.index()))
				this->template beginScanning_<false>(ref.getObstacle(), it, *it - node.id, projectionField.template makeDir<Dir::CW>());
			this->template beginScanning_<false>(inst.getObstruction(), inst.getIterCCW(), ub, projectionField.template makeDir<Dir::CCW>());
		}
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::EndNodeExpansionHook>(std::as_const(node)); }
	}
};

} // namespace expansion

namespace details
{

template <typename... Modules>
struct search_module<modules::BasicStartExpansionModule, Modules...>
{
	using type = expansion::BasicStartExpansion<Modules...>;
};

template <typename... Modules>
struct search_module<modules::BasicExpansionModule, Modules...>
{
	using type = expansion::BasicExpansion<Modules...>;
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_EXPANSION_BASICEXPANSION_HPP_INCLUDED
