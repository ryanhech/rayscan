#ifndef RAYSCANLIB_SEARCH_EXPANSION_SECTOREXPANSION_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_EXPANSION_SECTOREXPANSION_HPP_INCLUDED

#include "../RayScanBase.hpp"

namespace rayscanlib::search
{

namespace modules {
struct SectorStartExpansionModule { };
struct SectorExpansionModule { };
} // namespace modules

namespace expansion {

template <typename... Modules>
class SectorStartExpansion : public search_next_module<modules::SectorStartExpansionModule, Modules...>::type
{
public:
	using self = SectorStartExpansion<Modules...>;
	using super = typename search_next_module<modules::SectorStartExpansionModule, Modules...>::type;
	RAYSCANLIB_BASE_CHILD
	static_assert(has_module<modules::SingleTargetTag>(), "Must be used on single target");

	SectorStartExpansion() = default;

protected:
	void expandStartNode_(vertex& node) override
	{
		super::expandStartNode_(node);
		point st(this->getTarget().id - node.id);
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::BeginNodeExpansionHook>(std::as_const(node), geometry::AngledSector<coord, Between::WHOLE, Dir::CW>(st)); }
		auto inst = this->castRay(node.id, st);
		geometry::AngledSector<coord, Between::WHOLE, Dir::CW> projectionField(st);
		if (inst.getRayInt() > 0) // over shot
			this->pushTarget_(st.length());
		else {
			point scanlineCW = st, scanlineCCW = st;
			if (inst.getRayInt() < 0) { // intersected self, limit projection field
				auto it = --inst.getIterCW();
				if (inst.isCorner()) {
					auto [pv, at, nx] = it.edge_fwd();
					scanlineCW = nx-at; scanlineCCW = pv-at;
				} else {
					auto [at, nx] = it.segment_fwd();
					scanlineCW = nx-at; scanlineCCW = at-nx;
				}
				projectionField = geometry::AngledSector<coord, Between::WHOLE, Dir::CW>(scanlineCW, scanlineCCW);
			}
			
			point endRay = this->template beginStartScanningFrontier_<false>(inst.getObstruction(), inst.getIterCW(), scanlineCW, projectionField);
			if (endRay.isZero()) { // corner case, scan did not progress (i.e. hit the enclosure)
				this->template beginStartScanning_<false>(inst.getObstruction(), inst.getIterCCW(), scanlineCCW, projectionField.template makeDir<Dir::CCW>());
			} else if (!endRay.isColinFwd(st)) { // will not scan opposite if previous scan did a full loop
				this->template beginStartScanning_<false>(inst.getObstruction(), inst.getIterCCW(), scanlineCCW, projectionField.template splitInv<Dir::CW>(endRay));
			}
		}
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::EndNodeExpansionHook>(std::as_const(node)); }
	}

protected:
};

template <typename... Modules>
class SectorExpansion : public search_next_module<modules::SectorExpansionModule, Modules...>::type
{
public:
	using self = SectorExpansion<Modules...>;
	using super = typename search_next_module<modules::SectorExpansionModule, Modules...>::type;
	RAYSCANLIB_BASE_CHILD
	static_assert(has_module<modules::SingleTargetTag>(), "Must be used on single target");

	SectorExpansion() = default;

protected:
	void expandNode_(vertex& node) override
	{
		super::expandNode_(node);
		point ut(this->getTarget().id - node.id);
		if (ut.isZero())
			return;
		geometry::AngledSector<coord, Between::NARROW, Dir::CW> projectionField(this->getProjectionField(node));
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::BeginNodeExpansionHook>(std::as_const(node), projectionField); }
		intersection inst;
		point ub(node.id - node.pred->id);
		if (projectionField.within(ut)) { // target is within the projection field, shoot
			inst = this->castRay(node.id, ut);
			if (inst.getRayInt() > 0) // over shot
				this->pushTarget_(node.g + ut.length());
		}
		if (!this->getSearchComplete()) {
			if (inst)
			{
				point endCW = this->template beginScanningFrontier_<false>(inst.getObstruction(), inst.getIterCW(), ut, projectionField.template split<Dir::CW>(ut));
				if (endCW.isZero()) endCW = ut;
				point endCCW = this->template beginScanningFrontier_<false>(inst.getObstruction(), inst.getIterCCW(), ut, projectionField.template split<Dir::CCW>(ut));
				if (endCCW.isZero()) endCCW = ut;
				// shoot along the projection field
				inst = node.predIntersect;
				const auto& ref = node.ref;
				if (node.turn == Dir::CW) { // turn is CW
					if (auto it = ref.getIterCCW(); !ref.getObstacle()->enclosure || !ref.getObstacle()->shape.pointOnHull(it.index()))
						this->template beginScanning_<false>(ref.getObstacle(), it, *it - node.id, projectionField.template splitInv<Dir::CW>(endCW));
					this->template beginScanning_<false>(inst.getObstruction(), inst.getIterCW(), ub, projectionField.template splitInv<Dir::CCW>(endCCW));
				} else {
					if (auto it = ref.getIterCW(); !ref.getObstacle()->enclosure || !ref.getObstacle()->shape.pointOnHull(it.index()))
						this->template beginScanning_<false>(ref.getObstacle(), it, *it - node.id, projectionField.template splitInv<Dir::CCW>(endCCW));
					this->template beginScanning_<false>(inst.getObstruction(), inst.getIterCCW(), ub, projectionField.template splitInv<Dir::CW>(endCW));
				}
			} else { // target is outside, shoot along projection field only
				//
				// target is NOT within the projection field
				//
				inst = node.predIntersect;
				const auto& ref = node.ref;
				if (node.turn == Dir::CW) {
					if (auto it = ref.getIterCCW(); !ref.getObstacle()->enclosure || !ref.getObstacle()->shape.pointOnHull(it.index())) {
						auto endFrontier = this->template beginScanningFrontier_<false>(ref.getObstacle(), it, *it - node.id, projectionField.template makeDir<Dir::CCW>());
						if (!endFrontier.isZero())
							projectionField = projectionField.template split<Dir::CCW>(endFrontier);
					}
					if (!projectionField.isClose())
						this->template beginScanning_<false>(inst.getObstruction(), inst.getIterCW(), ub, projectionField.template makeDir<Dir::CW>());
				} else {
					if (auto it = ref.getIterCW(); !ref.getObstacle()->enclosure || !ref.getObstacle()->shape.pointOnHull(it.index())) {
						auto endFrontier = this->template beginScanningFrontier_<true>(ref.getObstacle(), it, *it - node.id, projectionField.template makeDir<Dir::CW>());
						if (!endFrontier.isZero())
							projectionField = projectionField.template split<Dir::CW>(endFrontier);
					}
					if (!projectionField.isClose())
						this->template beginScanning_<false>(inst.getObstruction(), inst.getIterCCW(), ub, projectionField.template makeDir<Dir::CCW>());
				}
			}
		}
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::EndNodeExpansionHook>(std::as_const(node)); }
	}

protected:
};

} // namespace expansion

namespace details
{

template <typename... Modules>
struct search_module<modules::SectorStartExpansionModule, Modules...>
{
	using type = expansion::SectorStartExpansion<Modules...>;
};

template <typename... Modules>
struct search_module<modules::SectorExpansionModule, Modules...>
{
	using type = expansion::SectorExpansion<Modules...>;
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_EXPANSION_SECTOREXPANSION_HPP_INCLUDED
