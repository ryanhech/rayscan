#ifndef RAYSCANLIB_SEARCH_EXPANSION_TARGETSKIPEXPANSION_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_EXPANSION_TARGETSKIPEXPANSION_HPP_INCLUDED

#include "../RayScanBase.hpp"

namespace rayscanlib::search
{

namespace modules {
template <bool RecordStats = false>
struct TargetSkipExpansionModule { };
} // namespace modules

namespace expansion {

template <bool RecordStats, typename... Modules>
class TargetSkipExpansion : public search_next_module<modules::TargetSkipExpansionModule<RecordStats>, Modules...>::type
{
public:
	using self = TargetSkipExpansion<RecordStats, Modules...>;
	using super = typename search_next_module<modules::TargetSkipExpansionModule<RecordStats>, Modules...>::type;
	RAYSCANLIB_BASE_CHILD
	static_assert(has_module<modules::SingleTargetTag>(), "Must be used on multi target");
	static constexpr bool target_skipped_stats() noexcept { return RecordStats; }

	TargetSkipExpansion() = default;

	struct TargetSkipStats
	{
		uint32 targetsWithinPF;
		uint32 targetsShot;
		uint32 extraRaysOnFinalExpansion;
		TargetSkipStats() noexcept : targetsWithinPF(0), targetsShot(0), extraRaysOnFinalExpansion(0)
		{ }
		void clear() noexcept
		{
			targetsWithinPF = 1; // 1 to handle starting node
			targetsShot = 1;
			extraRaysOnFinalExpansion = 0;
		}
	};

protected:
	void expandNode_(vertex& node) override
	{
		super::expandNode_(node);
		auto projectionField = this->getProjectionField(node);
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::BeginNodeExpansionHook>(std::as_const(node), projectionField); }
		intersection inst;
		point ub(node.id - node.pred->id);
		bool withinField = projectionField.within(this->getTarget().id - node.id);
		this->setupBlocking();

		uint32 wastedRays [[maybe_unused]] = 0; // used for the stats of the final block
		if constexpr (RecordStats) {
			if (withinField)
				m_targetSkipStats.targetsWithinPF += 1;
			wastedRays = this->getStats().rays;
		}
		inst = node.predIntersect;
		const auto& ref = node.ref;
		if (node.turn == Dir::CW) {
			if (auto it = ref.getIterCCW(); !ref.getObstacle()->enclosure || !ref.getObstacle()->shape.pointOnHull(it.index())) {
				if (withinField)
					this->template beginScanning_<true>(ref.getObstacle(), it, *it - node.id, projectionField.template makeDir<Dir::CCW>());
				else
					this->template beginScanning_<false>(ref.getObstacle(), it, *it - node.id, projectionField.template makeDir<Dir::CCW>());
			}
			if (withinField)
				this->template beginScanning_<true>(inst.getObstruction(), inst.getIterCW(), ub, projectionField.template makeDir<Dir::CW>());
			else
				this->template beginScanning_<false>(inst.getObstruction(), inst.getIterCW(), ub, projectionField.template makeDir<Dir::CW>());
		} else {
			if (auto it = ref.getIterCW(); !ref.getObstacle()->enclosure || !ref.getObstacle()->shape.pointOnHull(it.index())) {
				if (withinField)
					this->template beginScanning_<true>(ref.getObstacle(), it, *it - node.id, projectionField.template makeDir<Dir::CW>());
				else
					this->template beginScanning_<false>(ref.getObstacle(), it, *it - node.id, projectionField.template makeDir<Dir::CW>());
			}
			if (withinField)
				this->template beginScanning_<true>(inst.getObstruction(), inst.getIterCCW(), ub, projectionField.template makeDir<Dir::CCW>());
			else
				this->template beginScanning_<false>(inst.getObstruction(), inst.getIterCCW(), ub, projectionField.template makeDir<Dir::CCW>());
		}
		// shoot to target(s) if nessesary
		if (withinField) {
			if (!this->isTargetBlocked()) {
				point ut = this->getTarget().id - node.id;
				double g = node.g + ut.length();
				if (g < this->getTarget().g) {
					inst = this->castRay(node.id, ut);
					if constexpr (RecordStats) {
						m_targetSkipStats.targetsShot += 1;
					}
					if (inst.getRayInt() > 0) {
						this->pushTarget_(g);
						if constexpr (RecordStats) {
							m_targetSkipStats.extraRaysOnFinalExpansion += (this->getStats().rays - wastedRays - 1);
						}
					} else {
						this->template beginScanning_<false>(inst.getObstruction(), inst.getIterCW(), ut, projectionField.template split<Dir::CW>(ut));
						this->template beginScanning_<false>(inst.getObstruction(), inst.getIterCCW(), ut, projectionField.template split<Dir::CCW>(ut));
					}
				}
			}
		}
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::EndNodeExpansionHook>(std::as_const(node)); }
	}

	void setupSearchST_() override
	{
		super::setupSearchST_();
		if constexpr (RecordStats) {
			m_targetSkipStats.clear();
		}
	}

public:
	void setupStats(rayscanlib::utils::StatsTable* table, rayscanlib::utils::Stats* stats) const override
	{
		super::setupStats(table, stats);
		if constexpr (RecordStats) {
			using namespace std::literals::string_view_literals;
			if (table != nullptr) {
				table->emplace_column("targetInPF"sv, 5000);
				table->emplace_column("targetsShot"sv, 5100);
				table->emplace_column("extraRaysFinal"sv, 5200);
			}
			if (stats != nullptr) {
				stats->template try_emplace<int64>("targetInPF"sv);
				stats->template try_emplace<int64>("targetsShot"sv);
				stats->template try_emplace<int64>("extraRaysFinal"sv);
			}
		}
	}

	void saveStats(rayscanlib::utils::Stats& stats) const override
	{
		super::saveStats(stats);
		if constexpr (RecordStats) {
			using namespace std::literals::string_literals;
			stats.at("targetInPF"s) = m_targetSkipStats.targetsWithinPF;
			stats.at("targetsShot"s) = m_targetSkipStats.targetsShot;
			stats.at("extraRaysFinal"s) = m_targetSkipStats.extraRaysOnFinalExpansion;
		}
	}

public:
	const TargetSkipStats& getTargetSkipStats() const noexcept { return m_targetSkipStats; }

protected:
	TargetSkipStats m_targetSkipStats;
};

} // namespace expansion

namespace details
{

template <bool RecordStats, typename... Modules>
struct search_module<modules::TargetSkipExpansionModule<RecordStats>, Modules...>
{
	using type = expansion::TargetSkipExpansion<RecordStats, Modules...>;
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_EXPANSION_TARGETSKIPEXPANSION_HPP_INCLUDED
