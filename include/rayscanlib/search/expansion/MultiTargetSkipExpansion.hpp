#ifndef RAYSCANLIB_SEARCH_EXPANSION_MULTITARGETSKIPEXPANSION_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_EXPANSION_MULTITARGETSKIPEXPANSION_HPP_INCLUDED

#include "../RayScanBase.hpp"

namespace rayscanlib::search
{

namespace modules {
template <bool RecordStats = false>
struct MultiTargetSkipExpansionModule { };
} // namespace modules

namespace expansion {

template <bool RecordStats, typename... Modules>
class MultiTargetSkipExpansion : public search_next_module<modules::MultiTargetSkipExpansionModule<RecordStats>, Modules...>::type
{
public:
	using self = MultiTargetSkipExpansion<RecordStats, Modules...>;
	using super = typename search_next_module<modules::MultiTargetSkipExpansionModule<RecordStats>, Modules...>::type;
	RAYSCANLIB_BASE_CHILD
	static_assert(has_module<modules::MultiTargetTag>() && !has_module<modules::SingleTargetTag>(), "Must be used on multi target");
	constexpr static bool target_skipped_stats() noexcept { return RecordStats; }

	MultiTargetSkipExpansion() = default;

	struct TargetSkipStats
	{
		uint32 targetsWithinPF;
		uint32 targetsShot;
		TargetSkipStats() noexcept : targetsWithinPF(0), targetsShot(0)
		{ }
		void clear() noexcept
		{
			targetsWithinPF = 0;
			targetsShot = 0;
		}
	};

protected:
	void expandStartNode_(vertex& node) override
	{
		super::expandStartNode_(node);
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::BeginNodeExpansionHook>(std::as_const(node), geometry::AngledSector<coord, Between::WHOLE, Dir::CW>(point(1,0))); }
		intersection inst;
		this->rotatorSortStart_(node.id);
		this->setupBlocking();

		if constexpr (RecordStats) {
			m_targetSkipStats.targetsWithinPF += this->getRotatorSize();
		}
		// shoot to target(s) if nessesary
		while (!this->getRotatorList_().empty()) {
			this->getRotatorList_().clearCurrent();
			auto* current = this->getRotatorList_().first_ccw();
			point ray = current->uv;
			inst = this->castRay(node.id, ray);
			if constexpr (RecordStats) {
				m_targetSkipStats.targetsShot += 1;
			}
			auto I = this->getLastIntersection_();
			bool allVis = true;
			for ( ; current->v != nullptr && ray.isColinFwd(current->uv); ) {
				if (geometry::pc_op<geometry::PCop::le>(geometry::collinear_point_on_segment(current->uv, ray), I)) {
					this->pushMultiTarget_(*current->v, current->uv.length());
				} else {
					allVis = false;
				}
				this->getRotatorList_().erase(std::exchange(current, current->next));
			}
			if (!allVis) {
				this->getRotatorList_().setCurrent(this->getRotatorList_().first_ccw());
				this->template beginStartScanning_<true>(inst.getObstruction(), inst.getIterCW(), ray, geometry::AngledSector<coord, Between::WHOLE, Dir::CW>(ray));
				this->getRotatorList_().setCurrent(this->getRotatorList_().first_cw());
				this->template beginStartScanning_<true>(inst.getObstruction(), inst.getIterCCW(), ray, geometry::AngledSector<coord, Between::WHOLE, Dir::CCW>(ray));
			}
		}
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::EndNodeExpansionHook>(std::as_const(node)); }
	}

	void expandNode_(vertex& node) override
	{
		super::expandNode_(node);
		auto projectionField = this->getProjectionField(node);
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::BeginNodeExpansionHook>(std::as_const(node), projectionField); }
		intersection inst;
		point ub(node.id - node.pred->id);
		this->rotatorSort_(node.id, projectionField);
		this->setupBlocking();

		if constexpr (RecordStats) {
			m_targetSkipStats.targetsWithinPF += this->getRotatorSize();
		}
		inst = node.predIntersect;
		const auto& ref = node.ref;
		if (node.turn == Dir::CW) {
			if (auto it = ref.getIterCCW(); !ref.getObstacle()->enclosure || !ref.getObstacle()->shape.pointOnHull(it.index())) {
				if (!this->getRotatorList().empty()) {
					this->getRotatorList_().setCurrent(this->getRotatorList_().first_cw());
					this->template beginScanning_<true>(ref.getObstacle(), it, *it - node.id, projectionField.template makeDir<Dir::CCW>());
				} else
					this->template beginScanning_<false>(ref.getObstacle(), it, *it - node.id, projectionField.template makeDir<Dir::CCW>());
			}
			if (!this->getRotatorList().empty()) {
				this->getRotatorList_().setCurrent(this->getRotatorList_().first_ccw());
				this->template beginScanning_<true>(inst.getObstruction(), inst.getIterCW(), ub, projectionField.template makeDir<Dir::CW>());
			} else
				this->template beginScanning_<false>(inst.getObstruction(), inst.getIterCW(), ub, projectionField.template makeDir<Dir::CW>());
		} else {
			this->getRotatorList_().setCurrent(this->getRotatorList_().first_ccw());
			if (auto it = ref.getIterCW(); !ref.getObstacle()->enclosure || !ref.getObstacle()->shape.pointOnHull(it.index())) {
				if (!this->getRotatorList().empty()) {
					this->getRotatorList_().setCurrent(this->getRotatorList_().first_ccw());
					this->template beginScanning_<true>(ref.getObstacle(), it, *it - node.id, projectionField.template makeDir<Dir::CW>());
				} else
					this->template beginScanning_<false>(ref.getObstacle(), it, *it - node.id, projectionField.template makeDir<Dir::CW>());
			}
			if (!this->getRotatorList().empty()) {
				this->getRotatorList_().setCurrent(this->getRotatorList_().first_cw());
				this->template beginScanning_<true>(inst.getObstruction(), inst.getIterCCW(), ub, projectionField.template makeDir<Dir::CCW>());
			} else
				this->template beginScanning_<false>(inst.getObstruction(), inst.getIterCCW(), ub, projectionField.template makeDir<Dir::CCW>());
		}
		// shoot to target(s) if nessesary
		if (!this->getRotatorList().empty()) {
			while (!this->getRotatorList_().empty()) {
				this->getRotatorList_().clearCurrent();
				auto* current = this->getRotatorList_().first_ccw();
				point ray = current->uv;
				do {
					double g = node.g + current->uv.length();
					if (g < current->v->g)
						break;
					else
						this->getRotatorList_().erase(std::exchange(current, current->next));
				} while (current->v != nullptr && ray.isColinFwd(current->uv));
				if (current->v == nullptr || !ray.isColinFwd(current->uv))
					continue;
				inst = this->castRay(node.id, ray);
				if constexpr (RecordStats) {
					m_targetSkipStats.targetsShot += 1;
				}
				auto I = this->getLastIntersection_();
				bool allVis = true;
				for ( ; current->v != nullptr && ray.isColinFwd(current->uv); ) {
					double g = node.g + current->uv.length();
					if (g < current->v->g) {
						if (geometry::pc_op<geometry::PCop::le>(geometry::collinear_point_on_segment(current->uv, ray), I)) {
							this->pushMultiTarget_(*current->v, g);
						} else {
							allVis = false;
						}
					}
					this->getRotatorList_().erase(std::exchange(current, current->next));
				}
				if (!allVis) {
					this->template beginScanning_<false>(inst.getObstruction(), inst.getIterCCW(), ray, projectionField.template split<Dir::CCW>(ray));
					this->getRotatorList_().setCurrent(current);
					this->template beginScanning_<true>(inst.getObstruction(), inst.getIterCW(), ray, projectionField.template split<Dir::CW>(ray));
				}
			}
		}
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::EndNodeExpansionHook>(std::as_const(node)); }
	}

	void setupSearchMT_() override
	{
		super::setupSearchMT_();
		if constexpr (RecordStats) {
			m_targetSkipStats.clear();
		}
	}

public:
	void setupStats(rayscanlib::utils::StatsTable* table, rayscanlib::utils::Stats* stats) const override
	{
		super::setupStats(table, stats);
		if constexpr (RecordStats) {
			using namespace std::literals::string_view_literals;
			if (table != nullptr) {
				table->emplace_column("targetInPF"sv, 5000);
				table->emplace_column("targetsShot"sv, 5100);
			}
			if (stats != nullptr) {
				stats->template try_emplace<int64>("targetInPF"sv);
				stats->template try_emplace<int64>("targetsShot"sv);
			}
		}
	}

	void saveStats(rayscanlib::utils::Stats& stats) const override
	{
		super::saveStats(stats);
		if constexpr (RecordStats) {
			using namespace std::literals::string_literals;
			stats.at("targetInPF"s) = m_targetSkipStats.targetsWithinPF;
			stats.at("targetsShot"s) = m_targetSkipStats.targetsShot;
		}
	}

public:
	const TargetSkipStats& getTargetSkipStats() const noexcept { return m_targetSkipStats; }

protected:
	TargetSkipStats m_targetSkipStats;
};

} // namespace expansion

namespace details
{

template <bool RecordStats, typename... Modules>
struct search_module<modules::MultiTargetSkipExpansionModule<RecordStats>, Modules...>
{
	using type = expansion::MultiTargetSkipExpansion<RecordStats, Modules...>;
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_EXPANSION_MULTITARGETSKIPEXPANSION_HPP_INCLUDED
