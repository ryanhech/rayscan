#ifndef RAYSCANLIB_SEARCH_EXPANSION_CACHEEXPANSION_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_EXPANSION_CACHEEXPANSION_HPP_INCLUDED

#include "../RayScanBase.hpp"
#include <rayscanlib/geometry/AngledSector.hpp>

namespace rayscanlib::search
{

namespace modules {
struct CacheExpansionModule { };
} // namespace modules

namespace expansion {

template <typename... Modules>
class CacheExpansion : public search_next_module<modules::CacheExpansionModule, Modules...>::type
{
public:
	using self = CacheExpansion<Modules...>;
	using super = typename search_next_module<modules::CacheExpansionModule, Modules...>::type;
	RAYSCANLIB_BASE_CHILD

	CacheExpansion() : m_caching(nullptr)
	{ }

protected:
	void expandNode_(vertex& node) override
	{
		constexpr bool enable_single = has_module<modules::SingleTargetTag>();
		constexpr bool enable_multi = !enable_single && has_module<modules::MultiTargetTag>();
		static_assert(enable_single != enable_multi, "Either single or multi target must be enabled");

		super::expandNode_(node);
		geometry::AngledSector<coord, Between::NARROW, Dir::CW> projectionField(this->getProjectionField(node));
		point uid = node.id;
		point ub = uid - node.pred->id;
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::BeginNodeExpansionHook>(std::as_const(node), projectionField); }

		m_caching = nullptr;
		// scan for non-cached node as default
		bool scanCW = node.turn != Dir::CW;
		{ // scan cached-LB section
			bool continueScan = true;
			int cid = vertex::Cache::idx(node.turn);		
			auto& cache = node.cached.data[cid];
			const auto& ref = node.ref;
			int scanTurn = static_cast<int>(node.turn); // CW = -1, CCW = 1
			std::pair<iter_cw, iter_ccw> scanIt;
			obstacle* scanObst = ref.getObstacle();
			geometry::AngledSector<coord, Between::NARROW, Dir::CW> scanAs(projectionField);
			if (scanCW)
				scanIt.first = ref.getIterCW();
			else
				scanIt.second = ref.getIterCCW();
			if (!cache.empty()) {
				// data cache, reproduce scan phase and store
				// reproduce CCW scan
				point scanUB, scanLB;
				if (scanCW) {
					scanUB = projectionField.template get<Dir::CW>();
					scanLB = projectionField.template get<Dir::CCW>();
				} else {
					scanUB = projectionField.template get<Dir::CCW>();
					scanLB = projectionField.template get<Dir::CW>();
				}
				for (auto [succ, I] : cache) {
					assert(succ != nullptr);
					auto uv(succ->id - uid);
					if (point::isCW(scanUB.cross(uv)*scanTurn)) { // by mult scanTurn, isCW is CW if scanCW, otherwise is isCCW
						continueScan = false; // no need to scan at all
						break;
					}
					Dir nodeDir = succ->calcTurn(uid);
					// update shotRayNode
					this->initNode(*succ);
					this->storeRayNode_(*succ, I);
					this->getStats_().raysCached += 1;
					if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::RayCastHook>(uid, uv, I); }
					// push node if visible
					if (I.getRayInt() > 0) { // overshot, at is a potential successor, push it
						this->pushSuccessor_(*succ, node.g + node.id.distance(succ->id), I, nodeDir);
					}
					// update scanLb to frontier scan
					if (!point::isCCW(scanLB.cross(uv)*scanTurn)) {
						scanLB = uv;
						scanObst = I.getObstruction();
						if (scanCW)
							scanIt.first = I.getIterCW();
						else
							scanIt.second = I.getIterCCW();
					}
				}
				// update scanAs to represent projection field to scan next
				if (scanCW)
					scanAs = geometry::AngledSector<coord, Between::NARROW, Dir::CW>(scanLB, scanUB);
				else
					scanAs = geometry::AngledSector<coord, Between::NARROW, Dir::CCW>(scanLB, scanUB);
			}

			// continue scan to progress projection field or perform the first cache
			if (continueScan) {
				m_caching = &cache;
				if (scanCW) {
					if (!scanObst->enclosure || !scanObst->shape.pointOnHull(scanIt.first.index())) {
						this->template beginScanning_<false>(scanObst, scanIt.first, scanAs.template get<Dir::CCW>(), scanAs.template makeDir<Dir::CW>());
					}
				} else {
					if (!scanObst->enclosure || !scanObst->shape.pointOnHull(scanIt.second.index())) {
						this->template beginScanning_<false>(scanObst, scanIt.second, scanAs.template get<Dir::CW>(), scanAs.template makeDir<Dir::CCW>());
					}
				}
				m_caching = nullptr;
			}
		}

		// scan the upper boundary
		{
			intersection inst = node.predIntersect;
			if (scanCW) {
				if (auto it = inst.getIterCCW(); !inst.getObstruction()->enclosure || !inst.getObstruction()->shape.pointOnHull(it.index())) {
					this->template beginScanning_<false>(inst.getObstruction(), it, ub, projectionField.template makeDir<Dir::CCW>());
				}
			} else {
				if (auto it = inst.getIterCW(); !inst.getObstruction()->enclosure || !inst.getObstruction()->shape.pointOnHull(it.index())) {
					this->template beginScanning_<false>(inst.getObstruction(), it, ub, projectionField.template makeDir<Dir::CW>());
				}
			}
		}
		
		// shoot to target(s)
		uint32 tide = 1;
		if constexpr (enable_multi) {
			tide = static_cast<uint32>(this->getMultiTarget().size());
		}
		for (uint32 tid = 0; tid < tide; ++tid) {
			vertex* target;
			if constexpr (enable_single) {
				target = &this->getTarget_();
			} else if constexpr (enable_multi) {
				target = &this->getMultiTarget_()[tid];
				if (target->close)
					continue;
			} else {
				throw std::runtime_error("invalid interface");
			}
			assert(!target->close);
			point ut = target->id - node.id;
			if (ut.isZero() || !projectionField.within(ut))
				continue; // skip target not within projectionfield
			double g = node.g + ut.length();
			if (g >= target->g)
				continue; // no need to shoot to target that is further away than what is already found
			intersection inst = this->castRay(node.id, ut);
			if (inst.getRayInt() > 0) { // over shot
				if constexpr (enable_single) {
					this->pushTarget_(g);
				} else if constexpr (enable_multi) {
					this->pushMultiTarget_(*target, g);
				}
			} else {
				// scan dirs
				this->template beginScanning_<false>(inst.getObstruction(), inst.getIterCW(), ut, projectionField.template makeDir<Dir::CW>());
				this->template beginScanning_<false>(inst.getObstruction(), inst.getIterCCW(), ut, projectionField.template makeDir<Dir::CCW>());
			}
		}

		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::EndNodeExpansionHook>(std::as_const(node)); }
	}

	bool castRayNode_(const vertex& from, vertex& to) override
	{
		bool ans = super::castRayNode_(from, to);
		if (m_caching) {
			m_caching->emplace_back(&to, to.expansionIntersect);
		}
		return ans;
	}

protected:
	typename vertex::Cache::raynode_data* m_caching;
};

} // namespace expansion

namespace details
{

template <typename... Modules>
struct search_module<modules::CacheExpansionModule, Modules...>
{
	using type = expansion::CacheExpansion<Modules...>;
};

template <typename... Modules>
struct search_data<RayScanVertex, modules::CacheExpansionModule, Modules...>
	: search_next_data<RayScanVertex, modules::CacheExpansionModule, Modules...>::type
{
	using self = search_data<RayScanVertex, modules::CacheExpansionModule, Modules...>;
	using super = typename search_next_data<RayScanVertex, modules::CacheExpansionModule, Modules...>::type;
	RAYSCANLIB_STRUCT_CHILD

	struct Cache
	{
		using raynode = std::pair<vertex*, intersection>;
		using raynode_data = std::vector<raynode>;
		std::array<raynode_data, 2> data;
		constexpr static int idx(Dir d) noexcept
		{
			assert(d == Dir::CW || d == Dir::CCW);
			return ( (static_cast<int>(d) >> 1) & 1 );
		}
	};

	Cache cached;
};

} // namespace details

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_EXPANSION_CACHEEXPANSION_HPP_INCLUDED
