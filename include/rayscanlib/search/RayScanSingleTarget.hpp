#ifndef RAYSCANLIB_SEARCH_RAYSCANSINGLETARGETPATH_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_RAYSCANSINGLETARGETPATH_HPP_INCLUDED

#include "fwd.hpp"
#include "RayScanBase.hpp"
#include "RayScanHooks.hpp"
#include <rayscanlib/geometry/AngledSector.hpp>
#include <rayscanlib/utils/StatAverager.hpp>
#include <inx/alg/paring_heap.hpp>
#include <chrono>
#include <optional>

namespace rayscanlib::search
{

namespace modules {
struct RayScanSingleTargetModule : SingleTargetTag { };
} // namespace modules

namespace details {
struct RayScanSingleTargetStats : RayScanStats
{
	uint32 raysCached; /// how many rays were casted
	RayScanSingleTargetStats() noexcept { raysCached = 0; }
	void clear() noexcept
	{
		RayScanStats::clear();
		raysCached = 0;
	}
};
} // namespace details

template <typename... Modules>
class RayScanSingleTargetBase : public search_next_module<modules::RayScanSingleTargetModule, Modules...>::type
{
public:
	using self = RayScanSingleTargetBase<Modules...>;
	using super = typename search_next_module<modules::RayScanSingleTargetModule, Modules...>::type;
	RAYSCANLIB_BASE_CHILD

	struct HeapCmp
	{
		bool operator()(const vertex& a, const vertex& b) const noexcept
		{
			return a.f < b.f;
		}
	};

	RayScanSingleTargetBase() : m_searchComplete(false), m_current(nullptr)
	{ }

	virtual void setupStart(point pt)
	{
		m_start.id = pt;
		m_start.search.s = {m_stats.search.s.id, 0};
		m_start.open = true; m_start.close = false;
		m_start.f = m_start.g = m_start.h = 0;
		m_start.pred = nullptr;
	}
	virtual void setupTarget(point pt)
	{
		m_target.id = pt;
		m_target.search.s = {m_stats.search.s.id, 0};
		m_target.target = true;
		m_target.open = m_target.close = false;
		m_target.f = m_target.g = inf<double>;
		m_target.h = 0;
		m_target.pred = nullptr;
	}

	virtual void setDynamicSearch(bool dyn)
	{
		if constexpr (!has_module<modules::DynamicTag>()) {
			if (dyn)
				throw std::logic_error("not implemented");
		}
	}

	std::optional<double> search(point start, point target, bool retain[[maybe_unused]] = false)
	{
		setupSearchST_();
		auto tp = std::chrono::steady_clock::now();
		m_searchComplete = false;
		m_astarQueue.clear();
		setupStart(start);
		setupTarget(target);
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::BeginShortestPathSearchHook>(start, target); }
		search_();
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::EndShortestPathSearchHook>(m_target.g); }
		m_stats.searchTime = std::chrono::steady_clock::now() - tp;
		return m_target.g == inf<double> ? std::optional<double>() : std::optional<double>(m_target.g);
	}

	void initNode(vertex& node)
	{
		if (node.search.s.id != m_stats.search.s.id) {
			initNode_(node);
		}
	}

	static geometry::AngledSector<coord, Between::NARROW, Dir::CW> getProjectionField(vertex& node) noexcept
	{
		assert(node.pred != nullptr);
		return node.turn == Dir::CW ? geometry::AngledSector<coord, Between::NARROW, Dir::CW>(node.id - node.pred->id, node.ref.getPointCCW() - node.id)
		                            : geometry::AngledSector<coord, Between::NARROW, Dir::CW>(node.ref.getPointCW() - node.id, node.id - node.pred->id);
	}

	template <typename OutIt>
	void getPath(OutIt&& out)
	{
		getPath_();
		for (const point& p : m_path) {
			*out = p;
		}
	}
	const std::vector<point>& getPath()
	{
		getPath_();
		return m_path;
	}

	void setupStats(rayscanlib::utils::StatsTable* table, rayscanlib::utils::Stats* stats) const override
	{
		using namespace std::literals::string_view_literals;
		super::setupStats(table, stats);
		if (table != nullptr) {
			table->emplace_column("sExp"sv, 1000);
			table->emplace_column("sPushed"sv, 1100);
			table->emplace_column("sUpdated"sv, 1200);
			table->emplace_column("rays"sv, 2000);
			table->emplace_column("raysCached"sv, 2100);
			table->emplace_column("setupNs"sv, 9000);
			table->emplace_column("searchNs"sv, 9100);
		}
		if (stats != nullptr) {
			stats->template try_emplace<int64>("sExp"sv);
			stats->template try_emplace<int64>("sPushed"sv);
			stats->template try_emplace<int64>("sUpdated"sv);
			stats->template try_emplace<int64>("rays"sv);
			stats->template try_emplace<int64>("raysCached"sv);
			stats->template try_emplace<std::chrono::nanoseconds>("setupNs"sv, rayscanlib::utils::StatAverager());
			stats->template try_emplace<std::chrono::nanoseconds>("searchNs"sv, rayscanlib::utils::StatAverager());
		}
	}

	void saveStats(rayscanlib::utils::Stats& stats) const override
	{
		using namespace std::literals::string_literals;
		super::saveStats(stats);
		stats.at("sExp"s) = m_stats.search.s.expansions;
		stats.at("sPushed"s) = m_stats.successorsPushed;
		stats.at("sUpdated"s) = m_stats.successorsUpdated;
		stats.at("rays"s) = m_stats.rays;
		stats.at("raysCached"s) = m_stats.raysCached;
		stats.at("setupNs"s) = m_stats.dynSetupTime;
		stats.at("searchNs"s) = m_stats.searchTime - m_stats.rayCastTime;
	}

protected:
	void getPath_()
	{
		m_path.clear();
		for (vertex* at = &m_target; at != nullptr; at = at->pred) {
			m_path.push_back(at);
			at = at->pred;
		}
		std::reverse(m_path.begin(), m_path.end());
	}

	virtual void expandStartNode_(vertex& node)
	{
		assert(&node == &m_start && !node.close);
		node.close = true;
		m_stats.search.s.expansions += 1;
		m_current = &node;
	}
	virtual void expandNode_(vertex& node)
	{
		assert(node.id != this->m_target.id && !node.close);
		node.close = true;
		m_stats.search.s.expansions += 1;
		m_current = &node;
	}

	intersection castRay(point a, point ab)
	{
		m_stats.rays += 1;
		return this->castRay_(a, ab);
	}

	bool castRayNode(const vertex& from, vertex& to)
	{
		m_stats.rays += 1;
		return castRayNode_(from, to);
	}

	bool doneRayNode(const vertex& to)
	{
		return to.search.expansionId == m_stats.search.expansionId;
	}

	virtual bool castRayNode_(const vertex& from, vertex& to)
	{
		assert(to.search.s.id == m_stats.search.s.id);
		if (!doneRayNode(to)) {
			storeRayNode_(to, this->castRay_(from.id, to.id - from.id));
			return true;
		} else {
			m_stats.raysCached += 1;
			if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::RayCastHook>(from.id, to.id - from.id, to.expansionIntersect); }
			return false;
		}
	}

	virtual void storeRayNode_(vertex& to, intersection I)
	{
		to.search.expansionId = this->getStats().search.expansionId;
		to.expansionIntersect = I;
	}

	virtual void initNode_(vertex& node)
	{
		node.search.s = {m_stats.search.s.id, 0};
		node.open = node.close = false;
		node.f = node.g = inf<double>;
		node.h = 0;
		node.pred = nullptr;
	}

	virtual void pushSuccessor_(vertex& successor, double g, const intersection& inst, Dir turn)
	{
		assert(m_current != nullptr);
		assert(successor.search.s.id == m_stats.search.s.id);
		assert(m_current->id != successor.id);
		assert(successor.target || turn == successor.calcTurn(m_current->id));
		m_stats.successorsFound += 1;
		if (g >= successor.g || successor.close) {
			if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::PushSuccessorHook>(std::as_const(*m_current), std::as_const(successor), false, false); }
			return;
		}
		successor.g = g;
		heuristic_(successor);
		successor.f = g + successor.h;
		successor.pred = m_current;
		successor.turn = turn;
		successor.predIntersect = inst;
		if (!successor.open) {
			successor.open = true;
			m_stats.successorsPushed += 1;
			m_astarQueue.push(successor);
			if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::PushSuccessorHook>(std::as_const(*m_current), std::as_const(successor), true, true); }
		} else {
			m_stats.successorsUpdated += 1;
			m_astarQueue.raise(successor);
			if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::PushSuccessorHook>(std::as_const(*m_current), std::as_const(successor), true, false); }
		}
	}

	virtual void pushTarget_(double g)
	{
		if (m_astarQueue.empty() || m_astarQueue.top().f >= g) {
			m_stats.successorsFound += 1;
			m_target.g = g;
			m_target.open = m_target.close = true;
			m_searchComplete = true;
		} else {
			pushSuccessor_(m_target, g, intersection{}, Dir::COLIN);
		}
		if constexpr (search_types::hooks_enabled()) { this->template use_hook_<hooks::PushSuccessorHook>(std::as_const(*m_current), std::as_const(m_target), true, true); }
	}

	virtual bool heuristic_(vertex& node) = 0;

	void search_() override
	{
		expandStartNode_(m_start);

		while (!m_searchComplete && !m_astarQueue.empty()) {
			vertex& expand = m_astarQueue.top();
			if (expand.target) {
				m_searchComplete = true;
				break;
			}
			m_astarQueue.pop();
			expandNode_(expand);
		}
	}
	virtual void setupSearchST_()
	{
		m_stats.clear();
		m_stats.search.s.id += 1;
		m_stats.search.s.expansions = 0;
	}

public:
	const vertex& getStart() const noexcept { return m_start; }
	const vertex& getTarget() const noexcept { return m_target; }
	bool getSearchComplete() const noexcept { return m_searchComplete; }
	const auto& getStats() const noexcept { return m_stats; }
	auto& getStats_() noexcept { return m_stats; }
	const vertex* getCurrent() const noexcept { return m_current; }

protected:
	vertex& getStart_() noexcept { return m_start; }
	vertex& getTarget_() noexcept { return m_target; }
	void setSearchComplete_(bool searchComplete) noexcept { m_searchComplete = searchComplete; }
	vertex* getCurrent_() noexcept { return m_current; }

private:
	bool m_searchComplete;
	vertex* m_current;
	vertex m_start;
	vertex m_target;
	details::RayScanSingleTargetStats m_stats;
	std::vector<point> m_path;
protected:
	inx::alg::pairing_heap<vertex, HeapCmp, modules::RayScanSingleTargetModule> m_astarQueue;
};

namespace details
{

template <typename... Modules>
struct search_data<RayScanVertex, modules::RayScanSingleTargetModule, Modules...>
	: search_next_data<RayScanVertex, modules::RayScanSingleTargetModule, Modules...>::type,
	  inx::alg::pairing_heap_tag<modules::RayScanSingleTargetModule>
{
	using self = search_data<RayScanVertex, modules::RayScanSingleTargetModule, Modules...>;
	using super = typename search_next_data<RayScanVertex, modules::RayScanSingleTargetModule, Modules...>::type;
	RAYSCANLIB_STRUCT_CHILD

	SearchExpansion search;
	bool target;
	bool open;
	bool close;
	Dir turn; /// CW = forwards, CCW = backwards
	double g;
	double h;
	double f;
	vertex* pred;
	intersection expansionIntersect;
	intersection predIntersect;
	search_data() noexcept : search{}, target(false), open(false), close(false), turn(Dir::COLIN), g(0), h(0), f(0), pred(nullptr), expansionIntersect{}, predIntersect{}
	{ }
	
	bool isTarget() const noexcept { return target; }
};

template <typename... Modules>
struct search_module<modules::RayScanSingleTargetModule, Modules...>
{
	using type = RayScanSingleTargetBase<Modules...>;
};

} // namespace details

template <bool Hooks, typename CoordType, typename... Modules>
using RayScanSingleTarget = RayScan<Hooks, CoordType, Modules..., modules::RayScanSingleTargetModule>;

} // namespace rayscanlib::search

#endif // RAYSCANLIB_SEARCH_RAYSCANSINGLETARGETPATH_HPP_INCLUDED
