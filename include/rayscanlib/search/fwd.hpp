#ifndef RAYSCANLIB_SEARCH_FWD_HPP_INCLUDED
#define RAYSCANLIB_SEARCH_FWD_HPP_INCLUDED

#include <rayscanlib/inx.hpp>

namespace rayscanlib::search
{

namespace modules {
struct SingleTargetTag { };
struct MultiTargetTag { };
struct DynamicTag { };
}

}

#endif // RAYSCANLIB_SEARCH_FWD_HPP_INCLUDED
