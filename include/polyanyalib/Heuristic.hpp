#ifndef RAYSCAN_EXT_POLYANYALIB_HEURISTIC_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_HEURISTIC_HPP_INCLUDED

#include "fwd.hpp"

namespace polyanyalib
{

class Heuristic
{
public:
	using point = Point;
	Heuristic() : m_target{}
	{ }
	virtual ~Heuristic() = default;

	virtual void setTarget(const point& pt)
	{
		m_target.target = pt;
	}
	virtual void setTargets(const point* targets, size_t size)
	{
		m_target.targets.p = targets;
		m_target.targets.c = size;
	}

	virtual double getHvalue(const point& root[[maybe_unused]], const point& l[[maybe_unused]], const point& r[[maybe_unused]]) const
	{
		throw std::logic_error("not implmeneted");
	}
	virtual double getMultiHvalue(const point& root[[maybe_unused]], const point& l[[maybe_unused]], const point& r[[maybe_unused]]) const
	{
		throw std::logic_error("not implmeneted");
	}

protected:
	union {
		struct {
			const point* p;
			size_t c;
		} targets;
		point target;
	} m_target;
};
                   
}

#endif // RAYSCAN_EXT_POLYANYALIB_HEURISTIC_HPP_INCLUDED
