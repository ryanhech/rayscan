#ifndef RAYSCAN_EXT_POLYANYALIB_INTERVALFACER_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_INTERVALFACER_HPP_INCLUDED

#include "Facer.hpp"
#include <map>
#include <vector>
#include <inx/alg/redblack_tree.hpp>
#include <inx/data/factory.hpp>

namespace polyanyalib
{

struct IntervalSlab : inx::alg::redblack_tree_tag<IntervalSlab>
{
	double x;
	std::vector<int> polys;
	IntervalSlab(double lx) noexcept : x(lx)
	{ }
};

inline bool operator<(const IntervalSlab& a, const IntervalSlab& b) noexcept { return a.x < b.x; }
inline bool operator>(const IntervalSlab& a, const IntervalSlab& b) noexcept { return a.x > b.x; }
inline bool operator<=(const IntervalSlab& a, const IntervalSlab& b) noexcept { return a.x <= b.x; }
inline bool operator>=(const IntervalSlab& a, const IntervalSlab& b) noexcept { return a.x >= b.x; }
inline bool operator<(double a, const IntervalSlab& b) noexcept { return a < b.x; }
inline bool operator>(double a, const IntervalSlab& b) noexcept { return a > b.x; }
inline bool operator<=(double a, const IntervalSlab& b) noexcept { return a <= b.x; }
inline bool operator>=(double a, const IntervalSlab& b) noexcept { return a >= b.x; }
inline bool operator<(const IntervalSlab& a, double b) noexcept { return a.x < b; }
inline bool operator>(const IntervalSlab& a, double b) noexcept { return a.x > b; }
inline bool operator<=(const IntervalSlab& a, double b) noexcept { return a.x <= b; }
inline bool operator>=(const IntervalSlab& a, double b) noexcept { return a.x >= b; }

class IntervalFacer : public Facer
{
public:
	IntervalFacer() : m_slabsFactory(2048)
	{ }
	void setMesh(const Mesh& mesh) override;
	PointLocation findFace(const Point& p, bool onlyTrav = true) const override;

protected:
	void initalise();

protected:
	inx::data::Factory<IntervalSlab> m_slabsFactory;
	inx::alg::redblack_tree<IntervalSlab, IntervalSlab> m_slabs;
};
                   
}

#endif // RAYSCAN_EXT_POLYANYALIB_INTERVALFACER_HPP_INCLUDED
