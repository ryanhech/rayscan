#ifndef RAYSCAN_EXT_POLYANYALIB_VERTEX_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_VERTEX_HPP_INCLUDED

#include "fwd.hpp"
#include <vector>

namespace polyanyalib
{

// A point in the polygon mesh.
struct Vertex
{
	Point p;
	// "int" here means an array index.
	std::vector<int> polygons;

	int id;
	int corner;

	Vertex() : Vertex(0) { }
	Vertex(int lid) : p{}, id(lid), corner(0) { }

	bool isCorner() const noexcept { assert(corner >= 0); return corner != 0; }
	bool isAmbig() const noexcept { assert(corner >= 0); return corner > 2; }

	void reset() noexcept
	{
		id = 0;
		corner = 0;
	}
	bool isInitalised() const noexcept { return id > 0; }
};

}

#endif // RAYSCAN_EXT_POLYANYALIB_VERTEX_HPP_INCLUDED
