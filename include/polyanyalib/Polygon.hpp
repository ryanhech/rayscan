#ifndef RAYSCAN_EXT_POLYANYALIB_POLYGON_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_POLYGON_HPP_INCLUDED

#include "fwd.hpp"
#include <vector>
#include <rayscanlib/geometry/Box.hpp>
#include <inx/alg/redblack_tree.hpp>

namespace polyanyalib
{

struct Polygon
{
	// "int" here means an array index.
	Polygon() noexcept : Polygon(0) { }
	Polygon(int lid) noexcept : id(lid), traversable(0), is_one_way(false), bounds{}, floodFill(0) { }
	std::vector<int> vertices;
	std::vector<int> polygons;
	int id;
	int traversable;
	bool is_one_way;
	rayscanlib::geometry::Box<double> bounds;
	size_t floodFill;

	int size() const noexcept { return static_cast<int>(vertices.size()); }
	int next(int i) const noexcept { return (i+1) % size(); }
	int prev(int i) const noexcept { return (i+size()-1) % size();}

	bool isTraversable() const noexcept { assert(traversable >= 0); return traversable == 0; }

	// like findVertex but does allow for it to not exists, will return -1 in such case
	int findVertexOrEnd(int id) const noexcept
	{
		int i = static_cast<int>(std::distance(vertices.begin(), std::find(vertices.begin(), vertices.end(), id)));
		return i != static_cast<int>(vertices.size()) ? i : -1;
	}
	int findVertex(int id) const noexcept
	{
#ifndef NDEBUG
		int i = static_cast<int>(std::distance(vertices.begin(), std::find(vertices.begin(), vertices.end(), id)));
		assert(i < static_cast<int>(vertices.size()));
		return i;
#else
		return static_cast<int>(std::distance(vertices.begin(), std::find(vertices.begin(), vertices.end(), id)));
#endif
	}
	int findVertexNeighbour(int id) const noexcept
	{
		return (findVertex(id) + 1) % static_cast<int>(vertices.size());
	}

	void reset() noexcept
	{
		vertices.clear();
		polygons.clear();
		id = 0;
		traversable = 0;
		is_one_way = false;
		floodFill = 0;
	}

	bool fill(size_t id)
	{
		if (floodFill == id)
			return false;
		floodFill = id;
		return true;
	}

	bool isInitalised() const noexcept { return id > 0; }
};

struct EuclideanPolygon
{
	EuclideanPolygon() : EuclideanPolygon(0) { }
	EuclideanPolygon(int lid) : id(lid), vertex(0), cw{}, ccw{} { }
	int id;
	int vertex; // a vertex for the polygon
	Point pt;
	Point cw;
	Point ccw;

	void reset() noexcept
	{
		id = vertex = 0;
	}
};

}

#endif // RAYSCAN_EXT_POLYANYALIB_POLYGON_HPP_INCLUDED
