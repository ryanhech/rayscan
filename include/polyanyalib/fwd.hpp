#ifndef RAYSCAN_EXT_POLYANYALIB_FWD_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_FWD_HPP_INCLUDED

#include <rayscanlib/inx.hpp>

namespace polyanyalib
{
using namespace inx;
}

#include <rayscanlib/geometry/Point.hpp>

namespace polyanyalib
{

using Point = rayscanlib::geometry::Point<double>;
class Config;
class Facer;
class IntervalFacer;
class WalkFacer;
class Mesh;
class WarthogMesh;
template <typename PolyAlg, typename Mesh, typename Facer, typename Heuristic>
class Polyanya;
struct Polygon;
struct PolyNode;
class PolySearch;
struct Successor;
struct Vertex;
class FadeMesh;

enum class MeshMerge : uint32
{
	None,
	Simple,
	Greedy,
};

}

#endif // RAYSCAN_EXT_POLYANYALIB_FWD_HPP_INCLUDED
