#ifndef RAYSCAN_EXT_POLYANYALIB_BENCHMARKMESHWRAPPER_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_BENCHMARKMESHWRAPPER_HPP_INCLUDED

#include "fwd.hpp"
#include "Mesh.hpp"
#include "Config.hpp"
#include <ostream>

namespace polyanyalib
{

template <typename RayScanBenchmark>
class BenchmarkMeshWrapper : public virtual RayScanBenchmark
{
public:
	using super = RayScanBenchmark;

	const std::shared_ptr<Mesh>& getMesh() const noexcept { return m_mesh; }
	void setMesh(std::shared_ptr<Mesh>&& mesh) noexcept { m_mesh = std::move(mesh); }

protected:

	void save_(std::ostream& file, bool env) const override
	{
		saveMesh_(file, env);
		super::save_(file, env);
	}
	virtual void saveMesh_(std::ostream& file, bool env) const
	{
		if (env && m_mesh) {
			if (!m_mesh->isMeshValid())
				throw std::runtime_error("invalid mesh");
			m_mesh->save(file);
		}
	}
	
	void load_(const rayscanlib::scen::Archiver& archive, bool env) override
	{
		loadMesh_(archive);
		super::load_(archive, env);
	}
	virtual void loadMesh_(const rayscanlib::scen::Archiver& archive)
	{
		auto mesh = std::make_unique<Mesh>();
		mesh->load(archive);
		m_mesh = std::move(mesh);
	}

protected:
	std::shared_ptr<Mesh> m_mesh;
};

}

#endif // RAYSCAN_EXT_POLYANYALIB_SCENARIOMESHWRAPPER_HPP_INCLUDED
