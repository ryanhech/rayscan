#ifndef RAYSCAN_EXT_POLYANYALIB_EXPANSION_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_EXPANSION_HPP_INCLUDED

#include "fwd.hpp"
#include "PolyNode.hpp"
#include "Successor.hpp"
#include "Mesh.hpp"

namespace polyanyalib
{

// Gets the h value of a search node with interval l-r and root "root",
// given a goal.
double get_h_value(const Point& root, Point goal,
                   const Point& l, const Point& r);

double get_h_value(const Point& root, const Point& l, const Point& r);

// Generates the successors of the search node and sets them in the successor
// vector. Returns number of successors generated.
int get_successors(PolyNode& node, const Point& start, const Mesh& mesh,
                   Successor* successors);
                   
}

#endif // RAYSCAN_EXT_POLYANYALIB_EXPANSION_HPP_INCLUDED
