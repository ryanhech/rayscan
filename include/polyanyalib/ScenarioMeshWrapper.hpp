#ifndef RAYSCAN_EXT_POLYANYALIB_SCENARIOMESHWRAPPER_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_SCENARIOMESHWRAPPER_HPP_INCLUDED

#include "fwd.hpp"
#include "Mesh.hpp"
#include "Config.hpp"
#include "BenchmarkMeshWrapper.hpp"

namespace polyanyalib
{

template <typename RayScanBenchmark, typename MeshBenchmark>
class ScenarioMeshWrapper : public rsapp::scen::Scenario
{
public:
	using benchmark_type = RayScanBenchmark;
	using benchmark_ptr = std::shared_ptr<benchmark_type>;
	using benchmark_mesh_type = MeshBenchmark;
	void load(const archiver& archive) override
	{
		m_benchmark = std::make_shared<benchmark_mesh_type>();
		m_benchmark->load(archive);
		if (m_filter) {
			m_benchmark->applyFilter(*m_filter);
		}
	}

	bool run(rsapp::run::InstanceController& inst, std::ostream& results) override
	{
		return inst.runBenchmark(std::make_any<benchmark_ptr>(std::static_pointer_cast<benchmark_type>(m_benchmark)), results);
	}

	void saveFiltered(std::ostream& scenario) override
	{
		if (m_benchmark == nullptr)
			throw std::runtime_error("benchmark not loaded");
		auto filteredBenchmark = m_benchmark->refineBenchmarkByAppliedFilter();
		filteredBenchmark->save(scenario);
	}

	const std::shared_ptr<benchmark_type>& benchmark() const noexcept { return m_benchmark; }

protected:
	benchmark_ptr m_benchmark;
};

template <typename RayScanBenchmark>
using ScenarioMeshWrapperAuto = ScenarioMeshWrapper<RayScanBenchmark, BenchmarkMeshWrapper<RayScanBenchmark>>;

}

#endif // RAYSCAN_EXT_POLYANYALIB_SCENARIOMESHWRAPPER_HPP_INCLUDED
