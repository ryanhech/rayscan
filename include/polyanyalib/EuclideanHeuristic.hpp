#ifndef RAYSCAN_EXT_POLYANYALIB_EUCLIDEANHEURISTIC_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_EUCLIDEANHEURISTIC_HPP_INCLUDED

#include "fwd.hpp"
#include "Heuristic.hpp"
#ifndef NDEBUG
#include "Expansion.hpp"
#endif

namespace polyanyalib
{

class EuclideanHeuristic final : public Heuristic
{
public:
	double getHvalue(const point& root, const point& l, const point& r) const override
	{
		return getHvalueTarget(root, l, r, m_target.target);
	}
	double getMultiHvalue(const point& root, const point& l, const point& r) const override
	{
		return getHvalueTargets(root, l, r, m_target.targets.p, m_target.targets.c);
	}

	static double getHvalueTarget(const point& root, const point& l, const point& r, const point& target)
	{
		namespace geo = ::rayscanlib::geometry;
		const point lr = r - l;
		const point lroot = root - l;
		
		if (lr.isZero()) {
			const point rootT = target - root;
			if (rootT.isColin(lroot) && rootT * -lroot > point::neg_epsilon()) { // in angled sector l-r
				return rootT.length();
			} else {
				return lroot.length() + l.distance(target);
			}
		}
		if (auto lrXlroot = lr.cross(lroot); point::isColin(lrXlroot)) { // l, r, root are all collinear
			if (auto cps = geo::collinear_point_on_segment(lroot, lr); geo::pr_op<geo::PRop::ltZero>(cps)) { // collin: root - l - r
				return lroot.length() + l.distance(target);
			} else if (geo::pr_op<geo::PRop::gtOne>(cps)) { // collin: l - r - root
				return r.distance(root) + r.distance(target);
			} else { // collin: l - root - r; l or r may equal to root
				return root.distance(target);
			}
		} else if (point::isCW(lrXlroot) == lr.isCW(target - l)) { // point and target are on same side
			const point rootR = reflect(lroot, lr) + l;
			if (rootR.isCW(l, target)) { // target is to left of l
				return lroot.length() + l.distance(target);
			} else if (rootR.isCCW(r, target)) { // target is to right of r
				return r.distance(root) + r.distance(target);
			} else { // target distance is mirrored
				return root.distance(reflect(target - l, lr) + l); // distance to target mirror
			}
		} else { // point and target are on opposite sides or target is collinear to lr
			if (root.isCCW(l, target)) { // target is to left of l
				return lroot.length() + l.distance(target);
			} else if (root.isCW(r, target)) {
				return r.distance(root) + r.distance(target);
			} else {
				return root.distance(target); // target is visible through interval
			}
		}
	}

	static double getHvalueTargetRootReflect(const point& root, const point& rootR, const point& l, const point& r, const point& target)
	{
		namespace geo = ::rayscanlib::geometry;
		const point lr = r - l;
		const point lroot = root - l;
		if (auto lrXlroot = lr.cross(lroot); point::isColin(lrXlroot)) { // l, r, root are all collinear
			if (auto cps = geo::collinear_point_on_segment(lroot, lr); geo::pr_op<geo::PRop::ltZero>(cps)) { // collin: root - l - r
				return lroot.length() + l.distance(target);
			} else if (geo::pr_op<geo::PRop::gtOne>(cps)) { // collin: l - r - root
				return r.distance(root) + r.distance(target);
			} else { // collin: l - root - r; l or r may equal to root
				return root.distance(target);
			}
		} else if (point::isCW(lrXlroot) == lr.isCW(target - l)) { // point and target are on same side
			if (rootR.isCW(l, target)) { // target is to left of l
				return lroot.length() + l.distance(target);
			} else if (rootR.isCCW(r, target)) { // target is to right of r
				return r.distance(root) + r.distance(target);
			} else { // target distance is mirrored
				return root.distance(reflect(target - l, lr) + l); // distance to target mirror
			}
		} else { // point and target are on opposite sides or target is collinear to lr
			if (root.isCCW(l, target)) { // target is to left of l
				return lroot.length() + l.distance(target);
			} else if (root.isCW(r, target)) {
				return r.distance(root) + r.distance(target);
			} else {
				return root.distance(target); // target is visible through interval
			}
		}
	}

	static double getHvalueTargets(const point& root, const point& l, const point& r, const point* targets, size_t count)
	{
		namespace geo = ::rayscanlib::geometry;
		if (count == 0)
			return 0.0;
		const point lr = r - l;
		const point lroot = root - l;
		if (auto lrXlroot = lr.cross(lroot); point::isColin(lrXlroot)) { // l, r, root are all collinear
			if (auto cps = geo::collinear_point_on_segment(lroot, lr); geo::pr_op<geo::PRop::ltZero>(cps)) { // collin: root - l - r
				return lroot.length() + MinReduce::min(l, targets, count);
			} else if (geo::pr_op<geo::PRop::gtOne>(cps)) { // collin: l - r - root
				return r.distance(root) + MinReduce::min(r, targets, count);
			} else { // collin: l - root - r; l or r may equal to root
				return MinReduce::min(root, targets, count);
			}
		} else {
			const point rootR = reflect(lroot, lr) + l;
			const double lrootlen = lroot.length();
			const double rrootlen = r.distance(root);
			double ans = inf<double>;
			for ( ; count != 0; --count) { // loop all targets
				double ansT;
				if (point::isCW(lrXlroot) == lr.isCW(*targets - l)) { // point and target are on same side
					const point rootR = reflect(lroot, lr) + l;
					if (rootR.isCW(l, *targets)) { // target is to left of l
						ansT = lrootlen + l.distance(*targets);
					} else if (rootR.isCCW(r, *targets)) { // target is to right of r
						ansT = rrootlen + r.distance(*targets);
					} else { // target distance is mirrored
						ansT = root.distance(reflect(*targets - l, lr) + l); // distance to target mirror
					}
				} else { // point and target are on opposite sides or target is collinear to lr
					if (root.isCCW(l, *targets)) { // target is to left of l
						ansT = lrootlen + l.distance(*targets);
					} else if (root.isCW(r, *targets)) {
						ansT = rrootlen + r.distance(*targets);
					} else {
						ansT = root.distance(*targets); // target is visible through interval
					}
				}
				if (ansT < ans) ans = ansT; // update mininum
				++targets; // next target
			}
			return ans;
		}
	}

	// reflect vector x (pointing away from origin) through vector n, resulting point away from origin
	static point reflect(const point& x, const point& n) noexcept
	{
		assert(!x.isZero() && !n.isZero() && !x.isColin(n));
		return (2.0 * ( (x * n) / (n * n) )) * n - x;
	}

	struct MinReduce
	{
		const point pt;
		MinReduce(const point& p) noexcept : pt(p)
		{ };
		static double min(const point& p, const point* targets, size_t size)
		{
			const MinReduce mr(p);
			return mr.calcMin(targets, size);
		}
		double calcMin(const point* targets, size_t size) const
		{
			return std::transform_reduce(targets, targets + size, 0.0, &MinReduce::minOp, *this);
		}
		double operator()(const point& a) const noexcept
		{
			return pt.square(a);
		}
		static double minOp(double a, double b) noexcept
		{
			return (b < a) ? b : a;
		}
	};
};
                   
}

#endif // RAYSCAN_EXT_POLYANYALIB_EUCLIDEANHEURISTIC_HPP_INCLUDED
