#ifndef RAYSCAN_EXT_POLYANYALIB_TESTERFACER_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_TESTERFACER_HPP_INCLUDED

#include "Facer.hpp"

namespace polyanyalib
{

template <typename F1, typename F2>
class TesterFacer : public Facer
{
public:
	void setMesh(const Mesh& mesh) override
	{
		Facer::setMesh(mesh);
		m_facer1.setMesh(mesh);
		m_facer2.setMesh(mesh);
	}

	PointLocation findFace(const Point& p) const override
	{
		PointLocation a1 = m_facer1.findFace(p);
		PointLocation a2 = m_facer2.findFace(p);
		assert(a1 == a2);
		if (a1 != a2)
			throw std::runtime_error("facer produced different results");
		return a1;
	}

private:
	F1 m_facer1;
	F2 m_facer2;
};
                   
}

#endif // RAYSCAN_EXT_POLYANYALIB_TESTERFACER_HPP_INCLUDED
