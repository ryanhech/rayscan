#ifndef RAYSCAN_EXT_POLYANYALIB_MESH_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_MESH_HPP_INCLUDED

#include "fwd.hpp"
#include "MeshStructs.hpp"
#include "Polygon.hpp"
#include "Vertex.hpp"
#include "Facer.hpp"
#include <rayscanlib/geometry/Box.hpp>
#include <rayscanlib/scen/PolygonEnvironment.hpp>
#include <rayscanlib/scen/Archiver.hpp>
#include <vector>
#include <iostream>
#include <map>
#include <memory>
#include <chrono>
#include <unordered_map>
#include <memory_resource>

namespace polyanyalib
{

class Mesh
{
public:
	constexpr static uint32 VERSION = 1;
	// id relative to polygon edge
	struct LocalId
	{
		int id;
		bool operator<(LocalId b) const noexcept { return id < b.id; }
		bool operator==(LocalId b) const noexcept { return id == b.id; }
	};
	// id relative to mesh_ variables
	struct GlobalId
	{
		int id;
		bool operator<(GlobalId b) const noexcept { return id < b.id; }
		bool operator==(GlobalId b) const noexcept { return id == b.id; }
	};

	using environment = rayscanlib::scen::PolygonEnvironment;
	using polygon_id = std::pmr::unordered_map<const void*, GlobalId>;
	Mesh();
	Mesh(const Mesh&) = delete;
	Mesh(Mesh&&) = delete;
	virtual ~Mesh();

	virtual void clear(bool release = true);
	void copy(const Mesh& mesh);
	virtual void loadEnvironment(const environment& env);

	void print(std::ostream& outfile);
	bool lineEnterPolygonFromVertex(const Point& ab, const Polygon& poly, LocalId i);
	bool lineEnterPolygonFromEdgeId(const Point& ab, GlobalId id1, GlobalId id2);
	bool polygonFaceWithinEdge(const Point& cw, const Point& ccw, const Polygon& poly, LocalId i); // global vertex id
	LocalId findFaceFromVertex(const Point& cw, const Point& ccw, GlobalId vid);
	PolyContainment polyContainsPoint(int poly, const Point& p) const;
	PointLocation findFaceFromPoint(const Point& p, bool onlyTrav = true) const;

	void print_polygon(std::ostream& outfile, int index);
	void print_vertex(std::ostream& outfile, int index);

	int64_t getSetupNano() const noexcept { return m_setupTime.count(); }

	void insertEuclideanPolygon(const void* p, const environment::polygon& polygon, bool skip);
	void eraseEuclideanPolygon(const void* p, bool skip);
	void finalise();

	GlobalId newVertex();
	void freeVertex(GlobalId i);
	GlobalId newPolygon();
	void freePolygon(GlobalId i);
	GlobalId newEuclidean();
	void freeEuclidean(GlobalId i);

	template <typename Fn>
	void floodFill(GlobalId initalPolygon, Fn&& fn)
	{
		size_t ff = (m_floodFillId += 1);
		m_floodFillStack.clear();
		m_floodFillStack.push_back(initalPolygon);
		mesh_polygons[initalPolygon.id].fill(ff);
		while (!m_floodFillStack.empty()) {
			GlobalId id = m_floodFillStack.back(); m_floodFillStack.pop_back();
			for (auto i : mesh_polygons[id.id].polygons) {
				if (i > 0 && mesh_polygons[i].fill(ff))
					m_floodFillStack.push_back(GlobalId{i});
			}
			fn(id);
		}
	}

	template <typename FacerType, typename... Args>
	void emplace_facer(Args&&... args)
	{
		m_facer = std::make_unique<FacerType>(std::forward<Args>(args)...);
	}
	void facerInitalise();
	const Facer* getFacer() const noexcept { return m_facer.get(); }
	void meshFaceMerger(MeshMerge type);

	bool isMeshValid();

	void save(std::ostream& out) const;
	void save(::rayscanlib::scen::Archiver& archive, std::filesystem::path filename = std::filesystem::path()) const;
	void load(const ::rayscanlib::scen::Archiver& archive);

	// marks faces traversable or not based on PolygonEnvironment
	void autoTraversable(const environment& env);
	void autoConnect();
	void autoVertexSetup();
	void autoCalcBounds();

protected:
	void meshFaceMergeSimple(Polygon& poly);
	void meshFaceMergeGreedy(Polygon& poly);
	//void facerInsert(Polygon& ply);
	//void facerReinsert(Polygon& ply);
	//void facerErase(Polygon& ply);
	//void facerInsertOp(Polygon& ply);
	//void facerReinsertOp(Polygon& ply);
	//void facerEraseOp(Polygon& ply);
	//void facerOperations();
	GlobalId insertLine(PointLocation& pl, const Point& a, const Point& b); // returns index of a point
	void insertLineEdge_(PointLocation& pl, Polygon& polygon, GlobalId nextpoly, GlobalId nextvert, LocalId p);
	GlobalId insertLineFace_(PointLocation& pl, GlobalId poly, GlobalId nextpoly, LocalId p1, LocalId p2, double s1, double s2);
	void insertLineA_(Polygon& ply, const Point& a, LocalId i = LocalId{0}); // new point LocalId is same as i
	void insertLineA_(GlobalId poly, const Point& a, LocalId i = LocalId{0}); // new point LocalId is same as i
	bool insertLineAB_(Polygon& ply, const Point& a, const Point& b, LocalId i = LocalId{0}); // returns false if a is before b, thus false a-id = i, b = i+1
	bool insertLineAB_(GlobalId poly, const Point& a, const Point& b, LocalId i = LocalId{0}); // returns false if a is before b, thus false a-id = i, b = i+1
	GlobalId splitFace(GlobalId polygon, LocalId from, LocalId to, double fromM, double toM);
	GlobalId splitFace(GlobalId polygon, GlobalId from, GlobalId to, double fromM, double toM);
	GlobalId insertPoint(Polygon& orig, LocalId i, const Point& pt);
	GlobalId insertPoint(GlobalId polygon, LocalId i, const Point& pt);
	GlobalId insertPoint(GlobalId polygon, GlobalId vertex, const Point& pt);
	GlobalId insertPoint(GlobalId polygon, LocalId i, double iM);
	GlobalId insertPoint(GlobalId polygon, GlobalId vertex, double iM);
	void erasePoint(Vertex& orig);
	void navMeshDivide(Polygon& ply, LocalId i);
	void navMeshDivide(GlobalId polygon, GlobalId vertex);
	void navMeshConnect(Polygon& ply, LocalId i);
	std::vector<GlobalId>& findAdjVertices(Vertex& vtx);
	std::vector<GlobalId>& findOrderedVertices(Vertex& vtx);
	void calcBounds(Polygon& poly);
	double calcArea2(Polygon& poly);
	bool isPolygonValid(GlobalId poly, bool checkMesh = false);
	bool isVertexValid(GlobalId poly);

	void faceMerge(Polygon& face, Polygon& merge, LocalId faceNei);

	LocalId convVertexId(const Polygon& polygon, GlobalId vertex)
	{
		int s = static_cast<int>(std::distance(polygon.vertices.begin(), std::find(polygon.vertices.begin(), polygon.vertices.end(), vertex.id)));
		assert(0 <= s && s < static_cast<int>(polygon.vertices.size()));
		return LocalId{s};
	}
	template <typename... GlobalIds>
	std::array<LocalId, sizeof...(GlobalIds)> convVertexIds(const Polygon& polygon, GlobalIds... vertices)
	{
		static_assert(sizeof...(GlobalIds) > 0, "There must be at least 1 argument");
		if constexpr (sizeof...(GlobalIds) == 1) {
			return {convVertexId(polygon, vertices...)};
		} else {
			std::array<LocalId, sizeof...(GlobalIds)> ans;
			for (int i = 0, ie = static_cast<int>(polygon.vertices.size()); i < ie; i++) {
				int x = polygon.vertices[i];
				int j = 0;
				( (vertices.id == x ? (ans[j] = LocalId{i}, true) : (j++, false)) || ... );
			}
			return ans;
		}
	}

public:
	const auto& getBounds() const { return bounds; }

public:
	std::vector<Vertex> mesh_vertices;
	std::vector<Polygon> mesh_polygons;
	std::vector<EuclideanPolygon> mesh_euclidean;
	int max_poly_sides;
	std::chrono::nanoseconds m_setupTime;
	std::chrono::nanoseconds m_mergeTime;
protected:
	std::vector<GlobalId> mesh_verticesReuse;
	std::vector<GlobalId> mesh_polygonsReuse;
	std::vector<GlobalId> mesh_euclideanReuse;
	std::vector<GlobalId> m_removeVertices;
	std::vector<GlobalId> m_adjVertices;
	rayscanlib::geometry::Box<double> bounds;
	std::pmr::unsynchronized_pool_resource m_polygonRefPool;
	polygon_id m_polygonRef;
	size_t m_floodFillId;
	std::vector<GlobalId> m_floodFillStack;
	std::unique_ptr<Facer> m_facer;
};

std::istream& operator>>(std::istream& in, Mesh& mesh);
std::ostream& operator<<(std::ostream& out, const Mesh& mesh);

}

#endif // RAYSCAN_EXT_POLYANYALIB_MESH_HPP_INCLUDED
