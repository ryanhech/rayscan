#ifndef RAYSCAN_EXT_POLYANYALIB_TARGETHEURISTIC_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_TARGETHEURISTIC_HPP_INCLUDED

#include "fwd.hpp"
#include "Heuristic.hpp"
#include <rayscanlib/geometry/util/HilbertRtree.hpp>

namespace polyanyalib
{

class TargetHeuristic final : public Heuristic
{
public:
	struct TargetData : ::rayscanlib::geometry::util::HilbertRtreeBox<double>
	{
		point id;
	};
	using rtree = ::rayscanlib::geometry::util::HilbertRtree<TargetData, 8, 16>;
	
	//double getMultiHvalue(const point& root, const point& l, const point& r) const override;
	//void setTargets(const point* targets, size_t size) override;

	struct RTreeMinHeap
	{
		using value_type = std::pair<double, const typename rtree::node_type*>;
		static bool heap_op(const value_type& a, const value_type& b) noexcept { return a.first > b.first; }

		void push(double value, const typename rtree::node_type* entry)
		{
			m_heap.emplace_back(value, entry);
			std::push_heap(m_heap.begin(), m_heap.end(), &heap_op);
		}
		const auto& top() const noexcept { return m_heap.front(); }
		const auto pop()
		{
			std::pop_heap(m_heap.begin(), m_heap.end(), &heap_op);
			m_heap.pop_back();
		}

		bool empty() const noexcept { return m_heap.empty(); }
		size_t size() const noexcept { return m_heap.size(); }
		void clear() noexcept { m_heap.clear(); }
		auto& heap() noexcept { return m_heap; }
		const auto& heap() const noexcept { return m_heap; }
		std::vector<value_type> m_heap;
	};

protected:
	rtree m_rtree;
	RTreeMinHeap m_heap;
};
                   
}

#endif // RAYSCAN_EXT_POLYANYALIB_TARGETHEURISTIC_HPP_INCLUDED
