#ifndef RAYSCAN_EXT_POLYANYALIB_POLYANYALIB_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_POLYANYALIB_HPP_INCLUDED

#include "fwd.hpp"
#include "Stats.hpp"
#include "Mesh.hpp"
#include "Config.hpp"
#include <rayscanlib/scen/PolygonEnvironment.hpp>
#include <rayscanlib/scen/StaticBenchmarkSingleTarget.hpp>
#include <rayscanlib/geometry/Polygon.hpp>
#include <rayscanlib/utils/StatsTable.hpp>
#include <rayscanlib/utils/Stats.hpp>
#include <rayscanlib/utils/StatAverager.hpp>
#include <rayscanlib/scen/PolygonEnvironment.hpp>
#include <chrono>
#include <optional>

namespace polyanyalib
{

template <typename PolyAlg, typename Mesh, typename Facer, typename Heuristic>
class Polyanya
{
public:
	constexpr static size_t reload_threshold = 1000;
	using point = Point;
	static constexpr bool is_single_target_search() noexcept { return PolyAlg::is_polyanya_single_target(); }
	static constexpr bool is_multi_target_search() noexcept { return PolyAlg::is_polyanya_multi_target(); }
	static constexpr bool is_dynamic_search() noexcept { return true; }
	
	Polyanya() : m_mesh{}, m_alg{}, m_start{}, m_reload(0), m_reloadThreshold(reload_threshold)
	{
		if (Config* conf = Config::instance(); conf) {
			m_mergeMesh = conf->getMeshMerge();
			if (auto re = conf->param("reload"); re) {
				m_reloadThreshold = boost::lexical_cast<int32>(*re);
			}
		}
	}

	template <typename Env>
	std::chrono::nanoseconds loadEnvironment(const Env& env, bool reload = false)
	{
		if (!reload)
			m_stats.reset();
		auto tp = std::chrono::steady_clock::now();
		m_mesh.template emplace_facer<Facer>();
		m_mesh.loadEnvironment(env);
		m_mesh.meshFaceMerger(m_mergeMesh);
		m_mesh.getFacer()->resetTime();
		m_mesh.facerInitalise();
		if (!m_mesh.isMeshValid())
			throw std::runtime_error("invalid mesh");
		if (!reload) {
			m_alg.setMesh(m_mesh);
			auto dur = std::chrono::steady_clock::now() - tp;
			m_stats.meshTime = dur;
			m_stats.setupFacerTime = m_mesh.getFacer()->getTime();
			m_alg.setHeuristic(m_heuristic);
		}
		m_reload = 0;
		return m_stats.meshTime;
	}

	std::chrono::nanoseconds loadMesh(const ::polyanyalib::Mesh& mesh)
	{
		m_stats.reset();
		auto tp = std::chrono::steady_clock::now();
		m_mesh.copy(mesh);
		assert(m_mesh.isMeshValid());
		m_mesh.template emplace_facer<Facer>();
		m_mesh.meshFaceMerger(m_mergeMesh);
		m_mesh.getFacer()->resetTime();
		m_mesh.facerInitalise();
		if (!m_mesh.isMeshValid())
			throw std::runtime_error("invalid mesh");
		m_alg.setMesh(m_mesh);
		auto dur = std::chrono::steady_clock::now() - tp;
		m_stats.meshTime = dur;
		m_stats.setupFacerTime = m_mesh.getFacer()->getTime();
		m_alg.setHeuristic(m_heuristic);
		m_reload = 0;
		return m_stats.meshTime;
	}

	void envInitalise(const rayscanlib::scen::PolygonEnvironment* env[[maybe_unused]])
	{
		m_mesh.getFacer()->resetTime();
	}

	void envFinalise(const rayscanlib::scen::PolygonEnvironment* env)
	{
		if (env && static_cast<uint32>(m_reload) > static_cast<uint32>(m_reloadThreshold)) {
			loadEnvironment(*env, true);
			assert(m_reload == 0);
			m_alg.updateMesh();
		} else {
			m_mesh.finalise();
			m_alg.updateMesh();
		}
		m_stats.setupFacerTime = m_mesh.getFacer()->getTime();
	}

	void envFinaliseNull(const rayscanlib::scen::PolygonEnvironment* env[[maybe_unused]])
	{
		m_stats.setupFacerTime = std::chrono::nanoseconds(0);
	}
	
	template <typename Box>
	void envSetBox(const Box&)
	{ }
	
	template <typename Crd>
	void envAddObstacle(const void* uid, const rayscanlib::geometry::Polygon<Crd>& poly)
	{
		m_mesh.insertEuclideanPolygon(uid, poly, static_cast<uint32>(m_reload) > static_cast<uint32>(m_reloadThreshold));
	}
	
	void envRemoveObstacle(const void* uid)
	{
		m_mesh.eraseEuclideanPolygon(uid, static_cast<uint32>(++m_reload) > static_cast<uint32>(m_reloadThreshold));
	}

	void finalise()
	{
		m_alg.setMesh(m_mesh);
	}

	void setStart(point pt)
	{
		m_start = pt;
	}
	void setTarget(point pt)
	{
		m_targets.clear();
		m_targets.emplace_back(pt);
	}
	void setTargets(const point* pt, size_t count)
	{
		m_targets.clear();
		m_targets.insert(m_targets.end(), pt, pt+count);
	}

	void search(bool retain = false)
	{
		m_stats.instID += 1;
		m_stats.clear();
		auto tp = std::chrono::steady_clock::now();
		if constexpr (is_single_target_search()) {
			m_heuristic.setTarget(m_targets.at(0));
			m_alg.set_start_target(m_start, m_targets.at(0));
		} else if constexpr (is_multi_target_search()) {
			m_heuristic.setTargets(m_targets.data(), m_targets.size());
			m_alg.set_start_target(m_start, m_targets.size(), m_targets.data());
		} else {
			throw std::logic_error("invalid polyanya type");
		}
		bool s = m_alg.search(retain);
		m_stats.searchTime = std::chrono::steady_clock::now() - tp;
		m_alg.copyStats(m_stats);
		if (s) {
			m_cost = m_alg.get_cost();
		} else {
			m_cost = std::nullopt;
		}
	}

	std::optional<double> search(point start, point target, bool retain = false)
	{
		if constexpr (!is_single_target_search())
			throw std::logic_error("invalid call for single target search");
		setStart(start);
		setTarget(target);
		search(retain);
		return m_cost;
	}
	std::optional<double> search(point start, const point* targets, size_t count)
	{
		if constexpr (!is_multi_target_search())
			throw std::logic_error("invalid call for multi target search");
		setStart(start);
		setTargets(targets, count);
		search();
		return m_cost;
	}

	virtual void setDynamicSearch(bool dyn)
	{
		m_alg.deadend = !dyn;
	}

	void setupStats(rayscanlib::utils::StatsTable* table, rayscanlib::utils::Stats* stats)
	{
		using namespace std::literals::string_view_literals;
		if (table != nullptr) {
			table->emplace_column("sExp"sv, 1000);
			table->emplace_column("sPushed"sv, 1100);
			table->emplace_column("sUpdated"sv, 1200);
			table->emplace_column("sPruned"sv, 1300);
			table->emplace_column("setupNs"sv, 9000);
			table->emplace_column("searchNs"sv, 9100);
		}
		if (stats != nullptr) {
			stats->template try_emplace<std::int64_t>("sExp"sv);
			stats->template try_emplace<std::int64_t>("sPushed"sv);
			stats->template try_emplace<std::int64_t>("sUpdated"sv);
			stats->template try_emplace<std::int64_t>("sPruned"sv);
			stats->template try_emplace<std::chrono::nanoseconds>("setupNs"sv, rayscanlib::utils::StatAverager());
			stats->template try_emplace<std::chrono::nanoseconds>("searchNs"sv, rayscanlib::utils::StatAverager());
		}
	}

	void saveStats(rayscanlib::utils::Stats& stats)
	{
		using namespace std::literals::string_literals;
		stats.at("sExp"s) = m_stats.popped;
		stats.at("sPushed"s) = m_stats.pushed;
		stats.at("sUpdated"s) = m_stats.successors;
		stats.at("sPruned"s) = m_stats.pruned;
		stats.at("setupNs"s) = m_stats.dynSetupTime;
		stats.at("searchNs"s) = m_stats.searchTime - m_stats.searchFacerTime;
	}

	auto getCost() const noexcept { return m_cost; }
	const PolyanyaStatsST& getStats() { return m_stats; }
	PolyanyaStatsST& getStats_() { return m_stats; }
	std::chrono::nanoseconds getSetupFacerTime() const noexcept { return m_mesh.getFacer()->getTime(); }

	void setVerbose(bool v) noexcept { m_alg.verbose = v; }
	void setDeadend(bool v) noexcept { m_alg.deadend = v; }

	template <typename OutIt>
	void getPath(OutIt&& it)
	{
		getPath_();
		for (const point& p : m_path) {
			*it = p;
		}
	}
	const std::vector<point>& getPath()
	{
		getPath_();
		return m_path;
	}

	Mesh& getMesh() noexcept { return m_mesh; }
	PolyAlg& getAlg() noexcept { return m_alg; }
	Heuristic& getHeuristic() noexcept { return m_heuristic; }

protected:
	void getPath_()
	{
		m_alg.getPath(m_path);
	}

private:
	Mesh m_mesh;
	PolyAlg m_alg;
	Heuristic m_heuristic;
	point m_start;
	std::vector<point> m_targets;
	std::vector<point> m_path;
	std::optional<double> m_cost;
	PolyanyaStatsST m_stats;
	MeshMerge m_mergeMesh;
	int32 m_reload;
	int32 m_reloadThreshold;
};

struct RayScanBenchmarkMesh
{
	using scen_base = ::rayscanlib::scen::StaticBenchmarkSingleTarget;
	using list_type = std::list<Mesh* (*)(const scen_base& benchmark)>;
	static list_type s_meshConvs;
	template <typename T>
	static void registerType()
	{
		s_meshConvs.emplace_back(&RegTypeExec<T>::run);
	}
	static void clear()
	{
		s_meshConvs.clear();
	}
	template <typename T>
	struct RegTypeExec
	{
		static Mesh* run(const scen_base& benchmark)
		{
			if (const auto* mesh = dynamic_cast<const T*>(&benchmark); mesh != nullptr) {
				return mesh->getMesh().get();
			} else {
				return nullptr;
			}
		}
	};
};

} // namespace polyanyalib

namespace rayscanlib::scen
{

template <typename PolyAlg, typename Mesh, typename Facer, typename Heuristic, typename Benchmark>
struct rayscan_environment_exec<::polyanyalib::Polyanya<PolyAlg, Mesh, Facer, Heuristic>, Benchmark>
{
	std::chrono::nanoseconds operator()(::polyanyalib::Polyanya<PolyAlg, Mesh, Facer, Heuristic>& rayscan, const Benchmark& benchmark)
	{				
		for (auto& exec : polyanyalib::RayScanBenchmarkMesh::s_meshConvs) {
			if (const auto* mesh = exec(benchmark); mesh != nullptr) {
				auto dur = rayscan.loadMesh(*mesh);
				rayscan.getStats_().pushSetupTime(dur);
				return dur;
			}
		}
		throw std::logic_error("not registered");
	}
};

} // namespace rayscanlib::scen

#endif // RAYSCAN_EXT_POLYANYALIB_POLYANYALIB_HPP_INCLUDED
