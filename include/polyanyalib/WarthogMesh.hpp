#ifndef RAYSCAN_EXT_POLYANYALIB_WARTHOGMESH_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_WARTHOGMESH_HPP_INCLUDED

#include "Mesh.hpp"

namespace polyanyalib
{

class WarthogMesh : public Mesh
{
public:
	void load(std::istream& mesh, bool ir = false);
	using Mesh::load;
};

}

#endif // RAYSCAN_EXT_POLYANYALIB_WARTHOGMESH_HPP_INCLUDED
