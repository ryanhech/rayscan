#ifndef RAYSCAN_EXT_POLYANYALIB_WALKFACER_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_WALKFACER_HPP_INCLUDED

#include "Facer.hpp"

namespace polyanyalib
{

class WalkFacer : public Facer
{
public:
	WalkFacer() : m_startFace(0)
	{ }
	PointLocation findFace(const Point& p, bool onlyTrav = true) const override;
private:
	mutable int m_startFace;
};
                   
}

#endif // RAYSCAN_EXT_POLYANYALIB_WALKFACER_HPP_INCLUDED
