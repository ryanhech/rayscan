#ifndef RAYSCAN_EXT_POLYANYA_UTILITY_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYA_UTILITY_HPP_INCLUDED

#include "fwd.hpp"
#include "Mesh.hpp"
#include <rayscanlib/scen/PolygonEnvironment.hpp>
#include <rayscanlib/geometry/Box.hpp>
#include <vector>

namespace polyanyalib
{

class SaveData : public std::vector<std::vector<bool>>
{
public:
	using point = ::rayscanlib::geometry::Point<double>;
	using box_type = ::rayscanlib::geometry::Box<double>;
	using env_type = ::rayscanlib::scen::PolygonEnvironment;
	using mesh_type = ::polyanyalib::Mesh;
	static constexpr ssize_t Padding = 4;
	double scale;
	double iscale;
	box_type box;
	ssize_t width;
	ssize_t height;
	std::variant<env_type*, mesh_type*> env;
	point cellToEnv(ssize_t x, ssize_t y);
	std::pair<ssize_t,ssize_t> envToCell(point pt);
	void loadEnvironment(env_type& e);
	void loadMesh(mesh_type& m);
	void populateVars(double scale, bool setupData);
};

} // namespace polyanya

#endif
