#ifndef POLYANYALIB_CONFIG_HPP_INCLUDED
#define POLYANYALIB_CONFIG_HPP_INCLUDED

#include "fwd.hpp"
#include <rsapp/conf/Config.hpp>

namespace polyanyalib
{

class Config : public rsapp::conf::Config
{
public:
	using super = rsapp::conf::Config;
	Config();
	~Config();

protected:

public:
	static Config* instance() noexcept { return dynamic_cast<Config*>(super::instance()); }
	static void instanceCreate();

	MeshMerge getMeshMerge() const;

protected:
	std::ofstream m_outputFile;
};

} // namespace rsapp::conf

#endif // POLYANYALIB_CONFIG_HPP_INCLUDED
