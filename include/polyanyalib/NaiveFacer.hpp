#ifndef RAYSCAN_EXT_POLYANYALIB_NAIVEFACER_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_NAIVEFACER_HPP_INCLUDED

#include "Facer.hpp"

namespace polyanyalib
{

class NaiveFacer : public Facer
{
public:
	PointLocation findFace(const Point& p, bool onlyTrav = true) const override;
};
                   
}

#endif // RAYSCAN_EXT_POLYANYALIB_NAIVEFACER_HPP_INCLUDED
