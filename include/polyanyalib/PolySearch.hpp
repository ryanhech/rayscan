#ifndef RAYSCAN_EXT_POLYANYALIB_POLYSEARCH_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_POLYSEARCH_HPP_INCLUDED

#include "fwd.hpp"
#include "PolyNode.hpp"
#include "Mesh.hpp"
#include "Successor.hpp"
#include "cpool.h"
#include "Stats.hpp"
#include "Heuristic.hpp"
#include <queue>
#include <vector>

namespace polyanyalib
{

class PolySearch
{
public:
	static constexpr bool is_polyanya_single_target() { return true; }
	static constexpr bool is_polyanya_multi_target() { return false; }
	using pq = inx::alg::pairing_heap<PolyNode>;

	void getPath(std::vector<Point>& path) const
	{
		path.clear();
		if (final_node != nullptr) {
			path.push_back(goal);
			for (const PolyNode* at = final_node; at != nullptr && at->root != 0; at = at->parent) {
				path.push_back(mesh->mesh_vertices.at(at->root).p);
			}
			path.push_back(start);
		}
		std::reverse(path.begin(), path.end());
	}
private:
	std::unique_ptr<mem::cpool> node_pool;
	Mesh* mesh;
	const Heuristic* heuristic;
	Point start;
	Point goal;

	const PolyNode* final_node;
	int end_polygon; // set by init_search
	pq open_list;

	// Best g value for a specific vertex.
	std::vector<double> root_g_values;
	// Contains the current search id if the root has been reached by
	// the search.
	std::vector<int> root_search_ids;  // also used for root-level pruning
	int search_id;
	// retain for multi-single-target
	PointLocation start_pl;

	// Pre-initialised variables to use in search().
	std::unique_ptr<Successor[]> search_successors;
	std::unique_ptr<PolyNode[]> search_nodes_to_push;

	void init_update()
	{
		if (mesh_max_poly_sides < mesh->max_poly_sides) {
			mesh_max_poly_sides = mesh->max_poly_sides;
			search_successors = std::make_unique<Successor[]>(mesh_max_poly_sides + 2);
			search_nodes_to_push = std::make_unique<PolyNode[]>(mesh_max_poly_sides + 2);
		}
		if (mesh_num_vertices != static_cast<int>(mesh->mesh_vertices.size())) {
			mesh_num_vertices = static_cast<int>(mesh->mesh_vertices.size());
			root_g_values.resize(mesh_num_vertices);
			root_search_ids.resize(mesh_num_vertices);
		}
	}

	void init()
	{
		assert(mesh != nullptr);
		verbose = false;
		deadend = false;
		mesh_max_poly_sides = mesh->max_poly_sides;
		search_successors = std::make_unique<Successor[]>(mesh_max_poly_sides + 2);
		search_nodes_to_push = std::make_unique<PolyNode[]>(mesh_max_poly_sides + 2);
		node_pool = std::make_unique<mem::cpool>(sizeof(PolyNode));
		init_root_pruning();
	}
	void init_root_pruning()
	{
		assert(mesh != nullptr);
		search_id = 0;
		mesh_num_vertices = static_cast<int>(mesh->mesh_vertices.size());
		root_g_values.resize(mesh_num_vertices);
		root_search_ids.resize(mesh_num_vertices);
	}
	void init_search(bool retain)
	{
		assert(node_pool);
		node_pool->reclaim();
		search_id++;
		open_list = pq();
		final_node = nullptr;
		nodes_generated = 0;
		nodes_pushed = 0;
		nodes_popped = 0;
		nodes_pruned_post_pop = 0;
		successor_calls = 0;
		if (!retain) mesh->getFacer()->resetTime();
		set_end_polygon();
		gen_initial_nodes(retain);
		if (!retain) facer_time = mesh->getFacer()->getTime();
	}

	PointLocation get_point_location(Point p);
	void set_end_polygon();
	void gen_initial_nodes(bool retain);
	int succ_to_node(
		PolyNode* parent, Successor* successors,
		int num_succ, PolyNode* nodes
	);
	void print_node(const PolyNode* node, std::ostream& outfile);

public:
	int nodes_generated;		// Nodes stored in memory
	int nodes_pushed;		   // Nodes pushed onto open
	int nodes_popped;		   // Nodes popped off open
	int nodes_pruned_post_pop;  // Nodes we prune right after popping off
	int successor_calls;		// Times we call get_successors
	bool verbose;
	bool deadend;
	int mesh_max_poly_sides;
	int mesh_num_vertices;
	std::chrono::nanoseconds facer_time;

	PolySearch() { }
	PolySearch(Mesh* m) : mesh(m) { init(); }
	PolySearch(Mesh* m, Point s, Point g) :
		mesh(m), start(s), goal(g) { init(); }
	PolySearch(PolySearch const &) = delete;
	void operator=(PolySearch const &x) = delete;

	void setMesh(Mesh& m)
	{
		mesh = &m;
		init();
	}

	void updateMesh()
	{
		init_update();
	}

	void set_start_target(Point s, Point g)
	{
		start = s;
		goal = g;
		final_node = nullptr;
	}

	bool search(bool retain = false); // if true, will not relookup start point location as assuming it is the same
	double get_cost() const noexcept
	{
		if (final_node == nullptr)
		{
			return -1;
		}

		return final_node->f;
	}

	void setHeuristic(const Heuristic& h) noexcept
	{
		heuristic = &h;
	}

	void get_path_points(std::vector<Point>& out);
	void print_search_nodes(std::ostream& outfile);

	void copyStats(PolyanyaStatsST& stats) noexcept
	{
		stats.generated = nodes_generated;
		stats.pushed = nodes_pushed;
		stats.popped = nodes_popped;
		stats.pruned = nodes_pruned_post_pop;
		stats.successors = successor_calls;
		stats.searchFacerTime = facer_time;
	}

};

}

#endif // RAYSCAN_EXT_POLYANYALIB_POLYSEARCH_HPP_INCLUDED
