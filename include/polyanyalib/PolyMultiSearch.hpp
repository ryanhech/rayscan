#ifndef RAYSCAN_EXT_POLYANYALIB_POLYMULTISEARCH_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_POLYMULTISEARCH_HPP_INCLUDED

#include "fwd.hpp"
#include "PolyNode.hpp"
#include "Mesh.hpp"
#include "Successor.hpp"
#include "cpool.h"
#include "Stats.hpp"
#include "Heuristic.hpp"
#include <queue>
#include <vector>
#include <memory_resource>
#include <unordered_map>

namespace polyanyalib
{

class PolyMultiSearch
{
public:
	static constexpr bool is_polyanya_single_target() { return false; }
	static constexpr bool is_polyanya_multi_target() { return true; }
	using pq = inx::alg::pairing_heap<PolyNode>;
private:
	std::unique_ptr<mem::cpool> node_pool;
	Mesh* mesh;
	Point start;
	int count_targets;
	std::unique_ptr<Point[]> targets;
	using final_node_type = const PolyNode*;
	std::unique_ptr<final_node_type[]> final_nodes;
	int targets_reached;
	pq open_list;

	// Best g value for a specific vertex.
	std::vector<double> root_g_values;
	// Contains the current search id if the root has been reached by
	// the search.
	std::vector<int> root_search_ids;  // also used for root-level pruning
	int search_id;

	// Pre-initialised variables to use in search().
	std::unique_ptr<Successor[]> search_successors;
	std::unique_ptr<PolyNode[]> search_nodes_to_push;

	// target polygons index
	std::pmr::unsynchronized_pool_resource m_targetResource;
	std::pmr::unordered_map<int, std::vector<int>> m_targetPolygon; // first=polygon id, second=list of targets by target-id

	void init_update()
	{
		if (mesh_max_poly_sides < mesh->max_poly_sides) {
			mesh_max_poly_sides = mesh->max_poly_sides;
			search_successors = std::make_unique<Successor[]>(mesh_max_poly_sides + 2);
			search_nodes_to_push = std::make_unique<PolyNode[]>(mesh_max_poly_sides + 2);
		}
		if (mesh_num_vertices != static_cast<int>(mesh->mesh_vertices.size())) {
			mesh_num_vertices = static_cast<int>(mesh->mesh_vertices.size());
			root_g_values.resize(mesh_num_vertices);
			root_search_ids.resize(mesh_num_vertices);
		}
	}

	void init()
	{
		assert(mesh != nullptr);
		verbose = false;
		deadend = false;
		mesh_max_poly_sides = mesh->max_poly_sides;
		search_successors = std::make_unique<Successor[]>(mesh_max_poly_sides + 2);
		search_nodes_to_push = std::make_unique<PolyNode[]>(mesh_max_poly_sides + 2);
		node_pool = std::make_unique<mem::cpool>(sizeof(PolyNode));
		init_root_pruning();
	}
	void init_root_pruning()
	{
		assert(mesh != nullptr);
		search_id = 0;
		mesh_num_vertices = static_cast<int>(mesh->mesh_vertices.size());
		root_g_values.resize(mesh_num_vertices);
		root_search_ids.resize(mesh_num_vertices);
	}
	void init_search()
	{
		assert(node_pool);
		node_pool->reclaim();
		search_id++;
		open_list = pq();
		final_nodes = nullptr;
		targets_reached = 0;
		nodes_generated = 0;
		nodes_pushed = 0;
		nodes_popped = 0;
		nodes_pruned_post_pop = 0;
		successor_calls = 0;
		mesh->getFacer()->resetTime();
		set_end_polygon();
		gen_initial_nodes();
		facer_time = mesh->getFacer()->getTime();
	}

	PointLocation get_point_location(Point p);
	void set_end_polygon();
	void gen_initial_nodes();
	int succ_to_node(
		PolyNode* parent, Successor* successors,
		int num_succ, PolyNode* nodes
	);
	void print_node(const PolyNode* node, std::ostream& outfile);

public:
	int nodes_generated;		// Nodes stored in memory
	int nodes_pushed;		   // Nodes pushed onto open
	int nodes_popped;		   // Nodes popped off open
	int nodes_pruned_post_pop;  // Nodes we prune right after popping off
	int successor_calls;		// Times we call get_successors
	bool verbose;
	bool deadend;
	int mesh_max_poly_sides;
	int mesh_num_vertices;
	std::chrono::nanoseconds facer_time;

	PolyMultiSearch() : PolyMultiSearch(nullptr) { }
	PolyMultiSearch(Mesh* m) : mesh(m), m_targetPolygon(&m_targetResource) { if (m != nullptr) init(); }
	PolyMultiSearch(PolySearch const &) = delete;
	void operator=(PolySearch const &x) = delete;

	void setMesh(Mesh& m)
	{
		mesh = &m;
		init();
	}

	void updateMesh()
	{
		init_update();
	}

	template <typename It>
	void set_start_target(Point s, uint32 targets_count, It a)
	{
		start = s;
		targets = std::make_unique<Point[]>(targets_count);
		count_targets = targets_count;
		std::copy_n(a, targets_count, targets.get());
	}
	template <typename It>
	void set_start_target(Point s, It a, It b)
	{
		set_start_target(s, std::distance(a, b), a);
	}

	bool search(bool retain = false);
	double get_cost() const noexcept
	{
		if (targets_reached != count_targets)
		{
			return -1;
		}

		return std::accumulate(final_nodes.get(), final_nodes.get() + count_targets, 0.0, [](double a, const final_node_type& fn) { return a + fn->f; });
	}

	void setHeuristic(const Heuristic& h[[maybe_unused]]) noexcept
	{
		//heuristic = &h;
	}

	void copyStats(PolyanyaStatsST& stats) noexcept
	{
		stats.generated = nodes_generated;
		stats.pushed = nodes_pushed;
		stats.popped = nodes_popped;
		stats.pruned = nodes_pruned_post_pop;
		stats.successors = successor_calls;
		stats.searchFacerTime = facer_time;
	}

};

}

#endif // RAYSCAN_EXT_POLYANYALIB_POLYMULTISEARCH_HPP_INCLUDED
