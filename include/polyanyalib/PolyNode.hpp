#ifndef RAYSCAN_EXT_POLYANYALIB_POLYNODE_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_POLYNODE_HPP_INCLUDED

#include "fwd.hpp"
#include <inx/alg/paring_heap.hpp>

namespace polyanyalib
{

// A search node.
// Only makes sense given a mesh and an endpoint, which the node does not store.
// This means that the f value needs to be set manually.
struct PolyNode : inx::alg::pairing_heap_tag<>
{
	PolyNode* parent;
	// Note that all Points here will be in terms of a Cartesian plane.
	int root; // 0 if start

	// If possible, set the orientation of left / root / right to be
	// "if I'm standing at 'root' and look at 'left', 'right' is on my right"
	Point left, right;

	// The left vertex of the edge the interval is lying on.
	// When generating the successors of this node, end there.
	int left_vertex;

	// The right vertex of the edge the interval is lying on.
	// When generating the successors of this node, start there.
	int right_vertex;

	// Index of the polygon we're going to "push" into.
	// Every successor must lie within this polygon.
	int next_polygon;

	double f, g;

	PolyNode() noexcept = default;
	PolyNode(PolyNode* p, int r, Point pl, Point pr, int lv, int rv, int np, double lf, double lg) noexcept :
		parent(p), root(r), left(pl), right(pr), left_vertex(lv), right_vertex(rv), next_polygon(np), f(lf), g(lg)
	{ }

	// Comparison.
	// Always take the "smallest" search node in a priority queue.
	bool operator<(const PolyNode& other) const noexcept
	{
		if (this->f == other.f)
		{
			// If two nodes have the same f, the one with the bigger g
			// is "smaller" to us.
			return this->g > other.g;
		}
		return this->f < other.f;
	}

	bool operator>(const PolyNode& other) const noexcept
	{
		if (this->f == other.f)
		{
			return this->g < other.g;
		}
		return this->f > other.f;
	}

	friend std::ostream& operator<<(std::ostream& stream, const PolyNode& sn)
	{
		return stream << "SearchNode ([" << sn.root << ", [" << sn.left << ", "
					  << sn.right << "]], f=" << sn.f << ", g=" << sn.g
					  << ", poly=" << sn.next_polygon << ")";
	}
};

}

#endif // RAYSCAN_EXT_POLYANYALIB_POLYNODE_HPP_INCLUDED
