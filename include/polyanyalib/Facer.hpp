#ifndef RAYSCAN_EXT_POLYANYALIB_FACER_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_FACER_HPP_INCLUDED

#include "fwd.hpp"
#include "MeshStructs.hpp"
#include <chrono>

namespace polyanyalib
{

class Facer
{
public:
	Facer() : m_mesh{}, m_time{}
	{ }
	virtual ~Facer() = default;
	virtual void setMesh(const Mesh& mesh)
	{
		m_mesh = &mesh;
	}
	virtual PointLocation findFace(const Point& p, bool onlyTrav = true) const = 0;

	void resetTime() const noexcept { m_time = std::chrono::nanoseconds(0); }
	std::chrono::nanoseconds getTime() const noexcept { return m_time; }

protected:
	const Mesh* m_mesh;
	mutable std::chrono::nanoseconds m_time;
};

template <typename FacerType>
class FacerTime final : public FacerType
{
public:
	PointLocation findFace(const Point& p, bool onlyTrav = true) const override
	{
		auto tp = std::chrono::steady_clock::now();
		PointLocation pl = FacerType::findFace(p, onlyTrav);
		auto dur = std::chrono::steady_clock::now() - tp;
		this->m_time += dur;
		return pl;
	}
};
                   
}

#endif // RAYSCAN_EXT_POLYANYALIB_FACER_HPP_INCLUDED
