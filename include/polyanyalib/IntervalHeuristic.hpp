#ifndef RAYSCAN_EXT_POLYANYALIB_INTERVALHEURISTIC_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_INTERVALHEURISTIC_HPP_INCLUDED

#include "fwd.hpp"
#include "Heuristic.hpp"
#ifndef NDEBUG
#include "Expansion.hpp"
#endif

namespace polyanyalib
{

class IntervalHeuristic final : public Heuristic
{
public:
	double getHvalue(const point& root, const point& l, const point& r) const override
	{
#ifndef NDEBUG
		double ans = getHvalueInterval(root, l, r);
		assert(ans >= 0);
		return ans;
#else
		return getHvalueInterval(root, l, r);
#endif
	}
	double getMultiHvalue(const point& root, const point& l, const point& r) const override
	{
#ifndef NDEBUG
		double ans = getHvalueInterval(root, l, r);
		assert(ans >= 0);
		return ans;
#else
		return getHvalueInterval(root, l, r);
#endif
	}

	static double getHvalueInterval(const point& root, const point& l, const point& r)
	{
		namespace geo = ::rayscanlib::geometry;
		const point rootL(l - root);
		const point rootR(r - root);
		if (rootL.isColin(rootR)) {
			if (rootL * rootR > rayscanlib::geometry::epsilon) // if l-r are on same side, return closest
				return std::sqrt(std::min(rootL.square(), rootR.square()));
			else // [l,r] is either equal to root or on opposite sides of roots, return 0
				return 0.0;
		}
		const point lr = (r - l);
		if (auto rootLXlr = rootL * lr; !point::isCW(rootLXlr)) { // left is shortest
			return rootL.length();
		} else if (!rootR.isCCW(lr)) { // right is shortest
			return rootR.length();
		} else {
#if 0
			// shortest is on the line
			double square = lr.square();
			double scale = -( rootLXlr / square );
			assert(scale > rayscanlib::geometry::epsilon);
			return scale * std::sqrt(square);
#else
			return -rootLXlr / lr.length();
#endif
		}
	}

	static point getIntervalPoint(const point& root, const point& l, const point& r)
	{
		namespace geo = ::rayscanlib::geometry;
		const point rootL(l - root);
		const point rootR(r - root);
		if (rootL.isColin(rootR)) {
			if (rootL * rootR > rayscanlib::geometry::epsilon) // if l-r are on same side, return closest
				return rootL.square() < rootR.square() ? l : r;
			else // [l,r] is either equal to root or on opposite sides of roots, return 0
				return root;
		}
		const point lr = (r - l);
		if (auto rootLXlr = rootL * lr; !point::isCW(rootLXlr)) { // left is shortest
			return l;
		} else if (!rootR.isCCW(lr)) { // right is shortest
			return r;
		} else {
			// shortest is on the line
			double scale = -( rootLXlr / lr.square() );
			assert(scale > rayscanlib::geometry::epsilon);
			return l + scale * lr;
		}
	}
};
                   
}

#endif // RAYSCAN_EXT_POLYANYALIB_INTERVALHEURISTIC_HPP_INCLUDED
