#ifndef RAYSCAN_EXT_POLYANYALIB_GEOMETRY_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_GEOMETRY_HPP_INCLUDED

#include "fwd.hpp"

namespace polyanyalib
{

void line_intersect_time(const Point& a, const Point& b,
                         const Point& c, const Point& d,
                         double& ab_num, double& cd_num, double& denom);

// Returns the line intersect between ab and cd as fast as possible.
// Uses a and b for the parameterisation.
// ASSUMES NO COLLINEARITY.
inline Point line_intersect(const Point& a, const Point& b,
                            const Point& c, const Point& d)
{
    const Point ab = b - a;
    return a + (((c - a).cross(d - a)) / (ab.cross(d - c))) * ab;
}

enum struct ZeroOnePos
{
    LT_ZERO,  // n < 0
    EQ_ZERO,  // n = 0
    IN_RANGE, // 0 < n < 1
    EQ_ONE,   // n = 1
    GT_ONE,   // n > 1
};

// Returns where num / denom is in the range [0, 1].
inline ZeroOnePos line_intersect_bound_check(
    const double num, const double denom
)
{
    // Check num / denom == 0.
    if (std::abs(num) < rayscanlib::geometry::epsilon)
    {
        return ZeroOnePos::EQ_ZERO;
    }
    // Check num / denom == 1.
    // Note: Checking whether it is accurately near 1 requires us to check
    // |num - denom| < rayscanlib::geometry::epsilon^2 * denom
    // which requires a multiplication. Instead, we can assume that denom
    // is less than 1/rayscanlib::geometry::epsilon and check
    // |num - denom| < rayscanlib::geometry::epsilon
    // instead. This is less accurate but faster.
    if (std::abs(num - denom) < rayscanlib::geometry::epsilon)
    {
        return ZeroOnePos::EQ_ONE;
    }

    // Now we finally check whether it's greater than 1 or less than 0.
    if (denom > 0)
    {
        if (num < 0)
        {
            // strictly less than 0
            return ZeroOnePos::LT_ZERO;
        }
        if (num > denom)
        {
            // strictly greater than 1
            return ZeroOnePos::GT_ONE;
        }
    }
    else
    {
        if (num > 0)
        {
            // strictly less than 0
            return ZeroOnePos::LT_ZERO;
        }
        if (num < denom)
        {
            // strictly greater than 1
            return ZeroOnePos::GT_ONE;
        }
    }
    return ZeroOnePos::IN_RANGE;
}

// Given two points a, b and a number t, compute the point
//  a + (b-a) * t
inline Point get_point_on_line(const Point& a, const Point& b, const double t)
{
    return a + t * (b - a);
}
Point reflect_point(const Point& p, const Point& l, const Point& r);

enum struct Orientation
{
	CCW,       // counterclockwise
	COLLINEAR, // collinear
	CW,        // clockwise
};

inline Orientation get_orientation(
    const Point& a, const Point& b, const Point& c
)
{
    const double cross = (b - a).cross(c - b);
    if (cross > rayscanlib::geometry::epsilon)
    {
        return Orientation::CCW;
    }
    else if (cross < -rayscanlib::geometry::epsilon)
    {
        return Orientation::CW;
    }
    else
    {
        return Orientation::COLLINEAR;
    }
}

inline bool is_collinear(const Point& a, const Point& b, const Point& c)
{
    return std::abs((b - a).cross(c - b)) < rayscanlib::geometry::epsilon;
}

}

#endif // RAYSCAN_EXT_POLYANYALIB_GEOMETRY_HPP_INCLUDED
