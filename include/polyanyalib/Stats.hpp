#ifndef RAYSCAN_EXT_POLYANYALIB_STATS_HPP_INCLUDED
#define RAYSCAN_EXT_POLYANYALIB_STATS_HPP_INCLUDED

#include "fwd.hpp"
#include <chrono>

namespace polyanyalib
{

struct PolyanyaStatsST
{
	size_t instID;
	size_t generated;
	size_t pushed;
	size_t popped;
	size_t pruned;
	size_t successors;
	std::chrono::nanoseconds setupTime;
	std::chrono::nanoseconds dynSetupTime;
	std::chrono::nanoseconds setupFacerTime;
	std::chrono::nanoseconds searchTime;
	std::chrono::nanoseconds searchFacerTime;
	std::chrono::nanoseconds meshTime;

	PolyanyaStatsST() { reset(); }

	void clear() noexcept
	{
		generated = 0;
		pushed = 0;
		popped = 0;
		pruned = 0;
		successors = 0;
		searchTime = std::chrono::nanoseconds(0);
		searchFacerTime = std::chrono::nanoseconds(0);
	}

	void reset() noexcept
	{
		instID = 0;
		clear();
		setupTime = std::chrono::nanoseconds(0);
		dynSetupTime = std::chrono::nanoseconds(0);
		setupFacerTime = std::chrono::nanoseconds(0);
		meshTime = std::chrono::nanoseconds(0);
	}

	void pushSetupTime(std::chrono::nanoseconds t) noexcept
	{
		setupTime = t - setupFacerTime;
	}
	void pushDynSetupTime(std::chrono::nanoseconds t) noexcept
	{
		dynSetupTime = t - setupFacerTime;
	}
};

}

#endif // RAYSCAN_EXT_POLYANYALIB_STATS_HPP_INCLUDED
