#ifndef GMLIB_AVLTREE_HPP_INCLUDED
#define GMLIB_AVLTREE_HPP_INCLUDED

#include <inx/inx.hpp>
#include <deque>
#include <vector>
#include <unordered_set>

namespace inx {

template <typename ValueType>
class AvlTree
{
public:
	struct Node;
	using value_type = ValueType;
	using node_type = Node;
	struct Node
	{
		node_type* parent;
		std::array<node_type*, 2> child;
		int32 height;
		alignas(alignof(value_type)) std::array<std::byte, sizeof(value_type)> data;

		Node() = default; // : child{nullptr}, black(false) { }
		Node(const Node&) = delete;
		Node(Node&&) = delete;
		Node& operator=(const Node&) = delete;
		Node& operator=(Node&&) = delete;

		bool isRoot() const noexcept { return parent == nullptr; }
		bool isLeaf() const noexcept { return child[0] == nullptr && child[1] == nullptr; }
		bool isChain() const noexcept { return (child[0] == nullptr) != (child[1] == nullptr); }
		bool isChild(size_t i) const noexcept { return parent != nullptr && parent->child[i] == this; }
		size_t childNo() const noexcept { assert(parent->child[0] == this || parent->child[1] == this); return parent->child[0] == this ? 0 : 1; }
		size_t chain() const noexcept { assert(isChain()); return child[0] != nullptr ? 0 : 1; }
		bool isLeft() const noexcept { return isChild(0); }
		bool isRight() const noexcept { return isChild(1); }
		int32 balance() const noexcept { return (child[1] == nullptr ? 0 : child[1]->height) - (child[0] == nullptr ? 0 : child[0]->height); }
		int32 updateHeight() noexcept { return height = std::max(child[0] ? child[0]->height : 0, child[1] ? child[1]->height : 0) + 1; }
		value_type& value() noexcept { return reinterpret_cast<value_type&>(data); }
		const value_type& value() const noexcept { return reinterpret_cast<const value_type&>(data); }

		node_type* follow(size_t side) noexcept
		{
			assert(side < 2);
			node_type* node = this;
			while (node->child[side] != nullptr)
				node = node->child[side];
			return node;
		}
		const node_type* follow(size_t side) const noexcept
		{
			assert(side < 2);
			node_type* node = this;
			while (node->child[side] != nullptr)
				node = node->child[side];
			return node;
		}

		node_type* root(size_t side) noexcept
		{
			assert(side < 2);
			node_type* node = this;
			while (node->isChild(side))
				node = node->parent;
			return node->parent;
		}
		const node_type* root(size_t side) const noexcept
		{
			assert(side < 2);
			node_type* node = this;
			while (node->isChild(side))
				node = node->parent;
			return node->parent;
		}

		node_type* next() noexcept { return child[1] != nullptr ? child[1]->follow(0) : root(1); }
		const node_type* next() const noexcept { return child[1] != nullptr ? child[1]->follow(0) : root(1); }

		node_type* prev() noexcept { return child[0] != nullptr ? child[0]->follow(1) : root(0); }
		const node_type* prev() const noexcept { return child[0] != nullptr ? child[0]->follow(1) : root(0); }
	};


	AvlTree() : mRoot(nullptr)
	{ }

	~AvlTree()
	{
		for (node_type* it = first(); it != nullptr; it = it->next())
			destruct(it);
	}

	node_type* first() noexcept
	{
		if (mRoot == nullptr) return nullptr;
		return mRoot->follow(0);
	}
	const node_type* first() const noexcept
	{
		if (mRoot == nullptr) return nullptr;
		return mRoot->follow(0);
	}

	node_type* last() noexcept
	{
		if (mRoot == nullptr) return nullptr;
		return mRoot->follow(1);
	}
	const node_type* last() const noexcept
	{
		if (mRoot == nullptr) return nullptr;
		return mRoot->follow(1);
	}

	node_type* root() noexcept
	{
		return mRoot;
	}
	const node_type* root() const noexcept
	{
		return mRoot;
	}

	template <typename... Args>
	node_type* emplace(const node_type* cnode, Args&&... args)
	{
		node_type* node = const_cast<node_type*>(cnode);
		node_type* nnode = newNode();
		construct(nnode, std::forward<Args>(args)...);
		if (node == nullptr) {
			if ((node = last()) == nullptr) {
				assert(mRoot == nullptr);
				mRoot = nnode;
				nnode->parent = nullptr;
				assert(isValidTree());
				return nnode;
			} else {
				assert(node->child[1] == nullptr);
				node->child[1] = nnode;
			}
		} else {
			if (node->child[0] == nullptr)
				node->child[0] = nnode;
			else {
				node = node->follow(1);
				assert(node->child[1] == nullptr);
				node->child[1] = nnode;
			}
		}
		nnode->parent = node;
		assert(isValidTree());
		balance(nnode);
		return nnode;
	}

	void remove(const node_type* cnode)
	{
		assert(isValidTree());
		node_type* node = const_cast<node_type*>(cnode);
		freeNode(node);
		int p = -1;
		if (node->isLeaf()) {
			p = 0;
			if (node->isRoot()) {
				mRoot = nullptr;
				assert(isValidTree());
				return;
			}
			node->parent->child[node->childNo()] = nullptr;
			node = node->parent;
		} else if (node->isChain()) {
			p = 1;
			if (node->isRoot()) {
				mRoot = node->child[node->chain()];
				mRoot->parent = nullptr;
				assert(isValidTree());
				return;
			}
			node->parent->child[node->childNo()] = node->child[node->chain()];
			node = node->parent;
		} else {
			p = 2;
			node_type* sw = node->balance() <= 0 ? node->prev() : node->next();
			node_type* swp = sw->parent;
			swp->child[sw->childNo()] = nullptr;
			sw->parent = node->parent;
			if (sw->isRoot())
				mRoot = sw;
			sw->child = node->child;
			if (sw->child[0]) sw->child[0]->parent = sw;
			if (sw->child[1]) sw->child[1]->parent = sw;
			node = swp;
		}
		assert(isValidTree());
		balance(node);
	}

	template <typename T, typename Comp>
	const node_type* lower_bound(const T& value, Comp&& comp) const
	{
		const node_type* ans = nullptr;
		for (const node_type* node = mRoot; node != nullptr; ) {
			if (!comp(node->value(), value)) { // node >= value
				ans = node;
				node = node->child[0];
			} else
				node = node->child[1];
		}
		return ans;
	}
	template <typename T, typename Comp>
	node_type* lower_bound(const T& value, Comp&& comp)
	{
		node_type* ans = nullptr;
		for (node_type* node = mRoot; node != nullptr; ) {
			if (!comp(const_cast<const node_type*>(node)->value(), value)) { // node >= value
				ans = node;
				node = node->child[0];
			} else
				node = node->child[1];
		}
		return ans;
	}

	template <typename T, typename Comp>
	const node_type* upper_bound(const T& value, Comp&& comp) const
	{
		const node_type* ans = nullptr;
		for (const node_type* node = mRoot; node != nullptr; ) {
			if (comp(value, node->value())) { // node > value
				ans = node;
				node = node->child[0];
			} else
				node = node->child[1];
		}
		return ans;
	}
	template <typename T, typename Comp>
	node_type* upper_bound(const T& value, Comp&& comp)
	{
		node_type* ans = nullptr;
		for (node_type* node = mRoot; node != nullptr; ) {
			if (comp(value, const_cast<const node_type*>(node)->value())) { // node > value
				ans = node;
				node = node->child[0];
			} else
				node = node->child[1];
		}
		return ans;
	}

	bool empty() const noexcept { return mRoot == nullptr; }
	value_type& front() noexcept { return first()->value(); }
	const value_type& front() const noexcept { return first()->value(); }
	value_type& back() noexcept { return first()->value(); }
	const value_type& back() const noexcept { return first()->value(); }

	bool isValidTree() const
	{
		if (mFreeNodes.size() > mNodes.size())
			return false;
		int32 nodes = static_cast<int32>(mNodes.size() - mFreeNodes.size());
		if (mRoot == nullptr)
			return nodes == 0;
		if (mRoot->parent != nullptr)
			return false;
		std::unordered_set<const node_type*> seen;
		bool valid = isValidTree_aux(mRoot, seen);
		if (!valid)
			return false;
		if (static_cast<int32>(seen.size()) == nodes)
			return true;
		else
			return false;
	}

protected:
	node_type* newNode()
	{
		node_type* node;
		if (mFreeNodes.empty()) {
			mNodes.emplace_back();
			node = &mNodes.back();
		} else {
			node = mFreeNodes.back();
			mFreeNodes.pop_back();
		}
	//	node->parent = parent;
		node->child[0] = node->child[1] = nullptr;
		node->height = 1;
		return node;
	}

	template <typename... Args>
	void construct(node_type* node, Args&&... args)
	{
		::new(&node->value()) value_type(std::forward<Args>(args)...);
	}

	void destruct(node_type* node) noexcept
	{
		node->value().~value_type();
	}
	
	void freeNode(node_type* node)
	{
		destruct(node);
		mFreeNodes.push_back(node);
	}

	node_type* rotateRight(node_type* node)
	{
		assert(node->child[0] != nullptr);
		node_type* n = node->child[0];
		node->child[0] = n->child[1];
		if (n->child[1]) n->child[1]->parent = node;
		n->child[1] = node;
		n->parent = node->parent;
		if (node->parent)
			node->parent->child[node->childNo()] = n;
		else
			mRoot = n;
		node->parent = n;
		node->updateHeight();
		n->updateHeight();
		assert(isValidTree());
		return n;
	}

	node_type* rotateLeft(node_type* node)
	{
		assert(node->child[1] != nullptr);
		node_type* n = node->child[1];
		node->child[1] = n->child[0];
		if (n->child[0]) n->child[0]->parent = node;
		n->child[0] = node;
		n->parent = node->parent;
		if (node->parent)
			node->parent->child[node->childNo()] = n;
		else
			mRoot = n;
		node->parent = n;
		node->updateHeight();
		n->updateHeight();
		assert(isValidTree());
		return n;
	}

	node_type* autoRotateRight(node_type* node)
	{
		assert(node->child[0] != nullptr);
		switch (node->child[0]->balance()) {
		case 1:
			rotateLeft(node->child[0]);
		[[fallthrough]]
		case 0:
		case -1:
			return rotateRight(node);
			break;
		default:
			assert(false);
			return nullptr;
		}
	}

	node_type* autoRotateLeft(node_type* node)
	{
		assert(node->child[1] != nullptr);
		switch (node->child[1]->balance()) {
		case -1:
			rotateRight(node->child[1]);
		[[fallthrough]]
		case 0:
		case 1:
			return rotateLeft(node);
			break;
		default:
			assert(false);
			return nullptr;
		}
	}

	node_type* autoRotate(node_type* node)
	{
		assert(node != nullptr);
		switch (node->balance()) {
		case 0:
		case -1:
		case 1:
			return node;
			break;
		case -2:
			return autoRotateRight(node);
			break;
		case 2:
			return autoRotateLeft(node);
			break;
		default:
			assert(false);
			return nullptr;
		}
	}

	void balance(node_type* node)
	{
		for (node_type* parent = node->parent; parent != nullptr; parent = parent->parent) {
			parent->updateHeight();
			parent = autoRotate(parent);
		}
		assert(isValidTree());
	}

private:
	bool isValidTree_aux(const node_type* node, std::unordered_set<const node_type*>& seen) const
	{
		if (!seen.insert(node).second)
			return false;
		for (const node_type* c : node->child) {
			if (c == nullptr) continue;
			if (c->parent != node)
				return false;
			if (!isValidTree_aux(c, seen))
				return false;
		}
		return true;
	}

	std::deque<Node> mNodes;
	std::vector<node_type*> mFreeNodes;
	node_type* mRoot;
//	node_type* mFirst;
};

}

#endif // GMLIB_AVLTREE_HPP_INCLUDED
