#ifndef GMLIB_TUPLE_HPP_INCLUDED
#define GMLIB_TUPLE_HPP_INCLUDED

#include <inx/inx.hpp>
#include <variant>
#include <cstddef>

namespace inx {

template <typename... T>
struct empty_type
{ };

template <typename... T>
inline constexpr bool false_value = false;
template <auto... T>
inline constexpr bool false_valuea = false;

template <typename... T>
inline constexpr bool true_value = true;
template <auto... T>
inline constexpr bool true_valuea = true;

template<typename Array, std::size_t... I>
auto to_tuple_aux_(const Array& a, std::index_sequence<I...>)
{
    return std::make_tuple(a[I]...);
}

template<typename T, std::size_t N, typename Indices = std::make_index_sequence<N>>
auto to_tuple(const std::array<T, N>& a)
{
    return to_tuple_aux_(a, Indices{});
}

template <typename T, typename Enable = void>
struct is_complete : std::false_type
{ };

template <typename T>
struct is_complete<T, std::void_t<decltype(sizeof(T) != 0)>> : std::true_type
{ };

template <typename... Types>
struct type_list
{ };

template <typename TypeList, typename... Types>
struct append_type_list;
template <typename... TypeListTypes>
struct append_type_list<type_list<TypeListTypes...>>
{
	using type = type_list<TypeListTypes...>;
};
template <typename Type1, typename... TypeListTypes>
struct append_type_list<type_list<TypeListTypes...>, Type1>
{
	using type = type_list<TypeListTypes..., Type1>;
};
template <typename TypeList, typename Type1, typename Type2, typename... Types>
struct append_type_list<TypeList, Type1, Type2, Types...> :
	append_type_list<typename append_type_list<TypeList, Type1>::type, Type2, Types...>
{ };
template <typename TypeList, typename... Types>
using append_type_list_t = typename append_type_list<TypeList, Types...>::type;

template <typename... TypeLists>
struct merge_type_lists;
template <typename TypeList1>
struct merge_type_lists<TypeList1>
{
	using type = TypeList1;
};
template <typename TypeList1, typename... TypeListTypes>
struct merge_type_lists<TypeList1, type_list<TypeListTypes...>> : append_type_list<TypeList1, TypeListTypes...>
{ };
template <typename TypeList1, typename TypeList2, typename TypeList3, typename... TypeLists>
struct merge_type_lists<TypeList1, TypeList2, TypeList3, TypeLists...> :
	merge_type_lists<typename merge_type_lists<TypeList1, TypeList2>::type, TypeList3, TypeLists...>
{ };
template <typename... TypeLists>
using merge_type_lists_t = typename merge_type_lists<TypeLists...>::type;

template <size_t Start, size_t End, typename TypeListMade, typename... Types>
struct sub_type_list_impl_;
template <size_t Start, size_t End, typename TypeListMade, typename Type1, typename... Types>
struct sub_type_list_impl_<Start, End, TypeListMade, Type1, Types...> : sub_type_list_impl_<Start-1, End-1, TypeListMade, Types...>
{ };
template <size_t End, typename TypeListMade, typename Type1, typename... Types>
struct sub_type_list_impl_<0, End, TypeListMade, Type1, Types...> : sub_type_list_impl_<0, End-1, typename append_type_list<TypeListMade, Type1>::type, Types...>
{ };
template <typename TypeListMade, typename Type1, typename... Types>
struct sub_type_list_impl_<0, 0, TypeListMade, Type1, Types...>
{
	using type = TypeListMade;
};
template <typename TypeListMade>
struct sub_type_list_impl_<0, 0, TypeListMade>
{
	using type = TypeListMade;
};
template <typename TypeList, size_t Start, ssize_t Count = -1>
struct sub_type_list;
template <size_t Start, ssize_t Count, typename... Types>
struct sub_type_list<type_list<Types...>, Start, Count> : sub_type_list_impl_<Start, Start+Count, type_list<>, Types...>
{ };
template <size_t Start, typename... Types>
struct sub_type_list<type_list<Types...>, Start, -1> : sub_type_list_impl_<Start, sizeof...(Types), type_list<>, Types...>
{ };
template <typename TypeList, size_t Start, ssize_t Count = -1>
using sub_type_list_t = typename sub_type_list<TypeList, Start, Count>::type;

template <typename T>
struct is_tuple : std::false_type
{ };
template <typename... Types>
struct is_tuple<std::tuple<Types...>> : std::true_type
{ };
template <typename T>
inline constexpr bool is_tuple_v = is_tuple<T>::value;

template <typename T>
struct is_variant : std::false_type
{ };
template <typename... Types>
struct is_variant<std::variant<Types...>> : std::true_type
{ };
template <typename T>
inline constexpr bool is_variant_v = is_variant<T>::value;

template <typename T, typename... Types>
struct is_same_any : std::disjunction<std::is_same<T, Types>...>
{ };
template <typename T, typename... Types>
inline constexpr bool is_same_any_v = is_same_any<T, Types...>::value;

template <typename T, typename Template>
struct template_has_type;
template <typename T, template<typename...> typename Template, typename... Types>
struct template_has_type<T, Template<Types...>> : is_same_any<T, Types...>
{ };
template <typename T, typename Template>
inline constexpr bool template_has_type_v = template_has_type<T, Template>::value;

template <typename T, typename Tuple>
struct tuple_has_type;
template <typename T, typename... Types>
struct tuple_has_type<T, std::tuple<Types...>> : is_same_any<T, Types...>
{ };
template <typename T, typename Tuple>
inline constexpr bool tuple_has_type_v = tuple_has_type<T, Tuple>::value;

template <typename T, typename Tuple>
struct variant_has_type;
template <typename T, typename... Types>
struct variant_has_type<T, std::variant<Types...>> : is_same_any<T, Types...>
{ };
template <typename T, typename Tuple>
inline constexpr bool variant_has_type_v = variant_has_type<T, Tuple>::value;

template <typename T, typename Tuple>
struct tuple_count;
template <typename T, template<typename...> typename Tuple, typename... Types>
struct tuple_count<T, Tuple<Types...>> : std::integral_constant<size_t,
	((std::is_same_v<T, Types> ? static_cast<size_t>(1) : static_cast<size_t>(0)) + ...)
> { };
template <typename T, typename Tuple>
inline constexpr size_t tuple_count_v = tuple_count<T, Tuple>::value;

template <size_t I, typename T, typename... Types>
struct type_index_found_;
template <size_t I, typename T, typename TypeAt, typename... Types>
struct type_index_found_<I, T, TypeAt, Types...> : type_index_found_<I, T, Types...>
{ };
template <size_t I, typename T, typename... Types>
struct type_index_found_<I, T, T, Types...>
{
	static_assert(false_valuea<I>, "type T appears more than once");
};
template <size_t I, typename T>
struct type_index_found_<I, T> : std::integral_constant<size_t, I>
{ };
template <size_t I, typename T, typename... Types>
struct type_index_search_;
template <size_t I, typename T, typename TypeAt, typename... Types>
struct type_index_search_<I, T, TypeAt, Types...> : type_index_search_<I+1, T, Types...>
{ };
template <size_t I, typename T, typename... Types>
struct type_index_search_<I, T, T, Types...> : type_index_found_<I, T, Types...>
{ };
template <size_t I, typename T>
struct type_index_search_<I, T>
{
	static_assert(false_valuea<I>, "type T not found");
};

template <typename T, typename... Types>
struct type_index : type_index_search_<0, T, Types...>
{ };
template <typename T, typename... Types>
inline constexpr size_t type_index_v = type_index<T, Types...>::value;

template <size_t Index, typename... Types>
struct type_select_loop_;
template <size_t Index, typename TypeAt, typename... Types>
struct type_select_loop_<Index, TypeAt, Types...> : type_select_loop_<Index-1, Types...>
{ };
template <typename TypeAt, typename... Types>
struct type_select_loop_<0, TypeAt, Types...>
{
	using type = TypeAt;
};
template <size_t Index>
struct type_select_loop_<Index>
{
	static_assert(false_valuea<Index>, "Index exceeds number of types");
};
template <size_t Index, typename... Types>
struct type_select : type_select_loop_<Index, Types...>
{ };
template <size_t Index, typename... Types>
using type_select_t = typename type_select<Index, Types...>::type;

template <typename T, typename Tuple>
struct template_index;
template <typename T, template<typename...> typename Template, typename... Types>
struct template_index<T, Template<Types...>> : type_index<T, Types...>
{ };
template <typename T, typename... Types>
inline constexpr size_t template_index_v = template_index<T, Types...>::value;

template <typename T, typename Tuple>
struct tuple_index;
template <typename T, typename... Types>
struct tuple_index<T, std::tuple<Types...>> : type_index<T, Types...>
{ };
template <typename T, typename... Types>
inline constexpr size_t tuple_index_v = tuple_index<T, Types...>::value;

template <typename T, typename Tuple>
struct variant_index;
template <typename T, typename... Types>
struct variant_index<T, std::variant<Types...>> : type_index<T, Types...>
{ };
template <typename T, typename... Types>
inline constexpr size_t variant_index_v = variant_index<T, Types...>::value;

template <typename Variant, typename Tuple>
struct holds_alternatives_;
template <typename Variant, class... Types>
struct holds_alternatives_<Variant, std::tuple<Types...>>
{
	static_assert(is_variant_v<Variant>, "Variant is not std::variant");
	static constexpr bool test(const Variant& v)
	{
		size_t id = v.index();
		return ((variant_index_v<Types, Variant> == id) || ...);
	}
};

template <typename Tuple, class... Types>
inline constexpr std::enable_if_t<is_tuple_v<Tuple>, bool> holds_alternatives(const std::variant<Types...>& v)
{
	return holds_alternatives_<std::variant<Types...>, Tuple>::test(v);
}

template <typename Tuple>
struct variant_from_tuple;
template <typename... Types>
struct variant_from_tuple<std::tuple<Types...>>
{
	using type = std::variant<Types...>;
};
template <typename Tuple>
using variant_from_tuple_t = typename variant_from_tuple<Tuple>::type;

template <typename Tuple>
struct tuple_from_template;
template <template <typename...> typename Template, typename... Types>
struct tuple_from_template<Template<Types...>>
{
	using type = std::tuple<Types...>;
};
template <typename Tuple>
using tuple_from_template_t = typename tuple_from_template<Tuple>::type;

template <typename Type1, typename... Type>
struct repeated_typename
{
	static_assert(( std::is_same_v<Type1, Type> && ... ), "Type must be the same as Type1");
};
template <size_t N, typename Type, typename... Types>
struct make_repeated_typename_impl
{
	using type = typename make_repeated_typename_impl<N-1, Type, Type, Types...>::type;
};
template <typename Type, typename... Types>
struct make_repeated_typename_impl<0, Type, Types...>
{
	using type = repeated_typename<Types...>;
};
template <size_t N, typename Type>
using make_repeated_typename = typename make_repeated_typename_impl<N, Type>::type;

template <typename T, T... I>
constexpr auto make_tuple_from_sequence_impl(std::integer_sequence<T, I...>) noexcept
{
	return std::tuple(I...);
}

template <typename T>
inline constexpr auto make_tuple_from_sequence = make_tuple_from_sequence_impl(T());

template <typename T, T... I>
constexpr auto make_array_from_sequence_impl(std::integer_sequence<T, I...>) noexcept
{
	return std::array{I...};
}

template <typename T>
inline constexpr auto make_array_from_sequence = make_array_from_sequence_impl(T());

template <typename TypeList, typename Find, typename... Replace>
struct replace_type_list
{
	constexpr static size_t index = template_index<Find, TypeList>::value;
	using type = typename merge_type_lists<
			typename sub_type_list<TypeList, 0, index>::type,
	        type_list<Replace...>,
			typename sub_type_list<TypeList, index+1>::type
		>::type;
};
template <typename TypeList, typename Find, typename... Replace>
using replace_type_list_t = typename replace_type_list<TypeList, Find, Replace...>::type;

template <bool Enable, typename Type>
struct optional_type
{
	using type = Type;
};
template <typename Type>
struct optional_type<false, Type>
{
	using type = empty_type<>;
};
template <bool Enable, typename Type>
using optional_type_t = typename optional_type<Enable, Type>::type;

template <typename... VTypes>
struct manual_variant;

template <typename T, typename MV>
struct manual_variant_can_hold : std::false_type { };
template <typename T, typename... VTypes>
struct manual_variant_can_hold<T, manual_variant<VTypes...>> : std::bool_constant<is_same_any<T, VTypes...>::value> { };

template <typename... VTypes>
struct manual_variant
{
	static_assert(!(std::is_const_v<VTypes> || ...), "VTypes can not be const");
	using self = manual_variant<VTypes...>;
	static constexpr std::size_t size() noexcept { return std::max({sizeof(VTypes)...}); }
	alignas(VTypes...) std::array<std::byte, size()> m_data;

	manual_variant() = default;
	manual_variant(const self& other ) = delete;
	manual_variant(self&& other) = delete;
	template <typename T, typename... Args>
	constexpr explicit manual_variant(std::in_place_type_t<T>, Args&&... args)
	{
		emplace<T>(std::forward<Args>(args)...);
	}

	template <typename T, typename... Args>
	constexpr std::enable_if_t<manual_variant_can_hold<T, self>::value, T&> emplace(Args&&... args) noexcept(std::is_nothrow_constructible_v<T, Args&&...>)
	{
		return *(::new (m_data.data()) T(std::forward<Args>(args)...));
	}

	template <typename T>
	constexpr std::enable_if_t<manual_variant_can_hold<T, self>::value> release() noexcept
	{
		std::launder(reinterpret_cast<T*>(m_data.data()))->~T();
	}

	template <typename T>
	constexpr std::enable_if_t<manual_variant_can_hold<T, self>::value, T&> get() noexcept { return *std::launder(reinterpret_cast<T*>(m_data.data())); }
	template <typename T>
	constexpr std::enable_if_t<manual_variant_can_hold<T, self>::value, const T&> get() const noexcept { return *std::launder(reinterpret_cast<const T*>(m_data.data())); }
};

template <typename MV, typename T>
[[nodiscard]] std::enable_if_t<manual_variant_can_hold<T, MV>::value, MV*> manual_variant_cast(T* type) noexcept
{
	if constexpr (offsetof(MV, m_data) == 0) {
		return std::launder(reinterpret_cast<MV*>(type));
	} else {
		return std::launder(reinterpret_cast<MV*>(reinterpret_cast<std::byte*>(type) - offsetof(MV, m_data)));
	}
}
template <typename MV, typename T>
[[nodiscard]] std::enable_if_t<manual_variant_can_hold<T, MV>::value, const MV*> manual_variant_cast(const T* type) noexcept
{
	if constexpr (offsetof(MV, m_data) == 0) {
		return std::launder(reinterpret_cast<const MV*>(type));
	} else {
		return std::launder(reinterpret_cast<const MV*>(reinterpret_cast<std::byte*>(type) - offsetof(MV, m_data)));
	}
}
template <typename MV, typename T>
[[nodiscard]] std::enable_if_t<manual_variant_can_hold<T, MV>::value, MV&> manual_variant_cast(T& type) noexcept
{
	return *manual_variant_cast<MV*>(&type);
}
template <typename MV, typename T>
[[nodiscard]] std::enable_if_t<manual_variant_can_hold<T, MV>::value, const MV&> manual_variant_cast(const T& type) noexcept
{
	return *manual_variant_cast<const MV*>(&type);
}

template <typename T1, typename... Tn>
struct first_tuple : std::tuple<T1, Tn...>
{
	using std::tuple<T1, Tn...>::tuple;
};
template <typename... Tn>
first_tuple(Tn&&... args) -> first_tuple<Tn...>;

template <typename... Tn>
bool operator<(const first_tuple<Tn...> a, const first_tuple<Tn...> b) noexcept(noexcept(std::get<0>(a) < std::get<0>(b)))
{
	return std::get<0>(a) < std::get<0>(b);
}
template <typename... Tn>
bool operator<=(const first_tuple<Tn...> a, const first_tuple<Tn...> b) noexcept(noexcept(std::get<0>(a) <= std::get<0>(b)))
{
	return std::get<0>(a) <= std::get<0>(b);
}
template <typename... Tn>
bool operator>(const first_tuple<Tn...> a, const first_tuple<Tn...> b) noexcept(noexcept(std::get<0>(a) > std::get<0>(b)))
{
	return std::get<0>(a) > std::get<0>(b);
}
template <typename... Tn>
bool operator>=(const first_tuple<Tn...> a, const first_tuple<Tn...> b) noexcept(noexcept(std::get<0>(a) >= std::get<0>(b)))
{
	return std::get<0>(a) >= std::get<0>(b);
}
template <typename... Tn>
bool operator==(const first_tuple<Tn...> a, const first_tuple<Tn...> b) noexcept(noexcept(std::get<0>(a) == std::get<0>(b)))
{
	return std::get<0>(a) == std::get<0>(b);
}
template <typename... Tn>
bool operator!=(const first_tuple<Tn...> a, const first_tuple<Tn...> b) noexcept(noexcept(std::get<0>(a) != std::get<0>(b)))
{
	return std::get<0>(a) != std::get<0>(b);
}

} // namespace inx

namespace std
{

template <typename T, typename... VTypes>
T& get(inx::manual_variant<VTypes...>& v)
{
	return v.template get<T>();
}
template <typename T, typename... VTypes>
const T& get(const inx::manual_variant<VTypes...>& v)
{
	return v.template get<T>();
}

} // namespace std

#endif // GMLIB_VARIANT_HPP_INCLUDED
