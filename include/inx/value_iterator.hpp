#ifndef GMLIB_VALUE_ITERATOR_HPP_INCLUDED
#define GMLIB_VALUE_ITERATOR_HPP_INCLUDED

#include <inx/inx.hpp>
#include <iterator>
#include <boost/iterator/iterator_facade.hpp>

namespace inx {

template <typename T>
struct is_reverse_iterator_ : std::false_type
{ };
template <typename T>
struct is_reverse_iterator_<std::reverse_iterator<T>> : std::true_type
{ };
template <typename T>
struct is_reverse_iterator : is_reverse_iterator_<remove_cvref_t<T>>
{ };
template <typename T>
constexpr bool is_reverse_iterator_v = is_reverse_iterator<T>::value;

template <typename T, typename = std::enable_if_t<std::is_base_of_v<
	std::bidirectional_iterator_tag,
	typename std::iterator_traits<remove_cvref_t<T>>::iterator_category >>>
auto normalise_iterator(T&& it)
{
	if constexpr (is_reverse_iterator_v<T>)
		return std::prev(it.base());
	else
		return it;
}

template <bool Rev, typename T, typename = std::enable_if_t<std::is_base_of_v<
	std::bidirectional_iterator_tag,
	typename std::iterator_traits<remove_cvref_t<T>>::iterator_category >>>
auto set_iterator_direction(T&& it)
{
	if constexpr (Rev) {
		if constexpr (is_reverse_iterator_v<std::remove_reference_t<std::remove_cv_t<T>>>)
			return it;
		else
			return std::make_reverse_iterator(std::next(it));
	}
	else
		return normalise_iterator(it);
}

#if 0
template <typename T>
struct value_iterator
{
	static_assert(std::is_integral_v<T> && !std::is_volatile_v<T> && !std::is_const_v<T>, "T must be integeral");
	using self = value_iterator<T>;
	using value_type = T;
	using difference_type = std::conditional_t<(std::numeric_limits<T>::digits < std::numeric_limits<int32>::digits), int32, int64>;
	using reference = T;
	using pointer = const T*;
	using iterator_category = std::random_access_iterator_tag;

	T value;
	value_iterator() noexcept = default;
	constexpr value_iterator(const self&) noexcept = default;
	constexpr value_iterator(self&&) noexcept = default;
	constexpr value_iterator(T val) noexcept : value(val) { }
	~value_iterator() noexcept = default;

	constexpr self& operator=(const self&) noexcept = default;
	constexpr self& operator=(self&&) noexcept = default;

	constexpr reference operator*() const noexcept
	{
		return value;
	}

	constexpr self& operator++() noexcept
	{
		value++;
		return *this;
	}
	constexpr self operator++(int) noexcept
	{
		return self(value++);
	}

	constexpr self& operator--() noexcept
	{
		value--;
		return *this;
	}
	constexpr self operator--(int) noexcept
	{
		return self(value--);
	}

	constexpr self& operator+=(difference_type n) noexcept
	{
		value = static_cast<T>(static_cast<difference_type>(value) + n);
		return *this;
	}
	constexpr self operator+(difference_type n) const noexcept
	{
		return self(value) += n;
	}

	constexpr self& operator-=(difference_type n) noexcept
	{
		value = static_cast<T>(static_cast<difference_type>(value) - n);
		return *this;
	}
	constexpr self operator-(difference_type n) const noexcept
	{
		return self(value) -= n;
	}
	constexpr difference_type operator-(const self& b) const noexcept
	{
		return static_cast<difference_type>(value) - static_cast<difference_type>(b.value);
	}

	constexpr reference operator[](difference_type i) const noexcept
	{
		return *(*this + i);
	}

	constexpr bool operator==(self& b) const noexcept
	{
		return value == b.value;
	}
	constexpr bool operator!=(self& b) const noexcept
	{
		return value != b.value;
	}
	constexpr bool operator<(self& b) const noexcept
	{
		return value < b.value;
	}
	constexpr bool operator<=(self& b) const noexcept
	{
		return value <= b.value;
	}
	constexpr bool operator>(self& b) const noexcept
	{
		return value > b.value;
	}
	constexpr bool operator>=(self& b) const noexcept
	{
		return value >= b.value;
	}
};

template <typename T>
constexpr value_iterator<T> operator+(typename value_iterator<T>::difference_type a, const value_iterator<T>& b) noexcept
{
	return b + a;
}
#endif


template <typename Value, bool RevIter = false>
class circular_iterator
	: public ::boost::iterator_facade<
		 circular_iterator<Value, RevIter>
		,Value
		,boost::random_access_traversal_tag
		,const Value&
		,ptrdiff_t
	>
{
public:
	constexpr circular_iterator() noexcept : m_at(0), m_size(0)
	{ }
	constexpr circular_iterator(Value s) noexcept : m_at(0), m_size(s-1)
	{
		assert(s > 0);
	}
	constexpr circular_iterator(Value a, Value s) noexcept : m_at(a), m_size(s-1)
	{
		assert(s > 0);
		assert(static_cast<std::make_unsigned_t<Value>>(a) < static_cast<std::make_unsigned_t<Value>>(s));
	}

	constexpr const Value& dereference() const noexcept
	{
		return m_at;
	}

	constexpr bool equal(const circular_iterator<Value, RevIter>& rhs) const noexcept
	{
		assert(m_size == rhs.m_size);
		return m_at == rhs.m_at;
	}

	constexpr void increment() noexcept
	{
		if constexpr (!RevIter) {
			m_at = m_at != m_size ? m_at + 1 : 0;
		} else {
			m_at = m_at != 0 ? m_at - 1 : m_size;
		}
	}
	constexpr void decrement() noexcept
	{
		if constexpr (!RevIter) {
			m_at = m_at != 0 ? m_at - 1 : m_size;
		} else {
			m_at = m_at != m_size ? m_at + 1 : 0;
		}
	}

	constexpr void advance(ptrdiff_t n) noexcept
	{
		assert(-static_cast<ptrdiff_t>(m_size) <= n && n <= static_cast<ptrdiff_t>(m_size));
		if constexpr (RevIter) {
			n = -n;
		}
		ptrdiff_t d = static_cast<ptrdiff_t>(m_at) + n;
		m_at = d < 0                               ? static_cast<Value>(d + (m_size + 1)) :
			   d <= static_cast<ptrdiff_t>(m_size) ? static_cast<Value>(d):
		                                             static_cast<Value>(d - (m_size + 1));
	}

	constexpr ptrdiff_t distance_to(const circular_iterator<Value, RevIter>& rhs) const noexcept
	{
		assert(m_size == rhs.m_size);
		if constexpr (!RevIter) {
			return rhs.m_at - m_at;
		} else {
			return m_at - rhs.m_at;
		}
	}

	constexpr ptrdiff_t circ_distance_to(const circular_iterator<Value, RevIter>& rhs) const noexcept
	{
		assert(m_size == rhs.m_size);
		if constexpr (!RevIter) {
			if (m_at <= rhs.m_at) {
				return rhs.m_at - m_at;
			} else {
				return (m_size+1) - m_at + rhs.m_at;
			}
		} else {
			if (m_at >= rhs.m_at) {
				return m_at - rhs.m_at;
			} else {
				return m_at + (m_size+1) - rhs.m_at;
			}
		}
	}

	Value index() const noexcept
	{
		return m_at;
	}
	void index(Value at) noexcept
	{
		assert(static_cast<std::make_unsigned_t<Value>>(at) <= static_cast<std::make_unsigned_t<Value>>(m_size));
		m_at = at;
	}
	void index(Value at, Value sz) noexcept
	{
		assert(sz > 0);
		assert(static_cast<std::make_unsigned_t<Value>>(at) < static_cast<std::make_unsigned_t<Value>>(sz));
		m_at = at;
		m_size = sz - 1;
	}

	Value size() const noexcept
	{
		return m_size+1;
	}

	bool is_one() const noexcept
	{
		return m_size == 0;
	}

private:
	Value m_at;
	Value m_size;
};

} // namespace inx

#endif // GMLIB_VALUE_ITERATOR_HPP_INCLUDED
