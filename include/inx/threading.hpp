#ifndef GMLIB_THREADING_HPP_INCLUDED
#define GMLIB_THREADING_HPP_INCLUDED

#include <inx/inx.hpp>
#include <sstream>
#include <vector>
#include <thread>
#include <mutex>

namespace inx {

class OrderedOutputWorkers
{
public:
	OrderedOutputWorkers() : mOut(nullptr) { }
	void setOutput(std::ostream& out) { mOut = &out; }
	template <typename Pred>
	void run(std::ostream& out, size_t threads, size_t count, Pred&& pred)
	{
		if (threads == 0 || threads > 64)
			throw std::runtime_error("unable to start");
		if (count == 0) return;
		if (threads > count)
			threads = count;
		if (threads == 1) {
			for (size_t i = 0; i < count; ++i) {
				std::ostringstream str;
				pred(0, i, std::ref(static_cast<std::ostream&>(str)));
				out << str.str() << std::flush;
			}
		} else {
			mAt = 0;
			mNext = threads;
			mOut = &out;
			mCache.clear();
			mCache.resize(count);
			std::vector<std::thread> thr(threads-1);
			for (size_t i = 0; i < threads-1; ++i)
				thr[i] = std::thread([this, threads, count](size_t id, auto&& pred) { run_thread(id, count, std::forward<Pred>(pred)); }, i, std::forward<Pred>(pred));
			run_thread(threads-1, count, std::forward<Pred>(pred));
			for (size_t i = 0; i < threads-1; ++i)
				thr[i].join();
		}
	}

protected:
	template <typename Pred>
	void run_thread(size_t id, size_t count, Pred&& pred)
	{
		for (size_t i = id; i < count; )
		{
			pred(id, i, std::ref(static_cast<std::ostream&>(mCache[i].second)));
			std::lock_guard<std::mutex> lck(mMutex);
			mCache[i].first = true;
			if (mAt == i) {
				for (size_t j = i; j < count && mCache[j].first; ++j) {
					mAt++;
					*mOut << mCache[j].second.str() << std::flush;
					mCache[j].second.str({});
				}
			}
			i = mNext++;
		}
	}

protected:
	size_t mAt;
	size_t mNext;
	std::mutex mMutex;
	std::ostream* mOut;
	std::vector<std::pair<bool, std::ostringstream>> mCache;
};

} // namespace inx

#endif // GMLIB_THREADING_HPP_INCLUDED
