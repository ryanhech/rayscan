#ifndef GMLIB_VECTOR_TABLE_HPP_INCLUDED
#define GMLIB_VECTOR_TABLE_HPP_INCLUDED

#include "../inx.hpp"
#include <memory>
#include <memory_resource>
#include <utility>

namespace inx::data {

template <typename Value, size_t StaticSize, typename Allocator = std::allocator<Value>>
class vector_table
{
public:
	using data_value = Value;
	using value_type = Value;
	using allocator_type = typename std::allocator_traits<Allocator>::template rebind_alloc<value_type>;
	using size_type = size_t;
	using difference_type = std::ptrdiff_t;

	struct Vector
	{
		using value_type = data_value;
		using size_type = uint32;
		using difference_type = int32;
		using reference = value_type&;
		using const_reference = const value_type&;
		using pointer = value_type*;
		using const_pointer = const value_type*;
		using iterator = pointer;
		using const_iterator = const_pointer;

		value_type* m_data;
		size_type m_size;
		size_type m_reserved;
		Vector() noexcept : m_data(nullptr), m_size(0), m_reserved(0)
		{ }
		Vector(value_type* dat) noexcept : m_data(dat), m_size(0), m_reserved(StaticSize)
		{
			assert(dat != nullptr);
		}
		Vector(value_type* dat, size_type size, size_type res) noexcept : m_data(dat), m_size(size), m_reserved(res)
		{
			assert(dat != nullptr);
			assert(res > StaticSize);
		}

		value_type& operator[](size_type i) noexcept { assert(i < m_size); return m_data[i]; }
		const value_type& operator[](size_type i) const noexcept { assert(i < m_size); return m_data[i]; }

		size_type size() const noexcept { return m_size; }
		bool empty() const noexcept { return m_size == 0; }

		iterator begin() noexcept { return m_data; }
		const_iterator begin() const noexcept { return m_data; }
		const_iterator cbegin() const noexcept { return m_data; }
		iterator end() noexcept { return m_data + m_size; }
		const_iterator end() const noexcept { return m_data + m_size; }
		const_iterator cend() const noexcept { return m_data + m_size; }
	};

	struct M_DataType : allocator_type
	{
		Vector *data, *dataEnd;
		value_type *region, *regionEnd;
		size_type width, height, extraAlloc;
		std::pmr::memory_resource* dataResource;

		M_DataType() noexcept(std::is_nothrow_default_constructible<allocator_type>::value)
			: data(nullptr), dataEnd(nullptr), region(nullptr), regionEnd(nullptr), width(0), height(0), extraAlloc(0), dataResource(nullptr)
		{ }
		M_DataType(const Allocator& alloc) noexcept(std::is_nothrow_constructible<allocator_type, const Allocator&>::value)
			: allocator_type(alloc), data(nullptr), region(nullptr), regionEnd(nullptr), width(0), height(0), extraAlloc(0), dataResource(nullptr)
		{ }
		~M_DataType() noexcept
		{
			clear();
		}

		void resize(size_type l_width, size_type l_height)
		{
			if (l_width == 0 || l_height == 0)
				throw std::range_error("out of range");
			clear();
			dataResource = std::pmr::get_default_resource();
			size_type cells = l_width * l_height;
			data = static_cast<Vector*>(dataResource->allocate(cells * sizeof(Vector), alignof(Vector)));
			dataEnd = data + cells;
			region = std::allocator_traits<allocator_type>::allocate(static_cast<allocator_type&>(*this), StaticSize * cells);
			regionEnd = region; // + (StaticSize * cells);
			width = l_width;
			height = l_height;
			extraAlloc = 0;
			for (auto it = data, ite = dataEnd; it != ite; ++it, regionEnd += StaticSize) {
				it = std::launder(::new(it) Vector(regionEnd));
			}
		}
		void clear() noexcept
		{
			if (data != nullptr) {
				if constexpr (std::is_trivially_destructible<value_type>::value) {
					if (extraAlloc > 0) { // no need to free additional memory if none are allocated
						for (auto it = data, ite = dataEnd; it != ite; ++it) {
							if (it->m_reserved > StaticSize) {
								destroy_alloc(it);
								if (--extraAlloc == 0)
									break;
							}
						}
					}
				} else {
					for (auto it = data, ite = dataEnd; it != ite; ++it) {
						if (it->m_reserved > StaticSize) {
							destroy_alloc(it);
						} else {
							destroy(it);
						}
					}
				}
				std::allocator_traits<allocator_type>::deallocate(static_cast<allocator_type&>(*this), region, regionEnd - region);
				assert(dataResource != nullptr);
				dataResource->deallocate(data, (width*height) * sizeof(Vector), alignof(Vector));
				data = dataEnd = nullptr;
				region = regionEnd = nullptr;
				width = height = extraAlloc = 0;
				dataResource = nullptr;
			}
		}
		void free(value_type* data, size_type s)
		{
			std::allocator_traits<allocator_type>::deallocate(static_cast<allocator_type&>(*this), data, s);
			extraAlloc -= 1;
		}
		value_type* alloc(size_type s)
		{
			assert(s > StaticSize);
			return std::allocator_traits<allocator_type>::allocate(static_cast<allocator_type&>(*this), s);
		}
		size_type index(size_type x, size_type y) const noexcept
		{
			assert(x < width && y < height);
			return y * width + x;
		}
		void destroy_alloc(Vector* v)
		{
			try {
				std::destroy_n(v->m_data, v->m_size);
				free(v->m_data, v->m_reserved);
			}
			catch (...) {
				std::terminate();
			}
		}
		void destroy(Vector* v)
		{
			if constexpr (!std::is_trivially_destructible<value_type>::value) {
				try {
					std::destroy_n(v->m_data, v->m_size);
				}
				catch (...) {
					std::terminate();
				}
			}
		}
	};

	vector_table() = default;
	explicit vector_table(const Allocator& alloc) noexcept(noexcept(M_DataType(alloc)))
		: m_data(alloc)
	{ }
	
	template <typename... Args>
	value_type& emplace_back(Vector& v, Args&&... args)
	{
		assert(v.m_size <= v.m_reserved && v.m_reserved > 0);
		if (v.m_size == v.m_reserved) {
			realloc_(v, static_cast<typename Vector::size_type>(2*v.m_reserved));
		}
		value_type* p = v.m_data + v.m_size;
		std::allocator_traits<allocator_type>::construct(static_cast<allocator_type&>(m_data), p, std::forward<Args>(args)...);
		v.m_size += 1;
		return *std::launder(p);
	}
	value_type& push_back(Vector& v, const value_type& ins)
	{
		return emplace_back(v, ins);
	}
	value_type& push_back(Vector& v, value_type&& ins)
	{
		return emplace_back(v, std::move(ins));
	}

	template <typename... Args>
	value_type& emplace(Vector& v, typename Vector::const_iterator at, Args&&... args)
	{
		assert(v.m_data <= at && at < v.m_data);
		if (v.m_size == v.m_reserved) {
			realloc_(v, static_cast<typename Vector::size_type>(2*v.m_reserved));
		}
		value_type* p = const_cast<typename Vector::iterator>(at);
		std::move(p, v.m_data + v.m_size, p+1);
		std::allocator_traits<allocator_type>::construct(static_cast<allocator_type&>(m_data), p, std::forward<Args>(args)...);
		v.m_size += 1;
		return *std::launder(p);
	}
	value_type& insert(Vector& v, typename Vector::const_iterator at, const value_type& ins)
	{
		return emplace(v, at, ins);
	}
	value_type& insert(Vector& v, typename Vector::const_iterator at, value_type&& ins)
	{
		return emplace(v, at, std::move(ins));
	}
	
	void pop_back(Vector& v)
	{
		assert(v.m_size > 0);
		std::destroy_at(v.m_size);
		v.m_size -= 1;
	}

	void erase(Vector& v, typename Vector::const_iterator at)
	{
		assert(v.m_data <= at && at < v.m_data);
		std::move_backward(const_cast<typename Vector::iterator>(at + 1), v.m_data + v.m_size, const_cast<typename Vector::iterator>(at));
		std::destroy_at(v.m_size);
		v.m_size -= 1;
	}
	void erase_unordered(Vector& v, typename Vector::const_iterator at)
	{
		assert(v.m_data <= at && at < v.m_data);
		auto* end = v.m_data + (v.m_size-1);
		if (at != end) {
			*const_cast<typename Vector::iterator>(at) = std::move(*end);
		}
		std::destroy_at(end);
		v.m_size -= 1;
	}

	void resize(size_type width, size_type height)
	{
		m_data.resize(width, height);
	}

	void clear()
	{
		m_data.clear();
	}

	size_type size() const noexcept { return m_data.width * m_data.height; }
	bool empty() const noexcept { return (m_data.width | m_data.height) == 0; }

	size_type getWidth() const noexcept { return m_data.width; }
	size_type getHeight() const noexcept { return m_data.height; }

	size_type index(size_type x, size_type y) const noexcept { return m_data.index(x, y); }

	Vector& operator[](size_type i) noexcept { assert(i < m_data.width * m_data.height); return m_data.data[i]; }
	const Vector& operator[](size_type i) const noexcept { assert(i < m_data.width * m_data.height); return m_data.data[i]; }
	Vector& operator()(size_type x, size_type y) noexcept { assert(x < m_data.width && y < m_data.height); return m_data.data[m_data.index(x, y)]; }
	const Vector& operator()(size_type x, size_type y) const noexcept { assert(x < m_data.width && y < m_data.height); return m_data.data[m_data.index(x, y)]; }

protected:

	void realloc_(Vector& v, typename Vector::size_type ns)
	{
		assert(ns > v.m_reserved);
		value_type* data = nullptr;
		try {
			data = m_data.alloc(ns);
			std::uninitialized_move_n(v.m_data, v.m_size, data);
		}
		catch (...) {
			if (data != nullptr)
				m_data.free(data, ns);
			throw;
		}
		if (v.m_reserved > StaticSize) {
			m_data.destroy_alloc(&v);
		} else {
			m_data.extraAlloc += 1;
			m_data.destroy(&v);
		}
		v.m_data = data;
		v.m_reserved = ns;
	}

	M_DataType m_data;
};

}

#endif // GMLIB_VECTOR_TABLE_HPP_INCLUDED
