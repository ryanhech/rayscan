#ifndef GMLIB_DYNAMIC_OBJECT_POOL_HPP_INCLUDED
#define GMLIB_DYNAMIC_OBJECT_POOL_HPP_INCLUDED

#include <inx/inx.hpp>
#include <boost/pool/object_pool.hpp>
#include <typeindex>

namespace inx::data {

template <typename UserAllocator = boost::default_user_allocator_new_delete>
class dynamic_object_pool
{
public:
	using construct_function = void* (void*);
	using destroy_function = void (void*, void*);
	using delete_function = void (void*) noexcept;
	template <typename T>
	using pool_type = boost::object_pool<std::remove_cv_t<T>, UserAllocator>;

	dynamic_object_pool() noexcept : mPoolType(typeid(void)), mConstruct(nullptr), mDestroy(nullptr), mDeleter(nullptr)
	{ }
	dynamic_object_pool(const dynamic_object_pool&) = delete;
	dynamic_object_pool(dynamic_object_pool&&) = delete;
	~dynamic_object_pool() noexcept
	{
		destruct();
	}

	template <typename T, typename = std::enable_if_t<!std::is_reference_v<T>>>
	void initalise(size_t arg_next_size = 32, size_t arg_max_size = 0)
	{
		using pt = pool_type<T>;
		static_assert(sizeof(pt) == std::tuple_size_v<decltype(mPool)>, "pool must be the same size between types");
		destruct();
		mPoolType = typeid(pt);
		::new(mPool.data()) pt(arg_next_size, arg_max_size);
		mConstruct = [](void* pool) -> void* { return static_cast<pt*>(pool)->construct(); };
		mDestroy = [](void* pool, void* obj) { static_cast<pt*>(pool)->destroy(static_cast<T*>(obj)); };
		mDeleter = [](void* pool) noexcept { static_cast<pt*>(pool)->~pt(); };
	}

	void clear()
	{
		destruct();
	}

	void* construct()
	{
		return mConstruct(mPool.data());
	}

	void destroy(void* obj)
	{
		mDestroy(mPool.data(), obj);
	}
	
	template <typename T>
	pool_type<T>* getPool() const
	{
		return mPoolType == typeid(pool_type<T>) ? reinterpret_cast<pool_type<T>*>(mPool.data()) : nullptr;
	}

private:
	void destruct() noexcept
	{
		if (mDeleter != nullptr)
		{
			mDeleter(static_cast<void*>(mPool.data()));
			mConstruct = nullptr;
			mDestroy = nullptr;
			mDeleter = nullptr;
		}
	}

private:
	std::type_index mPoolType;
	alignas(pool_type<void>) std::array<std::byte, sizeof(pool_type<void>)> mPool;
	construct_function* mConstruct;
	destroy_function* mDestroy;
	delete_function* mDeleter;
};

} // namespace inx::data

#endif // GMLIB_DYNAMIC_OBJECT_POOL_HPP_INCLUDED
