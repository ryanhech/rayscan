#ifndef GMLIB_BLOCK_VECTOR_HPP_INCLUDED
#define GMLIB_BLOCK_VECTOR_HPP_INCLUDED

#include "../inx.hpp"
#include <memory>

namespace inx::data {

namespace details {

template <size_t TypeSize, size_t Bytes, typename Allocator>
class generic_block_vector
{
protected:
	static_assert(TypeSize > 0, "TypeSize must be at least 1");
	static_assert(Bytes >= TypeSize, "Not enough bytes per chunk given for TypeSize");

	constexpr size_t BlockElements = Bytes / TypeSize;
	using AllocTraits = std::allocator_traits<Allocator>;
	static_assert(std::is_same_v<AllocTraits::value_type, std::byte>, "Allocator::value_type must be std::byte");
	using BlockType = std::array<std::byte, Bytes>;
	using AllocatorBlock = AllocTraits::template rebind_alloc<BlockType>;
	using BlockTraits = std::allocator_traits<AllocatorBlock>;

	struct MapData
	{
		
	};
};

}

}

#endif // GMLIB_VIRTUAL_PTR_HPP_INCLUDED
