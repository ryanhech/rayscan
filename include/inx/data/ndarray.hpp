#ifndef GMLIB_DATA_NDARRAY_HPP_INCLUDED
#define GMLIB_DATA_NDARRAY_HPP_INCLUDED

#include <inx/inx.hpp>
#include <inx/tuple.hpp>

namespace inx::data {

template <typename ValueType, size_t Dims>
class ndarray_view
{
public:
	using self = ndarray_view<ValueType, Dims>;
	using value_type = ValueType;
	using size_type = size_t;
	static constexpr size_t dimensions = Dims;
	using dimensions_type = std::array<size_t, dimensions>;

protected:
	value_type* m_data;
	dimensions_type m_size;

public:
	ndarray_view() = default;
	ndarray_view(const self&) = default;
	ndarray_view(self&&) = default;
	ndarray_view(value_type* l_data, dimensions_type l_size = dimensions_type{0}) noexcept : m_data(l_data), m_size(l_size)
	{ }
	~ndarray_view() = default;

	self& operator=(const self&) = default;
	self& operator=(self&&) = default;

	template <size_type I>
	size_type dim_size() const noexcept
	{
		static_assert(I < dimensions, "I out of bounds");
		if constexpr (I == dimensions-1) {
			return 1;
		} else {
			return m_size[I+1] * dim_size<I+1>();
		}
	}
	size_type size() const noexcept
	{
		return m_size[0] * dim_size<0>();
	}
	template <size_t I>
	size_type size_i() const noexcept
	{
		static_assert(I < dimensions, "I out of bounds");
		return std::get<I>(m_size);
	}

	bool empty() const noexcept
	{
		return m_size == dimensions_type{0};
	}

	value_type* data() noexcept
	{
		return m_data;
	}
	const value_type* data() const noexcept
	{
		return m_data;
	}

	auto operator[](size_t i) noexcept
	{
		if constexpr (dimensions == 1) {
			return m_data[i];
		} else {
			using ld = ndarray_view<value_type, dimensions-1>;
			return ld(m_data+i*dim_size<0>(), std::apply(
				[&size=this->m_size](auto... I) noexcept { return typename ld::dimensions_type{size[I+1]...}; },
				make_array_from_sequence<std::make_index_sequence<dimensions-1>>)
			);
		}
	}
	const auto operator[](size_t i) const noexcept
	{
		if constexpr (dimensions == 1) {
			return m_data[i];
		} else {
			using ld = ndarray_view<value_type, dimensions-1>;
			return ld(m_data+i*dim_size<0>(), std::apply(
				[&size=this->m_size](auto... I) noexcept { return typename ld::dimensions_type{size[I+1]...}; },
				make_array_from_sequence<std::make_index_sequence<dimensions-1>>)
			);
		}
	}

	template <typename... T>
	std::enable_if_t<sizeof...(T) == dimensions && std::conjunction_v<std::is_integral<T>...>, size_type>
	index(T... is) const noexcept
	{
		return index_impl<dimensions-1>(dimensions_type{is...});
	}

	template <typename... T>
	std::enable_if_t<sizeof...(T) == dimensions && std::conjunction_v<std::is_integral<T>...>, size_type>
	index_at(T... is) const
	{
		return index_at_impl(dimensions_type{is...});
	}

	value_type& operator()(size_t i) noexcept
	{
		return m_data[i];
	}
	const value_type& operator()(size_t i) const noexcept
	{
		return m_data[i];
	}
	template <typename... T>
	value_type& operator()(T... i) noexcept
	{
		return m_data[index_at(i...)];
	}
	template <typename... T>
	const value_type& operator()(T... i) const noexcept
	{
		return m_data[index_at(i...)];
	}

private:
	template <size_t I>
	size_type index_impl(dimensions_type idx) const noexcept
	{
		static_assert(I < dimensions, "I exceeds bounds");
		if constexpr (I == 0) {
			return std::get<I>(idx);
		} else {
			return m_size[I] * index_impl<I-1>(idx) + std::get<I>(idx);
		}
	}
	size_type index_at_impl(dimensions_type idx) const
	{
		std::apply([&id=idx,&size=this->m_size](auto... i) {
			if ( ( (id[i] >= size[i]) || ... ) )
				throw std::out_of_range("idx");
		}, make_array_from_sequence<std::make_index_sequence<dimensions>>);
		return index_impl<dimensions-1>(idx);
	}
};

struct ndarray_value_init_t
{ };
inline constexpr ndarray_value_init_t ndarray_value_init = ndarray_value_init_t{};

template <typename ValueType, size_t Dims>
class ndarray : public ndarray_view<ValueType, Dims>
{
public:
	using self = ndarray<ValueType, Dims>;
	using super = ndarray_view<ValueType, Dims>;
	using value_type = typename super::value_type;
	using size_type = typename super::size_type;

private:
	size_type m_tsize;
	size_type m_capacity;

public:
	ndarray() noexcept : super(nullptr), m_tsize(0), m_capacity(0)
	{ }
	template <typename... I>
	ndarray(std::enable_if_t<sizeof...(I) == Dims-1 && std::conjunction_v<std::is_integral<I>...>, size_type> i, I... is)
	{
		this->m_size = typename super::dimensions_type{i, is...};
		m_tsize = super::size();
		m_capacity = m_tsize;
		this->m_data = alloc(m_capacity);
		try {
			std::uninitialized_value_construct_n(this->m_data, m_capacity);
		} catch (...) {
			free(this->m_data);
			throw;
		}
	}
	ndarray(const self& other)
	{
		this->m_size = other.m_size;
		m_tsize = super::size();
		m_capacity = m_tsize;
		this->m_data = alloc(m_capacity);
		try {
			std::uninitialized_copy_n(other.m_data, m_capacity, this->m_data);
		} catch (...) {
			free(this->m_data);
			throw;
		}
	}
	ndarray(self&& other)
	{
		this->m_data = std::exchange(other.m_data, nullptr);
		this->m_size = other.m_size;
		m_tsize = other.m_tsize;
		m_capacity = other.m_capacity;
	}
	~ndarray()
	{
		if (this->m_data != nullptr) {
			free_destroy(this->m_data, this->size());
		}
	}

	self& operator=(const self& other)
	{
		assign(other);
		return *this;
	}
	self& operator=(self&& other) noexcept
	{
		assign(std::move(other));
		return *this;
	}

	bool empty() const noexcept
	{
		return nullptr;
	}

	void clear()
	{
		if (this->m_data != nullptr) {
			free_destroy(this->m_data, this->size());
			clear_zero();
		}
	}

	void assign(const self& data)
	{
		size_type s = data.size();
		reserve(s);
		try {
			std::uninitialized_copy_n(data.m_data, s, this->m_data);
		}
		catch (...) {
			free(this->m_data);
			clear_zero();
			throw;
		}
		this->m_size = data.m_size;
		m_tsize = s;
	}
	void assign(self&& data) noexcept
	{
		std::swap(this->m_data, data.m_data);
		this->m_size = data.m_size;
		m_tsize = data.m_tsize;
		m_capacity = data.m_capacity;
	}
	template <typename... I>
	std::enable_if_t<sizeof...(I) == Dims && std::conjunction_v<std::is_integral<I>...>>
	assign(ndarray_value_init_t, I... i)
	{
		size_type s = (static_cast<size_type>(i) * ...);
		reserve(s);
		try {
			std::uninitialized_value_construct_n(this->m_data, s);
		}
		catch (...) {
			free(this->m_data);
			clear_zero();
			throw;
		}
		this->m_size = {i...};
		m_tsize = s;
	}
	template <typename... I>
	std::enable_if_t<sizeof...(I) == Dims && std::conjunction_v<std::is_integral<I>...>>
	assign(const value_type& value, I... i)
	{
		size_type s = (static_cast<size_type>(i) * ...);
		reserve(s);
		try {
			std::uninitialized_fill_n(this->m_data, s, value);
		}
		catch (...) {
			free(this->m_data);
			clear_zero();
			throw;
		}
		this->m_size = {i...};
		m_tsize = s;
	}

	size_type size() const noexcept
	{
		return m_tsize;
	}

private:
	void clear_zero() noexcept
	{
		this->m_data = nullptr;
		this->m_size = typename super::dimensions_type{0};
		m_tsize = 0;
		m_capacity = 0;
	}
	static value_type* alloc(size_t i)
	{
		return reinterpret_cast<value_type*>(::new std::byte[sizeof(value_type) * i]);
	}
	static void free(value_type* data)
	{
		::delete[] reinterpret_cast<std::byte*>(data);
	}
	static void free_destroy(value_type* data, size_t s)
	{
		if (data != nullptr) {
			std::destroy_n(data, s);
			free(data);
		}
	}
	void reserve(size_type s)
	{
		if (this->m_data != nullptr) {
			std::destroy_n(this->m_data, m_tsize);
			if (s > m_capacity) {
				free(this->m_data);
				this->m_data = nullptr;
			}
		}
		if (this->m_data == nullptr) {
			this->m_data = alloc(s);
			m_capacity = s;
		}
	}
};

} // namespace inx::alg

#endif // GMLIB_DATA_NDARRAY_HPP_INCLUDED