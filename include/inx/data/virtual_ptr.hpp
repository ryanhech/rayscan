#ifndef GMLIB_VIRTUAL_PTR_HPP_INCLUDED
#define GMLIB_VIRTUAL_PTR_HPP_INCLUDED

#include "../inx.hpp"
#include <typeindex>

namespace inx::data {

namespace details {

struct base_virtual_ptr_vars
{
	enum class op_code
	{
		copy,
		move,
		destruct
	};
	union Param
	{
		std::byte* array;
	};
	using op_fn = void(*)(op_code, )
};

template <typename T, size_t DataSize>
struct virtual_ptr_vars
{
	static_assert(DataSize >= sizeof(ptrdiff_t), "DataSize must be at least ptrdiff_t size");

	enum class op_code
	{
		copy,
		move,
		destruct
	};
	using op_fn = void(*)(op_code, )
	std::array<std::byte, DataSize> 
};

}

template <typename T, size_t DataSize = sizeof(ptrdiff_t), typename Deleter = std::default_delete<T>>
class virtual_ptr
{

};


} // namespace inx::data

#endif // GMLIB_VIRTUAL_PTR_HPP_INCLUDED
