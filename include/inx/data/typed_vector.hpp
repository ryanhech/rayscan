#ifndef GMLIB_TYPED_VECTOR_HPP_INCLUDED
#define GMLIB_TYPED_VECTOR_HPP_INCLUDED

#include "../inx.hpp"
#include <typeindex>
#include <memory>
#include <cstddef>
#include <vector>

namespace inx::data {

template <typename Allocator = std::allocator<std::byte>>
class typed_vector;
template <typename T>
class scoped_vector;

namespace details {

struct scoped_vector_base;

struct typed_vector_base
{
	using pointer = std::byte*;
	using const_pointer = const std::byte*;

	scoped_vector_base* scoped;
	std::byte* data;
	size_t size;

	enum class OpType
	{
		allocate,
		deallocate
	};
	using op_fn_type = pointer (OpType type, typed_vector_base* base, pointer p, size_t n);

	constexpr typed_vector_base() noexcept : scoped(nullptr), data(nullptr), size(0)
	{ }
};

struct scoped_vector_base
{
	typed_vector_base* resource; // resource to memory
	typed_vector_base::op_fn_type* res_fn; // function to allow scoped to call res_fn regardless of allocator type

	constexpr scoped_vector_base() noexcept : resource(nullptr)
	{ }
	constexpr scoped_vector_base(typed_vector_base* lResource) noexcept : resource(lResource)
	{ }
};

template <typename Allocator>
struct typed_vector_alloc_base : typed_vector_base
{
	using self = typed_vector_alloc_base<Allocator>;
	using allocator_type = typename std::allocator_traits<Allocator>::rebind_alloc<std::byte>;
	allocator_type alloc;

	constexpr typed_vector_base() noexcept(noexcept(allocator_type())) = default;
	constexpr typed_vector_base(const allocator_type& alloc) : alloc(alloc)
	{ }

	[[nodiscard]] pointer allocate(size_t n)
	{
		return n != 0 ? data.allocate(n, alignof(std::max_align_t)) : pointer();
	}
	void deallocate(pointer p, size_t n)
	{
		if (p)
			data.deallocate(p, n, alignof(std::max_align_t));
	}

	static pointer op_fn(OpType type, typed_vector_base* base, pointer p, size_t n)
	{
		switch (type)
		{
		case OpType::allocate:
			return static_cast<self*>(base)->allocate(n);
		case OpType::deallocate:
			static_cast<self*>(base)->deallocate(p, n);
			return nullptr;
		}
		assert(false); // never reaches
	}
};

} // namespace details

template <typename Allocator = std::allocator<std::byte>>
class typed_vector : protected details::typed_vector_base<Allocator>
{
protected:
	using super = details::typed_vector_base<Allocator>;
public:

public:
	constexpr typed_vector() noexcept(noexcept(super())) = default;
	constexpr typed_vector(const Allocator& alloc) noexcept : super(super)
	{ }

	template <typename T>
	constexpr scoped_vector<T> scope()
	{ }
};

} // namespace inx::data

#endif // GMLIB_TYPED_VECTOR_HPP_INCLUDED
