#ifndef GMLIB_VIRTUAL_PTR_HPP_INCLUDED
#define GMLIB_VIRTUAL_PTR_HPP_INCLUDED

#include "../inx.hpp"

namespace inx::data {

template <typename T>
struct dynamic_pointer
{
	using self = dynamic_pointer<T>;
	using value_type = T;
	using size_type = size_t;
	using difference_type = std::ptrdiff_t;
	using reference = value_type&;
	using const_reference = const value_type&;
	using pointer = value_type*;
	using const_pointer = const value_type*;

	std::unique_ptr<value_type[]> m_data;

	dynamic_pointer() noexcept = default;
	dynamic_pointer(self&&) noexcept = default;
	dynamic_pointer(const self&) = delete;
	dynamic_pointer(size_t l_size) : m_data(std::make_unique<value_type[]>(l_size))
	{ }

	~dynamic_pointer() noexcept = default;
};

}

#endif // GMLIB_VIRTUAL_PTR_HPP_INCLUDED
