#ifndef GMLIB_DYNAMIC_OBJECT_STACK_HPP_INCLUDED
#define GMLIB_DYNAMIC_OBJECT_STACK_HPP_INCLUDED

#include <inx/inx.hpp>
#include <boost/integer.hpp>

namespace inx::data {

namespace details {

template <size_t BlockSize, typename UserAllocator>
class dynamic_list
{
	static_assert(BlockSize >= 4*sizeof(size_t), "BlockSize needs to at least be 2*sizeof(size_t)");
	struct Block
	{
		using size_type = boost::uint_value_t<BlockSize>;
		Block* next;
		size_type at_prev;
		std::array<std::byte, BlockSize - alignof(next) - alignof(at)> data;
	};
	union BlockUnion
	{
		Block block;
		std::array<std::byte, sizeof(Block)> bytes;
	};
	constexpr size_t BlockDataStart = offsetof(Block,data);
protected:
	static constexpr size_t get_alignment(size_t at, size_t mask)
	{
		return ((at-1) & ~mask) + mask;
	}
	template <typename AlignType>
	static constexpr size_t get_alignment(size_t at)
	{
		return get_alignment(at, alignof(AlignType) - 1);
	}

	BlockUnion* get_block()
	{
		if (mSpare != nullptr) {
			BlockUnion* tmp = mSpare;
			mSpare = tmp->next;
			return tmp;
		} else {
			return static_cast<BlockUnion*>(static_cast<void*>(mAlloc.malloc(sizeof(BlockUnion))));
		}
	}
	void unget_block(BlockUnion* block)
	{
		block->next = mSpare;
		mSpare = block;
	}

	template <typename T>
	void push_type(const T& value)
	{
		static_assert(get_alignment<T>(BlockDataStart) + sizeof(T) <= BlockSize, "Cannot fit type into Block");
		size_t al = get_alignment<T>(mAt);
		if (al >= BlockSize) {
			al = get_alignment<T>(BlockDataStart);
			BlockUnion* block = get_block();
			block->next = mFront;
			block->at_prev = static_cast<Block::size_type>(mAt);
			mFront = block;
		}
		mAt = al + sizeof(T);
		mCount += 1;
		::new (mFront->bytes.data() + al)(value);
	}

	template <typename T>
	T pop_type()
	{
		static_assert(get_alignment<T>(BlockDataStart) + sizeof(T) <= BlockSize, "Cannot fit type into Block");
		assert(mCount > 0);
		if (mAt <= BlockDataStart) {
			assert(mFront != nullptr);
			BlockUnion* tmp = mFront;
			mFront = tmp->next;
			mAt = tmp->at_prev;
			assert(mAt > BlockDataStart);
		}
		assert(mFront != nullptr);
		size_t al = get_alignment<T>(mAt);
		if (al >= BlockSize) {
			al = get_alignment<T>(BlockDataStart);
			BlockUnion* block = static_cast<BlockUnion*>(static_cast<void*>(mAlloc.malloc(sizeof(BlockUnion))));
			block->next = mFront;
			mFront = block;
		}
		mAt = al + sizeof(T);
		::new (mFront->bytes.data() + al)(value);
	}
	template <typename T>
	void pop_type(T& value)
	{
		static_assert(get_alignment<T>(BlockDataStart) + sizeof(T) <= BlockSize, "Cannot fit type into Block");
		size_t al = get_alignment<T>(mAt);
		if (al >= BlockSize) {
			al = get_alignment<T>(BlockDataStart);
			BlockUnion* block = static_cast<BlockUnion*>(static_cast<void*>(mAlloc.malloc(sizeof(BlockUnion))));
			block->next = mFront;
			mFront = block;
		}
		mAt = al + sizeof(T);
		::new (mFront->bytes.data() + al)(value);
	}

protected:
	UserAllocator mAlloc;
	size_t mAt;
	size_t mCount;
	BlockUnion* mFront;
	BlockUnion* mSpare;
};

}

template <size_t BlockSize = 512, typename UserAllocator = boost::default_user_allocator_new_delete>
class dynamic_object_stack
{
public:
	
};

} // namespace inx::data

#endif // GMLIB_DYNAMIC_OBJECT_STACK_HPP_INCLUDED
