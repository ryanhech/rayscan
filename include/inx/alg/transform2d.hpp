#ifndef GMLIB_TRANSFORM2D_HPP_INCLUDED
#define GMLIB_TRANSFORM2D_HPP_INCLUDED

#include "matrix.hpp"
#include <cmath>

namespace inx::alg {

template <typename FT>
using transform2d_matrix = StaticMatrix<FT, 2>;
template <typename FT>
using transform2d_point = StaticMatrix<FT, 2, 1>;
template <typename FT>
using transform2dE_matrix = StaticMatrix<FT, 3>;
template <typename FT>
using transform2dE_point = StaticMatrix<FT, 3, 1>;

template <typename T>
struct is_transform2d_matrix : std::false_type { };
template <typename FT>
struct is_transform2d_matrix<transform2d_matrix<FT>> : std::true_type { };
template <typename FT>
struct is_transform2d_matrix<transform2dE_matrix<FT>> : std::true_type { };
template <typename T>
constexpr bool is_transform2d_matrix_v = is_transform2d_matrix<T>::value;

template <typename T>
struct is_transform2d_point : std::false_type { };
template <typename FT>
struct is_transform2d_point<transform2d_point<FT>> : std::true_type { };
template <typename FT>
struct is_transform2d_point<transform2dE_point<FT>> : std::true_type { };
template <typename T>
constexpr bool is_transform2d_point_v = is_transform2d_point<T>::value;

template <typename T>
struct is_transform2d_extend : std::false_type { };
template <typename FT>
struct is_transform2d_extend<transform2dE_matrix<FT>> : std::true_type { };
template <typename FT>
struct is_transform2d_extend<transform2dE_point<FT>> : std::true_type { };
template <typename T>
constexpr bool is_transform2d_extend_v = is_transform2d_extend<T>::value;

template <typename FT>
void transform2d_transpose(transform2dE_matrix<FT>& matrix, FT x, FT y) noexcept
{
	matrix_identity(matrix);
	matrix[0][2] = x;
	matrix[1][2] = y;
}
template <typename FT>
transform2d_matrix<FT> transform2d_transpose(FT x, FT y) noexcept
{
	transform2dE_matrix<FT> matrix;
	transform2d_transpose(matrix, x, y);
	return matrix;
}

template <typename FT>
void transform2d_scale(transform2dE_matrix<FT>& matrix, FT xscale, FT yscale) noexcept
{
	matrix_zero(matrix);
	matrix[0][0] = xscale;
	matrix[1][1] = yscale;
	matrix[2][2] = 1;
}
template <typename FT>
void transform2d_scale(transform2d_matrix<FT>& matrix, FT xscale, FT yscale) noexcept
{
	matrix[0][0] = xscale; matrix[0][1] = 0;
	matrix[1][0] = 0; matrix[1][1] = yscale;
}
template <typename FT>
transform2d_matrix<FT> transform2d_scale(FT xscale, FT yscale) noexcept
{
	transform2d_matrix<FT> matrix;
	transform2d_scale(matrix, xscale, yscale);
	return matrix;
}

template <typename FT>
void transform2d_rotate_cw(transform2dE_matrix<FT>& matrix, FT angle)
{
	const FT c = std::cos(angle), s = std::sin(angle);
	matrix_zero(matrix);
	matrix[0][0] = c; matrix[0][1] = s;
	matrix[1][0] = -s; matrix[1][1] = c;
	matrix[2][2] = 1;
}
template <typename FT>
void transform2d_rotate_cw(transform2d_matrix<FT>& matrix, FT angle)
{
	const FT c = std::cos(angle), s = std::sin(angle);
	matrix[0][0] = c; matrix[0][1] = s;
	matrix[1][0] = -s; matrix[1][1] = c;
}
template <typename FT>
transform2d_matrix<FT> transform2d_rotate_cw(FT angle)
{
	transform2d_matrix<FT> matrix;
	transform2d_rotate_cw(matrix, angle);
	return matrix;
}

template <typename FT>
void transform2d_rotate_ccw(transform2dE_matrix<FT>& matrix, FT angle)
{
	const FT c = std::cos(angle), s = std::sin(angle);
	matrix_zero(matrix);
	matrix[0][0] = c; matrix[0][1] = -s;
	matrix[1][0] = s; matrix[1][1] = c;
	matrix[2][2] = 1;
}
template <typename FT>
void transform2d_rotate_ccw(transform2d_matrix<FT>& matrix, FT angle)
{
	const FT c = std::cos(angle), s = std::sin(angle);
	matrix[0][0] = c; matrix[0][1] = -s;
	matrix[1][0] = s; matrix[1][1] = c;
}
template <typename FT>
transform2d_matrix<FT> transform2d_rotate_ccw(FT angle)
{
	transform2d_matrix<FT> matrix;
	transform2d_rotate_ccw(matrix, angle);
	return matrix;
}

template <typename To, typename From>
To transform2d_cast(const From& from);
template <typename FT>
transform2d_matrix<FT> transform2d_cast(const transform2d_matrix<FT>& from) noexcept
{
	return from;
}
template <typename FT>
transform2dE_matrix<FT> transform2d_cast(const transform2dE_matrix<FT>& from) noexcept
{
	return from;
}
template <typename FT>
transform2dE_matrix<FT> transform2d_cast(const transform2d_matrix<FT>& from) noexcept
{
	transform2dE_matrix<FT> matrix;
	matrix_zero(matrix);
	matrix[0][0] = from[0][0];
	matrix[0][1] = from[0][1];
	matrix[1][0] = from[1][0];
	matrix[1][1] = from[1][1];
	matrix[2][2] = 1;
	return matrix;
}
template <typename FT>
transform2d_point<FT> transform2d_cast(const transform2d_point<FT>& from) noexcept
{
	return from;
}
template <typename FT>
transform2dE_point<FT> transform2d_cast(const transform2dE_point<FT>& from) noexcept
{
	return from;
}
template <typename FT>
transform2dE_point<FT> transform2d_cast(const transform2d_point<FT>& from) noexcept
{
	return transform2dE_point<FT>{{{
		{{from[0][0]}},
		{{from[1][0]}},
		{{1}}
	}}};
}

template <typename FT>
transform2d_point<FT> transform2d_pt(FT x, FT y) noexcept
{
	return transform2d_point<FT>{{{
		{{x}},
		{{y}}
	}}};
}
template <typename FT>
transform2dE_point<FT> transform2d_ptE(FT x, FT y) noexcept
{
	return transform2dE_point<FT>{{{
		{{x}},
		{{y}},
		{{1}}
	}}};
}

template <typename FT>
FT transform2d_get_x(const transform2d_point<FT>& pt) noexcept
{
	return pt[0].front();
}
template <typename FT>
FT& transform2d_get_x(transform2d_point<FT>& pt) noexcept
{
	return pt[0].front();
}
template <typename FT>
FT transform2d_get_x(const transform2dE_point<FT>& pt) noexcept
{
	return pt[0].front();
}
template <typename FT>
FT& transform2d_get_x(transform2dE_point<FT>& pt) noexcept
{
	return pt[0].front();
}

template <typename FT>
FT transform2d_get_y(const transform2d_point<FT>& pt) noexcept
{
	return pt[1].front();
}
template <typename FT>
FT& transform2d_get_y(transform2d_point<FT>& pt) noexcept
{
	return pt[1].front();
}
template <typename FT>
FT transform2d_get_y(const transform2dE_point<FT>& pt) noexcept
{
	return pt[1].front();
}
template <typename FT>
FT& transform2d_get_y(transform2dE_point<FT>& pt) noexcept
{
	return pt[1].front();
}

template <typename Arg0, typename... Args>
auto transform2d_merge(const Arg0& arg0, const Args&... args) noexcept
{
	static_assert(is_transform2d_matrix_v<Arg0> && std::conjunction_v<is_transform2d_matrix<Args>...>, "All args must be transform2d matrix");
	using FT = typename Arg0::float_type;
	static_assert(std::conjunction_v<std::is_same<FT, typename Args::float_type>...>, "All args must have same float_type");
	if constexpr (sizeof...(args) == 0) { // only a single transformation, return
		return arg0;
	} else if constexpr (std::conjunction_v<std::is_same<Arg0, Args>...>) { // all same, 2x2 or 3x3
		Arg0 res = arg0;
		( (res = args * res) , ...); // fold operation
		return res;
	} else { // a mix of 2x2 or 3x3
		transform2dE_matrix res = transform2d_cast<transform2dE_matrix>(arg0);
		( (res = transform2d_cast<transform2dE_matrix>(args) * res) , ...); // fold operation
		return res;
	}
}

template <typename Transform, typename Pt>
auto transform2d_apply(const Transform& transform, const Pt& point) noexcept
{
	static_assert(is_transform2d_matrix_v<Transform> && is_transform2d_point_v<Pt>, "transform and point must be a valid matrix and point");
	static_assert(std::is_same_v<typename Transform::float_type, typename Pt::float_type>, "transform and point must have the same float_type");
	using FT = typename Transform::float_type;
	if constexpr (is_transform2d_extend_v<Transform> == is_transform2d_extend_v<Pt>) { // matrix and point are both correct dimensions
		return transform * point;
	} else { // matrix and point are opposite dimensions
		return transform2d_cast<transform2dE_matrix<FT>>(transform) * transform2d_cast<transform2dE_point<FT>>(point);
	}
}

} // namespace inx::alg

#endif // GMLIB_TRANSFORM2D_HPP_INCLUDED
