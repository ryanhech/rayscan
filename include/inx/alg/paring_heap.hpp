#ifndef GMLIB_PARING_HEAP_HPP_INCLUDED
#define GMLIB_PARING_HEAP_HPP_INCLUDED

#include "../inx.hpp"
#include <functional>
#include <unordered_set>
#include <vector>

namespace inx::alg {

template <typename Node, typename Compare = std::less<Node>, typename Tag = void> 
class pairing_heap;
template <typename Tag = void>
struct pairing_heap_tag;
template <typename Node, typename Compare, typename Tag>
bool is_valid_pairing_heap(const pairing_heap<Node, Compare, Tag>& heap, const typename pairing_heap<Node, Compare, Tag>::node_type* ign = nullptr);

template <typename Tag>
struct pairing_heap_tag
{
	struct NodeData {
		pairing_heap_tag* parent;
		pairing_heap_tag* prevSibling;
		pairing_heap_tag* nextSibling;
		pairing_heap_tag* child;
	} m_nData;

	pairing_heap_tag() noexcept = default;
	pairing_heap_tag(const pairing_heap_tag<Tag>&) noexcept = default;
	pairing_heap_tag(pairing_heap_tag<Tag>&&) noexcept = default;
	pairing_heap_tag(std::nullptr_t) noexcept : m_nData{nullptr, nullptr, nullptr, nullptr} { }

	pairing_heap_tag<Tag>& operator=(const pairing_heap_tag<Tag>&) noexcept = default;
	pairing_heap_tag<Tag>& operator=(pairing_heap_tag<Tag>&&) noexcept = default;
};

template <typename Node, typename Compare, typename Tag>
class pairing_heap
{
public:
	using value_type = Node;
	using value_compare = Compare;
	using node_type = pairing_heap_tag<Tag>;
	static_assert(std::is_convertible_v<value_type*, node_type*>, "value_type must be convertable to node_type");

	pairing_heap(value_compare compare = value_compare()) : mRoot(nullptr), mSize(0), cmp(compare)
	{ }

	bool empty() const noexcept { return !mRoot; }
	size_t size() const noexcept { return mSize; }

	value_type* root() noexcept { return static_cast<value_type*>(mRoot); }
	const value_type* root() const noexcept { return static_cast<const value_type*>(mRoot); }

	value_type& top() noexcept { assert(mRoot != nullptr); return *static_cast<value_type*>(mRoot); }
	const value_type& top() const noexcept { assert(mRoot != nullptr); return *static_cast<const value_type*>(mRoot); }

	void push(value_type& n)
	{
		if (mRoot != nullptr) {
			if (compare(mRoot, &n)) {
				n.node_type::m_nData.child = nullptr;
				insert_under(mRoot, &n);
			} else {
				n.node_type::m_nData.parent = n.node_type::m_nData.prevSibling = n.node_type::m_nData.nextSibling = nullptr;
				n.node_type::m_nData.child = mRoot; mRoot->node_type::m_nData.parent = &n;
				mRoot = &n;
				assert(mRoot->node_type::m_nData.parent == nullptr && mRoot->node_type::m_nData.prevSibling == nullptr && mRoot->node_type::m_nData.nextSibling == nullptr);
			}
		} else {
			n.node_type::m_nData.parent = n.node_type::m_nData.prevSibling = n.node_type::m_nData.nextSibling = n.node_type::m_nData.child = nullptr;
			mRoot = &n;
			assert(mRoot->node_type::m_nData.parent == nullptr && mRoot->node_type::m_nData.prevSibling == nullptr && mRoot->node_type::m_nData.nextSibling == nullptr);
		}
		mSize++;
	}

	void pop()
	{
		assert(mRoot != nullptr && mRoot->node_type::m_nData.nextSibling == nullptr && mRoot->node_type::m_nData.prevSibling == nullptr);
		assert(mSize > 0);
		mRoot = remove_root(mRoot);
		assert(mRoot == nullptr || (mRoot->node_type::m_nData.parent == nullptr && mRoot->node_type::m_nData.prevSibling == nullptr && mRoot->node_type::m_nData.nextSibling == nullptr));
		mSize--;
	}

	// merge will seperate n from a tree (if not root) and merge into current tree, works both on same or different queue but made for different in mind
	void merge(value_type& n)
	{
		// make n root
		if (n.node_type::m_nData.parent != nullptr)
		{
			detach_non_root(n);
		}
		// if n is root, do nothing
		if (&n != mRoot)
		{
			mRoot = merge(mRoot, &n);
		}
	}

	void raise(value_type& n)
	{
		if (n.node_type::m_nData.parent != nullptr && compare(&n, n.node_type::m_nData.parent))
		{
			detach_non_root(&n);
			mRoot = merge(mRoot, &n);
		}
	}
	void raise(value_type* n)
	{
		assert(n != nullptr);
		raise(*n);
	}

	void lower(value_type& n)
	{
		if (n.node_type::m_nData.child != nullptr) { // if has child, check to lower
			// merge children to make only a single node to compare against
			n.node_type::m_nData.child = merge_siblings(n.node_type::m_nData.child);
			assert(n.node_type::m_nData.child->node_type::m_nData.prevSibling == nullptr && n.node_type::m_nData.child->node_type::m_nData.nextSibling == nullptr);
			n.node_type::m_nData.child->node_type::m_nData.parent = &n;
			if (compare(n.node_type::m_nData.child, &n)) { // must lower
				if (n.node_type::m_nData.parent != nullptr) {
					// not root, replace child with n and reinsert n
#if 0
					replace_non_root_with(&n, n.node_type::m_nData.child); // replace n with child
					n.node_type::m_nData.child = nullptr;
					mRoot = merge(mRoot, &n);
#else
					detach_non_root(&n);
					auto* x = n.node_type::m_nData.child;
					n.node_type::m_nData.child = nullptr;
					insert_under(x, &n);
					insert_under(mRoot, x);
#endif
				} else {
					// is root, insert under child and make child root
					assert(mRoot == &n);
#if 1
					mRoot = n.node_type::m_nData.child;
					mRoot->node_type::m_nData.parent = nullptr;
					n.node_type::m_nData.child = nullptr;
					insert_under(mRoot, &n);
#else
					mRoot = merge(&n, std::exchange(n.node_type::m_nData.child, nullptr));
#endif
				}
			}
		}
	}
	void lower(value_type* n)
	{
		assert(n != nullptr);
		lower(*n);
	}

	// raises or lowers to correct single node
	void adjust(value_type& n)
	{
		if (n.node_type::m_nData.parent != nullptr && compare(&n, n.node_type::m_nData.parent)) { // raise
			detach_non_root(&n);
			mRoot = merge(mRoot, &n);
		} else {
			lower(n);
		}
	}
	void adjust(value_type* n)
	{
		assert(n != nullptr);
		adjust(*n);
	}

	void erase(value_type& n)
	{
		if (n.node_type::m_nData.parent != nullptr) {
			assert(mRoot != &n);
			detach_non_root(&n);
			if (auto x = remove_root(&n); x != nullptr)
				insert_under(mRoot, x);
		} else {
			assert(mRoot == &n);
			mRoot = remove_root(&n);
			assert(mRoot == nullptr || (mRoot->node_type::m_nData.parent == nullptr && mRoot->node_type::m_nData.prevSibling == nullptr && mRoot->node_type::m_nData.nextSibling == nullptr));
		}
		mSize--;
	}
	void erase(value_type* n)
	{
		assert(n != nullptr);
		erase(*n);
	}

	void clear() noexcept
	{
		mRoot = nullptr;
		mSize = 0;
	}

private:
	bool compare(const node_type* a, const node_type* b) const
	{
		assert(a != nullptr && b != nullptr);
		return compare(static_cast<const value_type*>(a), static_cast<const value_type*>(b));
	}
	bool compare(const value_type* a, const node_type* b) const
	{
		assert(a != nullptr && b != nullptr);
		return compare(a, static_cast<const value_type*>(b));
	}
	bool compare(const node_type* a, const value_type* b) const
	{
		assert(a != nullptr && b != nullptr);
		return compare(static_cast<const value_type*>(a), b);
	}
	bool compare(const value_type* a, const value_type* b) const
	{
		assert(a != nullptr && b != nullptr);
		return cmp(*a, *b);
	}

	node_type* remove_root(node_type* node)
	{
		assert(node != nullptr);
		if (!node->m_nData.child)
			return nullptr;
		return merge_siblings(node->m_nData.child);
	}
	node_type* merge_siblings(node_type* a)
	{
		assert(a != nullptr);
		assert(a->m_nData.prevSibling == nullptr);
		auto b = a->m_nData.nextSibling;
		if (!b) { // set a to root node and return
			a->m_nData.parent = a->m_nData.prevSibling = a->m_nData.nextSibling = nullptr;
			return a;
		}
		auto c = b->m_nData.nextSibling;
		a = merge(a, b);
		while (c != nullptr) {
			b = c->m_nData.nextSibling;
			if (!b) { return merge(a, c); }
			auto d = b->m_nData.nextSibling;
			b = merge(b, c);
			a = merge(a, b);
			c = d;
		}
		return a;
	}

	void detach_non_root(node_type* a) // must not be root
	{
		assert(a != nullptr && a->m_nData.parent != nullptr);
		if (a->m_nData.prevSibling) {
			assert(a->m_nData.prevSibling != a->m_nData.nextSibling);
			a->m_nData.prevSibling->m_nData.nextSibling = a->m_nData.nextSibling;
		} else {
			a->m_nData.parent->m_nData.child = a->m_nData.nextSibling;
		}
		if (a->m_nData.nextSibling) {
			a->m_nData.nextSibling->m_nData.prevSibling = a->m_nData.prevSibling;
		}
	}
	void detach_non_root_valid(node_type* a) // must not be root
	{
		detach_non_root(a);
		a->m_nData.parent = a->m_nData.prevSibling = a->m_nData.nextSibling = nullptr;
	}

	// moves rooted node rep into parent postion, parent cannot be root
	void replace_non_root_with(node_type* parent, node_type* rep) // must not be root
	{
		assert(parent != nullptr && rep != nullptr && parent->m_nData.parent != nullptr);
		rep->m_nData.parent = parent->m_nData.parent;
		rep->m_nData.prevSibling = parent->m_nData.prevSibling;
		rep->m_nData.nextSibling = parent->m_nData.nextSibling;
		if (rep->m_nData.prevSibling != nullptr) {
			assert(rep->m_nData.prevSibling->m_nData.nextSibling == parent);
			rep->m_nData.prevSibling->m_nData.nextSibling = rep;
		} else {
			assert(rep->m_nData.parent->m_nData.child == parent);
			rep->m_nData.parent->m_nData.child = rep;
		}
		if (rep->m_nData.nextSibling != nullptr) {
			assert(rep->m_nData.nextSibling->m_nData.prevSibling == parent);
			rep->m_nData.nextSibling->m_nData.prevSibling = rep;
		}
	}

	node_type* merge(node_type* a, node_type* b)
	{
		assert(a != b);
		if (compare(b, a)) std::swap(a, b);
		a->m_nData.parent = a->m_nData.prevSibling = a->m_nData.nextSibling = nullptr;
		b->m_nData.parent = a; b->m_nData.prevSibling = nullptr; b->m_nData.nextSibling = a->m_nData.child;
		a->m_nData.child = b;
		if (b->m_nData.nextSibling) {
			assert(b->m_nData.nextSibling->m_nData.prevSibling == nullptr);
			b->m_nData.nextSibling->m_nData.prevSibling = b;
		}
		return a;
	}

	void insert_under(node_type* parent, node_type* child)
	{
		assert(parent != nullptr && child != nullptr);
		child->m_nData.prevSibling = nullptr;
		child->m_nData.parent = parent;
		child->m_nData.nextSibling = parent->m_nData.child;
		parent->m_nData.child = child;
		if (child->m_nData.nextSibling) {
			assert(child->m_nData.nextSibling->m_nData.prevSibling == nullptr);
			child->m_nData.nextSibling->m_nData.prevSibling = child;
		}
	}

private:
	node_type* mRoot;
	size_t mSize;
	Compare cmp;

	template <typename FNode, typename FCompare, typename FTag>
	friend bool is_valid_pairing_heap(const pairing_heap<FNode, FCompare, FTag>&, const typename pairing_heap<FNode, FCompare, FTag>::node_type*);
};

template <typename Node, typename Compare, typename Tag>
bool is_valid_pairing_heap(const pairing_heap<Node, Compare, Tag>& heap, const typename pairing_heap<Node, Compare, Tag>::node_type* ign)
{
	using heap_type = const pairing_heap<Node, Compare, Tag>;
	using value_type = const typename heap_type::node_type*;
	std::vector<value_type> nodes;
	std::unordered_set<value_type> seen;
	size_t size = 0; // use for size check and also proection against a loop
	if (heap.mRoot != nullptr) {
		value_type root = heap.mRoot;
		if (root->m_nData.parent != nullptr || root->m_nData.prevSibling != nullptr || root->m_nData.nextSibling != nullptr)
			return false;
		nodes.push_back(root);
		if (size++ > heap.mSize)
			return false;
	}
	while (!nodes.empty()) {
		value_type n = nodes.back(); nodes.pop_back();
		if (!seen.emplace(n).second)
			return false; // already seen
		value_type c = n->m_nData.child;
		if (c != nullptr) {
			for (value_type cp = nullptr; c != nullptr; cp = c, c = c->m_nData.nextSibling) {
				if (c->m_nData.prevSibling != cp || c->m_nData.parent != n)
					return false;
				if (c != ign && n != ign && heap.compare(c, n)) // c is not meant to be above n
					return false;
				nodes.push_back(c);
				if (size++ > heap.mSize)
					return false;
			}
		}
	}
	if (size != heap.mSize)
		return false;
	return true;
}

template <typename PH>
bool pairing_heap_node_within_(const typename PH::node_type* at, const typename PH::node_type& node)
{
	if (at == nullptr)
		return false;
	for ( ; at != nullptr; at = at->m_nData.nextSibling) {
		if (at == &node)
			return true;
		if (pairing_heap_node_within_<PH>(at->m_nData.child, node))
			return true;
	}
	return false;
}
template <typename Node, typename Compare, typename Tag>
bool pairing_heap_node_within(const pairing_heap<Node, Compare, Tag>& heap, const Node& node)
{
	return pairing_heap_node_within_<pairing_heap<Node, Compare, Tag>>(heap.root(), node);
		
}

} // namespace inx::alg

#endif // GMLIB_PARING_HEAP_HPP_INCLUDED
