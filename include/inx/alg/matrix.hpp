#ifndef GMLIB_MATRIX_HPP_INCLUDED
#define GMLIB_MATRIX_HPP_INCLUDED

#include "../inx.hpp"
#include "mary_tree.hpp"

namespace inx::alg {

template <typename FloatType, size_t Rows, size_t Cols = Rows>
struct StaticMatrix : std::array<std::array<FloatType, Cols>, Rows>
{
	static_assert(std::is_same_v<FloatType, float> || std::is_same_v<FloatType, double>, "FloatType must be float or double");
	static_assert(Rows != 0 && Cols != 0, "rows and columns cannot equal 0");
	constexpr static size_t rows() noexcept { return Rows; }
	constexpr static size_t columns() noexcept { return Cols; }
	constexpr static size_t cells() noexcept { return Rows * Cols; }
	using float_type = FloatType;
	using flat_type = std::array<FloatType, cells()>;
	
	flat_type& flat() noexcept { return reinterpret_cast<flat_type&>(*this); }
	const flat_type& flat() const noexcept { return reinterpret_cast<const flat_type&>(*this); }
};

template <typename FT, size_t R, size_t C>
void matrix_zero(StaticMatrix<FT, R, C>& matrix) noexcept
{
	matrix = {0};
}

template <typename FT, size_t R>
void matrix_identity(StaticMatrix<FT, R, R>& matrix) noexcept
{
	matrix_zero(matrix);
	for (auto it = matrix.flat().begin(), ite = matrix.flat().end(); it < ite; it += matrix.columns() + 1) {
		*it = 0;
	}
}

template <typename FT, size_t R, size_t C>
void matrix_plus(const StaticMatrix<FT, R, C>& a, const StaticMatrix<FT, R, C>& b, StaticMatrix<FT, R, C>& results) noexcept
{
	assert(&a != &results && &b != &results);
	for (auto mt = results.flat().begin(), mte = results.flat().end(), it = a.flat().begin(), jt = a.flat().begin(); mt != mte; ++mt, ++it, ++jt)
	{
		*mt = *it + *jt;
	}
}
template <typename FT, size_t R, size_t C>
StaticMatrix<FT, R, C>& operator+=(StaticMatrix<FT, R, C>& lhs, const StaticMatrix<FT, R, C>& rhs) noexcept
{
	for (auto it = lhs.flat().begin(), ite = lhs.flat().end(), jt = rhs.flat().begin(); it != ite; ++it, ++jt)
	{
		*it += *jt;
	}
	return lhs;
}
template <typename FT, size_t R, size_t C>
StaticMatrix<FT, R, C> operator+(const StaticMatrix<FT, R, C>& lhs, const StaticMatrix<FT, R, C>& rhs) noexcept
{
	StaticMatrix<FT, R, C> matrix;
	matrix_plus(lhs, rhs, matrix);
	return matrix;
}

template <typename FT, size_t R, size_t C>
void matrix_minus(const StaticMatrix<FT, R, C>& a, const StaticMatrix<FT, R, C>& b, StaticMatrix<FT, R, C>& results) noexcept
{
	assert(&a != &results && &b != &results);
	for (auto mt = results.flat().begin(), mte = results.flat().end(), it = a.flat().begin(), jt = a.flat().begin(); mt != mte; ++mt, ++it, ++jt)
	{
		*mt = *it - *jt;
	}
}
template <typename FT, size_t R, size_t C>
StaticMatrix<FT, R, C>& operator-=(StaticMatrix<FT, R, C>& lhs, const StaticMatrix<FT, R, C>& rhs) noexcept
{
	for (auto it = lhs.flat().begin(), ite = lhs.flat().end(), jt = rhs.flat().begin(); it != ite; ++it, ++jt)
	{
		*it -= *jt;
	}
	return lhs;
}
template <typename FT, size_t R, size_t C>
StaticMatrix<FT, R, C> operator-(const StaticMatrix<FT, R, C>& lhs, const StaticMatrix<FT, R, C>& rhs) noexcept
{
	StaticMatrix<FT, R, C> matrix;
	matrix_minus(lhs, rhs, matrix);
	return matrix;
}

template <typename M1, typename M2>
struct matrix_multiply_type;
template <typename FT, size_t R, size_t C, size_t M>
struct matrix_multiply_type<StaticMatrix<FT, R, M>, StaticMatrix<FT, M, C>>
{
	using type = StaticMatrix<FT, R, C>;
};
template <typename M1, typename M2>
using matrix_multiply_type_t = typename matrix_multiply_type<M1, M2>::type;
template <typename FT, size_t R, size_t C, size_t M>
void matrix_multiply(const StaticMatrix<FT, R, M>& a, const StaticMatrix<FT, M, C>& b, StaticMatrix<FT, R, C>& results) noexcept
{
	for (size_t r = 0; r < R; ++r) {
		for (size_t c = 0; c < C; ++c) {
			FT s = 0;
			for (auto rt = a[r].begin(), rte = a[r].end(), ct = b.flat().begin() + c; rt != rte; ++rt, ct += C) {
				s += *rt * *ct;
			}
			results[r][c] = s;
		}
	}
}
template <typename FT, size_t M, size_t R, size_t C>
StaticMatrix<FT, R, C> operator*(const StaticMatrix<FT, R, M>& lhs, const StaticMatrix<FT, M, C>& rhs) noexcept
{
	StaticMatrix<FT, R, C> matrix;
	matrix_multiply(lhs, rhs, matrix);
	return matrix;
}

} // namespace inx::alg

#endif // GMLIB_MATRIX_HPP_INCLUDED
