#ifndef GMLIB_IO_TRANSFORMERS_HPP
#define GMLIB_IO_TRANSFORMERS_HPP

#include <inx/inx.hpp>
#include <ostream>
#include <limits>
#include <cmath>
#include "../math.hpp"

namespace inx::io
{

template <typename T>
std::enable_if_t<std::is_floating_point_v<std::decay_t<T>>, bool>
float_is_integer(T x)
{
	T ign;
	return std::modf(x, &ign) == 0;
}

template <typename Int, typename T>
std::enable_if_t<std::is_integral_v<std::decay_t<Int>> && std::is_floating_point_v<std::decay_t<T>>, bool>
float_fits_integer(T x)
{
	return float_is_integer(x) && std::abs(x) < static_cast<T>(0.97) * static_cast<T>(std::numeric_limits<Int>::max());
}

template <typename T>
struct accurate_number
{
	static_assert(std::is_same_v<std::decay_t<T>, T> && (std::is_integral_v<T> || std::is_floating_point_v<T>), "T must be fully decayed and a number");
	constexpr accurate_number(T val) noexcept : m_val(val)
	{ }

	T m_val;
};

template <typename T>
accurate_number(T x) -> accurate_number<std::decay_t<T>>;

template <typename T>
std::ostream& operator<<(std::ostream& out, const accurate_number<T>& num)
{
	if constexpr (std::is_integral_v<T>) {
		out << num.m_val;
	} else {
		double nabs = std::abs(static_cast<double>(num.m_val));
		constexpr size_t doublerep = std::numeric_limits<double>::max_digits10 - 3;
		if ( float_fits_integer<ssize_t>(nabs) ) {
			out << std::fixed << static_cast<ssize_t>(num.m_val);
		} else if (nabs >= 10e10 || nabs <= 10e-6) {
			out << std::scientific << std::setprecision(doublerep) << num.m_val;
		} else {
			std::ostringstream oss;
			oss << std::fixed << std::setprecision(2*doublerep) << nabs;
			std::string toStr = oss.str();
			std::size_t deciS = toStr.find('.');
			if (deciS == std::string::npos) {
				out << toStr; // should never occur, but just in case print integer
				return out;
			}
			int deci = static_cast<int>(deciS);
			int frac = static_cast<int>(doublerep) - deci;
			if (frac <= 0) { // no space left for decimal, should never occur but just in case
				toStr.erase(toStr.begin() + deci, toStr.end());
				out << toStr;
				return out;
			}
			deci += 1;
			bool rndUp = toStr[deci+frac] >= '5'; // round up
			toStr.erase(toStr.begin() + (deci+frac), toStr.end());
			if (rndUp) {
				int i;
				for (i = deci+frac-1; i >= deci; i--) {
					if (toStr[i] == '9') { toStr[i] = '0'; } else { toStr[i]++; break; }
				}
				if (i == deci-1) { // round up integer
					oss.str(std::string());
					oss << std::setprecision(0) << std::round(num.m_val);
					out << oss.str();
					return out;
				}
			}
			while (toStr.back() == '0') { toStr.pop_back(); }
			out << (num.m_val < 0 ? "-" : "") << toStr;
		}
	}
	return out;
}

} // namespace inx::io

#endif // GMLIB_IO_TRANSFORMERS_HPP
