#ifndef GMLIB_IO_WHITESPACE_LINE_FILTER_HPP
#define GMLIB_IO_WHITESPACE_LINE_FILTER_HPP

#include <inx/inx.hpp>
#include <boost/iostreams/filter/line.hpp>
#include <cctype>

namespace inx::io
{

class whitespace_line_filter : public boost::iostreams::line_filter
{
private:
	std::string do_filter(const std::string& line) override
	{
		m_buffer.clear();
		enum class Mode {
			start,
			space,
			copy
		};
		Mode mode = Mode::start;
		for (char c : line) {
			if (std::isblank(c)) {
				if (mode == Mode::copy)
					mode = Mode::space;
			} else {
				if (mode == Mode::space)
					m_buffer.push_back(' ');
				m_buffer.push_back(c);
				mode = Mode::copy;
			}
		}
		return m_buffer;
	}

	std::string m_buffer;
};

} // namespace inx::io

#endif // GMLIB_IO_WHITESPACE_LINE_FILTER_HPP
