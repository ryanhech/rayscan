#ifndef GMLIB_IO_CHUNKED_ARRAY_DEVICE_HPP
#define GMLIB_IO_CHUNKED_ARRAY_DEVICE_HPP

#include <inx/inx.hpp>
#include <inx/bits.hpp>
#include <boost/iostreams/concepts.hpp>
#include <memory_resource>

namespace inx::io
{

struct alignas(void*) chunked_data_
{
	chunked_data_** array() noexcept
	{
		return reinterpret_cast<chunked_data_**>(this);
	}
	chunked_data_* left() noexcept { return array()[0]; }
	chunked_data_* right() noexcept { return array()[1]; }
	void* data() noexcept { return static_cast<void*>(array() + 2);}
};

template <typename Ch = char>
class basic_chunked_array;
template <typename Ch = char>
class basic_chunked_array_source;

template <typename Ch>
class basic_chunked_array : public boost::iostreams::device<boost::iostreams::seekable, Ch>
{
public:
	basic_chunked_array(std::pmr::memory_resource* mr = nullptr, size_t chunkSize = 1024)
	{
		if (mr == nullptr) {
			mr = std::pmr::get_default_resource();
		}
		m_resource = mr;
		if (chunkSize < min_chunk_size()) {
			chunkSize = min_chunk_size();
		}
		m_chunkSize = chunkSize;
		m_dataSize = (chunkSize - header_size()) / sizeof(Ch);
		m_dataTotal = 0;
		m_chunkRoot = nullptr;
		m_chunkCount = 0;
		m_size = 0;
		m_seek = 0;
		m_seekLocal = 0;
		m_seekChunk = nullptr;
	}

	basic_chunked_array(const basic_chunked_array<Ch>&) = delete;

	basic_chunked_array(basic_chunked_array<Ch>&& rhs) = default;

	std::streamsize read(Ch* s, std::streamsize n)
	{
		assert(m_seek <= m_size);
		if (m_seek >= m_size)
			return -1;
		std::streamsize rdCount = 0;
		if (m_seekChunk == nullptr)
			setSeek(m_seek);
		while (n > 0) {
			if (m_seek >= m_size) // end of stream
				break;
			assert(m_seekChunk != nullptr);
			size_t count = m_dataSize - m_seekLocal;
			if (n < static_cast<std::streamsize>(count)) count = n; // count is less than n
			if (m_seek + count > m_size) count = m_size - m_seek; // ensure count is less than the remining reading size
			std::copy_n(static_cast<const Ch*>(m_seekChunk->data()) + m_seekLocal, count, s);
			s += count;
			n -= static_cast<std::streamsize>(count);
			rdCount += static_cast<std::streamsize>(count);
			if (n > 0) {
				setSeek(m_seek + count);
			} else {
				m_seek += count;
				m_seekLocal += count;
				assert(m_seek <= m_size && m_seekLocal <= m_dataSize);
			}
		}
		return rdCount;
	}

	std::streamsize write(const Ch* s, std::streamsize n)
	{
		std::streamsize ans = n;
		while (n > 0) {
			// extend size of stream
			if (m_seekChunk == nullptr) { // end of stream
				assert(m_seek == m_size);
				m_seekChunk = newChunk();
				m_seekLocal = 0;
			}

			assert(m_seekChunk != nullptr && m_seekLocal <= m_dataSize);
			size_t count = m_dataSize - m_seekLocal;
			if (n < static_cast<std::streamsize>(count)) count = n; // count is less than n
			if (m_seek + count > m_dataTotal) count = m_dataTotal - m_seek; // ensure count is less than the remining reading size
			std::copy_n(s, count, static_cast<Ch*>(m_seekChunk->data()) + m_seekLocal);
			s += count;
			n -= static_cast<std::streamsize>(count);
			m_size = std::max(m_size, m_seek + count);
			if (n > 0) {
				setSeek(m_seek + count);
			} else {
				m_seek += count;
				m_seekLocal += count;
				assert(m_seek <= m_size && m_seekLocal <= m_dataSize);
			}
		}
		return ans;
	}
	
	std::streampos seek(boost::iostreams::stream_offset off, std::ios_base::seekdir way)
	{
		switch (way) {
		case std::ios_base::beg:
			break; // do nothing, off is the position
		case std::ios_base::end:
			off = static_cast<boost::iostreams::stream_offset>(m_size) + off;
			break;
		case std::ios_base::cur:
			off = static_cast<boost::iostreams::stream_offset>(m_seek) + off;
			break;
		default:
			throw std::ios_base::failure("way enum not in seekdir");
		}
		if (off < 0 || off > static_cast<boost::iostreams::stream_offset>(m_size))
			throw std::ios_base::failure("seek outside of device bounds");
		setSeek(static_cast<size_t>(off));
		return static_cast<std::streampos>(off);
	}

protected:
	using chunks = chunked_data_;
	static_assert(alignof(Ch) <= alignof(chunks), "Ch should not be larger than pointer size");
	static constexpr size_t alignment() noexcept { return alignof(chunks); }
	static constexpr size_t header_size() noexcept { return 2 * sizeof(chunks); }
	static constexpr size_t min_chunk_size() noexcept { return header_size() + sizeof(Ch) * 8; }

protected:
	chunks* findChunk(size_t id) const noexcept
	{
		chunks* level = m_chunkRoot;
		// traverse perfect binary tree to chunk id
		for (size_t ibit = (static_cast<size_t>(1) << clz_index(id+1)) >> 1; ibit != 0 && level != nullptr; ibit >>= 1) {
			level = level->array()[(id & ibit) == 0 ? 0 : 1];
		}
		return level;
	}
	chunks* newChunk() noexcept
	{
		chunks** level = &m_chunkRoot;
		size_t id = m_chunkCount;
		// traverse perfect binary tree to chunk id
		for (size_t ibit = (static_cast<size_t>(1) << clz_index(id+1)) >> 1; ibit != 0; ibit >>= 1) {
			if (*level == nullptr)
				return nullptr; // error, should not happen but checked regardless
			level = (*level)->array() + ((id & ibit) == 0 ? 0 : 1);
		}
		*level = static_cast<chunks*>(m_resource->allocate(m_chunkSize, alignment()));
		std::uninitialized_default_construct_n(reinterpret_cast<std::byte*>(*level), m_chunkSize); // zero fill memory
		m_chunkCount += 1;
		m_dataTotal += m_dataSize;
		return *level;
	}

	void setSeek(size_t seek)
	{
		assert(seek <= m_size);
		m_seek = seek;
		size_t chunkId = seek / m_dataSize;
		m_seekLocal = seek % m_dataSize;
		if (chunkId >= m_chunkCount) {
			m_seekChunk = nullptr;
		} else {
			m_seekChunk = findChunk(chunkId);
		}
	}

protected:
	// chunk information
	size_t m_chunkSize; // the size of each chunk, must be at least header_size()
	size_t m_dataSize; // the amount of char_types that fit per chunk
	size_t m_dataTotal; // the total amount of data that is available for writing
	chunks* m_chunkRoot; // the root
	size_t m_chunkCount; // the number of chunks allocated
	// read/write information
	size_t m_size; // the size of device written
	size_t m_seek; // the seek head
	size_t m_seekLocal; // the local id
	chunks* m_seekChunk; // the chunk seek head is at
	// memory resource
	std::pmr::memory_resource* m_resource;

	template <typename>
	friend class basic_chunked_array_source;
};

using chunked_array = basic_chunked_array<char>;
using wchunked_array = basic_chunked_array<wchar_t>;

template <typename Ch>
class basic_chunked_array_source : public boost::iostreams::device<boost::iostreams::input_seekable, Ch>
{
public:
	using upstream = basic_chunked_array<Ch>;
	basic_chunked_array_source(const upstream& src, bool copyPosition = false)
	{
		m_upstream = &src;
		if (!copyPosition) {
			m_seek = 0;
			m_seekLocal = 0;
			m_seekChunk = nullptr;
		} else {
			m_seek = src.m_seek;
			m_seekLocal = src.m_seekLocal;
			m_seekChunk = src.m_seekChunk;
		}
	}
	basic_chunked_array_source(const basic_chunked_array_source<Ch>& src) = default;
	basic_chunked_array_source(basic_chunked_array_source<Ch>&& src) = default;

	std::streamsize read(Ch* s, std::streamsize n)
	{
		assert(m_seek <= m_upstream->m_size);
		if (m_seek >= m_upstream->m_size) // end of stream
			return -1;
		std::streamsize rdCount = 0;
		if (m_seekChunk == nullptr)
			setSeek(m_seek);
		while (n > 0) {
			if (m_seek >= m_upstream->m_size) // end of stream
				break;
			assert(m_seekChunk != nullptr);
			size_t count = m_upstream->m_dataSize - m_seekLocal;
			if (n < static_cast<std::streamsize>(count)) count = n; // count is less than n
			if (m_seek + count > m_upstream->m_size) count = m_upstream->m_size - m_seek; // ensure count is less than the remining reading size
			std::copy_n(static_cast<const Ch*>(m_seekChunk->data()) + m_seekLocal, count, s);
			s += count;
			n -= static_cast<std::streamsize>(count);
			rdCount += static_cast<std::streamsize>(count);
			if (n > 0) {
				setSeek(m_seek + count);
			} else {
				m_seek += count;
				m_seekLocal += count;
				assert(m_seek <= m_upstream->m_size && m_seekLocal <= m_upstream->m_dataSize);
			}
		}
		return rdCount;
	}

	std::streampos seek(boost::iostreams::stream_offset off, std::ios_base::seekdir way)
	{
		switch (way) {
		case std::ios_base::beg:
			break; // do nothing, off is the position
		case std::ios_base::end:
			off = static_cast<boost::iostreams::stream_offset>(m_upstream->m_size) + off;
			break;
		case std::ios_base::cur:
			off = static_cast<boost::iostreams::stream_offset>(m_seek) + off;
			break;
		default:
			throw std::ios_base::failure("way enum not in seekdir");
		}
		if (off < 0 || off > static_cast<boost::iostreams::stream_offset>(m_upstream->m_size))
			throw std::ios_base::failure("seek outside of device bounds");
		setSeek(static_cast<size_t>(off));
		return static_cast<std::streampos>(off);
	}

protected:
	void setSeek(size_t seek)
	{
		assert(seek <= m_upstream->m_size);
		m_seek = seek;
		size_t ds = m_upstream->m_dataSize;
		size_t chunkId = seek / ds;
		m_seekLocal = seek % ds;
		if (chunkId >= m_upstream->m_chunkCount) {
			m_seekChunk = nullptr;
		} else {
			m_seekChunk = m_upstream->findChunk(chunkId);
		}
	}

protected:
	const upstream* m_upstream;
	size_t m_dataSize;
	size_t m_seek; // the seek head
	size_t m_seekLocal; // the local id
	typename upstream::chunks* m_seekChunk; // the chunk seek head is at
};

using chunked_array = basic_chunked_array<char>;
using wchunked_array = basic_chunked_array<wchar_t>;
using chunked_array_source = basic_chunked_array_source<char>;
using wchunked_array_source = basic_chunked_array_source<wchar_t>;

} // namespace inx::io

#endif // GMLIB_IO_CHUNKED_ARRAY_DEVICE_HPP
