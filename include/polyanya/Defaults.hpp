#ifndef POLYPROG_DEFAULTS_HPP_INCLUDED
#define POLYPROG_DEFAULTS_HPP_INCLUDED

#include <rsapp/conf/Register.hpp>

namespace polyanya
{

class Defaults : public rsapp::conf::Register
{
public:
	void registerConfig() override;
};

} // namespace polyanya

#endif // POLYPROG_DEFAULTS_HPP_INCLUDED
