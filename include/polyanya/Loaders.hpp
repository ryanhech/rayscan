#ifndef POLYPROG_LOADERS_HPP_INCLUDED
#define POLYPROG_LOADERS_HPP_INCLUDED

#include <rsapp/conf/Register.hpp>

namespace polyanya
{

class Loaders : public rsapp::conf::Register
{
public:
	void registerConfig() override;
};

} // namespace rayscan

#endif // POLYPROG_LOADERS_HPP_INCLUDED
