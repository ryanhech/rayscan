#ifndef RAYSCAND_LOADERS_HPP_INCLUDED
#define RAYSCAND_LOADERS_HPP_INCLUDED

#include <rsapp/conf/Register.hpp>

namespace rayscand
{

class Loaders : public rsapp::conf::Register
{
public:
	void registerConfig() override;
};

} // namespace rayscand

#endif // RAYSCAND_LOADERS_HPP_INCLUDED
