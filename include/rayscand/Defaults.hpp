#ifndef RAYSCAND_DEFAULTS_HPP_INCLUDED
#define RAYSCAND_DEFAULTS_HPP_INCLUDED

#include <rsapp/conf/Register.hpp>

namespace rayscand
{

class Defaults : public rsapp::conf::Register
{
public:
	void registerConfig() override;
};

} // namespace rayscand

#endif // RAYSCAND_DEFAULTS_HPP_INCLUDED
