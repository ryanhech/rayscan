#ifndef RAYSCAN_DEFAULTRAYCAST_HXX_INCLUDED
#define RAYSCAN_DEFAULTRAYCAST_HXX_INCLUDED

//#define DEFAULT_RAYCAST_QUAD

#include <rayscanlib/search/raycast/BresenhamRayCast.hpp>

namespace rayscan
{

using default_raycast_module = rayscanlib::search::modules::BresenhamRayCastModule<true>;
using default_raycast_dyn_module = rayscanlib::search::modules::BresenhamRayCastModule<true>;
using default_raycast_oracle_module = rayscanlib::search::modules::BresenhamRayCastModule<true, true>;

} // namespace rayscan

#endif // RAYSCAN_CACHEDRAYSCAN_HPP_INCLUDED
