#ifndef RAYSCAN_RTREE_HPP_INCLUDED
#define RAYSCAN_RTREE_HPP_INCLUDED

#include <rsapp/conf/Register.hpp>

namespace rayscan
{

class RTree : public rsapp::conf::Register
{
public:
	void registerConfig() override;
};

} // namespace rayscan

#endif // RAYSCAN_MULTIH_HPP_INCLUDED
