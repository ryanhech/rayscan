#ifndef RAYSCAN_LOADERS_HPP_INCLUDED
#define RAYSCAN_LOADERS_HPP_INCLUDED

#include <rsapp/conf/Register.hpp>

namespace rayscan
{

class Loaders : public rsapp::conf::Register
{
public:
	void registerConfig() override;
};

} // namespace rayscan

#endif // RAYSCAN_LOADERS_HPP_INCLUDED
