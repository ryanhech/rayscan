#ifndef RAYSCAN_IMPROVEDSCANNERS_HPP_INCLUDED
#define RAYSCAN_IMPROVEDSCANNERS_HPP_INCLUDED

#include <rsapp/conf/Register.hpp>

namespace rayscan
{

class ImprovedScanners : public rsapp::conf::Register
{
public:
	void registerConfig() override;
};

} // namespace rayscan

#endif // RAYSCAN_IMPROVEDSCANNERS_HPP_INCLUDED
