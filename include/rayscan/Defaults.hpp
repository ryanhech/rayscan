#ifndef RAYSCAN_DEFAULTS_HPP_INCLUDED
#define RAYSCAN_DEFAULTS_HPP_INCLUDED

#include <rsapp/conf/Register.hpp>

namespace rayscan
{

class Defaults : public rsapp::conf::Register
{
public:
	void registerConfig() override;
};

} // namespace rayscan

#endif // RAYSCAN_DEFAULTS_HPP_INCLUDED
