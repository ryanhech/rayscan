#ifndef RAYSCAN_SKIPPER_HPP_INCLUDED
#define RAYSCAN_SKIPPER_HPP_INCLUDED

#include <rsapp/conf/Register.hpp>

namespace rayscan
{

class Skipper : public rsapp::conf::Register
{
public:
	void registerConfig() override;
};

} // namespace rayscan

#endif // RAYSCAN_SUBMODULES_HPP_INCLUDED
