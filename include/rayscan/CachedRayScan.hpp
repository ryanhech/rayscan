#ifndef RAYSCAN_CACHEDRAYSCAN_HPP_INCLUDED
#define RAYSCAN_CACHEDRAYSCAN_HPP_INCLUDED

#include <rsapp/conf/Register.hpp>

namespace rayscan
{

class CachedRayScan : public rsapp::conf::Register
{
public:
	void registerConfig() override;
};

} // namespace rayscan

#endif // RAYSCAN_CACHEDRAYSCAN_HPP_INCLUDED
