#ifndef RAYSCAN_EXT_CONV_FWD_HPP_INCLUDED
#define RAYSCAN_EXT_CONV_FWD_HPP_INCLUDED

#include <rayscanlib/inx.hpp>

namespace conv
{
using namespace inx;
}

namespace conv
{

enum class Format : uint32
{
	Archive,
	Mesh,
	MovingAi
};

}

#endif // RAYSCAN_EXT_CONV_FWD_HPP_INCLUDED
