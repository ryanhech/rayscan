#ifndef RAYSCAN_EXT_CONV_LOADER_HPP_INCLUDED
#define RAYSCAN_EXT_CONV_LOADER_HPP_INCLUDED

#include "fwd.hpp"
#include <rayscanlib/scen/Archiver.hpp>
#include "Config.hpp"
#include <filesystem>
#include <forward_list>
#include <unordered_map>

namespace conv
{

class Loader
{
public:
	using archiver = rayscanlib::scen::Archiver;
	using path = std::filesystem::path;
	using paths = Config::input_type;
	using paths_iter = paths::const_iterator;
	using load_func = std::function<void (archiver& archive, paths_iter it, paths_iter ite)>;
	using save_func = std::function<void (const archiver& archive, const path& path)>;

	Loader();

	void load();
	void save();

	void emplace_load(std::string_view type, load_func&& fn);
	void emplace_save(std::string_view type, save_func&& fn);
	void emplace(std::string_view type, load_func&& load, save_func&& save)
	{
		emplace_load(type, std::move(load));
		emplace_save(type, std::move(save));
	}
	template <typename T>
	void emplace(std::string_view type)
	{
		emplace(type, &T::load, &T::save);
	}

	static void loadArchive(archiver& archive, paths_iter it, paths_iter ite);
	static void saveArchive(const archiver& archive, const path& path);
	static void applyConvObst(archiver& archive);
	static void applyConvMesh(archiver& archive, int merge);
	template <typename Benchmark>
	static void applyCompute(archiver& archive);

protected:
	archiver m_archive;
	std::unordered_map<std::string, load_func> m_load;
	std::unordered_map<std::string, save_func> m_save;
};

} // namespace rsapp::conf

#endif // RAYSCAN_EXT_CONV_LOADER_HPP_INCLUDED
