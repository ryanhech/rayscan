#ifndef RAYSCAN_EXT_CONV_MOVINGAILOADER_HPP_INCLUDED
#define RAYSCAN_EXT_CONV_MOVINGAILOADER_HPP_INCLUDED

#include "fwd.hpp"
#include "Loader.hpp"
#include <polyanyalib/SaveData.hpp>
#include <vector>
#include <variant>

namespace conv
{

struct MovingAiLoader final
{
	using archiver = Loader::archiver;
	using path = Loader::path;
	using paths_iter = Loader::paths_iter;


	static void load(archiver& archive, paths_iter it, paths_iter ite);
	static void save(const archiver& archive, const path& path);

	static void saveFromObstacles(::polyanyalib::SaveData& data);
	static void saveFromMesh(::polyanyalib::SaveData& data);
};

struct MovingAiScenarioLoader final
{
	using archiver = Loader::archiver;
	using path = Loader::path;
	using paths_iter = Loader::paths_iter;

	static void load(archiver& archive, paths_iter it, paths_iter ite);
	static void save(const archiver& archive, const path& path);
};

} // namespace rsapp::conf

#endif // RAYSCAN_EXT_CONV_MOVINGAILOADER_HPP_INCLUDED
