#ifndef RAYSCAN_EXT_CONV_CONFIG_HPP_INCLUDED
#define RAYSCAN_EXT_CONV_CONFIG_HPP_INCLUDED

#include "fwd.hpp"
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/positional_options.hpp>
#include <filesystem>
#include <list>
#include <set>
#include <ostream>

namespace conv
{

enum class ConvType
{
	None = 0,
	Obstacle = 1 << 0,
	Mesh = 1 << 1,
	MCDT = 1 << 2,
	MCDTG = 1 << 3
};
enum ComputePath
{
	ComputeST = 1 << 1,
	ComputeMT = 1 << 2,
	ComputeDST = 1 << 3,
	ComputeDMT = 1 << 4
};

class Config
{
public:
	using input_type = std::list<std::filesystem::path>;
	Config();
	Config(const Config&) = delete;
	Config(Config&&) = delete;
	virtual ~Config();

protected:
	virtual void initConfig(int argc, const char *const *argv);
	virtual void setupProgramOptions();

public:
	void pushLoadOption(std::string_view type);
	void pushSaveOption(std::string_view type);
	void printHelp(std::ostream& out);

public:
	bool help() const noexcept { return m_options.help; }
	bool nocheck() const noexcept { return m_options.nocheck; }
	const std::string& loadFormat() const noexcept { return m_options.loadFormat; }
	const std::string& saveFormat() const noexcept { return m_options.saveFormat; }
	const input_type& inputFiles() const noexcept { return m_options.inputFiles; }
	const std::filesystem::path& outputFile() const noexcept { return m_options.outputFile; }
	double scale() const noexcept { return m_options.scale; }
	bool link() const noexcept { return m_options.link; }
	const std::set<std::string>& env() const noexcept { return m_options.env; }
	ConvType conv() const noexcept { return m_options.conv; }
	size_t compute() const noexcept { return m_options.computeFlags; }
	size_t batchSize() const noexcept { return m_options.batchSize; }


protected:
	static Format strtfmt_(std::string_view type);
	void loadFormat_(std::string_view type);
	void saveFormat_(std::string_view type);
	void setInputFiles_(const std::vector<std::string>& fnames);
	void setOutputFile_(std::string_view scenarioEnv);
	void setScale_(double scale);
	void setLink_(bool link);
	void addEnv_(const std::vector<std::string>& type);
	void setConv_(std::string_view conv);
	void addCompute_(std::string_view cmp);
	void setBatchSize_(size_t cmp);

private:
	static std::unique_ptr<Config> s_instance;
public:
	static Config* instance() noexcept { return s_instance.get(); }
	static void instanceCreate();
	static void instanceSetup(int argc, const char *const *argv);
protected:
	static void setInstance_(std::unique_ptr<Config>&& inst);

protected:
	boost::program_options::options_description m_help;
	boost::program_options::options_description m_convOpts;
	boost::program_options::positional_options_description m_positional;
	struct Options {
		bool help;
		bool nocheck;
		std::string loadFormat;
		std::string saveFormat;
		input_type inputFiles;
		std::filesystem::path outputFile;
		std::set<std::string> env;
		double scale;
		bool link;
		ConvType conv;
		size_t computeFlags;
		size_t batchSize;
		Options() : help(false), nocheck(false), loadFormat("Archive"), saveFormat("Archive"), scale(1), link(false), conv(ConvType::None), computeFlags(0), batchSize(0)
		{ }
	} m_options;
	std::set<std::string> m_loadOptions;
	std::set<std::string> m_saveOptions;
};

} // namespace rsapp::conf

#endif // RAYSCAN_EXT_CONV_CONFIG_HPP_INCLUDED
