#ifndef RSAPP_RUN_FWD_HPP_INCLUDED
#define RSAPP_RUN_FWD_HPP_INCLUDED

#include <rsapp/fwd.hpp>

namespace rsapp::run
{

class InstanceRunner;
class InstanceController;
template <typename RayScan>
class SingleTargetRunner;
template <typename RayScan>
class MultiTargetRunner;
template <typename RayScan>
class DynamicSingleTargetRunner;

}

#endif // RSAPP_RUN_FWD_HPP_INCLUDED
