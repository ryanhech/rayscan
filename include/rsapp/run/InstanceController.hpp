#ifndef RSAPP_RUN_INSTANCECONTROLLER_HPP_INCLUDED
#define RSAPP_RUN_INSTANCECONTROLLER_HPP_INCLUDED

#include "fwd.hpp"
#include <ostream>
#include <any>
#include <unordered_map>
#include <rayscanlib/utils/StatsTable.hpp>

namespace rsapp::run
{

class InstanceController
{
public:
	//using run_func = bool (const std::any&, size_t, std::ostream&);
	//using save_func = bool (const std::any&, std::ostream&);
	InstanceController();
	virtual ~InstanceController();

	bool runBenchmark(const std::any& benchmark, std::ostream& results);

	template <typename T, typename... Args>
	void emplace_instance(::rsapp::conf::ScenarioType type, Args&&... args)
	{
		emplace_instance_(type, std::make_unique<T>(std::forward<Args>(args)...));
	}

	template <typename T, typename... Args>
	void emplace_instance_auto(Args&&... args)
	{
		auto ir = std::make_unique<T>(std::forward<Args>(args)...);
		auto type = ir->scen();
		if (type == ::rsapp::conf::ScenarioType::SingleTarget) { // insert multi single target as well
			emplace_instance_(::rsapp::conf::ScenarioType::MultiSingleTarget, std::make_unique<T>(*ir));
		} else if (type == ::rsapp::conf::ScenarioType::DynamicSingleTarget) { // insert dynamic multi single target as well
			emplace_instance_(::rsapp::conf::ScenarioType::DynamicMultiSingleTarget, std::make_unique<T>(*ir));
		}
		emplace_instance_(type, std::move(ir));
	}

private:
	template <typename T>
	void emplace_instance_(::rsapp::conf::ScenarioType type, std::unique_ptr<T>&& uni)
	{
		auto add = m_runSuites.try_emplace(type, std::move(uni));
		if (!add.second)
			throw std::runtime_error("failed to add instance runner");
	}
	std::unordered_map<::rsapp::conf::ScenarioType, std::unique_ptr<InstanceRunner>> m_runSuites;
};

}

#endif // RSAPP_RUN_INSTANCECONTROLLER_HPP_INCLUDED
