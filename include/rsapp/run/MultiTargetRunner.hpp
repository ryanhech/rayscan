#ifndef RSAPP_RUN_MULTITARGETRUNNER_HPP_INCLUDED
#define RSAPP_RUN_MULTITARGETRUNNER_HPP_INCLUDED

#include "fwd.hpp"
#include "InstanceRunner.hpp"
#include <rsapp/conf/Config.hpp>
#include <rayscanlib/scen/StaticBenchmarkMultiTarget.hpp>
#include <vector>

namespace rsapp::run
{

template <typename RayScan>
class MultiTargetRunner : public virtual InstanceRunner
{
public:
	using benchmark_type = rayscanlib::scen::StaticBenchmarkMultiTarget;
	using benchmark_ptr = std::shared_ptr<benchmark_type>;

	::rsapp::conf::ScenarioType scen() noexcept override { return ::rsapp::conf::ScenarioType::MultiTarget; }

	bool run(const std::any& benchmark, std::ostream& results) override
	{
		benchmark_type& suite = *std::any_cast<benchmark_ptr>(benchmark);
		const auto& conf = ::rsapp::conf::Config::ref();
		size_t repeat = conf.repeat();
		return runMultiTarget(suite, repeat, results);
	}

protected:
	bool runMultiTarget(benchmark_type& multi, size_t repeat, std::ostream& results)
	{
		if constexpr (!RayScan::is_multi_target_search())
		{
			throw std::logic_error("not implemented");
		}
		else
		{
			using namespace std::literals::string_literals;
			if (repeat > 16)
				throw std::runtime_error("repeating too many times");
			size_t filterSize = multi.filter_size_multi();
			std::pmr::monotonic_buffer_resource statsPool;
			rayscanlib::utils::StatsTable table;
			std::vector<rayscanlib::utils::Stats> stats;
			stats.reserve(filterSize);
			{
				stats.emplace_back(&statsPool);
				RayScan rayscan;
				setupStatTable(rayscan, table, stats.front());
				setupStatTableMulti(table, stats.front());
				// setup all other stats
				for (size_t i = 1; i < filterSize; ++i) {
					stats.emplace_back(stats.front());
				}
				table.open(results);
			}
			bool correct = true;
			for (size_t j = 0; j < repeat; j++) {
				multi.resetInstance();
				RayScan rayscan;
				rayscanlib::scen::rayscan_environment(rayscan, multi);
				rayscan.setDynamicSearch(false);
				for (uint32 i = 0, ie = static_cast<uint32>(multi.size_multi()), fId = 0; i < ie; ++i) {
					bool success = true;
					auto& I = multi.multiInstances()[i];
					if (I.isFilteredOut())
						continue;
					auto& stat = stats.at(fId++);
					stat.clearOp();
					stat.at("vID"s) = I.validId;
					stat.at("fID"s) = I.filterId;
					stat.at("targets"s) = I.indexCount;
					auto sres = rayscanlib::scen::rayscan_search(rayscan, multi, i);
					rayscan.saveStats(stat);
					stat.at("dist"s) = sres.first.value_or(-1);
					stat.at("distExp"s) = sres.second.value_or(-1);
					if (!checkResults(sres.first, sres.second)) {
						success = false;
						correct = false;
					}
					if (!success)
						stat.at("correct") = 0;
					else if (j == 0) // set correct for first instance
						stat.at("correct") = 1;
				}
			}
			for (size_t i = 0; i < filterSize; ++i) {
				table.push(stats.at(i));
			}
			return correct;
		}
	}
};

}

#endif // RSAPP_RUN_SINGLETARGETRUNNER_HPP_INCLUDED
