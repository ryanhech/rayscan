#ifndef RSAPP_RUN_INSTANCERUNNER_HPP_INCLUDED
#define RSAPP_RUN_INSTANCERUNNER_HPP_INCLUDED

#include "fwd.hpp"
#include <vector>
#include <chrono>
#include <ostream>
#include <filesystem>
#include <any>
#include <unordered_map>
#include <rayscanlib/utils/StatsTable.hpp>

namespace rsapp::run
{

class InstanceRunner
{
public:
	virtual ~InstanceRunner() = default;
	virtual ::rsapp::conf::ScenarioType scen() noexcept = 0;
	virtual bool run(const std::any& benchmark[[maybe_unused]], std::ostream& results[[maybe_unused]]) { throw std::logic_error("not implemented"); }
	
	template <typename Alg>
	static void setupStatTable(Alg& alg, rayscanlib::utils::StatsTable& table, rayscanlib::utils::Stats& stats)
	{
		alg.setupStats(&table, &stats);
		setupStatTableSingle(table, stats);
	}
	
	static void setupStatTableSingle(rayscanlib::utils::StatsTable& table, rayscanlib::utils::Stats& stats)
	{
		using namespace std::literals::string_view_literals;
		table.emplace_column("vID"sv, 100);
		table.emplace_column("fID"sv, 200);
		table.emplace_column("dist"sv, 10000);
		table.emplace_column("distExp"sv, 10100);
		table.emplace_column("correct"sv, 10200);
		stats.template try_emplace<int64>("vID"sv, &rayscanlib::utils::statop_ign_clear);
		stats.template try_emplace<int64>("fID"sv, &rayscanlib::utils::statop_ign_clear);
		stats.template try_emplace<double>("dist"sv, &rayscanlib::utils::statop_ign_clear);
		stats.template try_emplace<double>("distExp"sv, &rayscanlib::utils::statop_ign_clear);
		stats.template try_emplace<int64>("correct"sv, &rayscanlib::utils::statop_ign_clear);
	}

	static void setupStatTableMulti(rayscanlib::utils::StatsTable& table, rayscanlib::utils::Stats& stats)
	{
		using namespace std::literals::string_view_literals;
		table.emplace_column("targets"sv, 300);
		stats.template try_emplace<int64>("targets"sv, &rayscanlib::utils::statop_ign_clear);
	}

	template <typename T>
	bool checkResults(const T& ans, const T& exp)
	{
		return exp.has_value() ? (ans.has_value() && std::abs(*ans - *exp) < 1e-4) : true;
	}
};

}

#endif // RSAPP_RUN_INSTANCERUNNER_HPP_INCLUDED
