#ifndef RSAPP_CONF_CONFIG_HPP_INCLUDED
#define RSAPP_CONF_CONFIG_HPP_INCLUDED

#include "fwd.hpp"
#include <rsapp/scen/Scenario.hpp>
#include <rsapp/run/InstanceController.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/positional_options.hpp>
#include <filesystem>
#include <fstream>
#include <unordered_map>
#include <functional>
#include <optional>

namespace rsapp::conf
{

class Config
{
public:
	Config();
	Config(const Config&) = delete;
	Config(Config&&) = delete;
	virtual ~Config();

protected:
	virtual void initConfig(int argc, const char *const * argv);
	virtual void setupProgramOptions();

public:
	bool help() const noexcept { return m_options.help; }
	ScenarioType type() const noexcept { return m_options.type; }
	const std::filesystem::path& scenario() const noexcept { return m_options.scenario; }
	const std::filesystem::path& saveScenario() const noexcept { return m_options.saveScenario; }
	const std::string& scenarioType() const noexcept { return m_options.scenarioType; }
	const std::string& rayscan() const noexcept { return m_options.rayscan; }
	const std::string& filter() const noexcept { return m_options.filter; }
	const std::size_t& repeat() const noexcept { return m_options.repeat; }
	std::ostream& output() const noexcept;
	std::optional<std::string_view> param(const std::string& p) const noexcept;

protected:
	void type_(const std::string& type);
	void scenario_(const std::string& scenario);
	void saveScenario_(const std::string& scenarioEnv);
	void scenarioType_(const std::string& scenarioType);
	void rayscan_(const std::string& rayscan);
	void filter_(const std::string& filter);
	void repeat_(const std::string& repeat);
	void output_(const std::string& output);
	void param_(const std::string& param);

public:
	void registerScenario(ScenarioType type, std::string_view name, const std::function<std::unique_ptr<scen::Scenario>()>& generator);
	std::unique_ptr<scen::Scenario> generateScenario(ScenarioType type, std::string_view name);
	std::unique_ptr<scen::Scenario> generateScenario(std::string_view name);
	std::unique_ptr<scen::Scenario> generateScenario();
	void registerInstance(std::string_view name, const std::function<std::unique_ptr<run::InstanceController>()>& generator);
	std::unique_ptr<run::InstanceController> generateInstance(std::string_view name);
	std::unique_ptr<run::InstanceController> generateInstance();
protected:
	std::string scenJoin(ScenarioType type, std::string_view name);

	static std::unique_ptr<Config> s_instance;
public:
	static Config* instance() noexcept { return s_instance.get(); }
	static Config& ref() { if (!s_instance) throw std::logic_error("config not loaded"); return *s_instance; }
	static void instanceCreate();
	static void instanceSetup(int argc, const char *const *argv);
protected:
	static void setInstance_(std::unique_ptr<Config>&& inst);

protected:
	boost::program_options::options_description m_help;
	boost::program_options::options_description m_scenarios;
	boost::program_options::positional_options_description m_positional;
	std::ofstream m_outputFile;
	struct Options {
		bool help;
		ScenarioType type;
		std::filesystem::path scenario;
		std::filesystem::path saveScenario;
		std::string scenarioType;
		std::string rayscan;
		std::string filter;
		std::size_t repeat;
		std::ostream* output;
		std::unordered_map<std::string, std::string> param;
		Options() : help(false), type(ScenarioType::SingleTarget), repeat(1), output(nullptr)
		{ }
	} m_options;

	std::unordered_map<std::string, std::function<std::unique_ptr<scen::Scenario>()>> m_registeredScenarios;
	std::unordered_map<std::string, std::function<std::unique_ptr<run::InstanceController>()>> m_registeredInstances;
};

} // namespace rsapp::conf

#endif // RSAPP_CONF_CONFIG_HPP_INCLUDED
