#ifndef RSAPP_CONF_REGISTER_HPP_INCLUDED
#define RSAPP_CONF_REGISTER_HPP_INCLUDED

#include "Config.hpp"
#include <rayscanlib/search/fwd.hpp>
#include <rsapp/run/SingleTargetRunner.hpp>
#include <rsapp/run/MultiTargetRunner.hpp>
#include <rsapp/run/DynamicSingleTargetRunner.hpp>
#include <rsapp/run/DynamicMultiTargetRunner.hpp>

namespace rsapp::conf
{

namespace reg
{
template <typename T>
struct ST
{
	using type = run::SingleTargetRunner<T>;
};
template <typename T>
struct MT
{
	using type = run::MultiTargetRunner<T>;
};
template <typename T>
struct DST
{
	using type = run::DynamicSingleTargetRunner<T>;
};
template <typename T>
struct DMT
{
	using type = run::DynamicMultiTargetRunner<T>;
};
}

class Register
{
public:
	template <typename Scen>
	void registerScenario(rsapp::conf::ScenarioType type, std::string_view name);

	template <typename... Tags>
	void registerInstance(std::string_view name);

	virtual void registerConfig() = 0;
};

template <typename Tag>
struct RegisterInstanceType;

template <typename Scen>
inline void Register::registerScenario(rsapp::conf::ScenarioType type, std::string_view name)
{
	rsapp::conf::Config::instance()->registerScenario(type, name, []() {
		return std::make_unique<Scen>();
	});
}

template <typename... Tags>
inline void Register::registerInstance(std::string_view name)
{
	rsapp::conf::Config::instance()->registerInstance(name, []() {
		auto inst = std::make_unique<rsapp::run::InstanceController>();
		(inst->emplace_instance_auto<typename Tags::type>(), ...);
		return inst;
	});
}

} // namespace rsapp::conf

#endif // RSAPP_CONF_REGISTER_HPP_INCLUDED
