#ifndef RSAPP_CONF_FWD_HPP_INCLUDED
#define RSAPP_CONF_FWD_HPP_INCLUDED

#include <rsapp/fwd.hpp>

namespace rsapp::conf
{

enum class ScenarioType
{
	SingleTarget,
	MultiSingleTarget,
	MultiTarget,
	DynamicSingleTarget,
	DynamicMultiTarget,
	DynamicMultiSingleTarget,
};

class Config;

}

#endif // RSAPP_CONF_FWD_HPP_INCLUDED
