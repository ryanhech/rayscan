#ifndef RSAPP_SCEN_DYNAMICSINGLETARGET_HPP_INCLUDED
#define RSAPP_SCEN_DYNAMICSINGLETARGET_HPP_INCLUDED

#include "fwd.hpp"
#include "Scenario.hpp"
#include <rayscanlib/scen/DynamicBenchmarkSingleTarget.hpp>
#include <rayscanlib/scen/DynamicBenchmarkMultiTarget.hpp>

namespace rsapp::scen
{

class DynamicSingleTarget : public Scenario
{
public:
	using benchmark_type = rayscanlib::scen::DynamicBenchmarkSingleTarget;
	using benchmark_ptr = std::shared_ptr<benchmark_type>;
	using benchmark_multi_type = rayscanlib::scen::DynamicBenchmarkMultiTarget;
	using benchmark_multi_ptr = std::shared_ptr<benchmark_multi_type>;
	void load(const archiver& archive) override;
	bool run(run::InstanceController& inst, std::ostream& results) override;
	void saveFiltered(std::ostream& scenario) override;

	const benchmark_ptr& benchmark() const noexcept { return m_benchmark; }
	const benchmark_multi_ptr& benchmarkMulti() const noexcept { return m_benchmarkMulti; }

protected:
	benchmark_ptr m_benchmark;
	benchmark_multi_ptr m_benchmarkMulti;
};

}

#endif // RSAPP_SCEN_DYNAMICSINGLETARGET_HPP_INCLUDED
