#ifndef RSAPP_SCEN_FWD_HPP_INCLUDED
#define RSAPP_SCEN_FWD_HPP_INCLUDED

#include <rsapp/fwd.hpp>

namespace rsapp::scen
{

class Scenario;
class StaticSingleTarget;
class StaticMultiSingleTarget;
class StaticMultiTarget;
class DynamicSingleTarget;

}

#endif // RSAPP_SCEN_FWD_HPP_INCLUDED
