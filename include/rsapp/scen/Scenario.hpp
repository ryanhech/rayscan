#ifndef RSAPP_SCEN_SCENARIO_HPP_INCLUDED
#define RSAPP_SCEN_SCENARIO_HPP_INCLUDED

#include "fwd.hpp"
#include <rsapp/run/fwd.hpp>
#include <rayscanlib/scen/Archiver.hpp>
#include <filesystem>
#include <istream>
#include <ostream>
#include <optional>

namespace rsapp::scen
{

class Scenario
{
public:
	using archiver = ::rayscanlib::scen::Archiver;
	Scenario() : m_repeat(1)
	{ }
	virtual ~Scenario() = default;

	void load();
	void load(const std::filesystem::path& filename);
	void load(std::istream& scenario, const std::filesystem::path& filename);
	virtual void load(const archiver& archive) = 0;
	void saveFiltered(const std::filesystem::path& scenario);
	virtual void saveFiltered(std::ostream& scenario[[maybe_unused]]) { throw std::logic_error("not implemented"); }
	virtual bool run(run::InstanceController& inst, std::ostream& results) = 0;
	virtual void filter(std::string_view F);
	virtual void clearFilter();
	virtual void repeat(size_t rep);

protected:
	std::optional<std::string> m_filter;
	size_t m_repeat;
};

}

#endif // RSAPP_SCEN_SCENARIO_HPP_INCLUDED
