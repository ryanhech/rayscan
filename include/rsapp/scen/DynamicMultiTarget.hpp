#ifndef RSAPP_SCEN_DYNAMICMUTLITARGET_HPP_INCLUDED
#define RSAPP_SCEN_DYNAMICMUTLITARGET_HPP_INCLUDED

#include "fwd.hpp"
#include "Scenario.hpp"
#include <rayscanlib/scen/DynamicBenchmarkMultiTarget.hpp>

namespace rsapp::scen
{

class DynamicMultiTarget : public Scenario
{
public:
	using benchmark_type = rayscanlib::scen::DynamicBenchmarkMultiTarget;
	using benchmark_ptr = std::shared_ptr<benchmark_type>;
	void load(const archiver& archive) override;
	bool run(run::InstanceController& inst, std::ostream& results) override;
	void saveFiltered(std::ostream& scenario) override;

	const benchmark_ptr& benchmark() const noexcept { return m_benchmark; }

protected:
	benchmark_ptr m_benchmark;
};

}

#endif // RSAPP_SCEN_DYNAMICMUTLITARGET_HPP_INCLUDED
