# Fade 2D

Fade2D is used to perform Constrainded Delaunary Triangulation for the
generation of a navigation mesh.

Download fade2d from their website [https://www.geom.at/products/fade2d/](https://www.geom.at/products/fade2d/).

Installing this lib will enable generation of meshes from obstacle sets; however,
fade2d will display a message to stdout so using pipe with the converter ultility
is not reccomended, use `-o`.

## Install

Add the library `libfade2d.so` (for linux) for your relative distro (or ubuntu if not listed) into
the subdirectory `bin`.

Copy the `include_fade2d` into the `include` subdirectory, and rename to `fade2d`.
