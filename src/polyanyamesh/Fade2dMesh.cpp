#include <polyanyamesh/Fade2dMesh.hpp>
#include <fade2d/Fade_2D.h>
#include <rayscanlib/geometry/AngledSector.hpp>
#include <boost/range/irange.hpp>

//#define DEBUG_POLYANYA

namespace polyanyalib
{

void Fade2dMesh::loadEnvironment(const environment& env)
{
	namespace fade = GEOM_FADE2D;
	clear(false);
	fade::Fade_2D fade2d;
	size_t pcount = env.countVertices();
	// load all points into array
	auto points = std::make_unique<double[]>(2*pcount);

	{
		size_t i = 0;
		for (auto p : env.getEnclosure()) {
			points[(i<<1)] = p.x;
			points[(i<<1)|1] = p.y;
			i += 1;
		}
		for (auto poly : env.getObstacles()) {
			for (auto p : poly) {
				points[(i<<1)] = p.x;
				points[(i<<1)|1] = p.y;
				i += 1;
			}
		}
	}
	auto phandle = std::vector<fade::Point2*>(pcount);
	fade2d.insert(static_cast<int>(pcount), points.get(), phandle.data());

	// setup constraints
	using rayscanlib::Dir;
	std::vector<fade::Segment2> segments;
	segments.reserve(pcount+8);
	{
		size_t i = 0;
		auto poly = &env.getEnclosure();
		size_t psize = poly->size();
		fade::Point2* pv = phandle[i+psize-1];
		fade::Point2* at;
		for (size_t j = 0; j < psize; ++j) {
			at = phandle[i];
			segments.emplace_back(*pv, *at);
			pv = at;
			i += 1;
		}
		for (auto xpoly : env.getObstacles()) {
			poly = &xpoly;
			psize = poly->size();
			pv = phandle[i+psize-1];
			for (size_t j = 0; j < psize; ++j) {
				at = phandle[i];
				segments.emplace_back(*pv, *at);
				pv = at;
				i += 1;
			}
		}
	}
	fade2d.createConstraint(segments, fade::CIS_CONSTRAINED_DELAUNAY);
	fade2d.applyConstraintsAndZones();

	// create the mesh structure
	// load the new points, as overlapping segments would of inserted additional points
	phandle.clear();
	fade2d.getVertexPointers(phandle);
	pcount = static_cast<int>(phandle.size());
	std::pmr::monotonic_buffer_resource mbr;
	std::pmr::unordered_map<fade::Point2*, int> pointMap(3*pcount/2, &mbr);
	mesh_vertices.resize(pcount+1);
	for (int i = 0; i < static_cast<int>(pcount); ++i) {
		auto& v = mesh_vertices[i+1];
		auto* h = phandle[i];
		v.id = i+1;
		v.p = Point(h->x(), h->y());
		v.polygons.reserve(8);
		pointMap.emplace(h, v.id);
	}
	// mesh_vertices already allocated points, continue with triangles
	std::vector<fade::Triangle2*> triangles;
	fade2d.getTrianglePointers(triangles);
	mesh_polygons.reserve(triangles.size()+129);
	mesh_polygons.emplace_back(); // poly 0 is blank for id purposes
	// rayscan::geometry::Box<double> bounds;
	// setup points
	for (auto* tri : triangles) {
		auto& face = mesh_polygons.emplace_back();
		face.id = static_cast<int>(mesh_polygons.size()-1);
		face.vertices.resize(3);
		for (int i = 0; i < 3; ++i) {
			face.vertices[i] = pointMap.at(tri->getCorner(i));
		}
	}

	// reserve mesh
	mesh_euclidean.reserve(128);
	mesh_euclidean.emplace_back();

	this->max_poly_sides = 3;
	autoTraversable(env);
	autoConnect();
	autoVertexSetup();
	autoCalcBounds();

	assert(isMeshValid());
}

}
