#include <rayscanlib/utils/StatsTable.hpp>
#include <rayscanlib/utils/Stats.hpp>
#include <algorithm>

namespace rayscanlib::utils
{

StatsTable::~StatsTable()
{
	close();
}

void StatsTable::emplace_column(std::string&& col, int64 order)
{
	if (getStream() == nullptr) {
		m_order.emplace(std::move(col), std::pair(order, m_order.size()));
	}
}

void StatsTable::emplace_column(std::string_view col, int64 order)
{
	emplace_column(std::string(col.begin(), col.end()), order);
}

void StatsTable::open(std::string_view filename)
{
	open(std::filesystem::path(filename));
}
void StatsTable::open(const std::filesystem::path& filename)
{
	if (getStream() == nullptr) {
		auto& stream = m_stream.emplace<1>(filename);
		if (!stream.is_open() || !stream.good())
			m_stream.emplace<0>(nullptr);
		else
			initColumns();
	}
}
void StatsTable::open(std::ostream& stream)
{
	if (getStream() == nullptr) {
		if (stream.good()) {
			m_stream.emplace<0>(&stream);
			initColumns();
		}
	}
}

void StatsTable::initColumns()
{
	if (auto* stream = getStream(); stream != nullptr) {
		m_columns.clear();
		m_columns.reserve(m_order.size());
		for (auto it = m_order.cbegin(), ite = m_order.cend(); it != ite; ++it) {
			m_columns.push_back(it);
		}
		std::sort(m_columns.begin(), m_columns.end(), [](auto& a, auto& b) noexcept { return a->second < b->second; });
		for (size_t i = 0, ie = m_columns.size()-1; i <= ie; ++i) {
			*stream << m_columns[i]->first << (i != ie ? '\t' : '\n');
		}
	}
}

void StatsTable::push(const Stats& stats)
{
	struct StreamPush
	{
		std::ostream* stream;
		void operator()(int64 a) { *stream << a; }
		void operator()(std::chrono::nanoseconds a) { *stream << a.count(); }
		void operator()(double a)
		{
			if (a < 1e-6 || a > 1e6)
				*stream << std::scientific << std::setprecision(12) << a;
			else
				*stream << std::fixed << std::setprecision(12) << a;
		}
	};
	if (StreamPush s{getStream()}; s.stream != nullptr) {
		for (size_t i = 0, ie = m_columns.size()-1; i <= ie; ++i) {
			const auto& stat = stats.at(m_columns[i]->first);
			std::visit(s, stat.value);
			*s.stream << (i != ie ? '\t' : '\n');
		}
	}
}

void StatsTable::close() noexcept
{
	m_stream.emplace<0>(nullptr);
}

void StatsTable::flush()
{
	if (auto* stream = getStream(); stream != nullptr) {
		stream->flush();
	}
}

std::ostream* StatsTable::getStream()
{
	switch (m_stream.index()) {
	case 0:
		return std::get<0>(m_stream);
	case 1:
		return &std::get<1>(m_stream);
	default:
		assert(false);
		return nullptr;
	}
}

} // namespace rayscanlib::utils
