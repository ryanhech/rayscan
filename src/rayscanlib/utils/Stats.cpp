#include <rayscanlib/utils/Stats.hpp>

namespace rayscanlib::utils
{

void statop_generic(StatValue& stat, const StatValue::value_type& val, StatValue::Operator op)
{
	switch (op)
	{
	case StatValue::OpAssign:
		if (!stat.assigned) {
			stat.assigned = true;
			std::visit([&val] (auto& statVal) {
				statVal = std::get< std::decay_t<decltype(statVal)> >(val);
			}, stat.value);
		}
		break;
	case StatValue::OpPlus:
		stat.assigned = true;
		std::visit([&val] (auto& statVal) {
			statVal += std::get< std::decay_t<decltype(statVal)> >(val);
		}, stat.value);
		break;
	case StatValue::OpMinus:
		stat.assigned = true;
		std::visit([&val] (auto& statVal) {
			statVal -= std::get< std::decay_t<decltype(statVal)> >(val);
		}, stat.value);
		break;
	case StatValue::OpClear:
	case StatValue::OpReset:
		stat.assigned = false;
		std::visit([&val] (auto& statVal) {
			statVal = std::decay_t<decltype(statVal)>{};
		}, stat.value);
		break;
	}
}

void statop_ign_clear(StatValue& stat, const StatValue::value_type& val, StatValue::Operator op)
{
	switch (op)
	{
	case StatValue::OpAssign:
	case StatValue::OpReset:
	case StatValue::OpPlus:
	case StatValue::OpMinus:
		statop_generic(stat, val, op);
	default:
		break;
	}
}

void statop_ign_clear_ign_reset(StatValue& stat, const StatValue::value_type& val, StatValue::Operator op)
{
	switch (op)
	{
	case StatValue::OpAssign:
	case StatValue::OpPlus:
	case StatValue::OpMinus:
		statop_generic(stat, val, op);
	default:
		break;
	}
}

void statop_single_op::operator()(StatValue& stat, const StatValue::value_type& val, StatValue::Operator op)
{
	switch (op)
	{
	case StatValue::OpAssign:
	case StatValue::OpPlus:
	case StatValue::OpMinus:
		if (!stat.assigned)
			statop(stat, val, op);
		break;
	default:
		statop(stat, val, op);
		break;
	}
}

Stats& Stats::operator=(const Stats& rhs)
{
	match(rhs, [](mapped_type& lhs, const mapped_type& rhs) { lhs = rhs.value; });
	return *this;
}

Stats& Stats::operator+=(const Stats& rhs)
{
	match(rhs, [](mapped_type& lhs, const mapped_type& rhs) { lhs += rhs.value; });
	return *this;
}

Stats& Stats::operator-=(const Stats& rhs)
{
	match(rhs, [](mapped_type& lhs, const mapped_type& rhs) { lhs -= rhs.value; });
	return *this;
}

void Stats::clearOp()
{
	for (auto& stat : m_stats) {
		stat.second.clear();
	}
}

void Stats::resetOp()
{
	for (auto& stat : m_stats) {
		stat.second.reset();
	}
}


} // namespace rayscanlib::utils
