#include <rayscanlib/utils/BresRay.hpp>
#include <rayscanlib/geometry/BresenhamLine.hpp>
#include <rayscanlib/geometry/util/RayScanGeometry.hpp>

namespace rayscanlib::utils
{
	
void BresRay::setBox(const box& b)
{
	auto width = b.width();
	auto height = b.height();
	m_rasterEnv.setup(width+2*BRES_PADDING, height+2*BRES_PADDING);
	m_lowerBound = b.lower();
	m_lowerBound -= point(BRES_PADDING, BRES_PADDING);
	m_rasterData.clear();
	m_dataPool.clear();
	m_rasterData.resize(height + 2*BRES_PADDING, std::vector<data_set>(width + 2*BRES_PADDING));
	m_rayId = 0;
}

void BresRay::addObstacleData(Data& dat)
{
	geometry::BresenhamLine<coord, !SEALED_RAY, true> line(dat.at, dat.nx);
	line.iterate([this,&dat] (point pt, const auto& len) {
		get_fill(pt.x, pt.y).emplace_back(&dat);
		return len.a < len.b;
	});
}

void BresRay::removeObstacleData(Data& dat)
{
	geometry::BresenhamLine<coord, !SEALED_RAY, true> line(dat.at, dat.nx);
	line.iterate([this,&dat] (point pt, const auto& len) {
		auto& e = get_fill(pt.x, pt.y);
		e.erase(std::remove(e.begin(), e.end(), &dat), e.end());
		return len.a < len.b;
	});
}

auto BresRay::shootRay(point a, point ab) -> std::pair<Data*, geometry::Intersect<coord>>
{
	m_rayId += 1;
	a -= m_lowerBound;
	std::pair<Data*, geometry::Intersect<coord>> ans(nullptr, geometry::Intersect<coord>(0, 1, 1));
	int overshoot = std::numeric_limits<int>::max();

	// do line-scan
	geometry::BresenhamLine<coord, SEALED_RAY, true> line(a, ab);
	line.iterate([this,&ans,&overshoot,a,ab,cid=m_rayId] (point pt, const auto& len) {
		if (test(pt.x, pt.y)) {
			auto& segs = get(pt.x, pt.y);
			for (auto* seg : segs) {
				if (seg->lastCheck == cid)
					continue;
				seg->lastCheck = cid;
				geometry::Intersect inst = geometry::util::intersect_corner_segment<false>(a, ab, seg->at, seg->nx, seg->pv);
				if (!inst.colin()) {
					if (inst.a >= 0 && inst.scaleA() <= ans.second.scaleA()) {
						overshoot = std::min(overshoot, static_cast<int>(
								( static_cast<int64>(inst.a) * len.b
								- static_cast<int64>(len.a) * inst.scale ) / inst.scale + 2));
						ans = {seg, inst};
					}
				}
			}
		}
		return --overshoot >= 0;
	});

	return ans;
}
	
void BresDblRay::setBox(const box& borig)
{
	box b = borig;
	b.first = m_iscale * b.first;
	b.second = m_iscale * b.second;
	uint32 width = static_cast<uint32>(std::ceil(b.width())) + 2*Padding;
	uint32 height = static_cast<uint32>(std::ceil(b.height())) + 2*Padding;
	m_grid.setup(width, height);
	m_lowerBound = b.lower() - point(Padding, Padding);
#ifndef BRES_DISABLE_VECTOR_TABLE
	m_gridData.resize(width, height);
#else
	m_gridData.clear();
	m_gridData.resize(width*height);
#endif
	m_dataPool.clear();
	m_rayId = 0;
}

void BresDblRay::addObstacleData(Data& dat)
{
	rayscanlib::geometry::BresenhamDblLine line;
	line.setup(dat.at, dat.nx, -1);
	line.prog.b += 1;
	details::bres_ray_loop<true>(line, m_grid, m_gridData, [&dat,&tab=m_gridData] (auto& vec) {
		if (auto ed = vec.end(); std::find(vec.begin(), ed, &dat) == ed)
#ifndef BRES_DISABLE_VECTOR_TABLE
			tab.emplace_back(vec, &dat);
#else
			vec.emplace_back(&dat);
#endif
	});
}

void BresDblRay::removeObstacleData(Data& dat)
{
	rayscanlib::geometry::BresenhamDblLine line;
	line.setup(dat.at, dat.nx, -2);
	line.prog.b += 1;
	details::bres_ray_loop<true>(line, m_grid, m_gridData, [&dat,&tab=m_gridData] (auto& vec) {
		if (auto ed = vec.end(), it = std::find(vec.begin(), ed, &dat); it != ed)
#ifndef BRES_DISABLE_VECTOR_TABLE
			tab.erase_unordered(vec, it);
#else
			vec.erase(it);
#endif
	});
}

auto BresDblRay::shootRay(point a, point ab) -> std::pair<Data*, std::pair<double, double>>
{
#ifdef BRES_MAP_OUTPUT_FILE
	static bool writeOut = false;
	if (!writeOut) {
		writeOut = true;
		details::BresOutput(m_grid, BRES_MAP_OUTPUT_FILE);
	}
#endif
	m_rayId += 1;
	a = m_iscale * a; ab = m_iscale * ab;
	a -= m_lowerBound;
	std::pair<Data*, std::pair<double, double>> ans(nullptr, {inf<double>, inf<double>});

	// do line-scan
	rayscanlib::geometry::BresenhamDblLine line;
	line.setup(a, ab, -1);
	line.make_ray();
	details::bres_ray_loop<false>(line, m_grid, m_gridData, [&ans,&line,cid=m_rayId,a,ab] (auto& vec) {
		assert(!vec.empty());
		for (auto* seg : vec) {
			if (seg->lastCheck == cid)
				continue;
			seg->lastCheck = cid;
			auto [d1,d2] = geometry::util::intersect_dbl_corner_segment(a, ab, seg->at, seg->nx, seg->pv);
			if (d1 != -1) {
				assert(d1 > -1e-7);
				if (d1 < ans.second.first) {
					ans = std::make_pair(seg, std::make_pair(d1, d2));
					auto pt = a + d1*ab;
					line.prog.b = static_cast<int32>(std::abs(pt[line.axis] - a[line.axis])) + 4;
				}
			}
		}
	}
#ifdef BRES_RAY_OUTPUT_FILE
#ifdef BRES_RAY_ID
	,m_rayId == BRES_RAY_ID
#endif
#endif
	);

	return ans;
}


} // namespace rayscanlib::utils
