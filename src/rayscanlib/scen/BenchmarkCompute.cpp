#include <rayscanlib/scen/BenchmarkCompute.hpp>
#include <rayscanlib/scen/StaticBenchmarkSingleTarget.hpp>
#include <rayscanlib/scen/StaticBenchmarkMultiTarget.hpp>
#include <rayscanlib/scen/DynamicBenchmarkSingleTarget.hpp>
#include <rayscanlib/scen/DynamicBenchmarkMultiTarget.hpp>
// rayscan types
#include <rayscanlib/scen/DynamicBenchmarkSingleTarget.hpp>
#include <rayscanlib/search/RayScanSingleTarget.hpp>
#include <rayscanlib/search/expansion/BasicExpansion.hpp>
#include <rayscanlib/search/scan/BasicScanner.hpp>
#include <rayscanlib/search/heuristic/EuclideanHeuristic.hpp>
#include <rayscanlib/search/raycast/BresenhamRayCast.hpp>

namespace rayscanlib::scen
{

namespace {
using namespace rayscanlib::search::modules;
using rayscan_type = rayscanlib::search::RayScanSingleTarget<false,
	int16,
	EuclideanHeuristicModule,
	BresenhamRayCastModule<>,
	BasicStartExpansionModule, BasicExpansionModule, BasicScannerModule<0>
>;
}

bool benchmarkComputeDistancesAuto(StaticBenchmarkSingleTarget& suite)
{
	bool success = false;
	if (typeid(suite) == typeid(StaticBenchmarkSingleTarget)) {
		benchmarkComputeDistances(suite);
		success = true;
	} else if (typeid(suite) == typeid(StaticBenchmarkMultiTarget)) {
		benchmarkComputeDistances(dynamic_cast<StaticBenchmarkMultiTarget&>(suite));
		success = true;
	} else if (typeid(suite) == typeid(DynamicBenchmarkSingleTarget)) {
		benchmarkComputeDistances(dynamic_cast<DynamicBenchmarkSingleTarget&>(suite));
		success = true;
	} else if (typeid(suite) == typeid(DynamicBenchmarkMultiTarget)) {
		benchmarkComputeDistances(dynamic_cast<DynamicBenchmarkMultiTarget&>(suite));
		success = true;
	}
	return success;
}

void benchmarkComputeDistances(StaticBenchmarkSingleTarget& suite)
{
	if (!suite.getEnvironment())
		throw std::runtime_error("invalid environment");
	if (!suite.getEnvironment()->isInteger()) // integer not supported
		throw std::runtime_error("not integer environment");
	suite.resetInstance();
	rayscan_type rayscan;
	rayscan_environment(rayscan, suite);
	auto& inst = suite.instancesMutable();

	for (auto& I : inst) {
		I.distance = rayscan.search(static_cast<rayscan_type::point>(I.start), static_cast<rayscan_type::point>(I.target));
	}
}

void benchmarkComputeDistances(StaticBenchmarkMultiTarget& suite)
{
	if (!suite.getEnvironment())
		throw std::runtime_error("invalid environment");
	if (!suite.getEnvironment()->isInteger()) // integer not supported
		throw std::runtime_error("not integer environment");

	benchmarkComputeDistances(dynamic_cast<StaticBenchmarkSingleTarget&>(suite));
}

void benchmarkComputeDistances(DynamicBenchmarkSingleTarget& suite)
{
	if (!suite.getEnvironment())
		throw std::runtime_error("invalid environment");
	if (!suite.getEnvironment()->isInteger()) // integer not supported
		throw std::runtime_error("not integer environment");
	
	// compute static distances
	benchmarkComputeDistances(dynamic_cast<StaticBenchmarkSingleTarget&>(suite));

	// compute dynamic distances
	suite.resetInstance();
	rayscan_type rayscan;
	rayscan_environment(rayscan, suite);
	rayscan.setDynamicSearch(true);

	for (size_t i = 0, ie = suite.size(); i < ie; ++i) {
		suite.selectInstance(i);
		auto& I = suite.currentInstance();
		rayscan_apply_dynamic_changes(rayscan, suite);
		std::optional<double> ans = rayscan.search(static_cast<rayscan_type::point>(I.start), static_cast<rayscan_type::point>(I.target));
		I.dynamic = ans;
	}
}

void benchmarkComputeDistances(DynamicBenchmarkMultiTarget& suite)
{
	if (!suite.getEnvironment())
		throw std::runtime_error("invalid environment");
	if (!suite.getEnvironment()->isInteger()) // integer not supported
		throw std::runtime_error("not integer environment");

	benchmarkComputeDistances(dynamic_cast<DynamicBenchmarkSingleTarget&>(suite));
}

} // namespace rayscanlib::scen
