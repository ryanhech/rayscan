#include <rayscanlib/scen/StaticBenchmarkMultiTarget.hpp>
#include <boost/iostreams/stream.hpp>
#include <inx/io/transformers.hpp>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <inx/functions.hpp>

namespace rayscanlib::scen
{

void StaticBenchmarkMultiTarget::clear()
{
	StaticBenchmarkSingleTarget::clear();
	clear_();
}
void StaticBenchmarkMultiTarget::clear_()
{
	m_multiInstance.clear();
	m_multiInstance.shrink_to_fit();
	m_multiInstanceAt = -1;
}

void StaticBenchmarkMultiTarget::selectMultiInstance(size_t i)
{
	if (i > size_multi())
		throw std::out_of_range("i");
	m_multiInstanceAt = static_cast<int32>(i);
	if (!m_selectInstanceOther) {
		m_selectInstanceOther = true;
		inx::destruct_adaptor fin([&x=m_selectInstanceOther] () noexcept { x = false; });
		selectInstance(m_multiInstance[m_multiInstanceAt].indexBegin);
	}
}

void StaticBenchmarkMultiTarget::selectInstance(size_t i)
{
	StaticBenchmarkSingleTarget::selectInstance(i);
	if (!m_selectInstanceOther) {
		m_selectInstanceOther = true;
		inx::destruct_adaptor fin([&x=m_selectInstanceOther] () noexcept { x = false; });
		selectMultiInstance(singleToMultiId(i));
	}
}

void StaticBenchmarkMultiTarget::resetInstance()
{
	StaticBenchmarkSingleTarget::resetInstance();
	m_multiInstanceAt = -1;
}

bool StaticBenchmarkMultiTarget::isValidScenario(environment::point start, environment::point target)
{
	if (m_multiInstanceAt != static_cast<int32>(size_multi()))
		return false;
	return StaticBenchmarkSingleTarget::isValidScenario(start, target);
}

bool StaticBenchmarkMultiTarget::addInstance(environment::point start, environment::point target)
{
	// check if new multi instance shuold be added
	if (!isValidScenario(start, target))
		return false;
	if (m_multiInstance.empty() && m_multiInstance.back().start != start) {
		if (!addMultiInstance(start))
			return false;
	}
	// check if target is already added, create new instance if so
	{
		auto& mi = m_multiInstance.back();
		if (target == mi.start || std::any_of(m_instances.begin() + mi.indexBegin, m_instances.begin() + (mi.indexBegin+mi.indexCount),
			[target](const Instance& inst) { return target == inst.target; })) {
			if (!addMultiInstance(start))
				return false;
		}
	}
	// add instance
	StaticBenchmarkSingleTarget::addInstance_(start, target);
	m_multiInstance.back().indexCount += 1;
	return true;
}

bool StaticBenchmarkMultiTarget::addMultiInstance(environment::point start)
{
	if (m_multiInstanceAt != static_cast<int32>(size_multi()))
		return false;
	if (!m_multiInstance.empty() && m_multiInstance.back().indexCount == 0)
		m_multiInstance.pop_back();
	auto& mi = m_multiInstance.emplace_back();
	mi.validId = size_multi()-1;
	mi.filterId = mi.validId;
	mi.start = start;
	mi.indexBegin = size();
	m_multiInstanceAt = size_multi();
	return true;
}

bool StaticBenchmarkMultiTarget::addTarget(environment::point target)
{
	if (m_multiInstance.empty())
		return false;
	auto& mi = m_multiInstance.back();
	if (target == mi.start || std::any_of(m_instances.begin() + mi.indexBegin, m_instances.begin() + (mi.indexBegin+mi.indexCount),
		[target](const Instance& inst) { return target == inst.target; }))
		return false;
	return addInstance(m_multiInstance.back().start, target);
}

size_t StaticBenchmarkMultiTarget::singleToMultiId(size_t i)
{
	if (i > size())
		throw std::out_of_range("i");
	return std::partition_point(m_multiInstance.begin(), m_multiInstance.end(), [x=static_cast<uint32>(i)] (const MultiInstance& a) {
		return a.indexBegin < x;
	}) - m_multiInstance.begin();
}

void StaticBenchmarkMultiTarget::applyFilter(const std::vector<bool>& filter)
{
	if (filter.size() != size_multi())
		throw std::runtime_error("invalid number of instances to filter");
	int32 filterId = 0;
	for (uint32 i = 0, ie = static_cast<uint32>(m_multiInstance.size()); i < ie; ++i) {
		auto& mi = m_multiInstance[i];
		mi.filterId = filter[i] ? filterId++ : -1;
		for (uint32 j = mi.indexBegin, je = j + mi.indexCount; j < je; ++j) {
			m_instances[j].filterId = mi.filterId;
		}
	}
}

void StaticBenchmarkMultiTarget::save_(std::ostream& file, bool env) const
{
	StaticBenchmarkSingleTarget::save_(file, env);
	saveMulti_(file);
}

void StaticBenchmarkMultiTarget::saveMulti_(std::ostream& file) const
{
	using inx::io::accurate_number;
	if (!(file << "start benchmarkMT version " << VERSION << '\n'))
		throw std::runtime_error("bad write");
	for (const auto& inst : m_multiInstance) {
		if (!(file << "instance start " << accurate_number(inst.start.x) << ' ' << accurate_number(inst.start.y) << " index " << inst.indexBegin << ' ' << inst.indexCount << '\n'))
			throw std::runtime_error("bad write");
	}
	if (!(file << "end benchmarkMT" << '\n'))
		throw std::runtime_error("bad write");
}

void StaticBenchmarkMultiTarget::load_(const Archiver& archive, bool env)
{
	clear();
	if (env)
		loadEnv_(archive);
	loadInst_(archive);
	loadMulti_(archive);
	resetInstance();
}

void StaticBenchmarkMultiTarget::loadMulti_(const Archiver& archive)
{
	struct Helper
	{
		static bool endOfStream(std::istream& is)
		{
			is >> std::ws;
			if (is.bad())
				throw std::runtime_error("bad read");
			return is.eof();
		}
		static bool getline(std::istream& is, std::istringstream& iss, std::string& line)
		{
			if (!std::getline(is, line)) {
				if (is.eof())
					return false;
				throw std::runtime_error("bad read");
			}
			iss.str(line);
			iss.clear();
			if (endOfStream(iss))
				throw std::runtime_error("empty line");
			return true;
		}
	};

	m_ioLevel += 1;
	auto& block = archive.at("benchmarkMT");
	auto input = boost::iostreams::stream(block.openRead());
	std::string line, str1;
	std::istringstream iss;

	while (Helper::getline(input, iss, line)) {
		if (!(iss >> str1))
			throw std::runtime_error("bad read");
		if (str1 != "instance")
			throw std::runtime_error("bad read");
		environment::point start{};
		int indexBegin{}, indexCount{};
		bool foundStart = false, foundIndex = false;
		while (!Helper::endOfStream(iss)) {
			if (!(iss >> str1))
				throw std::runtime_error("bad read");
			if (str1 == "start") {
				if (foundStart)
					throw std::runtime_error("duplicate start tags");
				foundStart = true;
				if (!(iss >> start.x >> start.y))
					throw std::runtime_error("bad read");
			} else if (str1 == "index") {
				if (foundIndex)
					throw std::runtime_error("duplicate index tags");
				foundIndex = true;
				if (!(iss >> indexBegin >> indexCount))
					throw std::runtime_error("bad read");
			} else {
				throw std::runtime_error("unknown tag");
			}
		}
		if (!foundStart)
			throw std::runtime_error("missing start tag");
		if (!foundIndex)
			throw std::runtime_error("missing index tag");
		auto& I = m_multiInstance.emplace_back();
		I.start = start;
		I.validId = static_cast<int32>(m_multiInstance.size() - 1);
		I.filterId = I.validId;
		I.indexBegin = indexBegin;
		I.indexCount = indexCount;
	}
	if (!Helper::endOfStream(input)) // line and str1 already read
		throw std::runtime_error("bad read");

	m_ioLevel -= 1;
}

} // namespace rayscanlib::scen
