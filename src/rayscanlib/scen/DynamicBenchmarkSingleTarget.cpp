#include <rayscanlib/scen/DynamicBenchmarkSingleTarget.hpp>
#include <boost/iostreams/stream.hpp>
#include <inx/io/transformers.hpp>
#include <algorithm>
#include <fstream>
#include <sstream>

namespace rayscanlib::scen
{

void DynamicBenchmarkSingleTarget::clear()
{
	StaticBenchmarkSingleTarget::clear();
	clear_();
}

void DynamicBenchmarkSingleTarget::clear_()
{
	m_dynEnvironment = nullptr;
	m_selectDeletionsId.clear(); m_selectDeletionsId.shrink_to_fit();
	m_selectInsertionsId.clear(); m_selectInsertionsId.shrink_to_fit();
	m_selectDeletions.clear(); m_selectDeletions.shrink_to_fit();
	m_selectInsertions.clear(); m_selectInsertions.shrink_to_fit();
	m_dynamicChangesAt = 0;
}

void DynamicBenchmarkSingleTarget::setEnvironment(std::shared_ptr<environment>&& env)
{
	StaticBenchmarkSingleTarget::setEnvironment(std::move(env));
	setupEnvironmentObstacles();
}

void DynamicBenchmarkSingleTarget::setupEnvironmentObstacles()
{
	assert(m_dynEnvironment == nullptr);
	auto& obst = getCurrentEnvironment()->getObstacles();
	m_dynamicPolygons.reserve(3*obst.size() / 2);
	int32 id = 0;
	for (auto it = obst.begin(), ite = obst.end(); it != ite; ++it) {
		auto [jt, added] = m_dynamicPolygons.try_emplace(++id, *it);
		if (!added)
			throw std::runtime_error("unable to instert polygon");
		jt->second.envIt = it;
		jt->second.inEnv = true;
		jt->second.isOrigional = true;
	}
}

void DynamicBenchmarkSingleTarget::syncEnvironmentObstacles()
{
	assert(m_dynEnvironment == nullptr);
	auto& obst = getCurrentEnvironment()->getObstacles();
	auto endit = obst.end();
	for (auto& env : m_dynamicPolygons) {
		env.second.envIt = endit;
		env.second.inEnv = false;
		env.second.isOrigional = false;
	}
	int32 id = 0;
	for (auto it = obst.begin(); it != endit; ++it) {
		auto& poly = m_dynamicPolygons.at(++id);
		poly.envIt = it;
		poly.inEnv = true;
		poly.isOrigional = true;
	}
}

auto DynamicBenchmarkSingleTarget::getCurrentEnvironment() -> const std::shared_ptr<environment>&
{
	if (m_dynEnvironment == nullptr) {
		if (this->m_environment == nullptr)
			throw std::runtime_error("m_environment");
		m_dynEnvironment = std::make_shared<environment>(*this->m_environment);
	}
	return m_dynEnvironment;
}
auto DynamicBenchmarkSingleTarget::getCurrentEnvironment() const -> const std::shared_ptr<environment>&
{
	if (m_dynEnvironment == nullptr) {
		throw std::runtime_error("m_dynEnvironment");
	}
	return m_dynEnvironment;
}

void DynamicBenchmarkSingleTarget::selectInstance(size_t i)
{
	auto instAt = this->m_instanceAt;
	StaticBenchmarkSingleTarget::selectInstance(i);
	m_selectDeletions.clear();
	assert(m_selectDeletionsId.empty());
	m_selectInsertions.clear();
	assert(m_selectInsertionsId.empty());
	size_t at;
	size_t select = i == size() ? i-1 : i;
	if (instAt < 0) { // base case, move to instance 0
		select += 1;
		at = 0;
	} else {
		at = instAt;
	}
	if (select > at) {
		forwardInstances(select - at);
	} else if (select < at) {
		reverseInstances(at - select);
	}
	applyDynamicChanges();
}

void DynamicBenchmarkSingleTarget::resetInstance()
{
	StaticBenchmarkSingleTarget::resetInstance();
	m_dynEnvironment = nullptr;
	m_selectDeletionsId.clear();
	m_selectInsertionsId.clear();
	m_selectDeletions.clear();
	m_selectInsertions.clear();
	m_dynamicChangesAt = 0;
	syncEnvironmentObstacles();
}

void DynamicBenchmarkSingleTarget::beginInserting()
{
	StaticBenchmarkSingleTarget::beginInserting();
	m_selectDeletionsId.clear();
	m_selectInsertionsId.clear();
	m_selectDeletions.clear();
	m_selectInsertions.clear();
}

void DynamicBenchmarkSingleTarget::endInserting()
{
	StaticBenchmarkSingleTarget::endInserting();
}

bool DynamicBenchmarkSingleTarget::addInstance(environment::point start, environment::point target)
{
	if (m_dynamicChangesAt != static_cast<uint32>(m_dynamicChanges.size()))
		throw std::runtime_error("m_dynamicChangesAt not in right state to add instance");
	if (!StaticBenchmarkSingleTarget::addInstance(start, target))
		return false;
	addDynamicChanges_();
	return true;
}

void DynamicBenchmarkSingleTarget::addDynamicChanges_()
{
	if (m_dynamicChangesAt != static_cast<uint32>(m_dynamicChanges.size()))
		throw std::runtime_error("m_dynamicChangesAt not in right state to add instance");
	int32 changesSize = static_cast<int32>(m_selectDeletionsId.size() + m_selectInsertionsId.size());
	m_dynamicChanges.push_back(changesSize);
	for (auto id : m_selectDeletionsId) {
		assert(id > 0);
		m_dynamicChanges.push_back(-id);
	}
	for (auto id : m_selectInsertionsId) {
		assert(id > 0);
		m_dynamicChanges.push_back(id);
	}
	m_selectDeletionsId.clear();
	m_selectInsertionsId.clear();
	m_dynamicChanges.push_back(changesSize);
	m_dynamicChangesAt = m_dynamicChanges.size();
}

uint32 DynamicBenchmarkSingleTarget::addDynamicPolygon(const environment::polygon& p, uint32 id)
{
	if ( m_ioLevel == 0 && (!m_inserting || !p.isFinalised() || p.orientation() != environment::obstacle_ori) )
		return 0;
	if (m_ioLevel != 0 || getCurrentEnvironment()->polygonValid(p)) {
		if (id == 0)
			id = static_cast<uint32>(m_dynamicPolygons.size()+1);
		if (!m_dynamicPolygons.try_emplace(id, p).second)
			id = 0;
	} else {
		id = 0;
	}
	return id;
}

bool DynamicBenchmarkSingleTarget::addDynamicInsertion(uint32 id)
{
	if (!m_inserting)
		return 0;
	auto& X = m_dynamicPolygons.at(id);
	auto& env = getCurrentEnvironment();
	//   not in env                not already inserted                                                                                  not already deleted
	if (!X.inEnv && std::find(m_selectInsertionsId.begin(), m_selectInsertionsId.end(), id) == m_selectInsertionsId.end() && std::find(m_selectDeletionsId.begin(), m_selectDeletionsId.end(), id) == m_selectDeletionsId.end()) {
		if (env->polygonValid(X.P)) {
			X.envIt = env->pushObstacleNoCheck(X.P);
			X.inEnv = true;
			m_selectInsertions.push_back(&X.P);
			m_selectInsertionsId.push_back(id);
			return true;
		}
	}
	return false;
}

bool DynamicBenchmarkSingleTarget::addDynamicDeletion(uint32 id)
{
	if (!m_inserting)
		return 0;
	auto& X = m_dynamicPolygons.at(id);
	auto& env = getCurrentEnvironment();
	//  in env   deletions comes before insertions             // not already deleted
	if (X.inEnv && m_selectInsertionsId.empty() && std::find(m_selectDeletionsId.begin(), m_selectDeletionsId.end(), id) == m_selectDeletionsId.end()) {
		env->eraseObstacleNoCheck(X.envIt);
		X.inEnv = false;
		X.envIt = env->getObstacles().end();
		m_selectDeletions.push_back(&X.P);
		m_selectDeletionsId.push_back(id);
		return true;
	}
	return false;
}

void DynamicBenchmarkSingleTarget::forwardInstances(size_t count)
{
	assert(count != 0 && m_selectDeletionsId.empty() && m_selectInsertionsId.empty());
	std::unordered_map<uint32, uint32> change; // first: id, second: 0=ignore, 1=del, 2=ins
	for ( ; count > 0; count--) {
		int32 n = m_dynamicChanges.at(m_dynamicChangesAt++);
		assert(n >= 0);
		while (n-- > 0) {
			int32 id = m_dynamicChanges.at(m_dynamicChangesAt++);
			if (id < 0) {
				id = -id;
				auto& m = change[id];
				switch (m) {
				case 2: m = 0; break;
				default: assert(false); break;
				case 0:
					if (m_dynamicPolygons.at(id).inEnv) m = 1;
					else { assert(false); }
					break;
				}
			} else if (id > 0) {
				auto& m = change[id];
				switch (m) {
				case 1: m = 0; break;
				default: assert(false); break;
				case 0:
					if (!m_dynamicPolygons.at(id).inEnv) m = 2;
					else { assert(false); }
					break;
				}
			} else {
				throw std::runtime_error("invalid code");
			}
		}
		m_dynamicChangesAt++;
	}
	for (auto [id, m] : change) {
		if (m == 1)
			m_selectDeletionsId.push_back(id);
		else if (m == 2)
			m_selectInsertionsId.push_back(id);
	}
}
void DynamicBenchmarkSingleTarget::reverseInstances(size_t count)
{
	assert(count != 0 && m_selectDeletionsId.empty() && m_selectInsertionsId.empty());
	std::unordered_map<uint32, uint32> change; // first: id, second: 0=ignore, 1=del, 2=ins
	for ( ; count > 0; count--) {
		assert(m_dynamicChangesAt > 0);
		int32 n = m_dynamicChanges.at(--m_dynamicChangesAt);
		assert(n >= 0);
		while (n-- > 0) {
		assert(m_dynamicChangesAt > 0);
			int32 id = m_dynamicChanges.at(--m_dynamicChangesAt);
			if (id < 0) { // for reverse you do the inverse, so this will be insert
				id = -id;
				auto& m = change[id];
				switch (m) {
					case 1: m = 0; break;
					default: assert(false); break;
					case 0:
						if (!m_dynamicPolygons.at(id).inEnv) m = 2;
						else assert(false);
						break;
				}
			} else if (id > 0) {
				auto& m = change[id];
				switch (m) {
					case 2: m = 0; break;
					default: assert(false); break;
					case 0:
						if (m_dynamicPolygons.at(id).inEnv) m = 1;
						else assert(false);
						break;
				}
			} else {
				throw std::runtime_error("invalid code");
			}
		}
		assert(m_dynamicChangesAt > 0);
		--m_dynamicChangesAt;
	}
	for (auto [id, m] : change) {
		if (m == 1)
			m_selectDeletionsId.push_back(id);
		else if (m == 2)
			m_selectInsertionsId.push_back(id);
	}
}

void DynamicBenchmarkSingleTarget::applyDynamicChanges()
{
	auto& dynevn = *getCurrentEnvironment();
	auto endit = dynevn.getObstacles().end();
	for (uint32 id : m_selectDeletionsId) {
		DynamicPolygon& X = m_dynamicPolygons.at(id);
		assert(X.inEnv);
		if (X.inEnv) {
			dynevn.eraseObstacleNoCheck(X.envIt);
			X.inEnv = false;
			X.envIt = endit;
			m_selectDeletions.push_back(&X.P);
		}
	}
	for (uint32 id : m_selectInsertionsId) {
		DynamicPolygon& X = m_dynamicPolygons.at(id);
		assert(!X.inEnv);
		if (!X.inEnv) {
			X.envIt = dynevn.pushObstacleNoCheck(X.P);
			X.inEnv = true;
			m_selectInsertions.push_back(&X.P);
		}
	}
	m_selectDeletionsId.clear();
	m_selectInsertionsId.clear();
}

std::shared_ptr<StaticBenchmarkSingleTarget> DynamicBenchmarkSingleTarget::refineBenchmarkByAppliedFilter() const
{
	std::shared_ptr<StaticBenchmarkSingleTarget> refine = std::make_shared<DynamicBenchmarkSingleTarget>();
	refineBenchmarkByAppliedFilter_(dynamic_cast<DynamicBenchmarkSingleTarget&>(*refine), std::count_if(m_instances.begin(), m_instances.end(), [](auto& inst) { return !inst.isFilteredOut(); }));
	return refine;
}

void DynamicBenchmarkSingleTarget::refineBenchmarkByAppliedFilter_(DynamicBenchmarkSingleTarget& reff, size_t num) const
{
	struct NewPoly
	{
		const std::unordered_map<uint32, DynamicPolygon>::value_type* orig;
		bool inEnv;
		bool inStart;
		int32 newId;
		std::vector<int32> inInst;
		NewPoly(const std::unordered_map<uint32, DynamicPolygon>::value_type* lorig, bool linEnv) : orig(lorig), inEnv(linEnv), inStart(false), newId(0) { }
	};
	StaticBenchmarkSingleTarget::refineBenchmarkByAppliedFilter_(reff, num);
	if (num == 0)
		return;
	auto& dynamicPolygons = reff.m_dynamicPolygons;
	auto& dynamicChanges = reff.m_dynamicChanges;
	uint32 dynamicPolygonsAt = 0;
	std::unordered_map<uint32, NewPoly> insdel;
	{
	size_t origs = m_environment->getObstacles().size();
	insdel.reserve(5*m_dynamicPolygons.size()/4);
	for (const auto& ply : m_dynamicPolygons) {
		if (!insdel.try_emplace(ply.first, &ply, ply.first <= origs).second)
			throw std::runtime_error("unable to add polygon");
	}
	}
	auto env = std::make_shared<environment>();
	env->pushEnclosureNoCheck(m_environment->getEnclosure());
	uint32 cnt = size();
	uint32 start = cnt;
	for (size_t i = 0; i < cnt; i++) {
		// handle change in polygons
		int32 dchange = m_dynamicChanges[dynamicPolygonsAt++];
		while (dchange-- > 0) {
			if (int32 polyid = m_dynamicChanges[dynamicPolygonsAt++]; polyid < 0) {
				auto& ply = insdel.at(static_cast<uint32>(-polyid));
				if (!ply.inEnv)
					throw std::runtime_error("unable to erase polygon that does not exists");
				ply.inEnv = false;
			} else {
				assert(polyid != 0);
				auto& ply = insdel.at(static_cast<uint32>(polyid));
				if (ply.inEnv)
					throw std::runtime_error("unable to insert polygon that already exists");
				ply.inEnv = true;
			}
		}
		dynamicPolygonsAt++;

		// check to see if retained scenario
		if (!m_instances[i].isFilteredOut()) {
			bool isStart;
			if (start == cnt) {
				start = i; isStart = true;
			} else {
				isStart = false;
			}
			for (auto& ply : insdel) {
				if (ply.second.inEnv) {
					if (isStart)
						ply.second.inStart = true;
					ply.second.inInst.emplace_back(i); // add polygon to instance
				}
			}
		}
	}

	// determinte removed polygons (ones that never appear)
	std::vector<uint32> removed;
	for (auto&& [id,ply] : insdel) {
		if (ply.inInst.empty())
			removed.push_back(id);
	}
	// remove removed polygons
	for (auto id : removed) {
		insdel.erase(id);
	}
	// create a new id
	uint32 newId = 0; // the new id
	auto&& getNewId = [&newId,&insdel,&dynamicPolygons] (uint32 id) {
		auto& P = insdel.at(id);
		if (P.newId == 0) {
			P.newId = ++newId;
			if (!dynamicPolygons.try_emplace(P.newId, P.orig->second.P).second)
				throw std::runtime_error("id already exists");
		}
	};
	// handle first instance, which holds the starting polygons
	for (auto&& [id,ply] : insdel) {
		if (ply.inStart) {
			ply.inEnv = true;
			getNewId(id);
			env->pushObstacleNoCheck(ply.orig->second.P);
		} else {
			ply.inEnv = false;
		}
	}
	dynamicChanges.push_back(0);
	dynamicChanges.push_back(0);
	// process all other instances
	dynamicPolygonsAt = 2; // first instance is 0 0
	std::vector<int32> inspoly;
	std::vector<int32> delpoly;
	dynamicPolygonsAt = 0;
	for (size_t i = 0; i < cnt; ++i) { // start processing changes
		if (!m_instances[i].isFilteredOut()) {
			inspoly.clear();
			delpoly.clear();
			for (auto&& [id,ply] : insdel) { // check what has added and removed since
				if (std::binary_search(ply.inInst.begin(), ply.inInst.end(), static_cast<int32>(i))) { // polygon is in instance
					if (!ply.inEnv) { // add polygon to environment
						ply.inEnv = true;
						getNewId(id);
						assert(ply.newId > 0);
						inspoly.push_back(ply.newId);
					}
				} else { // polygon is not in instance
					if (ply.inEnv) { // remove polygon from environment
						ply.inEnv = false;
						assert(ply.newId > 0);
						delpoly.push_back(-ply.newId);
					}
				}
			}
			int32 dcount = static_cast<int32>(inspoly.size() + delpoly.size());
			dynamicChanges.push_back(dcount);
			dynamicChanges.insert(dynamicChanges.end(), delpoly.begin(), delpoly.end());
			dynamicChanges.insert(dynamicChanges.end(), inspoly.begin(), inspoly.end());
			dynamicChanges.push_back(dcount);
		}
	}
	// ensure dynenvironement is not set
	reff.m_dynEnvironment = nullptr;
	reff.m_environment = env;
	reff.resetInstance();
}

void DynamicBenchmarkSingleTarget::save_(std::ostream& file, bool env) const
{
	StaticBenchmarkSingleTarget::save_(file, env);
	saveDyn_(file);
}

void DynamicBenchmarkSingleTarget::saveDyn_(std::ostream& file) const
{
	using inx::io::accurate_number;
	if (!(file << "start dynamic version " << VERSION << '\n'))
		throw std::runtime_error("bad write");
	
	// start saving dynamic polygon structure, sort by id order
	std::vector<const decltype(m_dynamicPolygons)::value_type*> dynplys;
	size_t origId = m_environment->getObstacles().size();
	dynplys.reserve(m_dynamicPolygons.size() - origId);
	for (const auto& dp : m_dynamicPolygons) {
		if (dp.first > origId)
			dynplys.push_back(&dp);
	}
	std::sort(dynplys.begin(), dynplys.end(), [](auto* a, auto* b) { return a->first < b->first; });
	for (auto* dp : dynplys) {
		if (!(file << "polygon " << dp->first))
			throw std::runtime_error("bad write");
		auto& p = dp->second.P;
		if (!(file << ' ' << p.size()))
			throw std::runtime_error("bad write");
		for (auto& pt : p) {
			if (!(file << ' ' << accurate_number(pt.x) << ' ' << accurate_number(pt.y)))
				throw std::runtime_error("bad write");
		}
		if (!(file << '\n'))
			throw std::runtime_error("bad write");
	}
	
	for (size_t i = 0, ie = m_dynamicChanges.size(); i < ie; i++) {
		int32 opsize = m_dynamicChanges.at(i++);
		if (!(file << "op " << opsize))
			throw std::runtime_error("bad write");
		for ( ; opsize > 0; --opsize) {
			if (!(file << ' ' << m_dynamicChanges.at(i++)))
				throw std::runtime_error("bad write");
		}
		if (!(file << '\n'))
			throw std::runtime_error("bad write");
	}
	if (!(file << "end dynamic" << std::endl))
		throw std::runtime_error("bad write");
}

void DynamicBenchmarkSingleTarget::load_(const Archiver& archive, bool env)
{
	clear();
	if (env)
		loadEnv_(archive);
	loadInst_(archive);
	loadDyn_(archive);
	resetInstance();
}

void DynamicBenchmarkSingleTarget::loadDyn_(const Archiver& archive)
{
	struct Helper
	{
		static bool endOfStream(std::istream& is)
		{
			is >> std::ws;
			if (is.bad())
				throw std::runtime_error("bad read");
			return is.eof();
		}
		static bool getline(std::istream& is, std::istringstream& iss, std::string& line)
		{
			if (!std::getline(is, line)) {
				if (is.eof())
					return false;
				throw std::runtime_error("bad read");
			}
			iss.str(line);
			iss.clear();
			if (endOfStream(iss))
				throw std::runtime_error("empty line");
			return true;
		}
	};

	m_ioLevel += 1;
	auto& block = archive.at("dynamic");
	auto input = boost::iostreams::stream(block.openRead());
	std::string line, str1, str2;
	int32 int1, int2;
	std::istringstream iss;

	
	if (!Helper::getline(input, iss, line))
	{	m_ioLevel -= 1; return;	}
	if (!(iss >> str1))
		throw std::runtime_error("bad read");
	
	while (str1 == "polygon") {
		environment::polygon poly;
		if (!(iss >> int1 >> int2))
			throw std::runtime_error("bad read");
		if (int1 <= 0 || int2 < 3)
			throw std::runtime_error("bad input range");
		environment::point pt;
		for ( ; int2 > 0; --int2) {
			if (!(iss >> pt.x >> pt.y))
				throw std::runtime_error("bad read");
			poly.addPoint(pt);
		}
		if (!Helper::endOfStream(iss))
			throw std::runtime_error("invalid line");
		if (!poly.finalise(rayscanlib::geometry::PolyType::SimplePolygon) || addDynamicPolygon(poly, int1) == 0) {
			throw std::runtime_error("polygon invalid");
		}
		if (!Helper::getline(input, iss, line))
		{	m_ioLevel -= 1; return;	}
		if (!(iss >> str1))
			throw std::runtime_error("bad read");
	}
	while (str1 == "op") {
		if (!(iss >> int1))
			throw std::runtime_error("bad read");
		if (int1 < 0)
			throw std::runtime_error("bad input range");
		m_dynamicChanges.push_back(int1);
		for (int32 i = 0; i < int1; i++) {
			if (!(iss >> int2))
				throw std::runtime_error("bad read");
			if (int2 == 0)
				throw std::runtime_error("bad input range");
			m_dynamicChanges.push_back(int2);
		}
		m_dynamicChanges.push_back(int1);
		if (!Helper::endOfStream(iss))
			throw std::runtime_error("invalid line");
		if (!Helper::getline(input, iss, line))
		{	m_ioLevel -= 1; return;	}
		if (!(iss >> str1))
			throw std::runtime_error("bad read");
	}
	m_ioLevel -= 1;
}

} // namespace rayscanlib::scen
