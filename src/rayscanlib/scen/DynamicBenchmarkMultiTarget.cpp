#include <rayscanlib/scen/DynamicBenchmarkMultiTarget.hpp>
#include <algorithm>
#include <fstream>
#include <sstream>

namespace rayscanlib::scen
{

void DynamicBenchmarkMultiTarget::clear()
{
	StaticBenchmarkMultiTarget::clear();
	DynamicBenchmarkSingleTarget::clear_();
}

void DynamicBenchmarkMultiTarget::resetInstance()
{
	DynamicBenchmarkSingleTarget::resetInstance();
	m_multiInstanceAt = 0;
}

bool DynamicBenchmarkMultiTarget::addInstance(environment::point start, environment::point target)
{
	if (m_dynamicChangesAt != static_cast<uint32>(m_dynamicChanges.size()))
		throw std::runtime_error("m_dynamicChangesAt not in right state to add instance");
	// check if new multi instance shuold be added
	if (!StaticBenchmarkMultiTarget::addInstance(start, target))
		return false;
	addDynamicChanges_();
	return true;
}

void DynamicBenchmarkMultiTarget::selectInstance(size_t i)
{
	DynamicBenchmarkSingleTarget::selectInstance(i);
	StaticBenchmarkMultiTarget::selectInstance(i);
}

void DynamicBenchmarkMultiTarget::save_(std::ostream& file, bool env) const
{
	StaticBenchmarkMultiTarget::save_(file, env);
	saveDyn_(file);
}

void DynamicBenchmarkMultiTarget::load_(const Archiver& archive, bool env)
{
	clear();
	if (env)
		loadEnv_(archive);
	loadInst_(archive);
	loadMulti_(archive);
	loadDyn_(archive);
	resetInstance();
}

} // namespace rayscanlib::scen
