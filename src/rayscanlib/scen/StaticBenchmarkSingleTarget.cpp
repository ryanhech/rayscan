#include <rayscanlib/scen/StaticBenchmarkSingleTarget.hpp>
#include <rayscanlib/scen/BenchmarkCompute.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <inx/io/transformers.hpp>
#include <algorithm>
#include <fstream>
#include <sstream>

namespace rayscanlib::scen
{

void StaticBenchmarkSingleTarget::clear()
{
	m_instanceAt = -1;
	m_inserting = false;
	m_instances.clear();
	m_instances.shrink_to_fit();
	m_environment = nullptr;
}

void StaticBenchmarkSingleTarget::setEnvironment(std::shared_ptr<environment>&& env)
{
	clear();
	m_environment = std::move(env);
}

void StaticBenchmarkSingleTarget::beginInserting()
{
	m_inserting = true;
	selectInstance(size());
}

void StaticBenchmarkSingleTarget::endInserting()
{
	m_inserting = false;
}

bool StaticBenchmarkSingleTarget::isValidScenario(environment::point start, environment::point target)
{
	if (!m_inserting || m_instanceAt != static_cast<int32>(size()))
		return false;
	auto& env = *getCurrentEnvironment();
	return (env.isOverlap() || (!geometry::pos_is_outside(env.pointLocation(start).first) && !geometry::pos_is_outside(env.pointLocation(target).first)));
}

bool StaticBenchmarkSingleTarget::addInstance(environment::point start, environment::point target)
{
	if (!isValidScenario(start, target))
		return false;
	addInstance_(start, target);
	return true;
}

bool StaticBenchmarkSingleTarget::addInstanceNoCheck(environment::point start, environment::point target)
{
	addInstance_(start, target);
	return true;
}

void StaticBenchmarkSingleTarget::addInstance_(environment::point start, environment::point target)
{
	Instance inst;
	if (m_instanceAt < 0)
		m_instanceAt = 0;
	inst.validId = m_instanceAt++;
	inst.filterId = inst.validId;
	inst.start = start;
	inst.target = target;
	m_instances.emplace_back(inst);
}

void StaticBenchmarkSingleTarget::applyFilter(std::string_view filter, bool remove)
{
	std::vector<bool> scenarios;
	computeFilter(scenarios, filter, remove);
	applyFilter(scenarios);
}
void StaticBenchmarkSingleTarget::applyFilter(const std::vector<bool>& filter)
{
	if (filter.size() != m_instances.size())
		throw std::runtime_error("invalid number of instances to filter");
	int32 filterId = 0;
	for (uint32 i = 0, ie = static_cast<uint32>(m_instances.size()); i < ie; ++i) {
		m_instances[i].filterId = filter[i] ? filterId++ : -1;
	}
}

void StaticBenchmarkSingleTarget::computeFilter(std::vector<bool>& scenarios, std::string_view filter, bool remove) const
{
	size_t cnt = size_major();
	scenarios.clear();
	auto at = filter.begin();
	auto num = [filter,&at,maxs=cnt]() {
		if (at == filter.end())
			return -1;
		uint32_t n = *at - '0';
		if (n >= 10)
			return -1;
		while (++at != filter.end() && n < 1000000 && static_cast<uint32_t>(*at - '0') < 10) {
			n = 10 * n + static_cast<uint32_t>(*at - '0');
		}
		if (n >= 1000000)
			return -1;
		if (n >= maxs)
			return static_cast<int>(maxs-1);
		return static_cast<int>(n);
	};
	if (filter == "") {
		scenarios.resize(cnt, !remove);
	} else if (filter == "-") {
		scenarios.resize(cnt, remove);
	} else if (filter[0] == ':') { // special filters
		scenarios.resize(cnt, remove);
		++at;
		int n = num();
		if (n < 0) throw std::runtime_error("invalid filter");
		if (at != filter.end()) throw std::runtime_error("invalid filter");
		for (uint64 i = 0; i < static_cast<uint64>(n); i++) {
			scenarios[(i*cnt) / static_cast<uint64>(n)] = !remove;
		}
	} else {
		scenarios.resize(cnt, remove);
		while (at != filter.end()) {
			int n = num();
			if (n < 0) throw std::runtime_error("invalid filter");
			if (at == filter.end() || *at == ',') {
				if (at != filter.end()) ++at;
				scenarios[n] = !remove;
			} else if (*at == '-') {
				++at;
				int m = num();
				if (m < n) throw std::runtime_error("invalid filter range");
				if (at != filter.end()) ++at;
				for ( ; n <= m; n++) {
					scenarios[n] = !remove;
				}
			} else {
				throw std::runtime_error("invalid filter");
			}
		}
	}
}

void StaticBenchmarkSingleTarget::computeDistances()
{
	benchmarkComputeDistancesAuto(*this);
}

void StaticBenchmarkSingleTarget::orderByDistances()
{
	std::stable_sort(m_instances.begin(), m_instances.end(), [] (const Instance& a, const Instance& b) noexcept {
		if (!a.distance)
			return false;
		else if (!b.distance)
			return true;
		else
			return *a.distance < *b.distance;
	});
	for (int i = 0, ie = static_cast<int>(m_instances.size()); i < ie; ++i) {
		m_instances[i].validId = m_instances[i].filterId = i;
	}
}

std::shared_ptr<StaticBenchmarkSingleTarget> StaticBenchmarkSingleTarget::refineBenchmarkByAppliedFilter() const
{
	auto refine = std::make_shared<StaticBenchmarkSingleTarget>();
	refineBenchmarkByAppliedFilter_(*refine, std::count_if(m_instances.begin(), m_instances.end(), [](auto& inst) { return !inst.isFilteredOut(); }));
	return refine;
}

void StaticBenchmarkSingleTarget::refineBenchmarkByAppliedFilter_(StaticBenchmarkSingleTarget& reff, size_t num) const
{
	reff.clear();
	reff.m_environment = m_environment;
	reff.m_instances.reserve(num);
	std::copy_if(m_instances.begin(), m_instances.end(), std::back_inserter(reff.m_instances), [] (const Instance& inst) {
		return !inst.isFilteredOut();
	});
	if (reff.m_instances.size() != num)
		throw std::runtime_error("invalid number of instances copied compared to number found");
	size_t i = 0;
	for (auto it = reff.m_instances.begin(); i < num; ++i, ++it) {
		it->validId = i;
		it->filterId = i;
	}
}

void StaticBenchmarkSingleTarget::selectInstance(size_t i)
{
	if (i > size())
		throw std::out_of_range("i");
	m_instanceAt = static_cast<int32>(i);
}

void StaticBenchmarkSingleTarget::resetInstance()
{
	m_instanceAt = -1;
}

void StaticBenchmarkSingleTarget::save(std::ostream& file) const
{
	save(file, true);
}
void StaticBenchmarkSingleTarget::save(std::ostream& file, bool env) const
{
	save_(file, env);
}
void StaticBenchmarkSingleTarget::save(Archiver& archive, bool env) const
{
	inx::io::chunked_array tmpfile;
	boost::iostreams::filtering_stream<boost::iostreams::seekable> tmpstream;
	tmpstream.push(boost::ref(tmpfile));
	save(tmpstream, env);
	tmpstream.seekg(0, std::ios::beg);
	archive.load(tmpstream, std::filesystem::path());
}

void StaticBenchmarkSingleTarget::save_(std::ostream& file, bool env) const
{
	saveEnv_(file, env);
	saveInst_(file);
}

void StaticBenchmarkSingleTarget::saveEnv_(std::ostream& file, bool env) const
{
	if (env && m_environment) {
		m_environment->save(file);
	}
}

void StaticBenchmarkSingleTarget::saveInst_(std::ostream& file) const
{
	using inx::io::accurate_number;
	if (!(file << "start benchmarkST version " << VERSION << '\n'))
		throw std::runtime_error("bad write");
	for (auto& inst : m_instances) {
		if (!(file << "instance start " << accurate_number(inst.start.x) << ' ' << accurate_number(inst.start.y) << " target " << accurate_number(inst.target.x) << ' ' << accurate_number(inst.target.y)))
			throw std::runtime_error("bad write");
		if (inst.distance.has_value()) {
			if (!(file << " cost " << accurate_number(*inst.distance)))
				throw std::runtime_error("bad write");
		}
		if (inst.dynamic.has_value()) {
			if (!(file << " dynamic " << accurate_number(*inst.dynamic)))
				throw std::runtime_error("bad write");
		}
		if (!(file << '\n'))
			throw std::runtime_error("bad write");
	}
	if (!(file << "end benchmarkST" << '\n'))
		throw std::runtime_error("bad write");
}

void StaticBenchmarkSingleTarget::load(const Archiver& archive, bool env)
{
	m_ioLevel = 1;
//	try {
		load_(archive, env);
//	}
//	catch (...) {
//		m_ioLevel = 0;
//		throw;
//	}
	m_ioLevel = 0;
}

void StaticBenchmarkSingleTarget::load_(const Archiver& archive, bool env)
{
	clear();
	if (env)
		loadEnv_(archive);
	loadInst_(archive);
	resetInstance();
}

void StaticBenchmarkSingleTarget::loadEnv_(const Archiver& archive)
{
	m_ioLevel += 1;
	if (archive.has("polygonEnv")) {
		auto envptr = std::make_shared<environment>();
		envptr->load(archive);
		setEnvironment(std::move(envptr));
	}
	m_ioLevel -= 1;
}

void StaticBenchmarkSingleTarget::loadInst_(const Archiver& archive)
{
	struct Helper
	{
		static bool endOfStream(std::istream& is)
		{
			is >> std::ws;
			if (is.bad())
				throw std::runtime_error("bad read");
			return is.eof();
		}
		static bool getline(std::istream& is, std::istringstream& iss, std::string& line)
		{
			if (!std::getline(is, line)) {
				if (is.eof())
					return false;
				throw std::runtime_error("bad read");
			}
			iss.str(line);
			iss.clear();
			if (endOfStream(iss))
				throw std::runtime_error("empty line");
			return true;
		}
	};

	m_ioLevel += 1;
	auto& block = archive.at("benchmarkST");
	auto input = boost::iostreams::stream(block.openRead());
	std::string line, str1, str2;
	std::istringstream iss;

	while (Helper::getline(input, iss, line)) {
		if (!(iss >> str1))
			throw std::runtime_error("bad read");
		if (str1 != "instance")
			break;
		environment::point start{}, target{};
		std::optional<double> dist, dyn;
		bool startFound = false, targetFound = false;
		while (!Helper::endOfStream(iss)) {
			if (!(iss >> str1))
				throw std::runtime_error("bad read");
			if (str1 == "start") {
				if (startFound)
					throw std::runtime_error("duplicate start points");
				startFound = true;
				if (!(iss >> start.x >> start.y))
					throw std::runtime_error("bad read");
			} else if (str1 == "target") {
				if (targetFound)
					throw std::runtime_error("duplicate target points");
				targetFound = true;
				if (!(iss >> target.x >> target.y))
					throw std::runtime_error("bad read");
			} else if (str1 == "cost") {
				if (dist)
					throw std::runtime_error("duplicate cost");
				dist = 0;
				if (!(iss >> *dist))
					throw std::runtime_error("bad read");
			} else if (str1 == "dynamic") {
				if (dyn)
					throw std::runtime_error("duplicate cost");
				dyn = 0;
				if (!(iss >> *dyn))
					throw std::runtime_error("bad read");
			} else {
				throw std::runtime_error("unknown tag");
			}
		}
		if (!startFound || !targetFound)
			throw std::runtime_error("missing required tags");
		if (!addInstanceNoCheck(start, target))
			throw std::runtime_error("instance not added");
		m_instances.back().distance = dist;
		m_instances.back().dynamic = dyn;
	}
	if (!Helper::endOfStream(input)) // line and str1 already read
		throw std::runtime_error("bad read");

	m_ioLevel -= 1;
}

std::ostream& operator<<(std::ostream& s, const StaticBenchmarkSingleTarget& poly)
{
	poly.save(s);
	return s;
}
std::istream& operator>>(std::istream& s, StaticBenchmarkSingleTarget& poly)
{
	Archiver archive;
	archive.load(s, std::filesystem::path());
	poly.load(archive);
	return s;
}

} // namespace rayscanlib::scen
