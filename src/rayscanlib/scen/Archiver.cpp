#include <rayscanlib/scen/Archiver.hpp>
#include <inx/io/whitespace_line_filter.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/device/null.hpp>
#include <boost/iostreams/operations.hpp>
#include <boost/iostreams/stream_buffer.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/lexical_cast.hpp>
#include <fstream>
#include <sstream>
#include <unordered_set>
#include <inx/functions.hpp>

namespace rayscanlib::scen
{

Archiver::Archiver() : m_loadedFiles(&m_resource), m_blocks(&m_resource), m_parent(nullptr)
{ }

void Archiver::load(const std::filesystem::path& filename, bool raw)
{
	if (filename.empty())
		throw std::runtime_error("empty filename");
	auto fname = std::filesystem::canonical(filename);
	std::ifstream file(filename);
	m_parent = nullptr;
	load(file, filename, raw);
}
void Archiver::load(std::istream& input, const std::filesystem::path& filename, bool raw)
{
	using namespace std::string_view_literals;
	inx::destruct_adaptor delevel = [&p=m_parent,r=m_parent]() { p = r; };
	boost::iostreams::filtering_istream finput;
	bool emp = filename.empty() || filename.string().substr(0, 2) == "//";
	std::filesystem::path filenameA = emp ? filename : std::filesystem::absolute(filename);
	std::filesystem::path filenameC = emp ? filename : std::filesystem::canonical(filenameA);
	if (hasLoaded_(filenameC)) {
		return;
	} else if (!filenameC.empty()) {
		m_parent = &m_loadedFiles.try_emplace(filenameC.string(), filenameC.string(), filenameA.string(), m_parent).first->second;
	}
	finput.set_auto_close(false);
	finput.push(inx::io::whitespace_line_filter());
	finput.push(input);
	std::string line;
	enum class Mode
	{
		search,
		copy
	};
	Mode mode = Mode::search;
	boost::iostreams::filtering_ostream foutput;
	std::string_view outputType; // the output type to check at end against
	foutput.set_auto_close(false);
	// if loading as raw file, load file into raw without any other blocks
	if (raw) {
		mode = Mode::copy;
		auto block = m_blocks.try_emplace("raw", m_resource);
		if (block.second) { // insert successful
			outputType = block.first->second.m_name = block.first->first;
			block.first->second.m_version = 0;
			foutput.push(boost::ref(block.first->second.m_data));
		} else {
			foutput.push(boost::iostreams::null_sink());
		}
	}
	while (std::getline(finput, line)) {
		if (line.empty())
			continue;
		std::string_view lv = line;
		switch (mode) {
		case Mode::search:
			{
				if (lv.compare(0, 5, "load "sv) == 0) { // load from relative file
					std::filesystem::path fname;
					std::string issstr(lv.substr(5));
					std::istringstream iss(issstr);
					iss >> fname;
					if (!filenameC.empty() && !fname.is_absolute()) {
						fname = filenameC.parent_path() / fname;
					}
					load(fname);
					break;
				}
				if (lv.compare(0, 6, "start "sv) != 0)
					throw std::runtime_error("invalid block start");
				lv.remove_prefix(6);
				auto pos = lv.find_first_of(' ');
				std::string_view type = lv.substr(0, pos);
				std::size_t version = 0;
				if (pos != std::string_view::npos) {
					lv.remove_prefix(pos+1);
					if (lv.compare(0, 8, "version "sv) != 0) {
						throw std::runtime_error("invalid version");
					}
					lv.remove_prefix(8);
					version = boost::lexical_cast<std::size_t>(lv.data(), lv.size());
				}
				auto block = m_blocks.try_emplace(std::string(type), m_resource);
				if (block.second) { // insert successful
					auto& blk = block.first->second;
					if (!filenameC.empty()) {
						auto it = m_loadedFiles.find(filenameC.string());
						if (it == m_loadedFiles.end())
							throw std::runtime_error("invalid file");
						blk.m_filename = it->first;
					}
					outputType = blk.m_name = block.first->first;
					blk.m_version = version;
					foutput.push(boost::ref(blk.m_data));
				} else {
					foutput.push(boost::iostreams::null_sink());
				}
				mode = Mode::copy;
			}
			break;
		case Mode::copy:
			{
				if (!raw && lv.compare(0, 4, "end "sv) == 0) { // end of file, close stream
					lv.remove_prefix(4);
					if (lv != outputType)
						throw std::runtime_error("start and end tags did not match type");
					outputType = std::string_view();
					foutput.pop();
					mode = Mode::search;
				} else {
					foutput << lv << std::endl;
				}
			}
		}
	}
}

void Archiver::save(const std::filesystem::path& filename, Link link) const
{
	if (filename.empty())
		throw std::runtime_error("empty filename");
	std::ofstream file(filename);
	save(file, filename, link);
}
void Archiver::save(std::ostream& output, const std::filesystem::path& filename, Link link) const
{
	namespace bio = boost::iostreams;
	// link to parent archives
	bool addLine = false;
	std::filesystem::path base;
	std::filesystem::path filenameA;
	std::filesystem::path filenameC;
	if (filename.empty())
		base = std::filesystem::current_path();
	else {
		filenameA = std::filesystem::absolute(filename);
		filenameC = std::filesystem::weakly_canonical(filenameA);
		base = filenameA.parent_path();
	}
	if (link != NoLink) {
		std::unordered_set<std::string_view> loadF;
		for (const auto& [name,block] : m_blocks) {
			if (!block.getFilename().empty())
				loadF.emplace(block.getFilename());
		}
		loadF.erase(std::string_view(filenameC.string())); // do not link to self
		for (auto& lf : loadF) {
			auto& tlf = m_loadedFiles.at(std::string(lf));
			if (tlf.parent != nullptr && loadF.count(tlf.parent->canonical) > 0) // parent exists, do not link to
				continue;
			std::filesystem::path fname(tlf.absolute);
			if (link == LinkRel) {
				fname = fname.lexically_proximate(base);
			}
			output << "load " << fname << std::endl;
			addLine = true;
		}
	}
	for (const auto& [name,block] : m_blocks) {
		if (name == "raw")
			continue; // skip writing
		if (link != NoLink && !block.getFilename().empty() && block.getFilename() != filenameC) // if linked and block is from a file that is not the same as the saving one
			continue;
		if (addLine)
			output << std::endl;
		output << "start " << name;
		if (auto v = block.getVersion(); v != 0)
			output << " version " << v;
		output << std::endl;
		bio::stream_buffer<inx::io::chunked_array_source> buff(block.openRead());
		output << &buff;
		output << "end " << name << std::endl;
		addLine = true;
	}
}

void Archiver::copy(Archiver& other, bool override) const
{
	for (auto& B : m_blocks) {
		copy(other, std::string_view(B.first), override);
	}
}
void Archiver::copy(Archiver& other, std::string_view type, bool override) const
{
	std::string stype(type);
	bool has = other.has(type);
	if (has && override) {
		other.eraseBlock(stype);
	}
	if (!has || override)
		copy_(other, std::move(stype));
}

void Archiver::copy_(Archiver& other, std::string&& stype) const
{
	namespace bio = boost::iostreams;
	auto& B = m_blocks.at(stype);
	auto& block = other.m_blocks.try_emplace(std::move(stype), other.m_resource).first->second;
	block.m_name = B.m_name;
	block.m_filename = B.m_filename;
	block.m_fileLoaded = B.m_fileLoaded;
	if (!block.m_filename.empty()) {
		if (auto it = m_loadedFiles.find(std::string(block.m_filename)); it != m_loadedFiles.end())
			other.m_loadedFiles.emplace(*it);
		else
			throw std::out_of_range("loaded file out of range.");
	}
	bio::stream_buffer<inx::io::chunked_array_source> buff(B.openRead());
	bio::stream<boost::reference_wrapper<inx::io::chunked_array>> out(boost::ref(block.m_data));
	out << &buff;
}

} // namespace rayscan::scen
