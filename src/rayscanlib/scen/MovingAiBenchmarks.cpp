#include <rayscanlib/scen/MovingAiBenchmarks.hpp>
#include <fstream>
#include <sstream>
#include <iostream>
#include <forward_list>
#include <filesystem>

namespace rayscanlib::scen
{

MovingAiBenchmarks::MovingAiBenchmarks() : m_width(0), m_height(0)
{ }

void MovingAiBenchmarks::loadMap(std::istream& file)
{
	std::string tmp1, tmp2;

	// load header
	if (!(file >> tmp1 >> tmp2) || tmp1 != "type" || tmp2 != "octile") throw std::runtime_error("invalid map file");
	if (!(file >> tmp1 >> m_height) || tmp1 != "height" || m_height <= 0 || m_height >= 2048) throw std::runtime_error("invalid height");
	if (!(file >> tmp1 >> m_width) || tmp1 != "width" || m_width <= 0 || m_width >= 2048) throw std::runtime_error("invalid width");
	if (!(file >> tmp1) || tmp1 != "map") throw std::runtime_error("invalid map file");

	// read in grid
	m_grid.setup(m_width, m_height);
	m_grid.set_buffer(1);
	for (int32 y = static_cast<int32>(m_height-1); y >= 0; y--) {
		if (!(file >> tmp1) || static_cast<uint32>(tmp1.size()) != m_width) throw std::runtime_error("grid size wrong");
		for (int32 x = 0; x < static_cast<int32>(m_width); x++) {
			switch (tmp1[x]) {
			// blocked cell
			case '@':
			case 'O':
			case 'T':
			case 'S':
			case 'W':
				m_grid.bit_or(x, y, 1);
			// traversable cell
			case '.':
			case 'G':
				break;
			default:
				throw std::runtime_error("cell unknown");
			}
		}
	}

	m_environment = traceGrid();
}

void MovingAiBenchmarks::loadScenario(std::istream& file, bool loadMapFromScenario, const std::filesystem::path& fname)
{
	std::string line, tmp1;
	int itmp1, itmp2, itmp3;
	double dtmp1;
	environment::point ptmp1, ptmp2;
	std::istringstream iss;
	auto&& endOfStream = [](std::istringstream& iss) { iss >> std::ws; if (iss.bad()) throw std::runtime_error("bad read"); return iss.eof(); };
	if (!std::getline(file, line)) throw std::runtime_error("failed to read line");
	iss.str(line); iss.clear();
	if (!(iss >> tmp1 >> itmp1) || tmp1 != "version" || itmp1 != 1 || !endOfStream(iss)) throw std::runtime_error("invalid file version");

	// load scenarios into suite
	m_suite = std::make_shared<suite>();
	if (!loadMapFromScenario) {
		if (!m_environment)
			throw std::runtime_error("environment is not set");
		m_suite->setEnvironment(m_environment);
		m_suite->beginInserting();
	}
	while (std::getline(file, line)) {
		iss.str(line); iss.clear();
		if (endOfStream(iss)) continue;
		if (!(iss >> itmp1 >> tmp1 >> itmp2 >> itmp3 >> ptmp1.x >> ptmp1.y >> ptmp2.x >> ptmp2.y >> dtmp1) || !endOfStream(iss)) throw std::runtime_error("invalid scenario");
		if (loadMapFromScenario) {
			loadMapFromScenario = false;
			namespace fs = std::filesystem;
			fs::path mf = tmp1;
			if (!fname.empty() && !mf.has_parent_path()) {
				fs::path mf2(fname);
				mf2.replace_filename(mf);
				if (fs::is_regular_file(mf2))
					mf = mf2;
			}
			std::ifstream mapfile(mf);
			loadMap(mapfile);
			m_suite->setEnvironment(m_environment);
			m_suite->beginInserting();
		}
		ptmp1 = translatePointScale(ptmp1);
		ptmp2 = translatePointScale(ptmp2);
		if (!m_suite->addInstance(ptmp1, ptmp2)) {
			std::cerr << "Invalid instance " << ptmp1 << " - " << ptmp2 << std::endl;
		}
	}
	m_suite->endInserting();
	if (!file.eof() && file.fail()) throw std::runtime_error("faild to setup scenario");
}

auto MovingAiBenchmarks::traceGrid() -> std::shared_ptr<environment>
{
	uint32 width = m_grid.getWidth();
	uint32 height = m_grid.getHeight();
	enum ActionType {
		Inner = -1,
		None,
		Outer
	};
	enum DirType {
		North,
		East,
		South,
		West
	};
	inx::alg::bit_table<1, 1> cells(width, height);

	auto env = std::make_shared<environment>();
	env->makeOverlap();
	std::forward_list<environment::polygon> enclosures;

	for (uint32 y = 0; y < height; ++y)
	for (uint32 x = 0; x < width; ++x) {
		if (cells.bit_test(x, y)) continue;
		cells.bit_or(x, y, 1);
		
		// identify start of polygon
		// 2 3
		// 0 1
		DirType dir;
		switch (m_grid.region<1, 1, 2, 2>(x, y)) {
		//  #.  ..
		//  ##  .#
		case 0b0111:
		case 0b0010:
			dir = East;
			break;
		default:
			dir = North;
		}
		if (dir != East)
			continue;

		// follow edge of cell, forming the polygon in the process
		using point = environment::point;
		point origin(x, y), at(origin);
		environment::polygon poly;
		poly.addPoint(at);
		at.x += 1;
		while (at != origin) {
			cells.bit_or(at.x, at.y, 1);
			poly.addPoint(at);
			switch (m_grid.region<1, 1, 2, 2>(at.x, at.y)) {
			// Go east  #.  ..  ..
			//          ##  .#  ##
			case 0b0111:
			case 0b0010:
			case 0b0011:
				at.x += 1;
				dir = East;
				break;
			// Go west  ##  #.  ##
			//          .#  ..  ..
			case 0b1110:
			case 0b0100:
			case 0b1100:
				at.x -= 1;
				dir = West;
				break;
			// Go north  .#  .#  .#
			//           ##  ..  .#
			case 0b1011:
			case 0b1000:
			case 0b1010:
				at.y += 1;
				dir = North;
				break;
			// Go south  ##  ..  #.
			//           #.  #.  #.
			case 0b1101:
			case 0b0001:
			case 0b0101:
				at.y -= 1;
				dir = South;
				break;
			// Go hori  #.
			//          .#
			case 0b0110:
				if (dir == South) {
					at.x += 1;
					dir = East;
				} else if (dir == North) {
					at.x -= 1;
					dir = West;
				} else {
					throw std::runtime_error("invalid state");
				}
				break;
			// Go vert  .#
			//          #.
			case 0b1001:
				if (dir == East) {
					at.y += 1;
					dir = North;
				} else if (dir == West) {
					at.y -= 1;
					dir = South;
				} else {
					throw std::runtime_error("invalid state");
				}
				break;
			case 0b0000:
			case 0b1111:
				throw std::runtime_error("invalid state");
			}
		}

		if (poly.normaliseRotate() && poly.normaliseOctile(Dir::CCW) &&
			poly.normaliseDia() && poly.normaliseCollinear() &&
			poly.finalise(geometry::PolyType::SimplePolygon)) {
			if (poly.orientation() == Dir::CCW) { // enclosure
				enclosures.emplace_front(std::move(poly));
			} else {
				env->pushObstacle(std::move(poly));
			}
		}
	}
	for (auto& enc : enclosures) {
		env->pushEnclosure(std::move(enc));
	}
	env->finalise();
	return env;
}

} // namespace rayscan::scen
