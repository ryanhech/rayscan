#include <rayscanlib/scen/PolygonEnvironment.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/range/join.hpp>
#include <boost/range/any_range.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <inx/io/transformers.hpp>
#include <fstream>
#include <sstream>
#include <cmath>

namespace rayscanlib::scen
{


int PolygonHierarchy::is_obstacle_within(const Obstacle& O, const Obstacle& E)
{
	if (O.P == E.P)
		return 0;
	int res = is_obstacle_within_impl_(O, E);
	if (res == 0) {
		res = -is_obstacle_within_impl_(E, O);
	}
	return res;
}
int PolygonHierarchy::is_obstacle_within_impl_(const Obstacle& O, const Obstacle& E)
{
	if (!E.boxed.strictly_overlap(O.boxed))
		return -1;
	int res = 0;
	for (auto p : *O.P) {
		if (auto Owithin = geometry::point_in_polygon(p, *E.P); geometry::pos_is_inside(Owithin)) {
			if (res < 0)
				return 0;
			res = 1;
		} else if (geometry::pos_is_outside(Owithin)) {
			if (res > 0)
				return 0;
			res = -1;
		}
	}
	return res;
}

auto PolygonHierarchy::find_point_within(point p, bool strict) const -> const Enclosure*
{
	for (const auto* E : m_topological) {
		if (E->point_within(p, strict)) {
			return E;
		}
	}
	return nullptr;
}
bool PolygonHierarchy::Obstacle::point_within(point p, bool strict) const
{
	assert(static_cast<int>(strict) == 0 || static_cast<int>(strict) == 1);
	if (!boxed.within(p)) {
		return false;
	} else {
		return static_cast<int8>(geometry::point_in_polygon(p, *P)) >= static_cast<int8>(strict);
	}
}

void PolygonHierarchy::insert_obstacle(const polygon& p)
{
	if (m_built)
		throw std::logic_error("Hierarchy already built");
	Obstacle& O = m_obstaclePool.emplace_back();
	init_obstacle(O, p);
}

void PolygonHierarchy::insert_enclosure(const polygon& p)
{
	if (m_built)
		throw std::logic_error("Hierarchy already built");
	Enclosure& E = m_enclosurePool.emplace_back(&m_encRes);
	init_obstacle(E, p);
}

void PolygonHierarchy::init_obstacle(Obstacle& O, const polygon& p)
{
	// bool enclosure = p.orientation() == PolygonEnvironment::enclsoure_ori;
	O.P = &p;
	O.boxed = geometry::box_polygon(p);
	// int found = 0;
	// for (auto it = P.begin(), ite = P.end(); it != ite; ++it) {
	// 	auto [a,b,c] = it.edge();
	// 	a -= b; c -= b;
	// 	auto crs = a.cross(c);
	// 	if (point::isColin(crs))
	// 		continue;
	// 	auto x = a + c;
	// 	x = 0.5 * x.normalise();
	// 	if (point::isCW(crs)) {
	// 		x = -x;
	// 	}
	// 	if (auto pwithin = geometry::point_in_polygon(b + x, p); geometry::pos_is_inside(pwithin)) {
	// 		found = 2;
	// 		O.within = b + x;
	// 		break;
	// 	} else if (found == 0) {
	// 		if (auto as = a.square(); bs = b.square(); as >= 1 + point::neg_epsilon() && bs >= 1 + point::neg_epsilon()) {
	// 			x = std::sqrt(std::min(as, bs)) * x;
	// 			if (auto pwithin = geometry::point_in_polygon(b + x, p); geometry::pos_is_inside(pwithin)) {
	// 				found = 1;
	// 				O.within = b + x;
	// 			}
	// 		}
	// 	}
	// }
	// if (found == 0) {
	// 	auto [a, b] = P.segment(0);
	// 	O.within = a + 0.5 * (b - a);
	// }
}


void PolygonHierarchy::build()
{
	if (m_built)
		throw std::logic_error("Hierarchy already built");
	m_built = true;
	Enclosures obst;
	obst.resize(m_enclosurePool.size());
	for (size_t i = 0, ie = m_enclosurePool.size(); i < ie; ++i) {
		obst[i] = &m_enclosurePool[i];
	}
	auto divide = build_partition_roots(obst.begin(), obst.end());
	m_roots.assign(obst.begin(), divide);
	m_topological.clear();
	m_topological.reserve(m_enclosurePool.size());
	build_recurse(m_roots, divide, obst.end());
	build_obstacles();
	build_enclosed_within();
}

void PolygonHierarchy::build_recurse(Enclosures& E, Enclosures::iterator from, Enclosures::iterator to)
{
	for (auto* e : E) {
		if (from == to)
			break;
		auto children = build_partition_within(*e, from, to);
		auto divide = build_partition_roots(from, children);
		e->children.assign(from, divide);
		build_recurse(e->children, divide, children);
		from = children;
	}
	m_topological.insert(m_topological.end(), E.begin(), E.end());
}

auto PolygonHierarchy::build_partition_roots(Enclosures::iterator from, Enclosures::iterator to) -> Enclosures::iterator
{
	auto pat = from;
	for (auto it = from; it != to; ++it) {
		std::swap(*pat, *it);
		if (std::all_of(from, to, [O=*pat] (const Enclosure* Oother) {
			return PolygonHierarchy::is_obstacle_within(*O, *Oother) <= 0;
		})) {
			++pat;
		}
	}
	return pat;
}
auto PolygonHierarchy::build_partition_within(Obstacle& O, Enclosures::iterator from, Enclosures::iterator to) -> Enclosures::iterator
{
	return partition(from, to, [&O] (const Enclosure* Oother) {
		return PolygonHierarchy::is_obstacle_within(*Oother, O) > 0;
	});
}

void PolygonHierarchy::build_obstacles()
{
	for (auto& O : m_obstaclePool) {
		bool added = false;
		for (auto* E : m_topological) {
			if (PolygonHierarchy::is_obstacle_within(O, *E) > 0) {
				E->obstructions.push_back(&O);
				added = true;
				break;
			}
		}
		if (!added) {
			m_loseObstacles.push_back(&O);
		}
	}
}

void PolygonHierarchy::build_enclosed_within()
{
	for (auto* E : m_topological) {
		Obstacles* obsts = E->enclosure != nullptr ? &E->enclosure->obstructions : &m_loseObstacles;
		for (auto* O : *obsts) {
			if (PolygonHierarchy::is_obstacle_within(*E, *O) >= 0) {
				E->enclosed_within = O;
				break;
			}
		}
	}
}

PolygonEnvironment::PolygonEnvironment() : m_enclosureBox{}, m_obstaclesBox{}, m_countVertices(0), m_overlap(false), m_convex(true), m_integer(true)
{ }

void PolygonEnvironment::clear()
{
	m_enclosures.clear();
	m_obstacles.clear();
	m_enclosureBox = box::zeroBox();
	m_obstaclesBox = box::zeroBox();
	m_integer = true;
	m_convex = true;
}

bool PolygonEnvironment::pushEnclosure(const polygon& poly)
{
	if (!poly.isFinalised())
		throw std::runtime_error("bad polygon");
	if (poly.orientation() == enclsoure_ori)
		return pushEnclosure(polygon(poly, geometry::PolyType::SimplePolygon));
	else
		return pushEnclosure(polygon(poly.rbegin(), poly.rend(), geometry::PolyType::SimplePolygon));
}
bool PolygonEnvironment::pushEnclosure(polygon&& poly)
{
	if (!poly.isFinalised())
		throw std::runtime_error("bad polygon");
	if (poly.orientation() != enclsoure_ori)
		throw std::runtime_error("invalid orientation");
	if (!geometry::is_simple_polygon<true>(poly))
		throw std::runtime_error("not a simple polygon");
	
	updateFlags(poly);
	box bp = geometry::box_polygon(poly);
	m_countVertices = 0;
	if (m_overlap || m_enclosures.empty()) {
		if (m_enclosures.empty())
			m_enclosureBox = bp;
		else
			m_enclosureBox = m_enclosureBox << bp;
		m_enclosures.emplace_back(std::move(poly));
		return true;
	} else {
		if (m_enclosureBox.area2() <= bp.area2()) {
			removeObstaclesFilter(m_enclosures.front(), geometry::Pos::Outside);
			m_enclosures.front().assign(std::move(poly));
			m_enclosureBox = bp;
			return true;
		} else {
			removeObstaclesFilter(poly, geometry::Pos::Outside);
			return false;
		}
	}
}
bool PolygonEnvironment::pushEnclosureNoCheck(const polygon& poly)
{
	if (!poly.isFinalised())
		throw std::runtime_error("bad polygon");
	if (poly.orientation() == enclsoure_ori)
		return pushEnclosureNoCheck(polygon(poly, geometry::PolyType::SimplePolygon));
	else
		return pushEnclosureNoCheck(polygon(poly.rbegin(), poly.rend(), geometry::PolyType::SimplePolygon));
}
bool PolygonEnvironment::pushEnclosureNoCheck(polygon&& poly)
{
	if (!poly.isFinalised())
		throw std::runtime_error("bad polygon");
	if (poly.orientation() != enclsoure_ori)
		throw std::runtime_error("invalid orientation");
	
	updateFlags(poly);
	box bp = geometry::box_polygon(poly);
	m_countVertices = 0;
	if (m_overlap || m_enclosures.empty()) {
		if (m_enclosures.empty())
			m_enclosureBox = bp;
		else
			m_enclosureBox = m_enclosureBox << bp;
		m_enclosures.emplace_back(std::move(poly));
		return true;
	} else {
		if (m_enclosureBox.area2() <= bp.area2()) {
			removeObstaclesFilter(m_enclosures.front(), geometry::Pos::Outside);
			m_enclosures.front().assign(std::move(poly));
			m_enclosureBox = bp;
			return true;
		} else {
			removeObstaclesFilter(poly, geometry::Pos::Outside);
			return false;
		}
	}
}

auto PolygonEnvironment::pushObstacle(const polygon& poly) -> std::list<polygon>::iterator
{
	if (!poly.isFinalised())
		throw std::runtime_error("bad polygon");
	if (poly.orientation() == obstacle_ori || poly.orientation() == Dir::COLIN)
		return pushObstacle(polygon(poly, geometry::PolyType::SimplePolygon));
	else
		return pushObstacle(polygon(poly.rbegin(), poly.rend(), geometry::PolyType::SimplePolygon));
}
auto PolygonEnvironment::pushObstacle(polygon&& poly) -> std::list<polygon>::iterator
{
	if (!m_overlap && !m_enclosures.empty())
		throw std::runtime_error("obstacle must be added before enclosure");
	if (!poly.isFinalised())
		throw std::runtime_error("bad polygon");
	if (poly.orientation() != obstacle_ori && poly.orientation() != Dir::COLIN)
		throw std::runtime_error("invalid orientation");
	if (!geometry::is_simple_polygon<true>(poly))
		throw std::runtime_error("not a simple polygon");
	
	// check if scenario is not integer
	updateFlags(poly);
	if (m_convex && !geometry::is_convex_polygon<true>(poly)) {
		m_convex = false;
	}
	m_obstacles.emplace_back(std::move(poly));
	m_countVertices = 0;
	return std::prev(m_obstacles.end());
}
auto PolygonEnvironment::pushObstacleNoCheck(const polygon& poly) -> std::list<polygon>::iterator
{
	if (!poly.isFinalised())
		throw std::runtime_error("bad polygon");
	if (poly.orientation() == obstacle_ori)
		return pushObstacleNoCheck(polygon(poly, geometry::PolyType::SimplePolygon));
	else
		return pushObstacleNoCheck(polygon(poly.rbegin(), poly.rend(), geometry::PolyType::SimplePolygon));
}
auto PolygonEnvironment::pushObstacleNoCheck(polygon&& poly) -> std::list<polygon>::iterator
{
	if (!poly.isFinalised())
		throw std::runtime_error("bad polygon");
	if (poly.orientation() != obstacle_ori && poly.orientation() != Dir::COLIN)
		throw std::runtime_error("invalid orientation");
	
	//assert(polygonValid(poly));
	updateFlags(poly);
	if (m_convex && !geometry::is_convex_polygon<true>(poly)) {
		m_convex = false;
	}
	m_obstacles.emplace_back(std::move(poly));
	m_countVertices = 0;
	return std::prev(m_obstacles.end());
}

void PolygonEnvironment::eraseObstacleNoCheck(std::list<polygon>::iterator it)
{
	assert(it != m_obstacles.end());
	m_obstacles.erase(it);
	m_countVertices = 0;
}

void PolygonEnvironment::updateFlags(const polygon& poly)
{
	if (m_integer) {
		for (const auto& pt : poly) {
			if (!inx::io::float_fits_integer<int>(pt.x) || !inx::io::float_fits_integer<int>(pt.y)) {
				m_integer = false;
				break;
			}
		}
	}
	if (m_convex && !geometry::is_convex_polygon<true>(poly)) {
		m_convex = false;
	}
}

void PolygonEnvironment::finalise()
{
	if (m_enclosures.empty()) { // auto-generate a boxed-enclosure
		reboxObstacles();
		m_enclosureBox = m_obstaclesBox;
		m_enclosureBox.first -= point(1, 1);
		m_enclosureBox.second += point(1, 1);
		polygon enc;
		enc.addPoint(m_enclosureBox.lowerLeft());
		enc.addPoint(m_enclosureBox.lowerRight());
		enc.addPoint(m_enclosureBox.upperRight());
		enc.addPoint(m_enclosureBox.upperLeft());
		enc.finalise(geometry::PolyType::SimplePolygon);
		pushEnclosure(std::move(enc));
	} else {
		if (!m_overlap) {
			removeObstaclesFilter(m_enclosures.front(), geometry::Pos::Inside);
			reboxObstacles();
		}
	}
}

size_t PolygonEnvironment::countVertices_() const noexcept
{
	size_t cv = 0;
	for (auto& ply : m_obstacles) {
		cv += ply.size();
	}
	for (auto& ply : m_enclosures) {
		cv += ply.size();
	}
	return cv;
}

void PolygonEnvironment::removeObstaclesFilter(const polygon& enclosure, geometry::Pos keepIfTrue)
{
	if (!enclosure.isFinalised())
		throw std::runtime_error("not finalised");
	m_obstacles.remove_if([&enc=enclosure,keepIfTrue] (const polygon& p) {
		return geometry::point_in_polygon(p[0], enc) != keepIfTrue;
	});
}

void PolygonEnvironment::reboxObstacles()
{
	if (m_obstacles.empty())
		m_obstaclesBox = box{};
	else {
		m_obstaclesBox = geometry::box_polygon(m_obstacles.front());
		for (auto it = std::next(m_obstacles.begin()), ite = m_obstacles.end(); it != ite; ++it) {
			m_obstaclesBox << geometry::box_polygon(*it);
		}
	}
}

void PolygonEnvironment::save(std::ostream& file) const
{
	using inx::io::accurate_number;
	struct Helper
	{
		static void write_polygon(std::ostream& file, const char* type, const polygon& poly)
		{
			if (!poly.isFinalised())
				throw std::runtime_error("polygon not finalised");
			if (!(file << type << ' ' << poly.size()))
				throw std::runtime_error("bad write");
			for (auto& pt : poly) {
				if (!(file << ' ' << accurate_number(pt.x) << ' ' << accurate_number(pt.y)))
					throw std::runtime_error("bad write");
			}
			if (!(file << '\n'))
				throw std::runtime_error("bad write");
		}
	};

	if (!(file << "start polygonEnv version " << VERSION << '\n'))
		throw std::runtime_error("bad write");
	if (m_overlap) {
		file << "overlap on" << std::endl;
	}
	for (auto& poly : m_enclosures) {
		Helper::write_polygon(file, "enclosure", poly);
	}
	for (auto& poly : m_obstacles) {
		Helper::write_polygon(file, "obstacle", poly);
	}
	if (!(file << "end polygonEnv" << std::endl))
		throw std::runtime_error("bad write");
}
void PolygonEnvironment::save(Archiver& archive, std::filesystem::path filename) const
{
	inx::io::chunked_array tmpfile;
	boost::iostreams::stream<boost::reference_wrapper<inx::io::chunked_array>> tmpstream(boost::ref(tmpfile));
	save(tmpstream);
	tmpstream.seekg(0, std::ios::beg);
	archive.load(tmpstream, filename);
}

void PolygonEnvironment::load(const Archiver& archive)
{
	struct Helper
	{
		static bool endOfStream(std::istream& is)
		{
			is >> std::ws;
			if (is.bad())
				throw std::runtime_error("bad read");
			return is.eof();
		}
		static bool getline(std::istream& is, std::istringstream& iss, std::string& line)
		{
			if (!std::getline(is, line)) {
				if (is.eof())
					return false;
				throw std::runtime_error("bad read");
			}
			iss.str(line);
			iss.clear();
			if (endOfStream(iss))
				throw std::runtime_error("empty line");
			return true;
		}
	};

	auto& block = archive.at("polygonEnv");
	auto input = boost::iostreams::stream(block.openRead());
	clear();
	std::string line, str1, str2;
	int32 int1;
	polygon::coord_type crd2, crd3;
	std::istringstream iss;

	bool overlap = false, overlapFound = false, enclosure = false;
	while (Helper::getline(input, iss, line)) {
		if (!(iss >> str1))
			throw std::runtime_error("bad read");
		if (str1 == "overlap") {
			if (overlapFound)
				throw std::runtime_error("overlap found");
			overlapFound = true;
			if (!(iss >> str1))
				throw std::runtime_error("bad read");
			if (str1 == "on") {
				overlap = true;
				makeOverlap();
			} else if (str1 != "off")
				throw std::runtime_error("bad read");
			if (!Helper::endOfStream(iss))
				throw std::runtime_error("bad read");
			continue;
		}
		if (str1 != "enclosure" && str1 != "obstacle")
			throw std::runtime_error("bad read");
		polygon poly;
		if (!(iss >> int1))
			throw std::runtime_error("bad read");
		for (int32 i = 0; i < int1; i++) {
			if (!(iss >> crd2 >> crd3))
				throw std::runtime_error("bad read");
			poly.addPoint(polygon::point_type(crd2, crd3));
		}
		if (!Helper::endOfStream(iss))
			throw std::runtime_error("invalid polygon");
		if (!poly.finalise(geometry::PolyType::SimplePolygon))
			throw std::runtime_error("invalid polygon");
		if (str1 == "enclosure") {
			if (!overlap && enclosure)
				throw std::runtime_error("duplicate enclosures");
			enclosure = true;
			pushEnclosureNoCheck(std::move(poly));
		} else {
			pushObstacleNoCheck(std::move(poly));
		}
	}
	if (!Helper::endOfStream(input)) // line and str1 already read
		throw std::runtime_error("bad read");
	
	finalise();
}

auto PolygonEnvironment::pointLocation(point pt) const -> std::pair<geometry::Pos, const polygon*>
{
	if (!m_enclosureBox.within(pt))
		return {geometry::Pos::Outside, nullptr};
	bool inEnc = false;
	std::pair<geometry::Pos, const polygon*> ans(geometry::Pos::Inside, nullptr);
	for (auto& enc : m_enclosures) {
		if (auto p = geometry::point_in_polygon(pt, enc); geometry::pos_is_inside(p)) {
			inEnc = true;
			break;
		} else if (geometry::pos_is_onedge(p)) {
			inEnc = true;
			ans.first = geometry::Pos::OnEdge;
			break;
		}
	}
	if (!inEnc)
		return {geometry::Pos::Outside, nullptr};
	for (auto& P : m_obstacles) {
		if (auto p = geometry::point_in_polygon(pt, P); geometry::pos_is_inside(p)) {
			return {geometry::Pos::Outside, &P};
		} else if (geometry::pos_is_onedge(p)) {
			ans = {geometry::Pos::OnEdge, &P};
		}
	}
	return ans;
}

bool PolygonEnvironment::polygonValid(const polygon& poly) const
{
	if (!(poly.orientation() == obstacle_ori || poly.orientation() == Dir::COLIN) || !geometry::is_simple_polygon<true>(poly))
		return false; // invalid polygon
	if (poly.size() == 2) {
		return poly[0] != poly[1];
	}
	// check that no edges are collinear
	for (auto it = poly.begin(), ite = poly.end(); it != ite; ++it) {
		auto [pv, at, nx] = it.edge();
		pv -= at; nx -= at;
		pv = pv.normalise();
		nx = nx.normalise();
		if (double a = std::acos(std::clamp(static_cast<double>(pv * nx), -1.0, 1.0)); a < -pi<double> + 1e-6 || a > pi<double> - 1e-6)
			return false;
		else if (!m_overlap && std::abs(a) < 1e-6)
			return false;
	}
	if (m_overlap)
		return true;
	if (!m_overlap && !geometry::pos_is_inside(pointLocation(poly.front()).first))
		return false; // poly has point outside environment
	for (auto& P : boost::range::join(
		            typename boost::any_range_type_generator<decltype(m_enclosures)>::type(m_enclosures),
					typename boost::any_range_type_generator<decltype(m_obstacles)>::type(m_obstacles)
				))
	{
		if (!geometry::pos_is_outside(geometry::point_in_polygon(P.front(), poly)))
			return false; // poly is enclosing a point on another polygon
		for (size_t i = 0, ie = static_cast<size_t>(P.size()); i < ie; ++i) {
			auto [a, b] = P.segment(i);
			if (line_intersect_polygon(a, b-a, poly).first != point::scale_inf())
				return false; // poly is intersecting the environment
		}
	}
	return true;
}

auto PolygonEnvironment::findPoint(std::mt19937_64& rng, const geometry::Box<double>& withinScale, int repeat, bool allowEdge, int permutate) const -> point
{
	assert(withinScale.is_norm() && repeat >= 0);
	std::uniform_int_distribution<int> xrng(static_cast<int>(std::ceil(withinScale.lower().x * m_enclosureBox.width() + m_enclosureBox.lower().x)),
	                                          static_cast<int>(std::ceil(withinScale.upper().x * m_enclosureBox.width() + m_enclosureBox.lower().x)));
	std::uniform_int_distribution<int> yrng(static_cast<int>(std::ceil(withinScale.lower().y * m_enclosureBox.height() + m_enclosureBox.lower().y)),
	                                          static_cast<int>(std::ceil(withinScale.upper().y * m_enclosureBox.height() + m_enclosureBox.lower().y)));
	point pt;
	geometry::Pos p;
	const polygon* poly = nullptr;
	while (repeat-- >= 0) {
		point pt(xrng(rng), yrng(rng));
		if (std::tie(p, poly) = pointLocation(pt); allowEdge ? !geometry::pos_is_outside(p) : geometry::pos_is_inside(p))
			return pt;
	}
	if (poly == nullptr)
		poly = &m_enclosures.front();
	if (!allowEdge && permutate == 0)
		throw std::runtime_error("failed to find point");
	point pt2 = pt;
	pt = poly->front();
	for (auto ptx : *poly) {
		if ((ptx - pt2).square() < (pt - pt2).square())
			pt = ptx;
	}
	if (permutate > 0) {
		bool valid = false;
		std::uniform_int_distribution<int> permrng(-permutate, permutate);
		for (int i = 0; i < 16; i++) {
			int x, y;
			do {
				x = permrng(rng);
				y = permrng(rng);
			}
			while ( (x|y) == 0 );
			pt2.x = pt.x + x; pt2.y = pt.y + y;
			if (std::tie(p, poly) = pointLocation(pt2); allowEdge ? !geometry::pos_is_outside(p) : geometry::pos_is_inside(p)) {
				valid = true;
				pt = pt2;
				break;
			}
		}
		if (!valid) {
			// try all 9 neighbours for a valid point
			for (int y = -1; !valid && y < 2; ++y)
			for (int x = -1; !valid && x < 2; ++x) {
				if ((x|y) == 0)
					continue;
				pt2.x = pt.x + x; pt2.y = pt.y + y;
				if (std::tie(p, poly) = pointLocation(pt2); allowEdge ? !geometry::pos_is_outside(p) : geometry::pos_is_inside(p)) {
					valid = true;
					pt = pt2;
				}
			}
		}
		if (!valid)
			throw std::runtime_error("failed to find point");
	}
	return pt;
}

void PolygonEnvironment::makeHierarchy()
{
	if (!m_hierarchy) {
		m_hierarchy = std::make_unique<PolygonHierarchy>();
		for (const auto& p : m_enclosures) {
			m_hierarchy->insert_enclosure(p);
		}
		for (const auto& p : m_obstacles) {
			m_hierarchy->insert_obstacle(p);
		}
		m_hierarchy->build();
	}
}

std::ostream& operator<<(std::ostream& s, const PolygonEnvironment& poly)
{
	poly.save(s);
	return s;
}
std::istream& operator>>(std::istream& s, PolygonEnvironment& poly)
{
	Archiver archive;
	archive.load(s, std::filesystem::path());
	poly.load(archive);
	return s;
}

} // namespace rayscan::scen
