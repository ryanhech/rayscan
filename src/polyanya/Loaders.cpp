#include <polyanya/Loaders.hpp>
#include <rayscanlib/scen/StaticBenchmarkSingleTarget.hpp>
#include <rayscanlib/scen/StaticBenchmarkMultiTarget.hpp>
#include <rayscanlib/scen/DynamicBenchmarkSingleTarget.hpp>
#include <rayscanlib/scen/DynamicBenchmarkMultiTarget.hpp>
#include <polyanyalib/Polyanya.hpp>
#include <polyanyalib/ScenarioMeshWrapper.hpp>

namespace polyanya
{

void Loaders::registerConfig()
{
	using mesh_sst = polyanyalib::ScenarioMeshWrapperAuto<rayscanlib::scen::StaticBenchmarkSingleTarget>;
	using mesh_dst = polyanyalib::ScenarioMeshWrapperAuto<rayscanlib::scen::DynamicBenchmarkSingleTarget>;
	using mesh_smt = polyanyalib::ScenarioMeshWrapperAuto<rayscanlib::scen::StaticBenchmarkMultiTarget>;
	using mesh_dmt = polyanyalib::ScenarioMeshWrapperAuto<rayscanlib::scen::DynamicBenchmarkMultiTarget>;

	polyanyalib::RayScanBenchmarkMesh::registerType<mesh_sst::benchmark_mesh_type>();
	polyanyalib::RayScanBenchmarkMesh::registerType<mesh_dst::benchmark_mesh_type>();
	polyanyalib::RayScanBenchmarkMesh::registerType<mesh_smt::benchmark_mesh_type>();
	polyanyalib::RayScanBenchmarkMesh::registerType<mesh_dmt::benchmark_mesh_type>();

	registerScenario< mesh_sst >(rsapp::conf::ScenarioType::SingleTarget, "static");
	registerScenario< mesh_sst >(rsapp::conf::ScenarioType::MultiSingleTarget, "static");
	registerScenario< mesh_smt >(rsapp::conf::ScenarioType::MultiTarget, "static");
	registerScenario< mesh_dst >(rsapp::conf::ScenarioType::DynamicSingleTarget, "static");
	registerScenario< mesh_dst >(rsapp::conf::ScenarioType::DynamicMultiSingleTarget, "static");
	registerScenario< mesh_dmt >(rsapp::conf::ScenarioType::DynamicMultiTarget, "static");
}

} // namespace rayscan
