#include <polyanya/Defaults.hpp>
#include <polyanyalib/PolySearch.hpp>
#include <polyanyalib/PolyMultiSearch.hpp>
#include <polyanyalib/Mesh.hpp>
#include <polyanyalib/NaiveFacer.hpp>
#include <polyanyalib/IntervalFacer.hpp>
#include <polyanyalib/WalkFacer.hpp>
#include <polyanyalib/Polyanya.hpp>
#include <polyanyalib/EuclideanHeuristic.hpp>
#include <polyanyalib/IntervalHeuristic.hpp>
#include <polyanyalib/TargetHeuristic.hpp>
//#include <polyanyalib/TesterFacer.hpp>

namespace polyanya
{

void Defaults::registerConfig()
{
	using namespace polyanyalib;
	using namespace rsapp::conf::reg;
	using polyanya_st_default = Polyanya<PolySearch, Mesh, IntervalFacer, EuclideanHeuristic>;
	using polyanya_mt_default = Polyanya<PolyMultiSearch, Mesh, IntervalFacer, EuclideanHeuristic>;
	using polyanya_st_naive = Polyanya<PolySearch, Mesh, FacerTime<NaiveFacer>, EuclideanHeuristic>;
	using polyanya_mt_naive = Polyanya<PolyMultiSearch, Mesh, FacerTime<NaiveFacer>, EuclideanHeuristic>;
	using polyanya_st_walker = Polyanya<PolySearch, Mesh, WalkFacer, EuclideanHeuristic>;
	using polyanya_mt_walker = Polyanya<PolyMultiSearch, Mesh, WalkFacer, EuclideanHeuristic>;
	using polyanya_st_interval = Polyanya<PolySearch, Mesh, WalkFacer, IntervalHeuristic>;
	using polyanya_mt_interval = Polyanya<PolyMultiSearch, Mesh, WalkFacer, IntervalHeuristic>;

	registerInstance<ST<polyanya_st_walker>, DST<polyanya_st_walker>, MT<polyanya_mt_walker>, DMT<polyanya_mt_walker>>("default");
	registerInstance<ST<polyanya_st_default>, MT<polyanya_mt_default>>("static");
	registerInstance<ST<polyanya_st_naive>, DST<polyanya_st_naive>, MT<polyanya_mt_naive>, DMT<polyanya_mt_naive>>("naive");
	registerInstance<ST<polyanya_st_interval>, DST<polyanya_st_interval>, MT<polyanya_mt_interval>, DMT<polyanya_mt_interval>>("interval");
}

} // namespace polyanya
