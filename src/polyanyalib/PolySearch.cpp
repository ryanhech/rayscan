#include <polyanyalib/PolySearch.hpp>
#include <polyanyalib/Expansion.hpp>
#include <polyanyalib/Geometry.hpp>
#include <queue>
#include <vector>
#include <cassert>
#include <iostream>
#include <algorithm>

namespace polyanyalib
{

PointLocation PolySearch::get_point_location(Point p)
{
	assert(mesh != nullptr);
	PointLocation out = mesh->findFaceFromPoint(p);
	return out;
}

int PolySearch::succ_to_node(
	PolyNode* parent, Successor* successors, int num_succ,
	PolyNode* nodes
)
{
	assert(mesh != nullptr);
	const Polygon& polygon = mesh->mesh_polygons[parent->next_polygon];
	const std::vector<int>& V = polygon.vertices;
	const std::vector<int>& P = polygon.polygons;

	double right_g = -1, left_g = -1;

	int out = 0;
	for (int i = 0; i < num_succ; i++)
	{
		const Successor& succ = successors[i];
		const int next_polygon = P[succ.poly_left_ind];
		if (next_polygon <= 0)
		{
			continue;
		}

		// If the successor we're about to push pushes into a one-way polygon,
		// and the polygon isn't the end polygon, just continue.
		if (deadend && mesh->mesh_polygons[next_polygon].is_one_way &&
			next_polygon != end_polygon)
		{
			continue;
		}
		const int left_vertex  = V[succ.poly_left_ind];
		const int right_vertex = succ.poly_left_ind ?
								 V[succ.poly_left_ind - 1] :
								 V.back();

		// Note that g is evaluated twice here. (But this is a lambda!)
		// Always try to precompute before using this macro.
		// We implicitly set h to be zero and let search() update it.
		const auto p = [&](const int root, const double g)
		{
			if (root > 0)
			{
				assert(root > 0 && root < (int) root_g_values.size());
				// Can POSSIBLY prune?
				if (root_search_ids[root] != search_id)
				{
					// First time reaching root
					root_search_ids[root] = search_id;
					root_g_values[root] = g;
				}
				else
				{
					// We've been here before!
					// Check whether we've done better.
					if (root_g_values[root] + rayscanlib::geometry::epsilon < g)
					{
						// We've done better!
						return;
					}
					else
					{
						// This is better.
						root_g_values[root] = g;
					}
				}
			}
			nodes[out++] = PolyNode(nullptr, root, succ.left, succ.right, left_vertex,
				right_vertex, next_polygon, g, g);
		};

		const Point& parent_root = (parent->root <= 0 ?
									start :
									mesh->mesh_vertices[parent->root].p);
		#define get_g(new_root) parent->g + parent_root.distance(new_root)

		switch (succ.type)
		{
			case Successor::RIGHT_NON_OBSERVABLE:
				if (right_g == -1)
				{
					right_g = get_g(parent->right);
				}
				p(parent->right_vertex, right_g);
				break;

			case Successor::OBSERVABLE:
				p(parent->root, parent->g);
				break;

			case Successor::LEFT_NON_OBSERVABLE:
				if (left_g == -1)
				{
					left_g = get_g(parent->left);
				}
				p(parent->left_vertex, left_g);
				break;

			default:
				assert(false);
				break;
		}
		#undef get_g
	}

	return out;
}

void PolySearch::set_end_polygon()
{
	// Any polygon is fine.
	end_polygon = get_point_location(goal).poly1;
}

void PolySearch::gen_initial_nodes(bool retain)
{
	// {parent, root, left, right, next_polygon, right_vertex, f, g}
	// be VERY lazy and abuse how our function expands collinear search nodes
	// if right_vertex is not valid, it will generate EVERYTHING
	// and we can set right_vertex if we want to omit generating an interval.
	if (!retain)
		start_pl = get_point_location(start);
	const double h = start.distance(goal);
	#define get_lazy(next, left, right) new (node_pool->allocate()) PolyNode \
		(nullptr, 0, start, start, left, right, next, h, 0)

	#define v(vertex) mesh->mesh_vertices[vertex]

	const auto push_lazy = [&](PolyNode* lazy)
	{
		const int poly = lazy->next_polygon;
		if (poly <= 0)
		{
			return;
		}
		if (poly == end_polygon)
		{
			// Trivial case - we can see the goal from start!
			final_node = lazy;
			#ifndef NDEBUG
			if (verbose)
			{
				std::cerr << "got a trivial case " << goal << "!" << std::endl;
			}
			#endif
			// we should check final_node after each push_lazy
			return;
		}
		// iterate over poly, throwing away vertices if needed
		const std::vector<int>& vertices =
			mesh->mesh_polygons[poly].vertices;
		Successor* successors = new Successor [vertices.size()];
		int last_vertex = vertices.back();
		int num_succ = 0;
		for (int i = 0; i < (int) vertices.size(); i++)
		{
			const int vertex = vertices[i];
			if (vertex == lazy->right_vertex ||
				last_vertex == lazy->left_vertex)
			{
				last_vertex = vertex;
				continue;
			}
			successors[num_succ++] =
				{Successor::OBSERVABLE, v(vertex).p,
				 v(last_vertex).p, i};
			last_vertex = vertex;
		}
		PolyNode* nodes = new PolyNode [num_succ];
		const int num_nodes = succ_to_node(lazy, successors,
										   num_succ, nodes);
		delete[] successors;
		for (int i = 0; i < num_nodes; i++)
		{
			PolyNode* n = new (node_pool->allocate())
				PolyNode(nodes[i]);
			const Point& n_root = (n->root <= 0 ? start :
								   mesh->mesh_vertices[n->root].p);
			n->f += heuristic->getHvalue(n_root, n->left, n->right);
			n->parent = lazy;
			#ifndef NDEBUG
			if (verbose)
			{
				std::cerr << "generating init node: ";
				print_node(n, std::cerr);
				std::cerr << std::endl;
			}
			#endif
			open_list.push(*n);
		}
		delete[] nodes;
		nodes_generated += num_nodes;
		nodes_pushed += num_nodes;
	};

	switch (start_pl.type)
	{
		// Don't bother.
		case PointLocation::NOT_ON_MESH:
			break;

		// Generate all in an arbirary polygon.
		case PointLocation::ON_CORNER_VERTEX_AMBIG:
			// It's possible that it's -1!
			if (start_pl.poly1 <= 0)
			{
				break;
			}
		[[fallthrough]];
		case PointLocation::ON_CORNER_VERTEX_UNAMBIG:
		// Generate all in the polygon.
		case PointLocation::IN_POLYGON:
		case PointLocation::ON_MESH_BORDER:
		{
			PolyNode* lazy = get_lazy(start_pl.poly1, -1, -1);
			push_lazy(lazy);
			nodes_generated++;
		}
			break;

		case PointLocation::ON_EDGE:
			// Generate all in both polygons except for the shared side.
		{
			PolyNode* lazy1 = get_lazy(start_pl.poly2, start_pl.vertex1, start_pl.vertex2);
			PolyNode* lazy2 = get_lazy(start_pl.poly1, start_pl.vertex2, start_pl.vertex1);
			push_lazy(lazy1);
			nodes_generated++;
			if (final_node)
			{
				return;
			}
			push_lazy(lazy2);
			nodes_generated++;
		}
			break;


		case PointLocation::ON_NON_CORNER_VERTEX:
		{
			for (int poly : v(start_pl.vertex1).polygons)
			{
				PolyNode* lazy = get_lazy(poly, start_pl.vertex1, start_pl.vertex1);
				push_lazy(lazy);
				nodes_generated++;
				if (final_node)
				{
					return;
				}
			}
		}
			break;


		default:
			assert(false);
			break;
	}

	#undef v
	#undef get_lazy
}

#define root_to_point(root) ((root) <= 0 ? start : mesh->mesh_vertices[root].p)

bool PolySearch::search(bool retain)
{
	init_search(retain);
	if (mesh == nullptr || end_polygon <= 0)
	{
		return false;
	}

	if (final_node != nullptr)
	{
		return true;
	}

	while (!open_list.empty())
	{
		PolyNode* node = &open_list.top(); open_list.pop();

		#ifndef NDEBUG
		if (verbose)
		{
			std::cerr << "popped off: ";
			print_node(node, std::cerr);
			std::cerr << std::endl;
		}
		#endif

		nodes_popped++;
		const int next_poly = node->next_polygon;
		if (next_poly == end_polygon)
		{
			// Make the TRUE final node.
			// (We usually push it onto the open list, but we know it's going
			// to be immediately popped off anyway.)

			// We need to find whether we need to turn left/right to ge
			// to the goal, so we do an orientation check like how we
			// special case triangle successors.

			const int final_root = [&]()
			{
				const Point& root = root_to_point(node->root);
				const Point root_goal = goal - root;
				// If root-left-goal is not CW, use left.
				if (root_goal.cross(node->left - root) < -rayscanlib::geometry::epsilon)
				{
					return node->left_vertex;
				}
				// If root-right-goal is not CCW, use right.
				if ((node->right - root).cross(root_goal) < -rayscanlib::geometry::epsilon)
				{
					return node->right_vertex;
				}
				// Use the normal root.
				return node->root;
			}();

			const PolyNode* true_final =
				new (node_pool->allocate()) PolyNode
				(node, final_root, goal, goal, -1, -1, end_polygon,
				 node->f, node->g);

			nodes_generated++;

			#ifndef NDEBUG
			if (verbose)
			{
				std::cerr << "found end " << goal << " - terminating!" << std::endl;
			}
			#endif

			final_node = true_final;
			return true;
		}
		// We will never update our root list here.
		const int root = node->root;
	//	if (root <= 0)
	//	{
			assert(root >= 0 && root < (int) root_g_values.size());
			if (root_search_ids[root] == search_id)
			{
				// We've been here before!
				// Check whether we've done better.
				if (root_g_values[root] + rayscanlib::geometry::epsilon < node->g)
				{
					nodes_pruned_post_pop++;

					#ifndef NDEBUG
					if (verbose)
					{
						std::cerr << "node is dominated!" << std::endl;
					}
					#endif

					// We've done better!
					continue;
				}
			}
	//	}
		int num_nodes = 1;
		search_nodes_to_push[0] = *node;

		// We use a do while here because the first iteration is guaranteed
		// to work.
		do
		{
			PolyNode cur_node = search_nodes_to_push[0];
			// don't forget this!!!
			if (cur_node.next_polygon == end_polygon)
			{
				break;
			}
			int num_succ = get_successors(cur_node, start, *mesh,
										  search_successors.get());
			successor_calls++;
			num_nodes = succ_to_node(&cur_node, search_successors.get(),
									 num_succ, search_nodes_to_push.get());
			if (num_nodes == 1)
			{
				// Did we turn?
				if (cur_node.g != search_nodes_to_push[0].g)
				{
					// Turned. Set the parent of this, and set the current
					// node pointer to this after allocating space for it.
					search_nodes_to_push[0].parent = node;
					node = new (node_pool->allocate())
						PolyNode(search_nodes_to_push[0]);
					nodes_generated++;
				}

				#ifndef NDEBUG
				if (verbose)
				{
					std::cerr << "\tintermediate (" << num_nodes << "): ";
					print_node(&search_nodes_to_push[0], std::cerr);
					std::cerr << std::endl;
				}
				#endif
			}
		}
		while (num_nodes == 1); // if num_nodes == 0, we still want to break
#ifndef NDEBUG
		if (verbose)
		{
			std::cerr << "\tnum_nodes: " << num_nodes << std::endl;
		}
#endif

		for (int i = 0; i < num_nodes; i++)
		{
			// We need to update the h value before we push!
			PolyNode* n = new (node_pool->allocate())
				PolyNode(search_nodes_to_push[i]);
			const Point& n_root = (n->root <= 0 ? start :
								   mesh->mesh_vertices[n->root].p);
			n->f += heuristic->getHvalue(n_root, n->left, n->right);

			// This node's parent should be nullptr, so we should set it.
			n->parent = node;

			#ifndef NDEBUG
			if (verbose)
			{
				std::cerr << "\tpushing: ";
				print_node(n, std::cerr);
				std::cerr << std::endl;
			}
			#endif

			open_list.push(*n);
		}
		nodes_generated += num_nodes;
		nodes_pushed += num_nodes;
	}

	return false;
}

void PolySearch::print_node(const PolyNode* node, std::ostream& outfile)
{
	outfile << "root=" << root_to_point(node->root) << "; left=" << node->left
			<< "; right=" << node->right << "; f=" << node->f << ", g="
			<< node->g << ", next_poly=" << node->next_polygon;
	/*
	outfile << "; col=" << [&]() -> std::string
			{
				switch (node->col_type)
				{
					case PolyNode::NOT:
						return "NOT";
					case PolyNode::RIGHT:
						return "RIGHT";
					case PolyNode::LEFT:
						return "LEFT";
					case PolyNode::LAZY:
						return "LAZY";
					default:
						return "";
				}
			}();
	*/
}

void PolySearch::get_path_points(std::vector<Point>& out)
{
	if (final_node == nullptr)
	{
		return;
	}
	out.clear();
	out.push_back(goal);
	const PolyNode* cur_node = final_node;

	while (cur_node != nullptr)
	{
		if (root_to_point(cur_node->root) != out.back())
		{
			out.push_back(root_to_point(cur_node->root));
		}
		cur_node = cur_node->parent;
	}
	std::reverse(out.begin(), out.end());
}

void PolySearch::print_search_nodes(std::ostream& outfile)
{
	if (final_node == nullptr)
	{
		return;
	}
	const PolyNode* cur_node = final_node;
	while (cur_node != nullptr)
	{
		print_node(cur_node, outfile);
		outfile << std::endl;
		mesh->print_polygon(outfile, cur_node->next_polygon);
		outfile << std::endl;
		cur_node = cur_node->parent;
	}
}

#undef root_to_point

}
