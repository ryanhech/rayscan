#include <polyanyalib/WalkFacer.hpp>
#include <polyanyalib/Mesh.hpp>

namespace polyanyalib
{

PointLocation WalkFacer::findFace(const Point& p, bool onlyTrav) const
{
	const auto& mesh_polygons = m_mesh->mesh_polygons;
	const auto& mesh_vertices = m_mesh->mesh_vertices;
	int polygon;
	if (m_startFace <= 0 || !mesh_polygons[m_startFace].isInitalised()) {
		int polygon_end = static_cast<int>(mesh_polygons.size());
		for (polygon = 1; polygon < polygon_end; ++polygon)
		{
			if (mesh_polygons[polygon].isInitalised()) {
				m_startFace = polygon;
				break;
			}
		}
	} else {
		polygon = m_startFace;
	}
	assert(polygon > 0 && polygon < static_cast<int>(mesh_polygons.size()) && mesh_polygons[polygon].isInitalised());

	while (true) {
		const Polygon& poly = mesh_polygons[polygon];
		assert(poly.isInitalised());
		int polyNext = -1; // if it ends ge 0, than it is outside and should move over
		Point pv, at = mesh_vertices[poly.vertices.back()].p - p;
		double dist = inf<double>;
		for (int i = 0, ie = static_cast<int>(poly.vertices.size()); i < ie; ++i) {
			pv = at;
			at = mesh_vertices[poly.vertices[i]].p - p;
			if (double c1 = pv.cross(at); Point::isCW(c1)) { // is outside
				if (double ddist = at.square(); ddist < dist) { // is closer than previous answer
					dist = ddist;
					polyNext = i;
				}
			}
		}
		if (polyNext >= 0) { // move towards face with point
			int nei = poly.polygons[polyNext];
			if (nei == 0)
				break; // point is out of mesh
			else if (nei < 0)
				nei = -nei;
			polygon = nei;
		} else { // point contains polygon, finalise results
			PolyContainment pc = m_mesh->polyContainsPoint(polygon, p);
			switch (pc.type) {
			case PolyContainment::INSIDE:
				if (!onlyTrav || poly.isTraversable())
					return {PointLocation::IN_POLYGON, polygon, -1, -1, -1};
				break;

			case PolyContainment::ON_EDGE:
			// This one lies on the edge.
			// Chek whether the other one is -1.
				if (onlyTrav) {
					if (!poly.isTraversable()) {
						polygon = -pc.adjacent_poly;
						if (polygon <= 0 || !mesh_polygons[polygon].isTraversable())
							break;
						return { PointLocation::ON_MESH_BORDER, polygon, -1, pc.vertex2, pc.vertex1 };
					} else if (pc.adjacent_poly > 0 && pc.adjacent_poly < polygon) { // choose the lower id
						if (polygon <= 0 || !mesh_polygons[polygon].isTraversable())
							break;
						return { PointLocation::ON_EDGE, pc.adjacent_poly, polygon, pc.vertex2, pc.vertex1 };
					}
					if (pc.adjacent_poly <= 0)
						return { PointLocation::ON_MESH_BORDER, polygon, -1, pc.vertex1, pc.vertex2 };
					else
						return { PointLocation::ON_EDGE, polygon, pc.adjacent_poly, pc.vertex1, pc.vertex2 };
				} else {
					if (pc.adjacent_poly == 0)
						return { PointLocation::ON_MESH_BORDER, polygon, -1, pc.vertex1, pc.vertex2 };
					else
						return { PointLocation::ON_EDGE, polygon, pc.adjacent_poly, pc.vertex1, pc.vertex2 };
				}

			case PolyContainment::ON_VERTEX:
			// This one lies on a corner.
			{
				const Vertex& v = m_mesh->mesh_vertices[pc.vertex1];
				// find first polygon attached to vertex that is traversable
				if (onlyTrav) {
					int p1 = 0, pe = static_cast<int>(v.polygons.size());
					for ( ; p1 < pe; ++p1) {
						polygon = v.polygons[p1];
						if (mesh_polygons[polygon].isTraversable())
							break;
					}
					if (p1 >= pe)
						break;
				}
				if (v.isCorner())
				{
					if (v.isAmbig())
					{
						return {PointLocation::ON_CORNER_VERTEX_AMBIG, -1, -1,
								pc.vertex1, -1};
					}
					else
					{
						return {PointLocation::ON_CORNER_VERTEX_UNAMBIG,
								polygon, -1, pc.vertex1, -1};
					}
				}
				else
				{
					return {PointLocation::ON_NON_CORNER_VERTEX,
							polygon, -1,
							pc.vertex1, -1};
				}
			}

			default:
				assert(false);
				break;
			}
			break; // exit while loop
		}
	}
	// Haven't returned yet, therefore P does not lie on the mesh.
	return {PointLocation::NOT_ON_MESH, -1, -1, -1, -1};
}

}
