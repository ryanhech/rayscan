#include <polyanyalib/WarthogMesh.hpp>
#include <unordered_map>
#include <memory_resource>

namespace polyanyalib
{

void WarthogMesh::load(std::istream& mesh, bool ir)
{
	std::string str1;
	int version;
	int int1, int2;
	int V, P;
	if (!(mesh >> str1 >> version))
		throw std::runtime_error("bad read");
	if (str1 != "mesh" || version < 2 || version > 3)
		throw std::runtime_error("invalid format");
	if (!(mesh >> V >> P) || V < 1 || P < 1)
		throw std::runtime_error("bad read");
	bool ver3 = version == 3 && !ir;
	mesh_vertices.clear();
	mesh_vertices.resize(V+1);
	mesh_polygons.clear();
	mesh_polygons.resize(P+1);
	// vertices
	for (int i = 1; i <= V; ++i) {
		auto& vtx = mesh_vertices[i];
		vtx.id = i;
		if (!(mesh >> vtx.p.x >> vtx.p.y))
			throw std::runtime_error("error");
		if (!ver3) {
			if (!(mesh >> int1) || int1 < 2)
				throw std::runtime_error("error");
			for (int j = 0; j < int1; ++j) {
				if (!(mesh >> int2) || ver3 ? int2 > P : int2 >= P)
					throw std::runtime_error("error");
			}
		}
	}
	// faces
	std::pmr::unsynchronized_pool_resource nullpool;
	std::pmr::unordered_map< uint64, std::pair<int,int> > null_neighbour(&nullpool);
	for (int i = 1; i <= P; ++i) {
		auto& ply = mesh_polygons[i];
		ply.id = i;
		ply.traversable = 0;
		if (version == 3) {
			if (!(mesh >> int1) || int1 < 0 || int1 > 1)
				throw std::runtime_error("error");
			if (!int1) // mark non-traversable face
				ply.traversable = 1;
		}
		if (!(mesh >> int1) || int1 < 3)
			throw std::runtime_error("error");
		ply.vertices.resize(int1);
		ply.polygons.resize(int1);
		for (int j = 0; j < int1; ++j) {
			if (!(mesh >> int2))
				throw std::runtime_error("error");
			if (!ver3)
				int2++;
			if (int2 < 1 || int2 > V)
				throw std::runtime_error("error");
			ply.vertices[j] = int2;
			auto& vtx = mesh_vertices[int2];
			vtx.polygons.push_back(i);
		}
		int vpv, vat = ply.vertices.back();
		for (int j = 0; j < int1; ++j) {
			vpv = vat;
			vat = ply.vertices[j];
			if (!(mesh >> int2) || ver3 ? !(-P <= int2 && int2 <= P) : !(-1 <= int2 && int2 < P))
				throw std::runtime_error("error");
			if (ver3 ? int2 <= 0 : int2 < 0) {
				uint64 nullid;
				if (vpv < vat) nullid = inx::bit_pack_lsb<uint64,32>(static_cast<uint64>(vpv), static_cast<uint64>(vat));
				else nullid = inx::bit_pack_lsb<uint64,32>(static_cast<uint64>(vat), static_cast<uint64>(vpv));
				if (auto [jt, added] = null_neighbour.try_emplace(nullid, i, j); added) {
					ply.polygons[j] = 0;
				} else {
					// already added, check
					assert(jt != null_neighbour.end());
					int& jtnei = mesh_polygons[jt->second.first].polygons[jt->second.second];
					if (jtnei != 0)
						throw std::runtime_error("error");
					jtnei = -i;
					ply.polygons[j] = -jt->second.first;
				}
			} else {
				ply.polygons[j] = ver3 ? int2 : int2+1;
			}
		}
	}
	// update any constraint between edges so that they match
	for (int i = 1; i <= P; ++i) {
		auto& ply = mesh_polygons[i];
		for (int j = 0, je = static_cast<int>(ply.polygons.size()); j < je; ++j) { // check neighbour faces
			int k = ply.polygons[j];
			if (k > 0) {
				if (auto& nei = mesh_polygons[k]; ply.isTraversable() != nei.isTraversable() || // if neighbour is seperated by traversable regions, they should be constrained
					std::find(nei.polygons.begin(), nei.polygons.end(), -i) != nei.polygons.end() // if neighbour is constrained by not this edge
					) { 
					ply.polygons[j] = -k;
				}
			}
		}
	}
	// update vertex metadata
	autoVertexSetup();
	autoCalcBounds();
}

}
