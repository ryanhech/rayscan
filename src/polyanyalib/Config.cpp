#include <polyanyalib/Config.hpp>

namespace polyanyalib
{

Config::Config()
{ }
Config::~Config()
{ }

void Config::instanceCreate()
{
	setInstance_(std::make_unique<Config>());
}

MeshMerge Config::getMeshMerge() const
{
	using namespace std::string_view_literals;
	auto merge = param("merge");
	if (!merge)
		return MeshMerge::None;
	else if (merge->empty() || *merge == "simple"sv)
		return MeshMerge::Simple;
	else if (*merge == "greedy"sv)
		return MeshMerge::Greedy;
	else
		throw std::runtime_error("invalid parameter");
}

}
