#include <polyanyalib/SaveData.hpp>

namespace polyanyalib
{
	
auto SaveData::cellToEnv(ssize_t x, ssize_t y) -> point
{
	y = height - y - 1;
	return point((x - Padding) * iscale + box.lower().x, (y - Padding) * iscale + box.lower().y);
}
auto SaveData::envToCell(point pt) -> std::pair<ssize_t,ssize_t>
{
	pt -= box.lower();
	pt = scale * pt;
	pt.x += Padding; pt.y += Padding;
	pt.y = height - pt.y - 1;
	return {static_cast<ssize_t>(std::floor(pt.x + inx::high_epsilon<double>)),
			static_cast<ssize_t>(std::floor(pt.y + inx::high_epsilon<double>))};
}

void SaveData::loadEnvironment(env_type& e)
{
	box = e.getEnclosureBox();
	env.emplace<0>(&e);
}
void SaveData::loadMesh(mesh_type& m)
{
	box = m.getBounds();
	env.emplace<1>(&m);
}

void SaveData::populateVars(double scale, bool setupData)
{
	this->scale = scale;
	iscale = 1.0 / scale;
	box.lower().x = std::floor(box.lower().x);
	box.lower().y = std::floor(box.lower().y);
	box.upper().x = std::ceil(box.upper().x);
	box.upper().y = std::ceil(box.upper().y);
	width = static_cast<ssize_t>(std::ceil(box.width() * scale)) + 2*SaveData::Padding;
	height = static_cast<ssize_t>(std::ceil(box.height() * scale)) + 2*SaveData::Padding;
	if (setupData)
		assign(height, std::vector<bool>(width, false));
}

}
