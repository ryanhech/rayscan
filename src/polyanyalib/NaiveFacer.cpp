#include <polyanyalib/NaiveFacer.hpp>
#include <polyanyalib/Mesh.hpp>

namespace polyanyalib
{

PointLocation NaiveFacer::findFace(const Point& p, bool onlyTrav[[maybe_unused]]) const
{
	const auto& mesh_polygons = m_mesh->mesh_polygons;
	for (int polygon = 1, polygon_end = static_cast<int>(mesh_polygons.size()); polygon < polygon_end; ++polygon)
	{
		const Polygon& poly = mesh_polygons[polygon];
		if (!poly.isInitalised())
			continue;
		assert(poly.isTraversable() || m_mesh->polyContainsPoint(polygon, p).type != PolyContainment::INSIDE);
		if (!poly.isTraversable())
			continue;
		PolyContainment result = m_mesh->polyContainsPoint(polygon, p);
		switch (result.type)
		{
			case PolyContainment::OUTSIDE:
				// Does not contain: try the next one.
				break;

			case PolyContainment::INSIDE:
				// This one strictly contains the point.
				return {PointLocation::IN_POLYGON, polygon, -1, -1, -1};

			case PolyContainment::ON_EDGE:
				// This one lies on the edge.
				// Chek whether the other one is -1.
				if (result.adjacent_poly <= 0)
					return { PointLocation::ON_MESH_BORDER, polygon, -1, result.vertex1, result.vertex2 };
				else if (result.adjacent_poly < polygon) {
					return { PointLocation::ON_EDGE, result.adjacent_poly, polygon, result.vertex2, result.vertex1 };
				}
				return { PointLocation::ON_EDGE, polygon, result.adjacent_poly, result.vertex1, result.vertex2 };

			case PolyContainment::ON_VERTEX:
				// This one lies on a corner.
			{
				const Vertex& v = m_mesh->mesh_vertices[result.vertex1];
				if (v.isCorner())
				{
					if (v.isAmbig())
					{
						return {PointLocation::ON_CORNER_VERTEX_AMBIG, -1, -1,
								result.vertex1, -1};
					}
					else
					{
						return {PointLocation::ON_CORNER_VERTEX_UNAMBIG,
								polygon, -1, result.vertex1, -1};
					}
				}
				else
				{
					return {PointLocation::ON_NON_CORNER_VERTEX,
							polygon, -1,
							result.vertex1, -1};
				}
			}

			default:
				// This should not be reachable
				assert(false);
		}
	}
	// Haven't returned yet, therefore P does not lie on the mesh.
	return {PointLocation::NOT_ON_MESH, -1, -1, -1, -1};
}

}
