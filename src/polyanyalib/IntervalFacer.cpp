#include <polyanyalib/IntervalFacer.hpp>
#include <polyanyalib/Mesh.hpp>

namespace polyanyalib
{

void IntervalFacer::setMesh(const Mesh& mesh)
{
	Facer::setMesh(mesh);
	initalise();
}

PointLocation IntervalFacer::findFace(const Point& p, bool onlyTrav[[maybe_unused]]) const
{
	if (!m_mesh->getBounds().within(p))
	{
		return {PointLocation::NOT_ON_MESH, -1, -1, -1, -1};
	}
	auto slab = m_slabs.upper_bound(p.x);
	if (slab == m_slabs.begin())
	{
		return {PointLocation::NOT_ON_MESH, -1, -1, -1, -1};
	}
	--slab;
	const auto& polys = slab->polys;
	const auto close_it = std::lower_bound(polys.begin(), polys.end(), 2.0*p.y,
		[&meshpoly=m_mesh->mesh_polygons](const int poly_index, const double y_coord) noexcept -> bool
		{
			const Polygon& poly = meshpoly[poly_index];
			return poly.bounds.lower().y + poly.bounds.upper().y < y_coord;
		}
	);
	const int close_index = close_it - polys.begin()
							- (close_it == polys.end());
	// The plan is to take an index and repeatedly do:
	// +1, -2, +3, -4, +5, -6, +7, -8, ...
	// until it hits the edge. If it hits an edge, instead iterate normally.
	const int ps = polys.size();
	int i = close_index;
	int next_delta = 1;
	int walk_delta = 0; // way to go when walking normally

	while (i >= 0 && i < ps)
	{
		const int polygon = polys[i];
		const PolyContainment result = m_mesh->polyContainsPoint(polygon, p);
		switch (result.type)
		{
			case PolyContainment::OUTSIDE:
				// Does not contain: try the next one.
				break;

			case PolyContainment::INSIDE:
				// This one strictly contains the point.
				return {PointLocation::IN_POLYGON, polygon, -1, -1, -1};

			case PolyContainment::ON_EDGE:
				// This one lies on the edge.
				// Chek whether the other one is -1.
				return {
					(result.adjacent_poly <= 0 ?
					 PointLocation::ON_MESH_BORDER :
					 PointLocation::ON_EDGE),
					polygon, result.adjacent_poly,
					result.vertex1, result.vertex2
				};

			case PolyContainment::ON_VERTEX:
				// This one lies on a corner.
			{
				const Vertex& v = m_mesh->mesh_vertices[result.vertex1];
				if (v.isCorner())
				{
					if (v.isAmbig())
					{
						return {PointLocation::ON_CORNER_VERTEX_AMBIG, -1, -1,
								result.vertex1, -1};
					}
					else
					{
						return {PointLocation::ON_CORNER_VERTEX_UNAMBIG,
								polygon, -1, result.vertex1, -1};
					}
				}
				else
				{
					return {PointLocation::ON_NON_CORNER_VERTEX,
							polygon, -1,
							result.vertex1, -1};
				}
			}

			default:
				// This should not be reachable
				assert(false);
		}


		// do stuff
		if (walk_delta == 0)
		{
			const int next_i = i + next_delta * (2 * (next_delta & 1) - 1);
			if (next_i < 0)
			{
				// was going to go too far to the left.
				// start going right
				walk_delta = 1;
			}
			else if (next_i >= ps)
			{
				walk_delta = -1;
			}
			else
			{
				i = next_i;
				next_delta++;
			}
		}

		if (walk_delta != 0)
		{
			i += walk_delta;
		}
	}
	// Haven't returned yet, therefore P does not lie on the mesh.
	return {PointLocation::NOT_ON_MESH, -1, -1, -1, -1};
}

void IntervalFacer::initalise()
{
	m_slabsFactory.clear();
	for (const auto& v : m_mesh->mesh_vertices)
	{
		if (v.isInitalised()) {
			double x = v.p.x;
			m_slabs.insert(m_slabs.lower_bound(x), *m_slabsFactory.construct(x)); // setup the slab
		}
	}
	for (const Polygon& p : m_mesh->mesh_polygons)
	{
		if (p.isInitalised() && p.isTraversable()) {
			int id = p.id;
			for (auto it = m_slabs.lower_bound(p.bounds.lower().x), ite = m_slabs.upper_bound(p.bounds.upper().x); it != ite; ++it)
			{
				it->polys.push_back(id);
			}
		}
	}
	for (auto& slab : m_slabs)
	{
		std::sort(slab.polys.begin(), slab.polys.end(),
			[&poly=m_mesh->mesh_polygons](const int a, const int b) -> bool
			{
				// Sorts based on the midpoints.
				// If tied, sort based on width of poly.
				const auto& ap = poly[a].bounds, bp = poly[b].bounds;
				if (double diff = (ap.lower().y + ap.upper().y) - (bp.lower().y + bp.upper().y); diff < -rayscanlib::geometry::epsilon)
					return true; // less than
				else if (diff < rayscanlib::geometry::epsilon) // equal
					return ap.width() > bp.width();
				else
					return false; // greater than
			}
		);
	}
}

}
