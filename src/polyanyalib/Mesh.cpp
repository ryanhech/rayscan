#include <polyanyalib/Mesh.hpp>
#include <polyanyalib/Geometry.hpp>
#include <polyanyalib/Facer.hpp>
#include <rayscanlib/utils/Stream.hpp>
#include <rayscanlib/geometry/AngledSector.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/range/irange.hpp>
#include <inx/io/transformers.hpp>
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <unordered_set>
#include <optional>

//#define DEBUG_POLYANYA

namespace polyanyalib
{

Mesh::Mesh() : m_polygonRef(&m_polygonRefPool), m_floodFillId(0)
{
	mesh_euclidean.reserve(256);
}

Mesh::~Mesh()
{ }

void Mesh::clear(bool release)
{
	mesh_vertices.clear();
	mesh_polygons.clear();
	max_poly_sides = 0;
	m_setupTime = std::chrono::nanoseconds(0);
	m_mergeTime = std::chrono::nanoseconds(0);
	mesh_verticesReuse.clear();
	mesh_polygonsReuse.clear();
	bounds = rayscanlib::geometry::Box<double>{};
	m_floodFillId = 0;
	m_floodFillStack.clear();
	m_removeVertices.clear();
	if (release) {
		m_facer.release();
		m_polygonRef.clear();
		mesh_vertices.shrink_to_fit();
		mesh_polygons.shrink_to_fit();
		mesh_verticesReuse.shrink_to_fit();
		mesh_polygonsReuse.shrink_to_fit();
		m_floodFillStack.shrink_to_fit();
		m_removeVertices.shrink_to_fit();
		mesh_euclidean.clear(); mesh_euclidean.shrink_to_fit();
		mesh_euclideanReuse.clear(); mesh_euclideanReuse.shrink_to_fit();
	} else {
		for (auto& e : mesh_euclidean) {
			e.vertex = 0;
		}
	}
}

void Mesh::copy(const Mesh& mesh)
{
	clear(true);
	mesh_vertices = mesh.mesh_vertices;
	mesh_polygons = mesh.mesh_polygons;
	mesh_euclidean = mesh.mesh_euclidean;
	max_poly_sides = mesh.max_poly_sides;
	m_setupTime = mesh.m_setupTime;
	m_mergeTime = mesh.m_mergeTime;
	mesh_verticesReuse = mesh.mesh_verticesReuse;
	mesh_polygonsReuse = mesh.mesh_polygonsReuse;
	mesh_euclideanReuse = mesh.mesh_euclideanReuse;
	bounds = mesh.bounds;
	m_polygonRef.clear();
	for (const auto& r : mesh.m_polygonRef) {
		m_polygonRef.emplace(r);
	}
	m_floodFillId = 0;
	m_floodFillStack.clear();
	m_facer = nullptr;
}

void Mesh::loadEnvironment(const environment& env[[maybe_unused]])
{
	throw std::logic_error("not implmented");
}

// Finds out whether the polygon specified by "poly" contains point P.
PolyContainment Mesh::polyContainsPoint(int poly, const Point& p) const
{
#if 0
	// The below is taken from
	// "An Efficient Test for a Point to Be in a Convex Polygon"
	// from the Wolfram Demonstrations Project
	// demonstrations.wolfram.com/AnEfficientTestForAPointToBeInAConvexPolygon/

	// Assume points are in counterclockwise order.
	const Polygon& poly_ref = mesh_polygons[poly];
	if (!poly_ref.bounds.within(p))
	{
		return {PolyContainment::OUTSIDE, -1, -1, -1};
	}
	const Point& last_point_in_poly = mesh_vertices[poly_ref.vertices.back()].p;

	Point last = last_point_in_poly - p;
	if (last.isZero())
	{
		return {PolyContainment::ON_VERTEX, -1, poly_ref.vertices.back(), -1};
	}

	int last_index = poly_ref.vertices.back();
	for (int i = 0; i < (int) poly_ref.vertices.size(); i++)
	{
		const int point_index = poly_ref.vertices[i];
		const Point cur = mesh_vertices[point_index].p - p;
		if (cur.isZero())
		{
			return {PolyContainment::ON_VERTEX, -1, point_index, -1};
		}
		const double cur_a = last.cross(cur);
		if (std::abs(cur_a) < rayscanlib::geometry::epsilon)
		{
			// The line going from cur to last goes through p.
			// This means that they are collinear.
			// The associated polygon should simply be polygons[i] in version
			// 2 of the file format.

			// Ensure that cur = c*last where c is negative.
			// If not, this means that the point is either outside or that this
			// segment is collinear to an adjacent one.
			if (cur.x)
			{
				if (!((cur.x > 0) ^ (last.x > 0)))
				{
					last = cur;
					last_index = point_index;
					continue;
				}
			}
			else
			{
				if (!((cur.y > 0) ^ (last.y > 0)))
				{
					last = cur;
					last_index = point_index;
					continue;
				}
			}
			return {PolyContainment::ON_EDGE, poly_ref.polygons[i],
					point_index, last_index};
		}

		// Because we assume that the points are counterclockwise,
		// we can immediately terminate when we see a negatively signed area.
		if (cur_a < 0)
		{
			return {PolyContainment::OUTSIDE, -1, -1, -1};
		}
		last = cur;
		last_index = point_index;
	}
	return {PolyContainment::INSIDE, -1, -1, -1};
#else
	const Polygon& ply = mesh_polygons[poly];
	assert(ply.isInitalised());
	if (!ply.bounds.within(p))
		return {PolyContainment::OUTSIDE, -1, -1, -1};

	Point pv, at = mesh_vertices[ply.vertices.back()].p - p;
	for (int i = 0, ie = static_cast<int>(ply.vertices.size()); i < ie; ++i) { // check polygons
		pv = at;
		at = mesh_vertices[ply.vertices[i]].p - p;
		if (double d = pv.cross(at); Point::isCW(d))
			return {PolyContainment::OUTSIDE, -1, -1, -1};
		else if (!Point::isCCW(d)) {
			if (at.isZero())
				return {PolyContainment::ON_VERTEX, -1, ply.vertices[i], -1};
			else if (pv.isBack(at)) {
				return {PolyContainment::ON_EDGE, ply.polygons[i], ply.vertices[i], ply.vertices[(i+ie-1) % ie]};
			}
		}
	}
	return {PolyContainment::INSIDE, -1, -1, -1};
#endif
}

PointLocation Mesh::findFaceFromPoint(const Point& p, bool onlyTrav) const
{
#ifdef NDEBUG
	return m_facer->findFace(p, onlyTrav);
#else
	PointLocation ans = m_facer->findFace(p, onlyTrav);
	switch (ans.type) {
	case PointLocation::IN_POLYGON:
		assert(polyContainsPoint(ans.poly1, p).type == PolyContainment::INSIDE);
		assert(!onlyTrav || mesh_polygons[ans.poly1].isTraversable());
		break;
	case PointLocation::ON_EDGE:
		assert(mesh_polygons[ans.poly1].polygons[mesh_polygons[ans.poly1].findVertex(ans.vertex1)] == ans.poly2);
		assert(mesh_polygons[ans.poly2].polygons[mesh_polygons[ans.poly2].findVertex(ans.vertex2)] == ans.poly1);
		assert(polyContainsPoint(ans.poly2, p).type == PolyContainment::ON_EDGE);
		[[fallthrough]];
	case PointLocation::ON_MESH_BORDER:
		assert(polyContainsPoint(ans.poly1, p).type == PolyContainment::ON_EDGE);
		assert(!onlyTrav || mesh_polygons[ans.poly1].isTraversable());
		break;
	case PointLocation::ON_CORNER_VERTEX_UNAMBIG:
	case PointLocation::ON_NON_CORNER_VERTEX:
		assert(polyContainsPoint(ans.poly1, p).type == PolyContainment::ON_VERTEX);
		assert(!onlyTrav || mesh_polygons[ans.poly1].isTraversable());
		break;
	default:
		assert(!onlyTrav);
	}
	return ans;
#endif
}

void Mesh::facerInitalise()
{
	if (m_facer)
		m_facer->setMesh(*this);
}

void Mesh::insertEuclideanPolygon(const void* p, const environment::polygon& polygon, bool skipInsertion)
{
	// add to pointer indexer, so it can be deleted later
	EuclideanPolygon* ep;
	if (auto it = m_polygonRef.try_emplace(p, GlobalId{0}); it.second) {
		ep = &mesh_euclidean[(it.first->second = newEuclidean()).id];
		ep->id = it.first->second.id;
	} else {
		return;
	}
	
	// insert new polygon
	Point a, b, c;
	std::tie(a, b, c) = polygon.edge(polygon.size()-1);
	a -= b;
	c -= b;
	ep->pt = b;
	ep->cw = a;
	ep->ccw = c;
	if (skipInsertion) // only insert polygon metadata
		return;
	PointLocation pl = findFaceFromPoint(b, false);
	if (pl.type == PointLocation::ON_NON_CORNER_VERTEX)
		pl.poly1 = 0;
	GlobalId aPoint{0};
#ifdef DEBUG_POLYANYA
	std::cerr << std::endl << "insert: ";
	for (auto& pt : polygon) {
		std::cerr << "(" << pt << ") ";
	}
	std::cerr << std::endl;
#endif
	for (auto& pt : polygon) {
		a = b;
		b = pt;
		GlobalId tmp = insertLine(pl, a, b);
		if (aPoint.id == 0)
			aPoint = tmp;
		pl.poly1 = 0;
	}

	// polygon inserted, find all mesh points connected to aPoint, the first point of the polygon
	assert(aPoint.id > 0);
	Vertex& vtx = mesh_vertices[aPoint.id];
	assert(vtx.p == ep->pt);
	ep->vertex = aPoint.id;
#ifdef DEBUG_POLYANYA
	std::cerr << "face within: (" << b << "): (" << a << ") (" << c << ")" << std::endl;
#endif
	for (auto i : vtx.polygons) {
		{
		Polygon& plyx = mesh_polygons[i];
		LocalId xxx = convVertexId(plyx, aPoint);
		Point xa = mesh_vertices[plyx.vertices[xxx.id]].p;
		Point xb = mesh_vertices[plyx.vertices[(xxx.id+1) % static_cast<int>(plyx.vertices.size())]].p;
		Point xc = mesh_vertices[xxx.id == 0 ? plyx.vertices.back() : plyx.vertices[xxx.id-1]].p;
#ifdef DEBUG_POLYANYA
		std::cerr << "poly edge: (" << xa << "): (" << (xb-xa) << ") (" << (xc-xa) << ")" << std::endl;
#endif
		}
		if (Polygon& ply = mesh_polygons[i]; polygonFaceWithinEdge(ep->cw, ep->ccw, ply, convVertexId(ply, aPoint))) {
#ifdef DEBUG_POLYANYA
			std::cerr << "flood fill: ";
			for (int i : ply.vertices)
				std::cerr << '(' << mesh_vertices[i].p << ") ";
			std::cerr << std::endl;
			size_t found = 0;
			floodFill(GlobalId{i}, [this,&found] (GlobalId j) {
				found++;
				mesh_polygons[j.id].traversable += 1;
				assert(mesh_polygons[j.id].traversable == 1);
			});
			std::cerr << "\tfound: " << found << std::endl;
			assert(found != 0);
#else
			floodFill(GlobalId{i}, [this] (GlobalId j) {
				mesh_polygons[j.id].traversable += 1;
				assert(mesh_polygons[j.id].traversable == 1);
			});
#endif
			break;
		}
	}
	//facerOperations();

	assert(isMeshValid());
}

void Mesh::eraseEuclideanPolygon(const void* p, bool skip)
{
	GlobalId i;
	if (auto it = m_polygonRef.find(p); it == m_polygonRef.end()) {
		return;
	} else {
		i = it->second;
		m_polygonRef.erase(it);
	}
	EuclideanPolygon& euc = mesh_euclidean[i.id];
	if (skip) {
		freeEuclidean(i);
		return;
	}
	if (euc.vertex == 0) {
		auto f = findFaceFromPoint(euc.pt, false);
		euc.vertex = f.vertex1;
	}
	GlobalId euvid{euc.vertex};
	Vertex& euv = mesh_vertices[euvid.id];
	assert(euv.isCorner());
	LocalId fid = findFaceFromVertex(euc.cw, euc.ccw, euvid);
	floodFill(GlobalId{euv.polygons[fid.id]}, [this] (GlobalId pid) {
		Polygon& polygon = mesh_polygons[pid.id];
		polygon.traversable -= 1;
		assert(polygon.traversable == 0);
#if 0
		if (!polygon.is_traversable) {
			facerInsertOp(polygon);
		}
#endif
		for (int i = 0, ie = polygon.polygons.size(); i < ie; ++i) {
			if (int nid = polygon.polygons[i]; nid < 0) {
				navMeshConnect(polygon, LocalId{i});
			}
		}
	});
	//facerOperations();

	assert(isMeshValid());
}

void Mesh::finalise()
{
}

bool Mesh::lineEnterPolygonFromVertex(const Point& ab, const Polygon& poly, LocalId i)
{
	int s = static_cast<int>(poly.vertices.size());
	const Point& a = mesh_vertices[poly.vertices[i.id]].p;
	const Point& cw = mesh_vertices[poly.vertices[(i.id+1)%s]].p;
	const Point& ccw = mesh_vertices[poly.vertices[(i.id+s-1)%s]].p;
	return !ab.isWideBetweenCW(a, cw, ccw);
}

bool Mesh::lineEnterPolygonFromEdgeId(const Point& ab, GlobalId id1, GlobalId id2)
{
	const Point xy(mesh_vertices[id2.id].p-mesh_vertices[id1.id].p);
	if (auto d = xy.dir(ab); d != rayscanlib::Dir::COLIN) {
		return d == rayscanlib::Dir::CCW;
	} else {
		return ab * xy > 0;
	}
}

bool Mesh::polygonFaceWithinEdge(const Point& cw, const Point& ccw, const Polygon& poly, LocalId i)
{
	using AS = ::rayscanlib::geometry::AngledSector<double, ::rayscanlib::Between::WHOLE, ::rayscanlib::Dir::CCW>;
	int s = static_cast<int>(poly.vertices.size());
	const Point pat = mesh_vertices[poly.vertices[i.id]].p;
	const Point pcw = mesh_vertices[poly.vertices[(i.id+1)%s]].p - pat;
	const Point pccw = mesh_vertices[poly.vertices[(i.id+s-1)%s]].p - pat;
#ifdef DEBUG_POLYANYA
	std::cerr << "poly face within edge: edge(" << cw.normalise() << ") -- (" << ccw.normalise() << ") face(" << pcw.normalise() << ") -- (" << pccw.normalise() << ")";
	const AS as(cw, ccw);
	bool ans = as.within(pcw) && as.within<::rayscanlib::Dir::CCW>(pcw, pccw);
	std::cerr << " = " << ans << std::endl;
	return ans;
#else
	const AS as(cw, ccw);
	return as.within(pcw) && as.within<::rayscanlib::Dir::CCW>(pcw, pccw);
#endif
}

auto Mesh::findFaceFromVertex(const Point& cw, const Point& ccw, GlobalId vid) -> LocalId
{
	auto& pply = mesh_vertices[vid.id].polygons;
	for (int i = 0, ie = static_cast<int>(pply.size()); i < ie; ++i) {
		Polygon& poly = mesh_polygons[pply[i]];
		if (polygonFaceWithinEdge(cw, ccw, poly, convVertexId(poly, vid)))
			return LocalId{i};
	}
	assert(false);
	return LocalId{static_cast<int>(pply.size())};
}

auto Mesh::insertLine(PointLocation& pl, const Point& a, const Point& b) -> GlobalId
{
	enum class Ins : uint8 {
		NONE,
		EDGE,
		SPLIT
	};
#ifdef DEBUG_POLYANYA
	std::cerr << std::endl << "insert line: (" << a << ") -- (" << b << ')' << std::endl;
#define MESH_LINE_INT_TIME(p1,p2,p3,p4) line_intersect_time(p1, p2, p3, p4, d1, d2, d3); \
	std::cerr << "line_intersect_time: (" << (p1) << ")+(" << ((p2)-(p1)) << ")\t(" << (p3) << ")+(" << ((p4)-(p3)) << ")"; \
	std::cerr << "\t" << d1 << "\t" << d2 << "\t" << d3 << std::endl
#else
#define MESH_LINE_INT_TIME(p1,p2,p3,p4) line_intersect_time(p1, p2, p3, p4, d1, d2, d3)
#endif
	auto ab = b-a;
	bool end = false;
	GlobalId aPoint{0};
	assert(pl.poly1 <= 0 || polyContainsPoint(pl.poly1, a).type == pl.type);

	switch (pl.type) {
	case PointLocation::IN_POLYGON: {
#ifdef DEBUG_POLYANYA
		std::cerr << "IN_POLYGON" << std::endl;
#endif
		// split polygon and add a
		Polygon& polygon = mesh_polygons[pl.poly1];
		int plysize = static_cast<int>(polygon.vertices.size());
		// must use varaibles since we need to find both edges in this case
		LocalId p1{}, p2{};
		LocalId ip{-1}, i{-1};
		double d1, d2, d3;
		double s1 = 0.0, s2 = 0.0;
		Vertex *v1, *v2;
		v2 = &mesh_vertices[polygon.vertices.back()];
		for (ip.id = plysize-1, i.id = 0; i.id < plysize; ip = i, i.id++) {
			v1 = v2;
			v2 = &mesh_vertices[polygon.vertices[i.id]];
			MESH_LINE_INT_TIME(a,b,v2->p,v1->p);
			if (d3 == 0.0)
				continue;
			assert(line_intersect_bound_check(d1, d3) != ZeroOnePos::EQ_ZERO);
			if (auto zo1 = line_intersect_bound_check(d1, d3), zo2 = line_intersect_bound_check(d2, d3); zo2 == ZeroOnePos::EQ_ZERO) {
				if (zo1 == ZeroOnePos::LT_ZERO) {
					p1 = i;
				} else {
					p2 = i;
				}
			} else if (zo2 == ZeroOnePos::IN_RANGE) { // intersect on edge, will need to insert new point
				if (zo1 == ZeroOnePos::LT_ZERO) {
					p1 = i; s1 = d2 / d3;
				} else {
					p2 = i; s2 = d2 / d3;
				}
			}
		}
		GlobalId np = splitFace(GlobalId{pl.poly1}, p1, p2, s1, s2);
		Polygon& polygon2 = mesh_polygons[np.id];
		pl.vertex1 = insertPoint(polygon2, LocalId{0}, a).id;
		pl.type = PointLocation::ON_NON_CORNER_VERTEX;
		pl.poly1 = 0;
		}
		break;
	case PointLocation::ON_EDGE: {
#ifdef DEBUG_POLYANYA
		std::cerr << "ON_EDGE" << std::endl;
#endif
		Polygon& polygon = mesh_polygons[pl.poly1];
		int i = polygon.findVertex(pl.vertex1);
		pl.vertex1 = insertPoint(polygon, LocalId{i}, a).id;
		pl.type = PointLocation::ON_NON_CORNER_VERTEX;
		pl.poly1 = 0;
		}
		break;
	case PointLocation::ON_NON_CORNER_VERTEX:
#ifdef DEBUG_POLYANYA
		std::cerr << "ON_NON_CORNER_VERTEX" << std::endl;
#endif
		break;
	default:
		assert(false);
	}

#ifdef DEBUG_POLYANYA
		std::cerr << "START LOOP" << std::endl;
#endif
	while (!end) {
		LocalId ip{-1}, i{-1};
		GlobalId np{0};
		int plysize;
		double d1, d2, d3;
		Vertex *v1, *v2, *v3;
		if (aPoint.id == 0)
			aPoint.id = pl.vertex1;
		v3 = &mesh_vertices[pl.vertex1];
		assert(ab.isColin(v3->p - a));
		if (pl.poly1 <= 0) {
#ifdef DEBUG_POLYANYA
			std::cerr << "update polygon id" << std::endl;
#endif
			for (auto i : v3->polygons) {
				auto& tmp = mesh_polygons[i];
				if (lineEnterPolygonFromVertex(ab, tmp, convVertexId(tmp, GlobalId{pl.vertex1}))) {
					pl.poly1 = i;
					break;
				}
			}
		}
		assert(pl.poly1 >= 0);
		Polygon& polygon = mesh_polygons[pl.poly1];
		LocalId plvid = convVertexId(polygon, GlobalId{pl.vertex1});
#ifdef DEBUG_POLYANYA
		std::cerr << "enter: ("  << ab << ") : (" << (b - v3->p) << ")" << std::endl;
		std::cerr << "\tpolygon: ";
		for (int xx : polygon.vertices) {
			std::cerr << '(' << (mesh_vertices[xx].p - v3->p) << ") ";
		}
		std::cerr << std::endl;
#endif
		assert(lineEnterPolygonFromVertex(ab, polygon, plvid));
		plysize = static_cast<int>(polygon.vertices.size());
		// corner case, check prev and next point for collinear line
		for (i.id = -1; i.id < 2; i.id += 2) {
			// use v3 already set
			v1 = &mesh_vertices[polygon.vertices[ (ip.id = (plvid.id + plysize + i.id) % plysize) ]];
			if (auto v31 = v1->p - v3->p; ab.isColin(v31) && ab * v31 > rayscanlib::geometry::epsilon) { // follows edge corner case
				if (i.id > 0)
					plvid = ip;
				if (auto bend = ab * (v1->p - b); bend > -rayscanlib::geometry::epsilon) { // b is on edge v13, add edge
					if (bend > rayscanlib::geometry::epsilon) {
						insertLineA_(polygon, b, plvid);
						if (i.id < 0) {
							ip = plvid; plvid.id += 1;
						}
					}
					end = true;
				}
#ifdef DEBUG_POLYANYA
				std::cerr << "\tinsert line by edge " << i.id << ' ' << ip.id << ' ' << plvid.id << ' ' << std::boolalpha << end << std::endl;
#endif
				insertLineEdge_(pl, polygon, GlobalId{0}, GlobalId{polygon.vertices[ip.id]}, plvid);
				plysize = 0;
				break;
			}
		}
		if (!plysize)
			continue;
		v2 = &mesh_vertices[polygon.vertices.back()];
		for (ip.id = plysize-1, i.id = 0; i.id < plysize; ip = i, i.id++) {
			v1 = v2;
			v2 = &mesh_vertices[polygon.vertices[i.id]];
			MESH_LINE_INT_TIME(v3->p,b,v2->p,v1->p);
			if (d3 == 0.0) { // parallel lines
				continue;
			} else if (auto zo2 = line_intersect_bound_check(d2, d3); zo2 == ZeroOnePos::EQ_ZERO) {
			//	if (i.id+1 == plvid.id) // corner case
			//		continue;
			//	if (ab.isColin(v2->p - a)) // corner case
			//		continue;
				auto zo1 = line_intersect_bound_check(d1, d3);
				switch (zo1) {
				case ZeroOnePos::EQ_ONE:
				case ZeroOnePos::GT_ONE:
					end = true;
					[[fallthrough]];
				case ZeroOnePos::IN_RANGE: {
					np = insertLineFace_(pl, GlobalId{pl.poly1}, GlobalId{0}, convVertexId(polygon, GlobalId{pl.vertex1}), i, 0.0, 0.0);
					Polygon& polygon2 = mesh_polygons[np.id];
					if (zo1 == ZeroOnePos::GT_ONE) {
						insertLineA_(np, b);
						pl.vertex1 = polygon2.vertices[0]; // b updated, assign new point for next line
						plvid.id = 1;
					} else {
						plvid.id = 0;
					}
#ifdef DEBUG_POLYANYA
					std::cerr << "\tinsert line by zero " << i.id << ' ' << std::boolalpha << end << std::endl;
#endif
					navMeshDivide(polygon2, plvid);
					plysize = 0;
					break; }
				case ZeroOnePos::EQ_ZERO:
			//	case ZeroOnePos::LT_ZERO:
					break;
				default:
					assert(false);
				}
			} else if (zo2 == ZeroOnePos::IN_RANGE) {
				auto zo1 = line_intersect_bound_check(d1, d3);
				switch (zo1) {
				case ZeroOnePos::EQ_ONE:
				case ZeroOnePos::GT_ONE:
					end = true;
					[[fallthrough]];
				case ZeroOnePos::IN_RANGE: {
					np = insertLineFace_(pl, GlobalId{pl.poly1}, GlobalId{polygon.polygons[i.id]}, convVertexId(polygon, GlobalId{pl.vertex1}), i, 0.0, d2/d3);
					LocalId plvid;
					Polygon& polygon2 = mesh_polygons[np.id];
					if (zo1 == ZeroOnePos::GT_ONE) {
						insertLineA_(np, b);
						pl.vertex1 = polygon2.vertices[0]; // b updated, assign new point for next line
						plvid.id = 1;
					} else {
						plvid.id = 0;
					}
#ifdef DEBUG_POLYANYA
					std::cerr << "\tinsert line by range " << i.id << ' ' << std::boolalpha << end << std::endl;
#endif
					navMeshDivide(polygon2, plvid);
					plysize = 0;
					break; }
				case ZeroOnePos::EQ_ZERO:
					break;
				default:
					assert(false);
				}
			}
		}
		assert(plysize == 0);
	}
	assert(aPoint.id > 0); // aPoint must be set on first pass
#undef MESH_LINE_INT_TIME
	return aPoint;
}

void Mesh::insertLineEdge_(PointLocation& pl, Polygon& polygon, GlobalId nextpoly, GlobalId nextvert, LocalId p)
{
	navMeshDivide(polygon, p);
	pl.poly1 = nextpoly.id;
	pl.vertex1 = nextvert.id;
#ifdef DEBUG_POLYANYA
	std::cerr << "insertLineEdge " << pl.poly1 << '\t' << pl.vertex1 << "\t(" << mesh_vertices[pl.vertex1].p << ")" << std::endl;
#endif
}

auto Mesh::insertLineFace_(PointLocation& pl, GlobalId poly, GlobalId nextpoly, LocalId p1, LocalId p2, double s1, double s2) -> GlobalId
{
	GlobalId newpoly = splitFace(poly, p1, p2, s1, s2);
	pl.poly1 = nextpoly.id;
	pl.vertex1 = mesh_polygons[newpoly.id].vertices.back();
#ifdef DEBUG_POLYANYA
	std::cerr << "insertLineFace " << pl.poly1 << '\t' << pl.vertex1 << "\t(" << mesh_vertices[pl.vertex1].p << ")" << std::endl;
#endif
	return newpoly;
}

void Mesh::insertLineA_(Polygon& ply, const Point& a, LocalId i)
{
#ifdef DEBUG_POLYANYA
	std::cerr << "insertLineA_\t";
#endif
	insertPoint(ply, i, a);
}
void Mesh::insertLineA_(GlobalId poly, const Point& a, LocalId i)
{
#ifdef DEBUG_POLYANYA
	std::cerr << "insertLineA_\t";
#endif
	insertPoint(poly, i, a);
}
bool Mesh::insertLineAB_(Polygon& ply, const Point& a, const Point& b, LocalId i)
{
	Vertex& v = mesh_vertices[ply.vertices[i.id]];
	assert(a != b && v.p != a && v.p != b);
	assert(v.p.isColin(a, b));
	insertPoint(ply, i, a);
	bool ans;
	if ((v.p - a) * (b - a) > 0) { // b is closer to v.p than a
		ans = false;
		i.id += 1; // add one to insert b infront of v.p rather than a
	} else {
		ans = true;
	}
	insertPoint(ply, i, b);
	return ans;
}
bool Mesh::insertLineAB_(GlobalId poly, const Point& a, const Point& b, LocalId i)
{
	return insertLineAB_(mesh_polygons[poly.id], a, b, i);
}

void Mesh::navMeshDivide(Polygon& ply, LocalId i)
{
	int v1 = ply.vertices[i.id];
	int v2 = ply.vertices[(i.id + static_cast<int>(ply.vertices.size()) - 1) % static_cast<int>(ply.vertices.size())];
	if (int ix = ply.polygons[i.id]; ix >= 0) {
		assert(ix != 0);
		mesh_vertices[v1].corner += 1;
		mesh_vertices[v2].corner += 1;
	}
#ifdef DEBUG_POLYANYA
	std::cerr << "divide: (" << mesh_vertices[i.id==0 ? ply.vertices.back() : ply.vertices[i.id-1]].p << ") -- ("
	    << mesh_vertices[ply.vertices[i.id]].p << ')' << std::endl;
	std::cerr << "\tpolygon: ";
	for (int i : ply.vertices)
		std::cerr << "(" << mesh_vertices[i].p << ") ";
	std::cerr << std::endl;
#endif
	int& x = ply.polygons[i.id];
	assert(x != 0);
	if (x > 0)
		x = -x;
	Polygon& px = mesh_polygons[-x];
	int d = px.findVertexNeighbour(v1);
	px.polygons[d] = -ply.id;
	assert(isPolygonValid(GlobalId{ply.id}));
	assert(isPolygonValid(GlobalId{-x}));
}
void Mesh::navMeshDivide(GlobalId polygon, GlobalId vertex)
{
	Polygon& ply = mesh_polygons[polygon.id];
	navMeshDivide(ply, convVertexId(ply, vertex));
}

void Mesh::navMeshConnect(Polygon& ply, LocalId i)
{
	int v1 = ply.vertices[i.id];
	int v2 = ply.vertices[(i.id + static_cast<int>(ply.vertices.size()) - 1) % static_cast<int>(ply.vertices.size())];
	if (int ix = ply.polygons[i.id]; ix <= 0) {
		assert(ix != 0);
		mesh_vertices[v1].corner -= 1;
		mesh_vertices[v2].corner -= 1;
		assert(mesh_vertices[v1].corner >= 0 && mesh_vertices[v2].corner >= 0);
	}
	int& x = ply.polygons[i.id];
	assert(x != 0);
	if (x < 0)
		x = -x;
	Polygon& px = mesh_polygons[x];
	int d = px.findVertexNeighbour(v1);
	px.polygons[d] = ply.id;
//	assert(isPolygonValid(GlobalId{ply.id}));
//	assert(isPolygonValid(GlobalId{x}));
}

auto Mesh::splitFace(GlobalId polygon, LocalId from, LocalId to, double fromM, double toM) -> GlobalId
{
	assert(from.id != to.id);
	GlobalId face = newPolygon();
	Polygon& split = mesh_polygons[face.id];
	Polygon& orig = mesh_polygons[polygon.id];
	assert(isPolygonValid(polygon));
#ifdef DEBUG_POLYANYA
	std::cerr << "split polygon: ";
	for (int i : orig.vertices)
		std::cerr << "(" << mesh_vertices[i].p << ") ";
	for (int i : orig.polygons)
		std::cerr << ' ' << i;
	std::cerr << std::endl << "\tfrom: " << from.id << '/' << fromM << "; to: " << to.id << '/' << toM << std::endl;
#endif
	if (fromM != 0.0) {
		insertPoint(polygon, from, fromM);
		if (to.id > from.id)
			to.id++;
	}
	if (toM != 0.0) {
		insertPoint(polygon, to, toM);
		if (from.id > to.id)
			from.id++;
	}
	int size = static_cast<int>(orig.vertices.size());
#ifndef NDEBUG
	{
		int fromp1 = (from.id+1) % size;
		int top1 = (to.id+1) % size;
		assert(fromp1 != to.id && top1 != from.id);
		Vertex& vf = mesh_vertices[orig.vertices[from.id]];
		Vertex& vt = mesh_vertices[orig.vertices[to.id]];
		Vertex& vfp1 = mesh_vertices[orig.vertices[fromp1]];
		Vertex& vtp1 = mesh_vertices[orig.vertices[top1]];
		assert(!vfp1.p.isColin(vf.p, vt.p));
		assert(!vtp1.p.isColin(vf.p, vt.p));
	}
#endif
	LocalId origsplit;
	if (from.id < to.id) {
		LocalId too{to.id+1};
		int s = too.id - from.id;
		assert(s >= 3);
		// copy into split
		split.vertices.resize(s);
		std::copy_n(orig.vertices.begin() + from.id, s, split.vertices.begin());
		split.polygons.resize(s);
		std::copy_n(orig.polygons.begin() + (from.id+1), s-1, split.polygons.begin() + 1); // skip first as it will be assigned later
		// reshape orig
		orig.vertices.erase(orig.vertices.begin() + (from.id+1), orig.vertices.begin() + to.id);
		orig.polygons.erase(orig.polygons.begin() + (from.id+1), orig.polygons.begin() + to.id);
		origsplit.id = from.id + 1;
	} else {
		LocalId fromm{from.id+1};
		assert(fromm.id - to.id >= 3);
		int s = (size + 2) - (fromm.id - to.id);
		int s1 = size - from.id;
		assert(s >= 3);
		// copy into split
		split.vertices.resize(s);
		std::copy_n(orig.vertices.begin() + from.id, s1, split.vertices.begin());
		std::copy_n(orig.vertices.begin(), to.id+1, split.vertices.begin() + s1);
		split.polygons.resize(s);
		std::copy_n(orig.polygons.begin() + (from.id+1), s1-1, split.polygons.begin() + 1);
		std::copy_n(orig.polygons.begin(), to.id+1, split.polygons.begin() + s1);
		// reshape into orig
		s = (size + 2) - s;
		assert(s >= 3);
		if (from.id != 0) {
			std::copy_n(orig.vertices.begin() + to.id, s, orig.vertices.begin());
			std::copy_n(orig.polygons.begin() + (to.id+1), s-1, orig.polygons.begin() + 1);
		}
		orig.polygons.erase(orig.polygons.begin() + s, orig.polygons.end());
		orig.vertices.erase(orig.vertices.begin() + s, orig.vertices.end());
		origsplit.id = 0;
	}
	assert(!split.vertices.empty());
	assert(!orig.vertices.empty());
	// connect split to orig
	split.polygons.front() = polygon.id;
	orig.polygons[origsplit.id] = face.id;
#ifdef DEBUG_POLYANYA
	std::cerr << "\torig: ";
	for (int i : orig.vertices)
		std::cerr << "(" << mesh_vertices[i].p << ") ";
	for (int i : orig.polygons)
		std::cerr << ' ' << i;
	std::cerr << std::endl << "\tsplit: ";
	for (int i : split.vertices)
		std::cerr << "(" << mesh_vertices[i].p << ") ";
	for (int i : split.polygons)
		std::cerr << ' ' << i;
	std::cerr << std::endl;
#endif
	Vertex& v1 = mesh_vertices[split.vertices.front()];
	v1.polygons.push_back(face.id);
	Vertex& v2 = mesh_vertices[split.vertices.back()];
	v2.polygons.push_back(face.id);
	for (auto it = split.vertices.begin()+1, ite = split.vertices.end()-1; it != ite; ++it) {
		Vertex& v3 = mesh_vertices[*it];
		auto jt = std::find(v3.polygons.begin(), v3.polygons.end(), polygon.id);
		assert(jt != v3.polygons.end());
		*jt = face.id; // replace polygon with face, thus saves and erase and and
	}
	// reconnect neighbours
	for (int i = 1, ie = static_cast<int>(split.vertices.size()); i < ie; ++i) {
		int pid = split.polygons[i];
		if (pid != 0) { // if pid == 0, is on mesh edge
			Polygon& p2 = mesh_polygons[pid < 0 ? -pid : pid];
			LocalId d{ p2.findVertexNeighbour(split.vertices[i]) };
			assert(pid < 0 ? p2.polygons[d.id] == -polygon.id : p2.polygons[d.id] == polygon.id);
			p2.polygons[d.id] = pid < 0 ? -face.id : face.id; // replace
		}
	}
	// make final adjustments to faces
	split.traversable = orig.traversable;
	calcBounds(orig);
	calcBounds(split);
	//facerReinsertOp(orig);
	//facerInsertOp(split);
	// check that faces are CCW in debug mode
	assert(isPolygonValid(polygon));
	assert(isPolygonValid(face));
	return face;
}

auto Mesh::splitFace(GlobalId polygon, GlobalId from, GlobalId to, double fromM, double toM) -> GlobalId
{
	auto l = convVertexIds(mesh_polygons[polygon.id], from, to);
	return splitFace(polygon, l[0], l[1], fromM, toM);
}

auto Mesh::insertPoint(Polygon& orig, LocalId i, const Point& pt) -> GlobalId
{
	GlobalId op{orig.polygons[i.id]};
	GlobalId absop{std::abs(op.id)};
	Polygon& opp = mesh_polygons[absop.id];
	// create new vertex
	GlobalId p = newVertex();
	Vertex& v = mesh_vertices[p.id];
#ifndef NDEBUG
	{
	Vertex& ov = mesh_vertices[orig.vertices[i.id]];
	Vertex& vdbg = mesh_vertices[orig.vertices[ (i.id + static_cast<int>(orig.vertices.size()) - 1) % static_cast<int>(orig.vertices.size()) ]];
	assert(pt != ov.p && pt != vdbg.p && vdbg.p != ov.p);
	assert(pt.isColin(ov.p, vdbg.p) && (ov.p - pt) * (vdbg.p - pt) < -rayscanlib::geometry::epsilon);
	}
#endif
#ifdef DEBUG_POLYANYA
	std::cerr << "insert point " << pt << std::endl;
#endif
	v.p = pt;
	assert(v.corner == 0);
	if (op.id <= 0)
		v.corner = 2;
	v.polygons.push_back(orig.id);
	// insert vertex into polygon
	if (absop.id != 0) { // if edge not on mesh edge
		v.polygons.push_back(absop.id);
		int d = opp.findVertex(orig.vertices[i.id]) + 1;
		opp.vertices.insert(opp.vertices.begin() + d, p.id);
		opp.polygons.insert(opp.polygons.begin() + d, op.id < 0 ? -orig.id : orig.id);
	}
	orig.vertices.insert(orig.vertices.begin() + i.id, p.id);
	orig.polygons.insert(orig.polygons.begin() + i.id, op.id);
	if (int mp = std::max(static_cast<int>(opp.vertices.size()), static_cast<int>(orig.vertices.size()));
		mp > max_poly_sides)
		max_poly_sides = mp;
	// check that faces are CCW in debug mode
	assert(isPolygonValid(GlobalId{orig.id}));
	assert(isPolygonValid(absop));
	return p;
}
auto Mesh::insertPoint(GlobalId polygon, LocalId i, const Point& pt) -> GlobalId
{
	Polygon& orig = mesh_polygons[polygon.id];
	return insertPoint(orig, i, pt);
}
auto Mesh::insertPoint(GlobalId polygon, GlobalId vertex, const Point& pt) -> GlobalId
{
	Polygon& orig = mesh_polygons[polygon.id];
	return insertPoint(orig, convVertexId(orig, vertex), pt);
}
auto Mesh::insertPoint(GlobalId polygon, LocalId i, double iM) -> GlobalId
{
	Polygon& orig = mesh_polygons[polygon.id];
	int s = static_cast<int>(orig.vertices.size());
	return insertPoint(orig, i, get_point_on_line(mesh_vertices[orig.vertices[i.id]].p, mesh_vertices[orig.vertices[(i.id+s-1) % s]].p, iM));
}
auto Mesh::insertPoint(GlobalId polygon, GlobalId vertex, double iM) -> GlobalId
{
	Polygon& orig = mesh_polygons[polygon.id];
	LocalId i = convVertexId(orig, vertex);
	int s = static_cast<int>(orig.vertices.size());
	return insertPoint(orig, i, get_point_on_line(mesh_vertices[orig.vertices[i.id]].p, mesh_vertices[orig.vertices[(i.id+s-1) % s]].p, iM));
}

void Mesh::erasePoint(Vertex&)
{
}

auto Mesh::findAdjVertices(Vertex& vtx) -> std::vector<GlobalId>&
{
	m_adjVertices.clear();
	for (int p : vtx.polygons) {
		const Polygon& ply = mesh_polygons[p];
		assert(ply.isInitalised());
		int vid = ply.findVertex(vtx.id);
		int size = static_cast<int>(ply.vertices.size());
		m_adjVertices.push_back(GlobalId{ply.vertices[(vid+1) % size]});
		m_adjVertices.push_back(GlobalId{ply.vertices[(vid+size-1) % size]});
	}
	std::sort(m_adjVertices.begin(), m_adjVertices.end());
	m_adjVertices.erase(std::unique(m_adjVertices.begin(), m_adjVertices.end()), m_adjVertices.end());
	return m_adjVertices;
}

auto Mesh::findOrderedVertices(Vertex& vtx) -> std::vector<GlobalId>&
{
	findAdjVertices(vtx);
	auto mid = std::partition_point(m_adjVertices.begin(), m_adjVertices.end(), [p=vtx.p,&mv=mesh_vertices] (GlobalId i) {
		const auto& p2 = mv[i.id].p - p;
		return p2.y > Point::pos_epsilon() ? true : p2.y < Point::neg_epsilon() ? false : p2.x > Point::pos_epsilon();
	});
	auto lt = [p=vtx.p,&mv=mesh_vertices] (GlobalId i, GlobalId j) {
		return p.isCCW(mv[i.id].p, mv[j.id].p);
	};
	std::sort(m_adjVertices.begin(), mid);
	std::sort(mid, m_adjVertices.end());
	return m_adjVertices;
}

void Mesh::calcBounds(Polygon& poly)
{
	poly.bounds = mesh_vertices[poly.vertices.front()].p;
	for (int i = 1, ie = static_cast<int>(poly.vertices.size()); i < ie; i++) {
		poly.bounds << mesh_vertices[poly.vertices[i]].p;
	}
}

double Mesh::calcArea2(Polygon& poly)
{
	double area = 0.0;
	Point orig = mesh_vertices[poly.vertices[0]].p;
	Point pv, at = mesh_vertices[poly.vertices[1]].p - orig;
	for (int i = 2, ie = static_cast<int>(poly.vertices.size()); i < ie; ++i) {
		pv = at;
		at = mesh_vertices[poly.vertices[i]].p - orig;
		area += pv.cross(at);
	}
	assert(area > -rayscanlib::geometry::epsilon);
	return area;
}

bool Mesh::isPolygonValid(GlobalId polygon, bool checkMesh[[maybe_unused]])
{
	if (polygon.id == 0)
		return true;
	if (polygon.id < 0 || polygon.id >= static_cast<int>(mesh_polygons.size()))
		return false;
	const Polygon& poly = mesh_polygons[polygon.id];
	if (!poly.isInitalised())
		return true;
	if (poly.id != polygon.id)
		return false;
	if (poly.vertices.size() != poly.polygons.size() || poly.vertices.size() < 3 || static_cast<int>(poly.vertices.size()) > max_poly_sides)
		return false;
	const Vertex *v1, *v2, *v3;
	size_t size = poly.vertices.size();
	v2 = &mesh_vertices[poly.vertices[size-2]];
	v3 = &mesh_vertices[poly.vertices[size-1]];
	int pvid, vid;
	vid = poly.vertices.back();
	double area = 0;
	for (size_t i = 0; i < size; i++) {
		pvid = vid;
		vid = poly.vertices[i];
		if (!isVertexValid(GlobalId{static_cast<int>(vid)}))
			return false;
		v1 = v2;
		v2 = v3;
		v3 = &mesh_vertices[vid];
		area += v2->p.cross(v3->p);
		if (v2->p.square(v3->p) < 1e-12)
			return false;
		if (!std::all_of(poly.vertices.begin(), poly.vertices.end(), [p=v3->p, vid, &mv=mesh_vertices] (int j) { return vid == j || mv[j].p != p; }))
			return false; // duplicate points
		if (std::count(v3->polygons.begin(), v3->polygons.end(), polygon.id) != 1)
			return false;
		if (auto d = v2->p.cross(v3->p, v1->p); Point::isCW(d))
			return false;
		else if (!Point::isCCW(d) && !v2->p.isBack(v3->p, v1->p)) // colin && points equal or in same direciton
			return false;
		if (int nid = poly.polygons[i]; nid != 0) {
			const Polygon& p2 = mesh_polygons[std::abs(nid)];
#if 0
			if (checkMesh) {
				if (nid > 0 && poly.is_traversable != p2.is_traversable)
					return false;
				if (nid < 0 && poly.is_traversable == p2.is_traversable)
					return false;
			}
#endif
			int p2id = p2.findVertexOrEnd(vid);
			if (p2id < 0)
				return false;
			int p2idn = (p2id + 1) % static_cast<int>(p2.vertices.size());
			if (vid != p2.vertices[p2id] || pvid != p2.vertices[p2idn])
				return false;
			if (int onid = p2.polygons[p2idn]; nid < 0 ? onid != -polygon.id : onid != polygon.id)
				return false;
		}
	}
	if (std::abs(area) < Point::pos_epsilon())
		return false;
	return true;
}

bool Mesh::isVertexValid(GlobalId vertex)
{
	static std::vector<int> corner;
	if (vertex.id == 0)
		return true;
	if (vertex.id < 0 || vertex.id >= static_cast<int>(mesh_vertices.size()))
		return false;
	const Vertex& v = mesh_vertices[vertex.id];
	if (v.isInitalised()) {
		if (v.id != vertex.id)
			return false;
		corner.clear();
		for (int pid : v.polygons) {
			if (pid <= 0 || pid >= static_cast<int>(mesh_polygons.size()))
				return false;
			const Polygon& p = mesh_polygons[pid];
			if (!p.isInitalised())
				return false;
			int vid = p.findVertexOrEnd(vertex.id);
			if (vid < 0)
				return false;
			int psize = static_cast<int>(p.polygons.size());
			if (p.polygons[vid] <= 0)
				corner.push_back(p.vertices[(vid+psize-1) % psize]);
			if (p.polygons[(vid+1) % psize] <= 0)
				corner.push_back(p.vertices[(vid+1) % psize]);
		}
		std::sort(corner.begin(), corner.end());
		if (v.corner != static_cast<int>(std::distance(corner.begin(), std::unique(corner.begin(), corner.end()))))
			return false;
	}
	return true;
}

bool Mesh::isMeshValid()
{
	// check for valid vertex
	for (int i = 0, ie = static_cast<int>(mesh_vertices.size()); i < ie; i++) {
		if (!isVertexValid(GlobalId{i}))
			return false;
	}
	// check for valid face
	for (int i = 0, ie = static_cast<int>(mesh_polygons.size()); i < ie; i++) {
		if (!isPolygonValid(GlobalId{i}, true))
			return false;
	}
	// check that all vertices have edge degree of 0 or 2
	std::vector<int> closedPt;
	std::vector<int> borderPt;
	for (int i = 0, ie = static_cast<int>(mesh_vertices.size()); i < ie; i++) {
		Vertex& v = mesh_vertices[i];
		if (!v.isInitalised())
			continue;
		closedPt.clear();
		borderPt.clear();
		for (int pid : v.polygons) {
			Polygon& p = mesh_polygons[pid];
			int pv = p.findVertexOrEnd(i);
			int size = static_cast<int>(p.vertices.size());
			if (pv == size || p.vertices[pv] != i)
				return false;
			if (int pd = p.polygons[pv]; pd < 0)
				closedPt.push_back(p.vertices[(pv + size - 1) % size]);
			else if (pd == 0)
				borderPt.push_back(p.vertices[(pv + size - 1) % size]);
			if (int pd = p.polygons[(pv + 1) % size]; pd < 0)
				closedPt.push_back(p.vertices[(pv + 1) % size]);
			else if (pd == 0)
				borderPt.push_back(p.vertices[(pv + 1) % size]);
		}
		// handle border, it counts as 1 size
		std::sort(borderPt.begin(), borderPt.end());
		size_t bsize = std::distance(borderPt.begin(), std::unique(borderPt.begin(), borderPt.end()));
		std::sort(closedPt.begin(), closedPt.end());
		size_t csize = std::distance(closedPt.begin(), std::unique(closedPt.begin(), closedPt.end()));
		if (!v.isAmbig()) {
			if (bsize == 2)
				bsize = 1;
			else if (bsize != 0)
				return false;

			if (csize == 1) {
			//	if (bsize != 1)
			//		return false;
			} else if (csize > 2) {
				return false;
			}
		}
	}
	return true;
}

auto Mesh::newVertex() -> GlobalId
{
	GlobalId x;
	if (mesh_verticesReuse.empty()) {
		x.id = static_cast<int>(mesh_vertices.size());
		mesh_vertices.emplace_back(x.id);
	} else {
		x = mesh_verticesReuse.back();
		mesh_verticesReuse.pop_back();
		mesh_vertices[x.id].id = x.id;
	}
	return x;
}
void Mesh::freeVertex(GlobalId i)
{
	mesh_vertices[i.id].reset();
	mesh_verticesReuse.push_back(i);
}

auto Mesh::newPolygon() -> GlobalId
{
	GlobalId x;
	if (mesh_polygonsReuse.empty()) {
		x.id = static_cast<int>(mesh_polygons.size());
		mesh_polygons.emplace_back(x.id);
	} else {
		x = mesh_polygonsReuse.back();
		mesh_polygonsReuse.pop_back();
		mesh_polygons[x.id].id = x.id;
	}
	return x;
}
void Mesh::freePolygon(GlobalId i)
{
	mesh_polygons[i.id].reset();
	mesh_polygonsReuse.push_back(i);
}

auto Mesh::newEuclidean() -> GlobalId
{
	GlobalId x;
	if (mesh_euclideanReuse.empty()) {
		x.id = static_cast<int>(mesh_euclidean.size());
		mesh_euclidean.emplace_back(x.id);
	} else {
		x = mesh_euclideanReuse.back();
		mesh_euclideanReuse.pop_back();
		mesh_euclidean[x.id].id = x.id;
	}
	return x;
}
void Mesh::freeEuclidean(GlobalId i)
{
	mesh_euclidean[i.id].reset();
	mesh_euclideanReuse.push_back(i);
}


void Mesh::faceMerge(Polygon& face, Polygon& merge, LocalId faceNei)
{
	GlobalId origId{face.id};
	assert(face.isInitalised() && merge.isInitalised()); // are both initalised?
	assert(face.polygons[faceNei.id] == merge.id &&  merge.polygons[merge.findVertexNeighbour(face.vertices[faceNei.id])] == face.id); // do faces connect togeather
	assert(face.isTraversable() == merge.isTraversable()); // are both traversable? or are both untraversable?
//	LocalId faceNeiB{(faceNei.id + face.vertices.size() - 1) % face.vertices.size()};
	LocalId mergeLid{};
	int ie = merge.polygons.size();
	// udpate naibourgs faces
	for (int i = 0; i < ie; ++i) {

		if (int opid = merge.polygons[i]; opid == 0)
		{ }
		else if (opid == origId.id) { // the connection
			mergeLid.id = i;
		} else {
			assert(std::abs(opid) != origId.id);
			if (opid > 0) {
				Polygon& n = mesh_polygons[opid];
				n.polygons[n.findVertexNeighbour(merge.vertices[i])] = face.id;
			} else {
				Polygon& n = mesh_polygons[-opid];
				n.polygons[n.findVertexNeighbour(merge.vertices[i])] = -face.id;
			}
		}
	}
	// update verxtex polygon list
	LocalId mergeLid2{(mergeLid.id + ie - 1) % ie};
	for (int i = 0; i < ie; ++i) {
		auto& v = mesh_vertices[merge.vertices[i]].polygons;
		auto it = std::find(v.begin(), v.end(), merge.id);
		if (i == mergeLid.id || i == mergeLid2.id)
			v.erase(it);
		else
			*it = face.id;
	}
	// merge into face
	int addSize = merge.polygons.size() - 2;
	int phase1 = (addSize+1) - mergeLid.id;
	int phaseV = std::min(addSize, phase1);
	int phaseP = std::min(addSize+1, phase1);
	// prealloc new space
	face.vertices.insert(face.vertices.begin() + faceNei.id, addSize, 0);
	face.polygons.insert(face.polygons.begin() + faceNei.id, addSize, 0);
	// copy vertices over
	std::copy_n(merge.vertices.begin() + (mergeLid.id+1), phaseV, face.vertices.begin() + faceNei.id);
	std::copy_n(merge.vertices.begin(), addSize - phaseV, face.vertices.begin() + (faceNei.id + phaseV));
	// copy faces over
	std::copy_n(merge.polygons.begin() + (mergeLid.id+1), phaseP, face.polygons.begin() + faceNei.id);
	std::copy_n(merge.polygons.begin(), (addSize+1) - phaseP, face.polygons.begin() + (faceNei.id + phaseP));
	// cleanup
	face.is_one_way = false;
	freePolygon(GlobalId{merge.id});
	if (int fs = static_cast<int>(face.vertices.size()); fs > max_poly_sides)
		max_poly_sides = fs;
}

void Mesh::meshFaceMerger(MeshMerge type)
{
	if (type == MeshMerge::None)
	{
		m_mergeTime = std::chrono::nanoseconds(0);
		return;
	}

	auto timer = std::chrono::steady_clock::now();
	for (Polygon& poly : mesh_polygons) {
		if (poly.isInitalised()) {
			switch (type) {
			case MeshMerge::Simple:
				meshFaceMergeSimple(poly);
				break;
			case MeshMerge::Greedy:
				meshFaceMergeGreedy(poly);
				break;
			default:
				break;
			}
		}
	}
	auto dur = std::chrono::steady_clock::now() - timer;
	m_mergeTime = dur;

	autoCalcBounds();
}


void Mesh::meshFaceMergeSimple(Polygon& poly)
{
	Point pv, at;
	int ie = poly.vertices.size();
	at = mesh_vertices[poly.vertices[ie-1]].p;
	for (int i = 0; i < ie; ++i) {
		pv = at;
		int vid = poly.vertices[i];
		at = mesh_vertices[vid].p;
		if (int pid = poly.polygons[i]; pid > 0) { // only merge connect polygon faces
			Polygon& P = mesh_polygons[pid];
			int pvid = P.findVertex(vid);
			Point nx = mesh_vertices[poly.vertices[(i+1) % ie]].p;
			Point Ppv = mesh_vertices[P.vertices[(pvid+P.vertices.size()-1) % P.vertices.size()]].p;
			assert(at != nx && at != Ppv && nx != Ppv);
			if (!at.isCW(nx, Ppv)) { // at point is able to merge
				Point pv2 = mesh_vertices[poly.vertices[(i+ie-2) % ie]].p;
				Point Pnx2 = mesh_vertices[P.vertices[(pvid + 2) % P.vertices.size()]].p;
				assert(pv != pv2 && pv != Pnx2 && pv2 != Pnx2);
				if (!pv.isCW(Pnx2, pv2)) { // nx point is able to merge, thus faces can be merged
					faceMerge(poly, P, LocalId{i});
				//	assert(mesh_polygons.size() >= 10000 || isMeshValid());
					i--;
					ie = poly.vertices.size();
					at = pv;
				}
			}
		}
	}
	// update mesh structure
	autoVertexSetup();
	autoCalcBounds();
}
void Mesh::meshFaceMergeGreedy(Polygon& poly)
{
	while (true) {
		int mergeId = 0;
		int mergePid = 0;
		double area = 0;
		Point pv, at;
		int ie = poly.vertices.size();
		at = mesh_vertices[poly.vertices[ie-1]].p;
		for (int i = 0; i < ie; ++i) {
			pv = at;
			int vid = poly.vertices[i];
			at = mesh_vertices[vid].p;
			if (int pid = poly.polygons[i]; pid > 0) { // only merge connect polygon faces
				Polygon& P = mesh_polygons[pid];
				int pvid = P.findVertex(vid);
				Point nx = mesh_vertices[poly.vertices[(i+1) % ie]].p;
				Point Ppv = mesh_vertices[P.vertices[(pvid+P.vertices.size()-1) % P.vertices.size()]].p;
				assert(at != nx && at != Ppv && nx != Ppv);
				if (!at.isCW(nx, Ppv)) { // at point is able to merge
					Point pv2 = mesh_vertices[poly.vertices[(i+ie-2) % ie]].p;
					Point Pnx2 = mesh_vertices[P.vertices[(pvid + 2) % P.vertices.size()]].p;
					assert(pv != pv2 && pv != Pnx2 && pv2 != Pnx2);
					if (!pv.isCW(Pnx2, pv2)) { // nx point is able to merge, thus faces can be merged
						if (double a2 = calcArea2(P); a2 > area) {
							area = a2;
							mergeId = i;
							mergePid = pid;
						}
					}
				}
			}
		}
		if (mergePid > 0) { // merge with largest neighbour, then try merging again
			faceMerge(poly, mesh_polygons[mergePid], LocalId{mergeId});
		//	assert(mesh_polygons.size() >= 10000 || isMeshValid());
		} else // else stop merging
			break;
	}
	// update mesh structure
	autoVertexSetup();
	autoCalcBounds();
}

void Mesh::print(std::ostream& outfile)
{
	outfile << "mesh with " << mesh_vertices.size() << " vertices, " \
			<< mesh_polygons.size() << " polygons" << std::endl;
	outfile << "vertices:" << std::endl;
	for (Vertex vertex : mesh_vertices)
	{
		outfile << vertex.p << " " << vertex.isCorner() << std::endl;
	}
	outfile << std::endl;
	outfile << "polygons:" << std::endl;
	for (Polygon polygon : mesh_polygons)
	{
		for (int vertex : polygon.vertices)
		{
			outfile << mesh_vertices[vertex].p << " ";
		}
		outfile << std::endl;
	}
}

void Mesh::print_polygon(std::ostream& outfile, int index)
{
	if (index == -1)
	{
		outfile << "P!!!";
		return;
	}
	outfile << "P" << index << " [";
	Polygon& poly = mesh_polygons[index];
	const auto vertices = poly.vertices;
	const int size = (int) vertices.size();
	for (int i = 0; i < size; i++)
	{
		print_vertex(outfile, vertices[i]);
		if (i != size - 1)
		{
			outfile << ", ";
		}
	}
	outfile << "]";
}

void Mesh::print_vertex(std::ostream& outfile, int index)
{
	outfile << "V" << index << " " <<  mesh_vertices[index].p;
}

void Mesh::save(std::ostream& out) const
{
	using inx::io::accurate_number;
	if (!(out << "start meshEnv version " << VERSION << '\n'))
		throw std::runtime_error("bad write");
	std::pmr::unsynchronized_pool_resource memres;
	std::pmr::unordered_map<int, int> vertexMap(mesh_vertices.size() * 12 / 10, &memres);
	std::pmr::unordered_map<int, int> faceMap(mesh_polygons.size() * 12 / 10, &memres);
	// update mesh indexes
	for (auto& vtx : mesh_vertices) {
		if (vtx.isInitalised()) {
			if (!vertexMap.try_emplace(vtx.id, vertexMap.size()+1).second)
				throw std::runtime_error("bad mesh");
		}
	}
	// update mesh indexes
	for (auto& face : mesh_polygons) {
		if (face.isInitalised()) {
			if (!faceMap.try_emplace(face.id, faceMap.size()+1).second)
				throw std::runtime_error("bad mesh");
		}
	}
	// write header
	if (!(out << "vertices " << vertexMap.size() << " faces " << faceMap.size() << std::endl))
		throw std::runtime_error("bad write");
	// write vertices
	for (auto& vtx : mesh_vertices) {
		if (vtx.isInitalised()) {
			if (!(out << "vertex "))
				throw std::runtime_error("bad write");
			if (!(out << accurate_number(vtx.p.x) << ' ' << accurate_number(vtx.p.y) << std::endl))
				throw std::runtime_error("bad write");
		}
	}
	for (auto& face : mesh_polygons) {
		if (face.isInitalised()) {
			int ie = static_cast<int>(face.vertices.size());
			if (!(out << "face size " << ie))
				throw std::runtime_error("bad write");
			if (!face.isTraversable()) {
				if (!(out << " obst " << face.traversable))
					throw std::runtime_error("bad write");
			}
			if (!(out << "\n\tvertices"))
				throw std::runtime_error("bad write");
			for (int i : face.vertices) {
				if (!(out << ' ' << vertexMap.at(i)))
					throw std::runtime_error("bad write");
			}
			if (!(out << "\n\tneighbours"))
				throw std::runtime_error("bad write");
			for (int i : face.polygons) {
				if (i < 0)
					i = -faceMap.at(-i);
				else if (i > 0)
					i = faceMap.at(i);
				if (!(out << ' ' << i))
					throw std::runtime_error("bad write");
			}
			if (!(out << std::endl))
				throw std::runtime_error("bad write");
		}
	}
	if (!(out << "end meshEnv" << '\n'))
		throw std::runtime_error("bad write");
}
void Mesh::save(::rayscanlib::scen::Archiver& archive, std::filesystem::path filename) const
{
	inx::io::chunked_array tmpfile;
	boost::iostreams::filtering_stream<boost::iostreams::seekable> tmpstream;
	tmpstream.push(boost::ref(tmpfile));
	save(tmpstream);
	tmpstream.seekg(0, std::ios::beg);
	archive.load(tmpstream, filename);
}

void Mesh::load(const ::rayscanlib::scen::Archiver& in)
{
	namespace util = rayscanlib::utils;
	using util::bad_read_error;
	using util::invalid_syntax_error;

	enum class Phase
	{
		Header,
		Vertex,
		Face,
		Post
	};

	clear();
	auto& block = in.at("meshEnv");
	auto input = boost::iostreams::stream(block.openRead());
	std::string line, str1;
	int int1;
	int phaseV1 = 0;
	std::istringstream iss;

	Phase phase = Phase::Header;
	int64 V = -1, F = -1;
	while (util::Stream::getline(input, iss, line))
	{
		switch (phase) {
		case Phase::Header:
		{
			while (iss >> str1) {
				if (str1 == "vertices") {
					if (V >= 0)
						throw invalid_syntax_error("repeated vertices");
					if (!(iss >> V) || V < 3 || V > 1'000'000'000)
						throw invalid_syntax_error("invalid number of vertices");
				} else if (str1 == "faces") {
					if (F >= 0)
						throw invalid_syntax_error("repeated faces");
					if (!(iss >> F) || F < 1 || F > 1'000'000'000)
						throw invalid_syntax_error("invalid number of faces");
				} else {
					throw invalid_syntax_error("unknown tag");
				}
			}
			if (!util::Stream::endOfStream(iss) || V < 0 || F < 0)
				throw invalid_syntax_error("header");
			phase = Phase::Vertex;
			phaseV1 = 0; // the current vertex to read
			mesh_vertices.resize(V+1);
			mesh_polygons.resize(F+1);
			break;
		}
		//
		case Phase::Vertex:
		{
			if (!(iss >> str1) || str1 != "vertex")
				throw invalid_syntax_error("vertex");
			Vertex& v = mesh_vertices.at(++phaseV1);
			v.id = phaseV1;
			if (!(iss >> v.p.x >> v.p.y))
				throw invalid_syntax_error("vertex coordinates");
			if (!util::Stream::endOfStream(iss))
				throw invalid_syntax_error("vertex end");
			// chech for end of vertices section
			if (phaseV1 == V) {
				phase = Phase::Face;
				phaseV1 = 0;
			}
			break;
		}
		//
		case Phase::Face:
		{
			if (!(iss >> str1) || str1 != "face")
				throw invalid_syntax_error("face");
			Polygon& f = mesh_polygons.at(++phaseV1);
			f.id = phaseV1;
			int64 size;
			if (!(iss >> str1 >> size) || str1 != "size" || size < 3 || size > 1'000'000'000)
				throw invalid_syntax_error("size");
			max_poly_sides = std::max(max_poly_sides, static_cast<int>(size));
			f.vertices.resize(size);
			f.polygons.resize(size);
			f.traversable = 0;
			if ((iss >> str1)) {
				if (str1 == "obst" && (iss >> int1))
					f.traversable = int1;
				else
					throw invalid_syntax_error("face header");
			}
			if (!util::Stream::endOfStream(iss))
				throw invalid_syntax_error("face header");
			if (!util::Stream::getline(input, iss, line) || !(iss >> str1) || str1 != "vertices")
				throw invalid_syntax_error("face vertices");
			for (int64 i = 0; i < size; ++i) {
				int& id = f.vertices[i];
				if (!(iss >> id) || !(1 <= id && id <= V))
					throw invalid_syntax_error("face vertices index");
				mesh_vertices[id].polygons.emplace_back(f.id);
			}
			if (!util::Stream::getline(input, iss, line) || !(iss >> str1) || str1 != "neighbours")
				throw invalid_syntax_error("face neighbours");
			for (int64 i = 0; i < size; ++i) {
				int& id = f.polygons[i];
				if (!(iss >> id) || !(-F <= id && id <= F))
					throw invalid_syntax_error("face neighbours index");
			}
			// chech for end of faces section
			if (phaseV1 == F) {
				phase = Phase::Post;
			}
			break;
		}
		//
		case Phase::Post:
			throw invalid_syntax_error("unknown data");
		//
		}
	}
	// handle post
	if (phase != Phase::Post || !util::Stream::endOfStream(input))
		throw invalid_syntax_error("invalid end");
	// update vertex information
	bounds = mesh_vertices[1].p;
	for (int64 i = 1; i <= V; ++i) {
		Vertex& v = mesh_vertices[i];
		bounds << v.p;
	}
	autoVertexSetup();
	// update face block information with flood fill
	std::vector<bool> floodDone(F+1);
	for (int64 i = 1; i <= F; ++i) {
		Polygon& f = mesh_polygons[i];
		f.bounds = mesh_vertices[f.vertices[0]].p;
		for (int j = 1, je = static_cast<int>(f.vertices.size()); j < je; ++j) {
			f.bounds << mesh_vertices[f.vertices[j]].p;
		}
		if (floodDone[i])
			continue;
	}
	// done
}

void Mesh::autoTraversable(const environment& env)
{
	for (auto& ply : mesh_polygons) {
		if (!ply.isInitalised())
			continue;
		Point mid(0,0);
		for (int i : ply.vertices) {
			mid += mesh_vertices[i].p;
		}
		mid = (1.0 / ply.vertices.size()) * mid;
		// find if it is inside at least 1 enclosure
		bool inEnc = false;
		for (auto& enc : env.getEnclosures()) {
			if (::rayscanlib::geometry::pos_is_inside(::rayscanlib::geometry::point_in_polygon(mid, enc))) { // if mid point is inside
				inEnc = true;
				break;
			}
		}
		// find how many obstacles it is inside off
		int obstCount = 0;
		for (auto& obst : env.getObstacles()) {
			if (!::rayscanlib::geometry::pos_is_outside(::rayscanlib::geometry::point_in_polygon(mid, obst))) { // if mid point is inside
				obstCount += 1;
			}
		}
		// set traversable
		ply.traversable = (inEnc ? 0 : 1) + obstCount;
	}
}

void Mesh::autoConnect()
{
	int s = static_cast<int>(mesh_polygons.size());
#if 0
	// check for zero polys and erase
	for (int i = 1; i < s; ++i) {
		Polygon& ply = mesh_polygons[i];
		int vidp = ply.vertices.back();
		int vid;
		bool smallEdge;
		for (int j = 0, je = static_cast<int>(ply.vertices.size()); j < je; vidp = vid, ++j) {
			vid = ply.vertices[j];
			double len2 = mesh_vertices[vidp].p.square(mesh_vertices[vid].p);
		}
	}
#endif

	std::unordered_map<size_t, std::pair<int, int>> edges;
	auto make_id = [](int a, int b) noexcept -> size_t {
		if (b < a) std::swap(a, b);
		return inx::bit_pack_lsb<size_t, sizeof(size_t)/2 * inx::byte_size>(a, b);
	};
	// setup edge pairs
	for (int i = 1; i < s; ++i) {
		Polygon& ply = mesh_polygons[i];
		int vidp = ply.vertices.back();
		int vid;
		for (int j = 0, je = static_cast<int>(ply.vertices.size()); j < je; vidp = vid, ++j) {
			vid = ply.vertices[j];
			auto& edge = edges[make_id(vid, vidp)];
			if (edge.first == 0)
				edge.first = i;
			else if (edge.second == 0)
				edge.second = i;
			else
				throw std::runtime_error("invalid edge pair");
		}
	}
	// connect edges
	for (int i = 1; i < s; ++i) {
		Polygon& ply = mesh_polygons[i];
		ply.polygons.resize(ply.vertices.size());
		int vidp = ply.vertices.back();
		int vid;
		int entry = 0;
		for (int j = 0, je = static_cast<int>(ply.vertices.size()); j < je; vidp = vid, ++j) {
			vid = ply.vertices[j];
			auto& edge = edges.at(make_id(vid, vidp));
			int other = edge.first != i ? edge.first : edge.second;
			if (other == 0) // outer boundary
				ply.polygons[j] = 0;
			else // shared edge
				ply.polygons[j] = (ply.traversable == mesh_polygons[other].traversable ? 1 : -1) * other;
			if (ply.polygons[j] > 0)
				entry += 1;
		}
		ply.is_one_way = entry < 2;
	}
}

void Mesh::autoVertexSetup()
{
	// insert vertex info from polygons into vertex
	for (auto& vtx : mesh_vertices) {
		vtx.polygons.clear();
	}
	for (auto& ply : mesh_polygons) {
		if (!ply.isInitalised())
			continue;
		for (int v : ply.vertices) {
			mesh_vertices[v].polygons.emplace_back(ply.id);
		}
	}
	// init verticies
	std::vector<int> closedPt;
	std::vector<int> borderPt;
	for (int i = 0, ie = static_cast<int>(mesh_vertices.size()); i < ie; i++) {
		Vertex& v = mesh_vertices[i];
		if (!v.isInitalised())
			continue;
		closedPt.clear();
		borderPt.clear();
		for (int pid : v.polygons) {
			Polygon& p = mesh_polygons[pid];
			int pv = p.findVertexOrEnd(i);
			int size = static_cast<int>(p.vertices.size());
			if (pv == size || p.vertices[pv] != i)
				throw std::runtime_error("autoVertexSetup");
			if (int pd = p.polygons[pv]; pd < 0)
				closedPt.push_back(p.vertices[(pv + size - 1) % size]);
			else if (pd == 0)
				borderPt.push_back(p.vertices[(pv + size - 1) % size]);
			if (int pd = p.polygons[(pv + 1) % size]; pd < 0)
				closedPt.push_back(p.vertices[(pv + 1) % size]);
			else if (pd == 0)
				borderPt.push_back(p.vertices[(pv + 1) % size]);
		}
		// handle border, it counts as 1 size
		std::sort(borderPt.begin(), borderPt.end());
		size_t bsize = std::distance(borderPt.begin(), std::unique(borderPt.begin(), borderPt.end()));
		std::sort(closedPt.begin(), closedPt.end());
		size_t csize = std::distance(closedPt.begin(), std::unique(closedPt.begin(), closedPt.end()));
		assert(std::find_if(closedPt.begin(), closedPt.end(), [&borderPt](int g) { return std::binary_search(borderPt.begin(), borderPt.end(), g); }) == closedPt.end());
		v.corner = bsize+csize;
	}
}

void Mesh::autoCalcBounds()
{
	std::optional<rayscanlib::geometry::Box<double>> bnd;
	int maxSize = 0;
	for (Polygon& poly : mesh_polygons) {
		if (poly.isInitalised()) {
			calcBounds(poly);
			if (bnd)
				*bnd << poly.bounds;
			else
				bnd.emplace(poly.bounds);
			int count = std::count_if(poly.polygons.begin(), poly.polygons.end(), [](int g) { return g > 0; });
			poly.is_one_way = count < 2;
			maxSize = std::max(maxSize, static_cast<int>(poly.vertices.size()));
		}
	}
	bounds = *bnd;
	max_poly_sides = maxSize;
}

std::istream& operator>>(std::istream& in, Mesh& mesh)
{
	rayscanlib::scen::Archiver archive;
	archive.load(in, std::filesystem::path());
	mesh.load(archive);
	return in;
}

std::ostream& operator<<(std::ostream& out, const Mesh& mesh)
{
	mesh.save(out);
	return out;
}

}
