#include <rayscan/Loaders.hpp>
#include <rsapp/scen/StaticSingleTarget.hpp>
#include <rsapp/scen/StaticMultiTarget.hpp>
#include <rsapp/scen/DynamicSingleTarget.hpp>
#include <rsapp/scen/DynamicMultiTarget.hpp>

namespace rayscan
{

void Loaders::registerConfig()
{
	registerScenario<rsapp::scen::StaticSingleTarget>(rsapp::conf::ScenarioType::SingleTarget, "static");
	registerScenario<rsapp::scen::StaticSingleTarget>(rsapp::conf::ScenarioType::MultiSingleTarget, "static");
	registerScenario<rsapp::scen::StaticMultiTarget>(rsapp::conf::ScenarioType::MultiTarget, "static");
	registerScenario<rsapp::scen::DynamicSingleTarget>(rsapp::conf::ScenarioType::DynamicSingleTarget, "static");
	registerScenario<rsapp::scen::DynamicSingleTarget>(rsapp::conf::ScenarioType::DynamicMultiSingleTarget, "static");
	registerScenario<rsapp::scen::DynamicMultiTarget>(rsapp::conf::ScenarioType::DynamicMultiTarget, "static");
}

} // namespace rayscan
