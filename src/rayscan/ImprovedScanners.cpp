#include <rayscan/ImprovedScanners.hpp>
// base rayscan
#include <rayscanlib/search/RayScanSingleTarget.hpp>
// expansions
#include <rayscanlib/search/expansion/BasicExpansion.hpp>
#include <rayscanlib/search/scan/RefinedScanner.hpp>
#include <rayscanlib/search/scan/ConvexScanner.hpp>
#include <rayscanlib/search/scan/ProgressiveScanner.hpp>
// heuristic
#include <rayscanlib/search/heuristic/EuclideanHeuristic.hpp>
// raycaster
#include <rayscan/DefaultRayCast.hxx>
// mod
#include <rayscanlib/search/modulator/CacheMod.hpp>

namespace rayscan
{

void ImprovedScanners::registerConfig()
{
	using namespace rayscanlib::search;
	using namespace rayscanlib::search::modules;
	using namespace rsapp::conf::reg;
	using rayscan_st_refined = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, RefinedScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_module
	>;
	using rayscan_dst_refined = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, RefinedScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_dyn_module
	>;
	using rayscan_st_convex = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, ConvexScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_module
	>;
	using rayscan_dst_convex = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, ConvexScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_dyn_module
	>;
	using rayscan_st_cache_convex = RayScanSingleTarget<false, rayscanlib::int16,
		CacheModule, // cache comes before raycast & expansion
		BasicStartExpansionModule, BasicExpansionModule, ConvexScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_module
	>;
	using rayscan_dst_cache_convex = RayScanSingleTarget<false, rayscanlib::int16,
		CacheModule, // cache comes before raycast & expansion
		BasicStartExpansionModule, BasicExpansionModule, ConvexScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_dyn_module
	>;
	using rayscan_st_progressive = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, ProgressiveScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_module
	>;
	using rayscan_dst_progressive = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, ProgressiveScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_dyn_module
	>;

	registerInstance<ST<rayscan_st_refined>, DST<rayscan_dst_refined>>("refined");
	registerInstance<ST<rayscan_st_convex>, DST<rayscan_dst_convex>>("convex");
	registerInstance<ST<rayscan_st_cache_convex>, DST<rayscan_dst_cache_convex>>("cache+convex");
	registerInstance<ST<rayscan_st_progressive>, DST<rayscan_dst_progressive>>("progressive");
}

} // namespace rayscan
