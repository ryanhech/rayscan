#include <rayscan/Defaults.hpp>
// base rayscan
#include <rayscanlib/search/RayScanSingleTarget.hpp>
#include <rayscanlib/search/RayScanMultiSingleTarget.hpp>
// expansions
#include <rayscanlib/search/expansion/BasicExpansion.hpp>
#include <rayscanlib/search/expansion/SectorExpansion.hpp>
#include <rayscanlib/search/scan/RefinedScanner.hpp>
// heuristic
#include <rayscanlib/search/heuristic/EuclideanHeuristic.hpp>
// raycaster
#include <rayscan/DefaultRayCast.hxx>

namespace rayscan
{

void Defaults::registerConfig()
{
	using namespace rayscanlib::search;
	using namespace rayscanlib::search::modules;
	using namespace rsapp::conf::reg;
	using rayscan_st_default = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, RefinedScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_module
	>;
	using rayscan_dst_default = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, RefinedScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_dyn_module
	>;
	using rayscan_mt_default = RayScanMultiTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, RefinedScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_module
	>;
	using rayscan_dmt_default = RayScanMultiTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, RefinedScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_dyn_module
	>;
	using rayscan_st_oracle = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, RefinedScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_oracle_module
	>;
	using rayscan_mt_oracle = RayScanMultiTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, RefinedScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_oracle_module
	>;
	using rayscan_st_sector = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, SectorExpansionModule, RefinedScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_module
	>;

	registerInstance<ST<rayscan_st_default>, DST<rayscan_dst_default>, MT<rayscan_mt_default>, DMT<rayscan_dmt_default>>("default");
	registerInstance<ST<rayscan_st_oracle>, DST<rayscan_st_oracle>, MT<rayscan_mt_oracle>, DMT<rayscan_mt_oracle>>("oracle");
	registerInstance<ST<rayscan_st_sector>>("sector");
}

} // namespace rayscan
