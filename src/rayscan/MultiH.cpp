#include <rayscan/MultiH.hpp>
// base rayscan
#include <rayscanlib/search/RayScanMultiTarget.hpp>
// expansions
#include <rayscanlib/search/expansion/BasicExpansion.hpp>
#include <rayscanlib/search/expansion/MultiTargetSkipExpansion.hpp>
#include <rayscanlib/search/scan/ConvexScanner.hpp>
#include <rayscanlib/search/scan/ProgressiveScanner.hpp>
// heuristic
#include <rayscanlib/search/heuristic/ZeroHeuristic.hpp>
#include <rayscanlib/search/heuristic/EuclideanHeuristic.hpp>
// raycaster
#include <rayscan/DefaultRayCast.hxx>
// modulator
#include <rayscanlib/search/modulator/RotatorMod.hpp>
#include <rayscanlib/search/modulator/BlockingMod.hpp>
#include <rayscanlib/search/modulator/CacheMod.hpp>
#include <rayscanlib/search/modulator/TargetReassignMod.hpp>

namespace rayscan
{

void MultiH::registerConfig()
{
	using namespace rayscanlib::search;
	using namespace rayscanlib::search::modules;
	using namespace rsapp::conf::reg;
	using rayscan_mt_h0 = RayScanMultiTarget<false, rayscanlib::int16,
		MultiTargetSkipExpansionModule<true>, ConvexScannerModule<0>, // scan and expander
		ZeroHeuristicModule, // heuristic
		default_raycast_module,
		BlockingModule, RotatorModule<false>
	>;
	using rayscan_dmt_h0 = RayScanMultiTarget<false, rayscanlib::int16,
		MultiTargetSkipExpansionModule<true>, ConvexScannerModule<0>, // scan and expander
		ZeroHeuristicModule, // heuristic
		default_raycast_dyn_module,
		BlockingModule, RotatorModule<false>
	>;
	using rayscan_ora_h0 = RayScanMultiTarget<false, rayscanlib::int16,
		MultiTargetSkipExpansionModule<true>, ConvexScannerModule<0>, // scan and expander
		ZeroHeuristicModule, // heuristic
		default_raycast_oracle_module,
		BlockingModule, RotatorModule<false>
	>;
	using rayscan_mt_hnear_all = RayScanMultiTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, ConvexScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_module
	>;
	using rayscan_mt_cache_hnear_all = RayScanMultiTarget<false, rayscanlib::int16,
		CacheModule, // cache comes before raycast & expansion
		BasicStartExpansionModule, BasicExpansionModule, ConvexScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_module
	>;
	using rayscan_mt_hnear = RayScanMultiTarget<false, rayscanlib::int16,
		MultiTargetSkipExpansionModule<true>, ConvexScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_module,
		BlockingModule, RotatorModule<false>
	>;
	using rayscan_dmt_hnear = RayScanMultiTarget<false, rayscanlib::int16,
		MultiTargetSkipExpansionModule<true>, ConvexScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_dyn_module,
		BlockingModule, RotatorModule<false>
	>;
	using rayscan_mttr_hnear = RayScanMultiTarget<false, rayscanlib::int16,
		MultiTargetSkipExpansionModule<true>, ConvexScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_module,
		BlockingModule, RotatorModule<false>, TargetReassignModule
	>;
	using rayscan_dmttr_hnear = RayScanMultiTarget<false, rayscanlib::int16,
		MultiTargetSkipExpansionModule<true>, ConvexScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_dyn_module,
		BlockingModule, RotatorModule<false>, TargetReassignModule
	>;
	using rayscan_mt_chnear = RayScanMultiTarget<false, rayscanlib::int16,
		CacheModule, // cache comes before raycast & expansion
		MultiTargetSkipExpansionModule<true>, ConvexScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_module,
		BlockingModule, RotatorModule<false>
	>;
	using rayscan_dmt_chnear = RayScanMultiTarget<false, rayscanlib::int16,
		CacheModule, // cache comes before raycast & expansion
		MultiTargetSkipExpansionModule<true>, ConvexScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_dyn_module,
		BlockingModule, RotatorModule<false>
	>;
	using rayscan_ora_hnear = RayScanMultiTarget<false, rayscanlib::int16,
		MultiTargetSkipExpansionModule<true>, ConvexScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_oracle_module,
		BlockingModule, RotatorModule<false>
	>;

	registerInstance<MT<rayscan_mt_h0>, DMT<rayscan_dmt_h0>>("h0");
	registerInstance<MT<rayscan_mt_hnear>, DMT<rayscan_dmt_hnear>>("hnear");
	registerInstance<MT<rayscan_mt_chnear>, DMT<rayscan_dmt_chnear>>("cache+hnear");
	registerInstance<MT<rayscan_mt_hnear_all>>("hnear+all");
	registerInstance<MT<rayscan_mt_cache_hnear_all>>("cache+hnear+all");
	registerInstance<MT<rayscan_mttr_hnear>, DMT<rayscan_dmttr_hnear>>("hnear+tr");
	registerInstance<MT<rayscan_ora_h0>, DMT<rayscan_ora_h0>>("h0+oracle");
	registerInstance<MT<rayscan_ora_hnear>, DMT<rayscan_ora_hnear>>("hnear+oracle");
}

} // namespace rayscan
