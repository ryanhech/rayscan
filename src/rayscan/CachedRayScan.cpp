#include <rayscan/CachedRayScan.hpp>
// base rayscan
#include <rayscanlib/search/RayScanSingleTarget.hpp>
#include <rayscanlib/search/RayScanMultiTarget.hpp>
// expansions
#include <rayscanlib/search/expansion/BasicExpansion.hpp>
#include <rayscanlib/search/expansion/CacheExpansion.hpp>
#include <rayscanlib/search/scan/ConvexScanner.hpp>
#include <rayscanlib/search/scan/ProgressiveScanner.hpp>
// heuristic
#include <rayscanlib/search/heuristic/EuclideanHeuristic.hpp>
// raycaster
#include <rayscan/DefaultRayCast.hxx>

namespace rayscan
{

void CachedRayScan::registerConfig()
{
	using namespace rayscanlib::search;
	using namespace rayscanlib::search::modules;
	using namespace rsapp::conf::reg;
	using rayscan_st_cache = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, CacheExpansionModule, ConvexScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_module
	>;
	using rayscan_mt_cache = RayScanMultiTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, CacheExpansionModule, ConvexScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		default_raycast_module
	>;

	registerInstance<ST<rayscan_st_cache>, MT<rayscan_mt_cache>>("cache");
}

} // namespace rayscan
