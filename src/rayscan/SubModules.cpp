#include <rayscan/SubModules.hpp>
// base rayscan
#include <rayscanlib/search/RayScanSingleTarget.hpp>
#include <rayscanlib/search/RayScanMultiTarget.hpp>
// expansions
#include <rayscanlib/search/expansion/BasicExpansion.hpp>
#include <rayscanlib/search/expansion/TargetSkipExpansion.hpp>
#include <rayscanlib/search/scan/ConvexScanner.hpp>
#include <rayscanlib/search/scan/ProgressiveScanner.hpp>
// heuristic
#include <rayscanlib/search/heuristic/EuclideanHeuristic.hpp>
// raycaster
#include <rayscan/DefaultRayCast.hxx>
// Triangle mod
#include <rayscanlib/search/modulator/TriangleMod.hpp>
// Cache mod
#include <rayscanlib/search/modulator/CacheMod.hpp>

namespace rayscan
{

void SubModules::registerConfig()
{
	constexpr std::size_t dist = 8;
	using namespace rayscanlib::search;
	using namespace rayscanlib::search::modules;
	using namespace rsapp::conf::reg;
	using rayscan_st_block = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, TargetSkipExpansionModule<true>, ConvexScannerModule<0>,
		EuclideanHeuristicModule,
		default_raycast_module,
		BlockingModule
	>;
	using rayscan_dst_block = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, TargetSkipExpansionModule<true>, ConvexScannerModule<0>,
		EuclideanHeuristicModule,
		default_raycast_dyn_module,
		BlockingModule
	>;
	using rayscan_st_sbb = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, TargetSkipExpansionModule<true>, ConvexScannerModule<0>,
		EuclideanHeuristicModule,
		default_raycast_module,
		BypassModule<dist, true, true>,
		BlockingModule
	>;
	using rayscan_dst_sbb = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, TargetSkipExpansionModule<true>, ConvexScannerModule<0>,
		EuclideanHeuristicModule,
		default_raycast_dyn_module,
		BypassModule<dist, true, true>,
		BlockingModule
	>;
	using rayscan_st_osbb = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, TargetSkipExpansionModule<true>, ConvexScannerModule<0>,
		EuclideanHeuristicModule,
		default_raycast_oracle_module,
		BypassModule<dist, true, true>,
		BlockingModule
	>;
	using rayscan_dst_osbb = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, TargetSkipExpansionModule<true>, ConvexScannerModule<0>,
		EuclideanHeuristicModule,
		default_raycast_oracle_module,
		BypassModule<dist, true, true>,
		BlockingModule
	>;
	using rayscan_st_csbb = RayScanSingleTarget<false, rayscanlib::int16,
		CacheModule, // cache comes before raycast & expansion
		BasicStartExpansionModule, TargetSkipExpansionModule<true>, ConvexScannerModule<0>,
		EuclideanHeuristicModule,
		default_raycast_module,
		BypassModule<dist, true, true>,
		BlockingModule
	>;
	using rayscan_dst_csbb = RayScanSingleTarget<false, rayscanlib::int16,
		CacheModule, // cache comes before raycast & expansion
		BasicStartExpansionModule, TargetSkipExpansionModule<true>, ConvexScannerModule<0>,
		EuclideanHeuristicModule,
		default_raycast_dyn_module,
		BypassModule<dist, true, true>,
		BlockingModule
	>;

	registerInstance<ST<rayscan_st_block>, DST<rayscan_dst_block>>("block");
	registerInstance<ST<rayscan_st_sbb>, DST<rayscan_dst_sbb>>("skip+bypass+block");
	registerInstance<ST<rayscan_st_csbb>, DST<rayscan_dst_csbb>>("cache+skip+bypass+block");
	registerInstance<ST<rayscan_st_osbb>, DST<rayscan_dst_osbb>>("oracle+skip+bypass+block");
}

} // namespace rayscan
