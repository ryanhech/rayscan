#include <rayscan/RTree.hpp>
// base rayscan
#include <rayscanlib/search/RayScanMultiTarget.hpp>
// expansions
#include <rayscanlib/search/expansion/MultiTargetSkipExpansion.hpp>
#include <rayscanlib/search/scan/ConvexScanner.hpp>
#include <rayscanlib/search/scan/ProgressiveScanner.hpp>
// heuristic
#include <rayscanlib/search/heuristic/ZeroHeuristic.hpp>
#include <rayscanlib/search/heuristic/EuclideanHeuristic.hpp>
#include <rayscanlib/search/heuristic/EuclideanRTreeHeuristic.hpp>
// raycaster
#include <rayscan/DefaultRayCast.hxx>
// modulator
#include <rayscanlib/search/modulator/RotatorMod.hpp>
#include <rayscanlib/search/modulator/BlockingMod.hpp>
#include <rayscanlib/search/modulator/CacheMod.hpp>
#include <rayscanlib/search/modulator/TargetReassignMod.hpp>

namespace rayscan
{

namespace {
namespace reg {
using namespace rayscanlib::search;
using namespace rayscanlib::search::modules;
using namespace rsapp::conf::reg;
template <bool Dynamic, size_t F, size_t L>
using rayscan_rtree = RayScanMultiTarget<false, rayscanlib::int16,
	MultiTargetSkipExpansionModule<true>, ConvexScannerModule<0>, // scan and expander
	EuclideanRTreeHeuristicModule, // heuristic
	std::conditional_t<!Dynamic, default_raycast_module, default_raycast_dyn_module>,
	BlockingModule, RotatorModule<true>, RTreeModule<F, L>
>;
template <bool Dynamic, size_t F, size_t L>
using rayscan_tr_rtree = RayScanMultiTarget<false, rayscanlib::int16,
	MultiTargetSkipExpansionModule<true>, ConvexScannerModule<0>, // scan and expander
	EuclideanRTreeHeuristicModule, // heuristic
	std::conditional_t<!Dynamic, default_raycast_module, default_raycast_dyn_module>,
	BlockingModule, RotatorModule<true>, RTreeModule<F, L>, TargetReassignModule
>;
template <bool Dynamic, size_t F, size_t L>
using rayscan_cache_rtree = RayScanMultiTarget<false, rayscanlib::int16,
	CacheModule,
	MultiTargetSkipExpansionModule<true>, ConvexScannerModule<0>, // scan and expander
	EuclideanRTreeHeuristicModule, // heuristic
	std::conditional_t<!Dynamic, default_raycast_module, default_raycast_dyn_module>,
	BlockingModule, RotatorModule<true>, RTreeModule<F, L>
>;
}

}

void RTree::registerConfig()
{
	using namespace rayscanlib::search;
	using namespace rayscanlib::search::modules;
	using namespace rsapp::conf::reg;

	registerInstance< MT<reg::rayscan_rtree<false, 8, 24>>, DMT<reg::rayscan_rtree<true, 8, 24>> >("rtree");
	registerInstance< MT<reg::rayscan_tr_rtree<false, 8, 24>>, DMT<reg::rayscan_tr_rtree<true, 8, 24>> >("rtree+tr");
	registerInstance< MT<reg::rayscan_cache_rtree<false, 8, 24>>, DMT<reg::rayscan_cache_rtree<true, 8, 24>> >("cache+rtree");
}

} // namespace rayscan
