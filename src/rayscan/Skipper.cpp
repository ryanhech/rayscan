#include <rayscan/Skipper.hpp>
// base rayscan
#include <rayscanlib/search/RayScanSingleTarget.hpp>
#include <rayscanlib/search/RayScanMultiTarget.hpp>
// expansions
#include <rayscanlib/search/expansion/BasicExpansion.hpp>
#include <rayscanlib/search/scan/ConvexScanner.hpp>
#include <rayscanlib/search/scan/ProgressiveScanner.hpp>
// heuristic
#include <rayscanlib/search/heuristic/EuclideanHeuristic.hpp>
// raycaster
#include <rayscan/DefaultRayCast.hxx>

namespace rayscan
{

void Skipper::registerConfig()
{
	constexpr std::size_t dist = 8;
	using namespace rayscanlib::search;
	using namespace rayscanlib::search::modules;
	using namespace rsapp::conf::reg;
	using rayscan_st_skip = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, ConvexScannerModule<0>,
		EuclideanHeuristicModule,
		default_raycast_module,
		BypassModule<dist, true, false>
	>;
	using rayscan_dst_skip = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, ConvexScannerModule<0>,
		EuclideanHeuristicModule,
		default_raycast_dyn_module,
		BypassModule<dist, true, false>
	>;
	using rayscan_st_bypass = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, ConvexScannerModule<0>,
		EuclideanHeuristicModule,
		default_raycast_module,
		BypassModule<dist, false, true>
	>;
	using rayscan_dst_bypass = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, ConvexScannerModule<0>,
		EuclideanHeuristicModule,
		default_raycast_dyn_module,
		BypassModule<dist, false, true>
	>;
	using rayscan_st_sb = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, ConvexScannerModule<0>,
		EuclideanHeuristicModule,
		default_raycast_module,
		BypassModule<dist, true, true>
	>;
	using rayscan_dst_sb = RayScanSingleTarget<false, rayscanlib::int16,
		BasicStartExpansionModule, BasicExpansionModule, ConvexScannerModule<0>,
		EuclideanHeuristicModule,
		default_raycast_dyn_module,
		BypassModule<dist, true, true>
	>;

	registerInstance<ST<rayscan_st_skip>, DST<rayscan_dst_skip>>("skip");
	registerInstance<ST<rayscan_st_bypass>, DST<rayscan_dst_bypass>>("bypass");
	registerInstance<ST<rayscan_st_sb>, DST<rayscan_dst_sb>>("skip+bypass");
}

} // namespace rayscan
