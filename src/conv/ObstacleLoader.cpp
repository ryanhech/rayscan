#include <conv/ObstacleLoader.hpp>
#include <rayscanlib/scen/PolygonEnvironment.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <inx/io/transformers.hpp>
#include <fstream>

namespace conv
{

void ObstacleLoader::load(archiver& archive, paths_iter it, paths_iter ite)
{
	namespace bio = boost::iostreams;
	using PE = rayscanlib::scen::PolygonEnvironment;
	if (it == ite)
		throw std::runtime_error("no input files");
	const path& path = *it;
	PE env;
	env.makeOverlap();
	{
		std::ifstream in(path);
		std::string token1;
		int enc, obst;
		if (!(in >> token1 >> obst))
			throw std::runtime_error("bad read");
		if (token1 == "polygon" && obst == 1) {
			if (!(in >> token1 >> obst))
				throw std::runtime_error("bad read");
		}
		enc = boost::lexical_cast<int>(token1);
		if (enc < 0 || enc > 1'000'000 || obst < 0 || obst > 1'000'000)
			throw std::runtime_error("bad read");
		for (int i = 0, ie = enc + obst; i < ie; ++i) {
			int s;
			in >> s;
			if (s < 2 || s > 10'000)
				throw std::runtime_error("bad read");
			PE::polygon poly;
			double x, y;
			for (int i = 0; i < s; i++) {
				if (!(in >> x >> y))
					throw std::runtime_error("bad read");
				poly.addPoint(PE::point(x, y));
			}
			if (!poly.finalise(rayscanlib::geometry::PolyType::Auto))
				throw std::runtime_error("bad polygon");
			if (poly.orientation() == PE::obstacle_ori || poly.orientation() == ::rayscanlib::Dir::COLIN) {
				if (i < enc)
					throw std::runtime_error("inaccurate orientation for obstacle");
				env.pushObstacle(std::move(poly));
			} else {
				if (i >= enc)
					throw std::runtime_error("inaccurate orientation for enclosure");
				env.pushEnclosure(std::move(poly));
			}
		}
	}
	env.save(archive);
	++it;
	if (it != ite) {
		Loader::loadArchive(archive, it, ite);
	}
}

void ObstacleLoader::save(const archiver& archive, const path& path)
{
	using inx::io::accurate_number;

	std::ofstream outfile;
	if (path != "")
		outfile.open(path);
	std::ostream& output = (path == "" ? std::cout : outfile);

	::rayscanlib::scen::PolygonEnvironment env;
	env.load(archive);
	auto&& writePolygon = [&output] (const ::rayscanlib::scen::PolygonEnvironment::polygon& P) {
		output << P.size();
		for (const auto& p : P) {
			output << ' ' << accurate_number(p.x) << ' ' << accurate_number(p.y);
		}
		output << std::endl;
	};
	output << "polygon version 1\n";
	output << env.getEnclosures().size() << ' ' << env.getObstacles().size() << std::endl;
	for (const auto& P : env.getEnclosures()) {
		writePolygon(P);
	}
	for (const auto& P : env.getObstacles()) {
		writePolygon(P);
	}
}

} // namespace rsapp::conf
