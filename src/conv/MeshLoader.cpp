#include <conv/MeshLoader.hpp>
#include <conv/Config.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <inx/io/transformers.hpp>
#include <fstream>
#include <polyanyalib/WarthogMesh.hpp>
#ifdef POLYANYA_FADE2D
#include <polyanyamesh/Fade2dMesh.hpp>
#endif

namespace conv
{

void MeshLoader::load(archiver& archive, paths_iter it, paths_iter ite)
{
	namespace bio = boost::iostreams;
	if (it == ite)
		throw std::runtime_error("no input files");
	const path& path = *it;
	polyanyalib::WarthogMesh mesh;
	if (path == "") {
		mesh.load(std::cin);
	} else {
		std::ifstream input(path);
		mesh.load(input);
	}
	auto* config = Config::instance();
	if (!config || !config->nocheck()) {
		if (!mesh.isMeshValid())
			throw std::runtime_error("invalid mesh loaded");
	}
	mesh.save(archive);
	++it;
	if (it != ite) {
		Loader::loadArchive(archive, it, ite);
	}
}

void MeshLoader::save(const archiver& archive, const path& path)
{
	using point = polyanyalib::Point;
	using inx::io::accurate_number;
	struct VertexFace {
		int id;
		point cw, ccw;
		bool operator<(const VertexFace& rhs) const noexcept {
			return cw.isCCW(rhs.cw);
		}
	};

	std::ofstream outfile;
	if (path != "")
		outfile.open(path);
	std::ostream& output = (path == "" ? std::cout : outfile);

	polyanyalib::Mesh mesh;
	if (archive.has("meshEnv")) {
		mesh.load(archive);
	} else {
#ifdef POLYANYA_FADE2D
		polyanyalib::Fade2dMesh fademesh;
		rayscanlib::scen::PolygonEnvironment env;
		env.load(archive);
		fademesh.loadEnvironment(env);
		mesh.copy(fademesh);
#else
		throw std::runtime_error("libfade2d.so not loaded");
#endif
	}
	if (!mesh.isMeshValid())
		throw std::runtime_error("invalid mesh saved");
	outfile << "mesh\n3\n";
	std::map<int, int> vert;
	std::map<int, int> face;
	for (auto& v : mesh.mesh_vertices)
	{
		if (v.isInitalised()) {
			if (!vert.try_emplace(v.id, static_cast<int>(vert.size()+1)).second)
				throw std::runtime_error("invalid vertex");
		}
	}
	for (auto& p : mesh.mesh_polygons)
	{
		if (p.isInitalised()) {
			if (!face.try_emplace(p.id, static_cast<int>(face.size()+1)).second)
				throw std::runtime_error("invalid face");
		}
	}
	output << vert.size() << ' ' << face.size() << std::endl;

	for (auto& v : vert)
	{
		auto& V = mesh.mesh_vertices[v.first];
		point at = V.p;
		output << accurate_number(at.x) << ' ' << accurate_number(at.y);
		output << std::endl;
	}

	for (auto& f : face) {
		auto& F = mesh.mesh_polygons[f.first];
		output << (F.isTraversable() ? 1 : 0) << ' ' << F.vertices.size();
		for (int i : F.vertices) {
			output << ' ' << vert.at(i);
		}
		for (int i : F.polygons) {
			output << ' ';
			if (i < 0)
				output << -face.at(-i);
			else if (i > 0)
				output << face.at(i);
			else
				output << 0;
		}
		output << std::endl;
	}
}

} // namespace rsapp::conf
