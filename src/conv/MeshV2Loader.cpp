#include <conv/MeshV2Loader.hpp>
#include <conv/MeshLoader.hpp>
#include <polyanyalib/WarthogMesh.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <inx/io/transformers.hpp>
#include <fstream>
#ifdef POLYANYA_FADE2D
#include <polyanyamesh/Fade2dMesh.hpp>
#endif

namespace conv
{

void MeshV2Loader::load(archiver& archive, paths_iter it, paths_iter ite)
{
	MeshLoader::load(archive, it, ite);
}

void MeshV2Loader::save(const archiver& archive, const path& path)
{
	using point = polyanyalib::Point;
	using inx::io::accurate_number;
	struct VertexFace {
		int id;
		point cw, ccw;
		bool operator<(const VertexFace& rhs) const noexcept {
			return cw.isCCW(rhs.cw);
		}
	};

	std::ofstream outfile;
	if (path != "")
		outfile.open(path);
	std::ostream& output = (path == "" ? std::cout : outfile);

	polyanyalib::Mesh mesh;
	if (archive.has("meshEnv")) {
		mesh.load(archive);
	} else {
#ifdef POLYANYA_FADE2D
		polyanyalib::Fade2dMesh fademesh;
		rayscanlib::scen::PolygonEnvironment env;
		env.load(archive);
		fademesh.loadEnvironment(env);
		mesh.copy(fademesh);
#else
		throw std::runtime_error("libfade2d.so not loaded");
#endif
	}
	auto* config = Config::instance();
	if (!config || !config->nocheck()) {
		if (!mesh.isMeshValid())
			throw std::runtime_error("invalid mesh loaded");
	}
	outfile << "mesh\n2\n";
	struct VertMap
	{
		int newId;
		std::vector<int> connectedFaces;
		VertMap() : newId(0)
		{ }
		VertMap(int id) : newId(id)
		{ }
	};
	std::map<int, VertMap> vert;
	std::map<int, int> face;
	for (auto& v : mesh.mesh_vertices)
	{
		if (v.isInitalised()) {
			if (!vert.try_emplace(v.id).second)
				throw std::runtime_error("invalid vertex");
		}
	}
	for (auto& p : mesh.mesh_polygons)
	{
		if (p.isInitalised() && p.isTraversable()) {
			if (!face.try_emplace(p.id, static_cast<int>(face.size())).second)
				throw std::runtime_error("invalid face");
			for (int vid : p.vertices) {
				vert.at(mesh.mesh_vertices.at(vid).id).connectedFaces.push_back(p.id);
			}
		}
	}
	{
		// erase unneeded vertices and assign new id
		int id = 0;
		for (auto it = vert.begin(), ite = vert.end(); it != ite; ) {
			if (it->second.connectedFaces.empty()) {
				auto eit = it; ++it;
				vert.erase(eit);
			} else {
				it->second.newId = id++;
				++it;
			}
		}
	}
	output << vert.size() << ' ' << face.size() << std::endl;
	vert.emplace(-1,-1);
	face.emplace(-1,-1);

	struct VertFace
	{
		int face;
		point cw;
		point ccw;
		VertFace() noexcept : face(-1)
		{ }
		bool operator<(const VertFace& vf) const noexcept
		{
			return cw.isCCW(vf.cw);
		}
	};
	std::vector<VertFace> order;
	for (auto& v : vert)
	{
		if (v.first == -1)
			continue;
		auto& V = mesh.mesh_vertices.at(v.first);
		point at = V.p;
		output << accurate_number(at.x) << ' ' << accurate_number(at.y) << std::endl;
		// fill order with neighbouring faces
		order.clear();
		for (int p : v.second.connectedFaces) {
			auto& F = mesh.mesh_polygons.at(p);
			int Fv = F.findVertex(v.first);
			auto& emp = order.emplace_back();
			emp.face = p;
			emp.cw = mesh.mesh_vertices.at(F.vertices[F.next(Fv)]).p - V.p;
			emp.ccw = mesh.mesh_vertices.at(F.vertices[F.prev(Fv)]).p - V.p;
		}
		// sort faces CCW
		auto itm = std::partition(order.begin(), order.end(), [](const VertFace& vf) {
			auto d = point(1,0).dirx(vf.cw);
			return d == ::rayscanlib::Dir::CCW || d == ::rayscanlib::Dir::FWD;
		});
		std::sort(order.begin(), itm);
		std::sort(itm, order.end());
		for (size_t i = 0; i < order.size(); ++i) {
			if (!order[i].ccw.isColinFwd(order[(i+1) % order.size()].cw)) {
				order.emplace(order.begin() + (++i));
			}
		}
		output << order.size() << std::endl;
		for (size_t i = 0, ie = order.size(); i < ie; ++i) {
			output << face.at(order[i].face) << (i+1 != ie ? ' ' : '\n');
		}
	}

	for (auto& f : face) {
		if (f.first == -1)
			continue;
		auto& F = mesh.mesh_polygons.at(f.first);
		output << F.vertices.size() << std::endl;
		bool first = true;
		for (int i : F.vertices) {
			if (first) first = false;
			else output << ' ';
			output << vert.at(i).newId;
		}
		output << std::endl;
		first = true;
		for (int i : F.polygons) {
			if (first) first = false;
			else output << ' ';
			if (i < 0)
				output << -1;
			else
				output << face.at(i);
		}
		output << std::endl;
	}
}

} // namespace rsapp::conf
