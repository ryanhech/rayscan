#include <conv/MovingAiLoader.hpp>
#include <rayscanlib/scen/MovingAiBenchmarks.hpp>
#include <polyanyalib/Mesh.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <inx/io/transformers.hpp>
#include <fstream>
#include <vector>

namespace conv
{

namespace {
bool intersectThrough(const ::polyanyalib::SaveData::env_type& env, ::polyanyalib::SaveData::env_type::point a, ::polyanyalib::SaveData::env_type::point ab)
{
	for (auto& p : env.getEnclosures()) {
		if (::rayscanlib::geometry::line_intersect_polygon_incexc(a, ab, p).first != ::polyanyalib::SaveData::env_type::point::scale_inf())
			return true; // intersects
	}
	for (auto& p : env.getObstacles()) {
		if (::rayscanlib::geometry::line_intersect_polygon_incexc(a, ab, p).first != ::polyanyalib::SaveData::env_type::point::scale_inf())
			return true; // intersects
	}
	return false;
}
}

void MovingAiLoader::load(archiver& archive, paths_iter it, paths_iter ite)
{
	rayscanlib::scen::MovingAiBenchmarks mai;
	std::filesystem::path p1, p2;
	if (it == ite)
		throw std::runtime_error("No input given");
	if (*it != "-")
		p1 = *it;
	if (++it != ite) {
		if (*it != "-")
			p2 = *it;
		else if (it->empty())
			throw std::runtime_error("invalid map");
		++it;
	}
	if (p1.empty() && p2.empty()) {
		throw std::runtime_error("no map or scenario specified");
	}
	if (it != ite) {
		Loader::loadArchive(archive, it, ite);
	}
	if (!p2.empty()) {
		std::ifstream in(p2);
		mai.loadMap(in);
	}
	if (!p1.empty())
	{
		std::ifstream in(p1);
		mai.loadScenario(in, p2.empty(), p1);
	}
	if (archive.has("polygonEnv"))
		archive.eraseBlock("polygonEnv");
	if (!p1.empty() && archive.has("benchmarkST"))
		archive.eraseBlock("benchmarkST");
	mai.getEnvironment()->save(archive);
	if (!p1.empty())
		mai.getSuite()->save(archive, false);
}

void MovingAiLoader::save(const archiver& archive, const path& path)
{
	using SaveData = ::polyanyalib::SaveData;
	// load config vars
	double scale = Config::instance()->scale();
	// load enviromnent
	using namespace std::string_view_literals;
	SaveData data;
	if (archive.has("meshEnv"sv)) {
		SaveData::mesh_type mesh;
		mesh.load(archive);
		data.loadMesh(mesh);
		data.populateVars(scale, true);
		saveFromMesh(data);
	} else {
		SaveData data;
		SaveData::env_type env;
		env.load(archive);
		data.loadEnvironment(env);
		data.populateVars(scale, true);
		saveFromObstacles(data);
	}

	// write out
	std::ofstream outfile;
	if (path != "")
		outfile.open(path);
	std::ostream& output = (path == "" ? std::cout : outfile);
	output << "type octile\n";
	output << "height " << data.height << '\n';
	output << "width " << data.width << '\n';
	output << "map\n";
	std::string line(data.width+1, '.');
	for (ssize_t i = 0; i < data.height; ++i) {
		auto& b = data[i];
		std::transform(b.begin(), b.end(), line.begin(), [](bool g) noexcept { return g ? '.' : '@'; });
		line[data.width] = '\n';
		output << line;
	}
}

void MovingAiLoader::saveFromObstacles(::polyanyalib::SaveData& data)
{
	using SaveData = ::polyanyalib::SaveData;
	using point = SaveData::point;
	SaveData::env_type& env = *std::get<SaveData::env_type*>(data.env);
	// 0 = non-traversable, 1 = traversable
	//iterate through all grid cells, excluding padding
	for (ssize_t i = SaveData::Padding, ie = data.height - SaveData::Padding; i < ie; ++i)
	for (ssize_t j = SaveData::Padding, je = data.width - SaveData::Padding; j < je; ++j) {
		auto bl = data.cellToEnv(j, i); // the bottom left corner
		if (env.pointLocation(bl + point(0.5*data.iscale, 0.5*data.iscale)).first != ::rayscanlib::geometry::Pos::Inside) // if middle is OnEdge or Inside then cell is obstructed
			continue;
		// check for intersection through any corners
		if (intersectThrough(env, bl, point(data.iscale,0)))
			continue;
		if (intersectThrough(env, bl, point(0,data.iscale)))
			continue;
		if (intersectThrough(env, bl + point(0,data.iscale), point(data.iscale,0)))
			continue;
		if (intersectThrough(env, bl + point(data.iscale,0), point(0,data.iscale)))
			continue;
		// non-obstructed
		data[i][j] = true;
	}
}

void MovingAiLoader::saveFromMesh(::polyanyalib::SaveData& data)
{
	using SaveData = ::polyanyalib::SaveData;
	using point = SaveData::point;
	namespace geo = ::rayscanlib::geometry;
	SaveData::mesh_type& mesh = *std::get<SaveData::mesh_type*>(data.env);

	// draw the polygon on the grid
	auto&& cellInPolygon = [] (point bl, point diff, const geo::Polygon<double>& P) {
		point center = P.front();
		for (size_t i = 1, ie = P.size(); i <  ie; ++i) {
			center += P[i];
		}
		center = (1.0 / P.size()) * center;
		return geo::point_in_convex_polygon(bl + 0.5*diff, P) != geo::Pos::Outside
			 || geo::Box<double>(bl, bl+diff).strictly_within(center)
			 || geo::line_intersect_polygon_incexc(bl, point(diff.x,0), P).first != point::scale_inf()
			 || geo::line_intersect_polygon_incexc(bl, point(0,diff.y), P).first != point::scale_inf()
			 || geo::line_intersect_polygon_incexc(bl + point(0,diff.y), point(diff.x,0), P).first != point::scale_inf()
			 || geo::line_intersect_polygon_incexc(bl + point(diff.x,0), point(0,diff.y), P).first != point::scale_inf();
	};
	auto&& drawPoly = [&data,&cellInPolygon] (const geo::Polygon<double>& P, bool trav) {
		auto Pbox = geo::box_polygon(P);
		auto cellLower = data.envToCell(Pbox.lower());
		auto cellUpper = data.envToCell(Pbox.upper());
		std::swap(cellLower.second, cellUpper.second);
		assert(cellLower.first <= cellUpper.first && cellLower.second <= cellUpper.second);
		for (ssize_t i = std::max(cellLower.second - 2, SaveData::Padding), ie = std::min(cellUpper.second + 3, data.height - SaveData::Padding); i < ie; ++i)
		for (ssize_t j = std::max(cellLower.first - 2, SaveData::Padding), je = std::min(cellUpper.first + 3, data.width - SaveData::Padding); j < je; ++j) {
			auto bl = data.cellToEnv(j, i); // the bottom left corner
			if (cellInPolygon(bl, point(data.iscale, data.iscale), P)) {
				// cell is obstructed
				data[i][j] = trav;
			} else {
				bool skip = false;
				if (i+1 != ie) {
					if (!cellInPolygon(point(bl.x, bl.y - data.iscale), point(data.iscale, data.iscale), P)
					 &&  cellInPolygon(point(bl.x, bl.y - data.iscale), point(data.iscale, 2*data.iscale), P)) {
						 data[i][j] = trav;
						 skip = true;
					 }
				}
				if (!skip && j+1 != je) {
					if (!cellInPolygon(point(bl.x + data.iscale, bl.y), point(data.iscale, data.iscale), P)
					 &&  cellInPolygon(bl, point(2*data.iscale, data.iscale), P)) {
						 data[i][j] = trav;
					 }
				}
			}
		}
	};

	// for every polygon, draw every traversable polygon on the grid
	for (const auto& ply : mesh.mesh_polygons) {
		// test for filled in polygon
		if (!ply.isInitalised() || !ply.isTraversable())
			continue;
		// construct polygon we can use
		geo::Polygon<double> P;
		for (int i = 0, ie = static_cast<int>(ply.vertices.size()); i < ie; ++i) {
			auto at = mesh.mesh_vertices[ply.vertices[i]].p;
			P.addPoint(at);
		}
		if (!P.finalise(geo::PolyType::Auto) || !geo::is_convex_polygon<true>(P))
			throw std::runtime_error("invalid mesh face");
		// perform testing
		drawPoly(P, true);
	}

	// for every polygon, check the non-traversable polygons against the grid and fill
	for (const auto& ply : mesh.mesh_polygons) {
		// test for filled in polygon
		if (!ply.isInitalised())
			continue;
		bool trav = ply.isTraversable();
		// construct polygon we can use
		geo::Polygon<double> P;
		decltype(P)::point_type pv, at = mesh.mesh_vertices[ply.vertices.back()].p;
		for (int i = 0, ie = static_cast<int>(ply.vertices.size()); i < ie; ++i) {
			pv = at;
			at = mesh.mesh_vertices[ply.vertices[i]].p;
			if (!trav)
				P.addPoint(at);
			if (ply.polygons[i] <= 0) {
				geo::Polygon<double> P2;
				P2.addPoint(pv);
				P2.addPoint(at);
				if (!P2.finalise(geo::PolyType::Auto))
					throw std::runtime_error("invalid edge");
				drawPoly(P2, false);
			}
		}
		if (!trav) {
			if (!P.finalise(geo::PolyType::Auto) || !geo::is_convex_polygon<true>(P))
				throw std::runtime_error("invalid mesh face");
			// perform testing
			drawPoly(P, false);
		}
	}
}

void MovingAiScenarioLoader::load(archiver& archive, paths_iter it, paths_iter ite)
{
	rayscanlib::scen::MovingAiBenchmarks mai;
	std::filesystem::path p1, p2;
	if (it == ite)
		throw std::runtime_error("No input given");
	if (it->empty() || *it == "-")
		throw std::runtime_error("Invalid scenario file");
	p1 = *it;
	if (++it == ite) {
		throw std::runtime_error("No archive specified");
	} else if (*it != "-") {
		if (it->empty())
			throw std::runtime_error("Map parameter empty");
		p2 = *it;
	}
	if (++it == ite) {
		throw std::runtime_error("No archive specified");
	} else {
		Loader::loadArchive(archive, it, ite);
	}
	if (!p2.empty()) {
		std::ifstream in(p2);
		mai.loadMap(in);
	}
	{
		std::ifstream in(p1);
		mai.loadScenario(in, p2.empty(), p1);
	}
	if (archive.has("benchmarkST"))
		archive.eraseBlock("benchmarkST");
	mai.getSuite()->save(archive, false);
}

void MovingAiScenarioLoader::save(const archiver& archive, const path& path)
{
	using inx::io::accurate_number;
	const auto& fnames = Config::instance()->env();
	if (fnames.size() != 1) {
		throw std::runtime_error("Invalid scenario map name, must specify in env once");
	}
	std::string map = std::filesystem::path(*fnames.begin()).filename();
	uint32 width, height;
	using namespace std::string_view_literals;
	if (archive.has("meshEnv"sv)) {
		::polyanyalib::Mesh mesh;
		mesh.load(archive);
		width = static_cast<uint32>(std::ceil(mesh.getBounds().width()));
		height = static_cast<uint32>(std::ceil(mesh.getBounds().height()));
	} else if (archive.has("polyEnv"sv)) {
		::rayscanlib::scen::PolygonEnvironment env;
		env.load(archive);
		width = static_cast<uint32>(std::ceil(env.getEnclosureBox().width()));
		height = static_cast<uint32>(std::ceil(env.getEnclosureBox().height()));
	} else {
		width = height = 0;
	}
	::rayscanlib::scen::StaticBenchmarkSingleTarget benchmark;
	benchmark.load(archive, false);
	std::ofstream file(path);
	file << "version 1\n";
	int batchSize = static_cast<int>(Config::instance()->batchSize());
	int batchid = 0;
	int batchsub = 0;
	for (const auto& I : benchmark.instances()) {
		file << (batchSize == 0 ? static_cast<int>(I.validId) : batchid) << '\t' << map << '\t'
			<< width << '\t' << height << '\t'
			<< accurate_number(I.start.x) << '\t' << accurate_number(I.start.y) << '\t'
			<< accurate_number(I.target.x) << '\t' << accurate_number(I.target.y) << '\t'
			<< accurate_number(I.distance.value_or(-1.0)) << '\n';
		if (++batchsub == batchSize) {
			batchid += 1;
			batchsub = 0;
		}
	}
}

} // namespace rsapp::conf
