#include <conv/IrObstacleLoader.hpp>
#include <rayscan/scen/PolygonEnvironment.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <inx/io/transformers.hpp>
#include <fstream>

namespace conv
{

void IrObstacleLoader::load(archiver& archive, paths_iter it, paths_iter ite)
{
	namespace bio = boost::iostreams;
	using PE = rayscan::scen::PolygonEnvironment;
	if (it == ite)
		throw std::runtime_error("no input files");
	const path& path = *it;
	PE env;
	env.makeOverlap();
	{
		std::ifstream in(path);
		int s;
		while (in >> s) {
			if (s < 2 || s > 10'000)
				throw std::runtime_error("bad read");
			PE::polygon poly;
			int ign;
			double x, y;
			for (int i = 0; i < s; i++) {
				if (!(in >> ign >> x >> y))
					throw std::runtime_error("bad read");
				poly.addPoint(PE::point(x, y));
			}
			if (!poly.finalise(rayscan::geometry::PolyType::Auto))
				throw std::runtime_error("bad polygon");
			if (poly.orientation() == PE::obstacle_ori || poly.orientation() == ::rayscan::Dir::COLIN)
				env.pushObstacle(std::move(poly));
			else
				env.pushEnclosure(std::move(poly));
		}
	}
	env.save(archive);
	++it;
	if (it != ite) {
		Loader::loadArchive(archive, it, ite);
	}
}

void IrObstacleLoader::save(const archiver& archive, const path& path)
{
	using inx::io::accurate_number;

	std::ofstream outfile;
	if (path != "")
		outfile.open(path);
	std::ostream& output = (path == "" ? std::cout : outfile);

	::rayscan::scen::PolygonEnvironment env;
	env.load(archive);
	auto&& writePolygon = [&output] (const ::rayscan::scen::PolygonEnvironment::polygon& P) {
		output << P.size() << '\n';
		size_t id = 0;
		for (const auto& p : P) {
			output << ++id << ' ' << accurate_number(p.x) << ' ' << accurate_number(p.y) << '\n';
		}
		output << std::endl;
	};
	writePolygon(env.getEnclosure());
	for (const auto& P : env.getObstacles()) {
		writePolygon(P);
	}
}

} // namespace rsapp::conf
