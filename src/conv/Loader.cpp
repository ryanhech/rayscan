#include <conv/Loader.hpp>
#include <conv/Config.hpp>
#include <rayscanlib/scen/StaticBenchmarkSingleTarget.hpp>
#include <rayscanlib/scen/StaticBenchmarkMultiTarget.hpp>
#include <rayscanlib/scen/DynamicBenchmarkSingleTarget.hpp>
#include <rayscanlib/scen/DynamicBenchmarkMultiTarget.hpp>
#include <polyanyamesh/Fade2dMesh.hpp>
#include <list>
#include <fstream>
#include <iostream>
#include <map>
#include <algorithm>
// polyanya
#include <polyanyalib/Polyanya.hpp>
#include <polyanyalib/PolySearch.hpp>
#include <polyanyalib/WalkFacer.hpp>
#include <polyanyalib/EuclideanHeuristic.hpp>
#ifdef POLYANYA_FADE2D
#include <polyanyamesh/Fade2dMesh.hpp>
#endif

namespace conv
{

Loader::Loader()
{
	emplace_load("archive", &loadArchive);
	emplace_save("archive", &saveArchive);
}

void Loader::emplace_load(std::string_view type, load_func&& fn)
{
	if (auto* inst = Config::instance(); inst) {
		if (!m_load.try_emplace(std::string(type), std::move(fn)).second) {
			throw std::runtime_error("invalid");
		}
		inst->pushLoadOption(type);
	} else {
		throw std::runtime_error("invalid config");
	}
	
}
void Loader::emplace_save(std::string_view type, save_func&& fn)
{
	if (auto* inst = Config::instance(); inst) {
		if (!m_save.try_emplace(std::string(type), std::move(fn)).second) {
			throw std::runtime_error("invalid");
		}
		inst->pushSaveOption(type);
	} else {
		throw std::runtime_error("invalid config");
	}
}

void Loader::load()
{
	if (auto* inst = Config::instance(); inst) {
		const auto& paths = inst->inputFiles();
		m_load.at(inst->loadFormat())(m_archive, paths.cbegin(), paths.cend());
	} else {
		throw std::runtime_error("invalid config");
	}
}

void Loader::save()
{
	if (auto* inst = Config::instance(); inst) {
		const auto& path = inst->outputFile();
		m_save.at(inst->saveFormat())(m_archive, path);
	} else {
		throw std::runtime_error("invalid config");
	}
}

void Loader::loadArchive(archiver& archive, paths_iter it, paths_iter ite)
{
	if (it == ite)
		throw std::runtime_error("no input files");
	for ( ; it != ite; ++it) {
		const path& path = *it;
		if (path.empty()) {
			archive.load(std::cin, std::filesystem::path());
		} else {
			archive.load(path);
		}
	}
}

void Loader::saveArchive(const archiver& archive, const path& path)
{
	archiver::Link link = archiver::NoLink;
	auto* conf = Config::instance();
	if (conf != nullptr && conf->link())
		link = archiver::LinkRel;
	const archiver* archiveSave = &archive;
	archiver archive2;
	if (conf != nullptr && conf->conv() != ConvType::None) {
		archive.copy(archive2);
		archiveSave = &archive2;
		if (conf->conv() == ConvType::Obstacle)
			applyConvObst(archive2);
		int meshC = conf->conv() == ConvType::Mesh ? 0 : conf->conv() == ConvType::MCDT ? 1 : conf->conv() == ConvType::MCDTG ? 2 : -1;
		if (meshC >= 0)
			applyConvMesh(archive2, meshC);
	}
	if (conf != nullptr && conf->compute() != 0) {
		if (archiveSave != &archive2) {
			archive.copy(archive2);
			archiveSave = &archive2;
		}
		if ( (conf->compute() & ComputeST) )
			applyCompute<::rayscanlib::scen::StaticBenchmarkSingleTarget>(archive2);
		if ( (conf->compute() & ComputeMT) )
			applyCompute<::rayscanlib::scen::StaticBenchmarkMultiTarget>(archive2);
		if ( (conf->compute() & ComputeDST) )
			applyCompute<::rayscanlib::scen::DynamicBenchmarkSingleTarget>(archive2);
		if ( (conf->compute() & ComputeDMT) )
			applyCompute<::rayscanlib::scen::DynamicBenchmarkMultiTarget>(archive2);
	}
	if (conf != nullptr && !conf->env().empty()) {
		if (archiveSave != &archive2) {
			archive.copy(archive2);
			archiveSave = &archive2;
		}
		std::vector<std::string> eraseBlocks;
		const auto& envVars = conf->env();
		for (auto&& [name, block] : archive2.getBlocksMutable()) {
			if (envVars.contains(name)) {
				block.clearFilename();
			} else {
				eraseBlocks.emplace_back(name);
			}
		}
		if (link == archiver::NoLink) {
			for (auto& b : eraseBlocks) {
				if (archive2.has(b)) {
					archive2.eraseBlock(b);
				}
			}
		}
	}
	if (path.empty()) {
		archiveSave->save(std::cout, std::filesystem::path(), link);
	} else {
		archiveSave->save(path, link);
	}
}

void Loader::applyConvObst(archiver& archive)
{
	if (archive.has("meshEnv")) {
		using env_type = ::rayscanlib::scen::PolygonEnvironment;
		using polygon_type = env_type::polygon;
		using poly_list = std::pmr::list<polygon_type>;
		::polyanyalib::Mesh mesh;
		env_type env;
		mesh.load(archive);
		std::pmr::monotonic_buffer_resource lres(16*1024);
		std::pmr::monotonic_buffer_resource mres(16*1024);
		poly_list Penc(&lres); // the list of enclosure polygons
		poly_list Pob(&lres); // the list of obstruction polygons
		std::pmr::set<std::pair<int,int>> cellCheck(&mres);
		auto&& check = [&cellCheck] (int Fid, int Vid) {
			auto F = std::make_pair(Fid, Vid);
			if (auto Fit = cellCheck.lower_bound(F); Fit == cellCheck.end() || *Fit != F) {
				cellCheck.emplace_hint(Fit, F);
				return false;
			} else {
				return true;
			}
		};
		auto&& trace = [&] (int Fid, int Fi) {
			const auto* F = &mesh.mesh_polygons[Fid];
			const auto* V = &mesh.mesh_vertices[F->vertices[Fi]];
			const int origFid = Fid;
			const int origVid = V->id;
			assert(F->isInitalised() && V->isInitalised());
			polygon_type ply;
			do {
				if (ply.addPoint(V->p) < 0)
					throw std::runtime_error("invalid point");
				while (true) {
					assert(F->isInitalised() && V->isInitalised() && F->isTraversable());
					check(F->id, V->id);
					Fi = F->findVertex(V->id);
					Fi = F->next(Fi);
					Fid = F->polygons[Fi];
					if (Fid > 0) {
						F = &mesh.mesh_polygons[Fid];
					} else { // end
						V = &mesh.mesh_vertices[F->vertices[Fi]];
						break;
					}
				}
			}
			while (F->id != origFid || V->id != origVid);
			if (!ply.finalise(::rayscanlib::geometry::PolyType::SimplePolygon))
				throw std::runtime_error("failed to finalise polygon");
			if (ply.orientation() == env_type::enclsoure_ori)
				Penc.emplace_back(std::move(ply));
			else
				Pob.emplace_back(std::move(ply));
		};
		for (const auto& F : mesh.mesh_polygons) {
			if (!F.isInitalised() || !F.isTraversable())
				continue;
			for (int Fi = 0, Fie = static_cast<int>(F.vertices.size()); Fi < Fie; ++Fi) {
				const auto& V = mesh.mesh_vertices[F.vertices[Fi]];
				if (!V.isCorner() || check(F.id, V.id)) // obstacle edges must be on corner, do not start on ambig as that can appear multiple times
					continue;
				if (F.polygons[Fi] > 0) // not on the CCW most vertex neighbours constraint, thus do not start on this face
					continue;
				int Vccw = F.vertices[F.prev(Fi)];
				int Vcw = F.vertices[F.next(Fi)];
				for (int nF = F.polygons[F.next(Fi)]; nF > 0; ) {
					const auto& F2 = mesh.mesh_polygons[nF];
					assert(F2.isInitalised());
					int F2vi = F2.findVertexNeighbour(V.id);
					Vcw = F2.vertices[F2vi];
					nF = F2.polygons[F2vi];
				}
				{
					auto ccwn = (mesh.mesh_vertices[Vccw].p-V.p).normalise();
					auto cwn = (mesh.mesh_vertices[Vcw].p-V.p).normalise();
					if (::rayscanlib::geometry::pr_op<::rayscanlib::geometry::PRop::eqZero>(ccwn.cross(cwn))) { // colin
						auto pm = ccwn.pair_mult(cwn);
						if (polygon_type::point_type::isBack(pm.x, pm.y))
							continue; // cannot start on stright edge, must be convex/concave or a line polygon
					}
				}
				trace(F.id, Fi);
			}
		}
		// order enclosures
		for (auto it = Penc.begin(), ite = Penc.end(); it != ite; ) {
			auto& P = *it;
			auto pt = P.front();
			bool valid = true;
			for (auto jt = std::next(it); jt != ite; ++jt) {
				if (::rayscanlib::geometry::point_in_polygon(pt, *jt) == ::rayscanlib::geometry::Pos::Inside) {
					valid = false;
					break;
				}
			}
			if (valid) {
				++it;
			} else {
				auto jt = std::next(it);
				Penc.splice(ite, Penc, it);
				it = jt;
			}
		}
		// handle environment
		env.makeOverlap();
		for (auto& P : Penc) {
			env.pushEnclosure(std::move(P));
		}
		for (auto& P : Pob) {
			env.pushObstacle(std::move(P));
		}
		using namespace std::string_view_literals;
		if (archive.has("polygonEnv"sv))
			archive.eraseBlock("polygonEnv");
		env.save(archive);
	} else {
		throw std::runtime_error("missing polygonEnv in archive");
	}
}

void Loader::applyConvMesh(archiver& archive, int merge)
{
	if (archive.has("polygonEnv")) {
#ifdef POLYANYA_FADE2D
		::polyanyalib::Fade2dMesh mesh;
		::rayscanlib::scen::PolygonEnvironment env;
		env.load(archive);
		mesh.loadEnvironment(env);
		if (merge) {
			mesh.meshFaceMerger(merge == 1 ? ::polyanyalib::MeshMerge::Simple : ::polyanyalib::MeshMerge::Greedy);
		}
		using namespace std::string_view_literals;
		if (archive.has("meshEnv"sv))
			archive.eraseBlock("meshEnv");
		mesh.save(archive);
#else
		throw std::runtime_error("libfade2d.so not loaded");
#endif
	} else {
		throw std::runtime_error("missing polygonEnv in archive");
	}
}

template <typename Benchmark>
void Loader::applyCompute(archiver& archive)
{
	Benchmark benchmark;
	benchmark.load(archive);
#ifdef POLYANYA_FADE2D
	constexpr bool enable_polyanya = true;
#else
	constexpr bool enable_polyanya = false;
#endif
	if constexpr (enable_polyanya && (std::is_same_v<Benchmark, ::rayscanlib::scen::StaticBenchmarkSingleTarget> ||
		std::is_same_v<Benchmark, ::rayscanlib::scen::StaticBenchmarkMultiTarget>)) {
		using namespace polyanyalib;
		Polyanya<PolySearch, Mesh, WalkFacer, EuclideanHeuristic> polyanya;
		Mesh mesh;
		if (archive.has("meshEnv")) {
			mesh.load(archive);
		} else {
#ifdef POLYANYA_FADE2D
			Fade2dMesh fademesh;
			fademesh.loadEnvironment(*benchmark.getEnvironment());
			mesh.copy(fademesh);
#else
			throw std::runtime_error("libfade2d.so not loaded");
#endif
		}
		polyanya.loadMesh(mesh);
		for (auto& I : benchmark.instancesMutable()) {
			I.distance = polyanya.search(I.start, I.target);
		}
	} else {
		benchmark.computeDistances();
	}
	archive.eraseBlock("benchmarkST");
	benchmark.save(archive);
}

} // namespace rsapp::conf
