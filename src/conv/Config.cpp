#include <conv/Config.hpp>
#include <boost/program_options.hpp>

namespace conv
{

std::unique_ptr<Config> Config::s_instance;

Config::Config()
{ }

Config::~Config()
{ }

void Config::instanceCreate()
{
	setInstance_(std::make_unique<Config>());
}

void Config::instanceSetup(int argc, const char *const *argv)
{
	s_instance->initConfig(argc, argv);
}

void Config::setInstance_(std::unique_ptr<Config>&& inst)
{
	s_instance = std::move(inst);
}

void Config::setupProgramOptions()
{
	using namespace boost::program_options;
	m_help.add_options()
		("help", "display program command line");
	
	std::string saveopts, loadopts;
	{
		bool first = true;
		saveopts.append("format to save as, accepted [");
		for (auto& s : m_saveOptions) {
			if (!first)
				saveopts.append(", ");
			else
				first = false;
			saveopts.append(s);
		}
		saveopts.push_back(']');
		first = true;
		loadopts.append("format to load as, accepted [");
		for (auto& s : m_loadOptions) {
			if (!first)
				loadopts.append(", ");
			else
				first = false;
			loadopts.append(s);
		}
		loadopts.push_back(']');
	}

	m_convOpts.add_options()
		("no-check", bool_switch()->notifier([this] (bool w) { m_options.nocheck = w; }), saveopts.c_str())
		("save-format,S", value<std::string>()->default_value("archive")->notifier([this] (const std::string& s) { saveFormat_(s); }), saveopts.c_str())
		("load-format,L", value<std::string>()->default_value("archive")->notifier([this] (const std::string& s) { loadFormat_(s); }), loadopts.c_str())
		("input,i", value<std::vector<std::string>>()->multitoken()->required()->notifier([this] (const std::vector<std::string>& s) { setInputFiles_(s); }))
		("output,o", value<std::string>()->implicit_value("")->notifier([this] (const std::string& s) { setOutputFile_(s); }))
		("scale", value<double>()->implicit_value(1.0)->notifier([this] (double d) { setScale_(d); }), "scale the grid maps in mai")
		("link", bool_switch()->notifier([this] (bool w) { setLink_(w); }), "output archive will add load commands to files")
		("env", value<std::vector<std::string>>()->multitoken()->notifier([this] (const std::vector<std::string>& s) { addEnv_(s); }), "keep only select environment options. In scen, map filename.")
		("conv", value<std::string>()->implicit_value("")->notifier([this] (const std::string& w) { setConv_(w); }), "when outputting archive type, specify obst, mesh, mcdt, mcdtg to apply conversions")
		("compute", value<std::string>()->notifier([this] (const std::string& w) { addCompute_(w); }), "compute shortest path lengths, st, mt, dst, dmt")
		("batch", value<size_t>()->implicit_value(0)->notifier([this] (size_t d) { setBatchSize_(d); }), "set batch sizes")
		;
	m_positional.add("input", -1);
}

void Config::pushLoadOption(std::string_view type)
{
	m_loadOptions.emplace(type);
}
void Config::pushSaveOption(std::string_view type)
{
	m_saveOptions.emplace(type);
}

void Config::printHelp(std::ostream& out)
{
	boost::program_options::options_description help;
	help.add(m_convOpts).add(m_help);
	out << help;
}

void Config::initConfig(int argc, const char *const *argv)
{
	using namespace boost::program_options;
	setupProgramOptions();
	boost::program_options::options_description root;
	root.add(m_help);
	command_line_parser cmd(argc, argv);
	cmd.allow_unregistered();
	cmd.options(root);
	variables_map vm;
	store(cmd.run(), vm);
	if (!vm["help"].empty()) {
		if (vm.size() != 1)
			throw std::runtime_error("too many arguments for help");
		m_options.help = true;
	} else {
		vm.clear();
		command_line_parser cmd2(argc, argv);
		cmd2.options(m_convOpts);
		cmd2.positional(m_positional);
		root.add(m_convOpts);
		store(cmd2.run(), vm);
		notify(vm);
	}
}

void Config::saveFormat_(std::string_view type)
{
	m_options.saveFormat = type;
}

void Config::loadFormat_(std::string_view type)
{
	m_options.loadFormat = type;
}

void Config::setInputFiles_(const std::vector<std::string>& fnames)
{
	for (auto& fname : fnames) {
		auto& file = m_options.inputFiles.emplace_back(fname);
		if (file != "" && file != "-" && !std::filesystem::is_regular_file(file))
			throw std::runtime_error("invalid input file");
	}
}

void Config::setOutputFile_(std::string_view fname)
{
	m_options.outputFile.assign(fname);
	if (m_options.outputFile != "" && std::filesystem::is_directory(m_options.outputFile))
		throw std::runtime_error("invalid output file");
}

void Config::setScale_(double scale)
{
	if (!std::isfinite(scale) || scale < 1.0 || scale > 100.0)
		throw std::runtime_error("invalid scale, must fall between 1 and 100");
	m_options.scale = scale;
}

void Config::setLink_(bool link)
{
	m_options.link = link;
}

void Config::addEnv_(const std::vector<std::string>& envs)
{
	for (auto& s : envs) {
		m_options.env.emplace(s);
	}
}

void Config::setConv_(std::string_view link)
{
	using namespace std::string_view_literals;
	if (link.empty())
		m_options.conv = ConvType::None;
	else if (link == "obst"sv)
		m_options.conv = ConvType::Obstacle;
	else if (link == "mesh"sv)
		m_options.conv = ConvType::Mesh;
	else if (link == "mcdt"sv)
		m_options.conv = ConvType::MCDT;
	else if (link == "mcdtg"sv)
		m_options.conv = ConvType::MCDTG;
	else
		throw std::runtime_error("invalid conv argument");
}

void Config::addCompute_(std::string_view cmp)
{
	using namespace std::string_view_literals;
	if (cmp == "st"sv)
		m_options.computeFlags |= ComputePath::ComputeST;
	else if (cmp == "mt"sv)
		m_options.computeFlags |= ComputePath::ComputeMT;
	else if (cmp == "dst"sv)
		m_options.computeFlags |= ComputePath::ComputeDST;
	else if (cmp == "dmt"sv)
		m_options.computeFlags |= ComputePath::ComputeDMT;
	else
		throw std::runtime_error("invalid parameter");
}

void Config::setBatchSize_(size_t batch)
{
	m_options.batchSize = batch;
}

} // namespace rsapp::conf
