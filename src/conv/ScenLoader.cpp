#include <conv/ScenLoader.hpp>
#include <rayscanlib/scen/StaticBenchmarkSingleTarget.hpp>
#include <polyanyalib/Mesh.hpp>
#include <inx/io/transformers.hpp>
#include <fstream>

namespace conv
{

void ScenLoader::load(archiver& archive, paths_iter it, paths_iter ite)
{
	using BENCH = ::rayscanlib::scen::StaticBenchmarkSingleTarget;
	using POINT = BENCH::environment::point;
	auto bench = std::make_shared<BENCH>();
	std::filesystem::path p1;
	if (it == ite)
		throw std::runtime_error("No input given");
	if (it->empty() || *it == "-")
		throw std::runtime_error("Invalid scenario file");
	p1 = *it;
	if (++it != ite) {
		Loader::loadArchive(archive, it, ite);
	}
	{
		std::ifstream in(p1);
		std::string s_ign;
		double d_ign;
		double sx, sy, tx, ty, dist;
		if (!(in >> s_ign >> d_ign))
			throw std::runtime_error("invalid scenario format header");
		bench->beginInserting();
		while (in >> d_ign >> s_ign >> d_ign >> d_ign >> sx >> sy >> tx >> ty >> dist) {
			POINT start(sx, sy);
			POINT target(tx, ty);
			bench->addInstanceNoCheck(start, target);
			if (std::isfinite(dist) && dist > -::rayscanlib::epsilon<double>) {
				if (dist > ::rayscanlib::epsilon<double>)
					bench->instancesMutable().back().distance = dist;
				else if (start == target)
					bench->instancesMutable().back().distance = 0.0;
			}
		}
		bench->endInserting();
	}
	if (archive.has("benchmarkST"))
		archive.eraseBlock("benchmarkST");
	bench->save(archive, false);
}

void ScenLoader::save(const archiver& archive, const path& path)
{
	using inx::io::accurate_number;
	const auto& fnames = Config::instance()->env();
	std::string map = "-";
	if (fnames.size() > 1) {
		throw std::runtime_error("Invalid scenario map name, must specify in env once");
	} else if (fnames.size() == 1) {
		map = *fnames.begin();
		if (map == "")
			map = "-";
	}
	uint32 width, height;
	using namespace std::string_view_literals;
	if (archive.has("meshEnv"sv)) {
		::polyanyalib::Mesh mesh;
		mesh.load(archive);
		width = static_cast<uint32>(std::ceil(mesh.getBounds().width()));
		height = static_cast<uint32>(std::ceil(mesh.getBounds().height()));
	} else if (archive.has("polyEnv"sv)) {
		::rayscanlib::scen::PolygonEnvironment env;
		env.load(archive);
		width = static_cast<uint32>(std::ceil(env.getEnclosureBox().width()));
		height = static_cast<uint32>(std::ceil(env.getEnclosureBox().height()));
	} else {
		width = height = 0;
	}
	::rayscanlib::scen::StaticBenchmarkSingleTarget benchmark;
	benchmark.load(archive, false);
	std::ofstream file(path);
	file << "version 1\n";
	int batchSize = static_cast<int>(Config::instance()->batchSize());
	int batchid = 0;
	int batchsub = 0;
	for (const auto& I : benchmark.instances()) {
		file << (batchSize == 0 ? static_cast<int>(I.validId) : batchid) << '\t' << map << '\t'
			<< width << '\t' << height << '\t'
			<< accurate_number(I.start.x) << '\t' << accurate_number(I.start.y) << '\t'
			<< accurate_number(I.target.x) << '\t' << accurate_number(I.target.y) << '\t'
			<< accurate_number(I.distance.value_or(-1.0)) << '\n';
		if (++batchsub == batchSize) {
			batchid += 1;
			batchsub = 0;
		}
	}
}

} // namespace rsapp::conf
