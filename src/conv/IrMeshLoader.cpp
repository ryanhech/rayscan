#include <conv/IrMeshLoader.hpp>
#include <polyanya/WarthogMesh.hpp>
#include <polyanya/Fade2dMesh.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <inx/io/transformers.hpp>
#include <fstream>

namespace conv
{

void IrMeshLoader::load(archiver& archive, paths_iter it, paths_iter ite)
{
	namespace bio = boost::iostreams;
	if (it == ite)
		throw std::runtime_error("no input files");
	const path& path = *it;
	polyanya::WarthogMesh mesh;
	if (path == "") {
		mesh.load(std::cin, true);
	} else {
		std::ifstream input(path);
		mesh.load(input, true);
	}
	if (!mesh.isMeshValid())
		throw std::runtime_error("invalid mesh loaded");
	mesh.save(archive);
	++it;
	if (it != ite) {
		Loader::loadArchive(archive, it, ite);
	}
}

void IrMeshLoader::save(const archiver& archive[[maybe_unused]], const path& path[[maybe_unused]])
{
	throw std::logic_error("not implmeneted");
}

} // namespace rsapp::conf
