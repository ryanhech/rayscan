#include <rsapp/conf/Config.hpp>
#include <rayscand/Loaders.hpp>
#include <rayscand/Defaults.hpp>

namespace {
template <typename Reg>
void registerConfig()
{
	Reg reg;
	reg.registerConfig();
}
}

int main(int argc, char* argv[])
{
	using namespace rayscand;
	using namespace rsapp::conf;
	Config::instanceCreate();
	registerConfig<Loaders>();
	registerConfig<Defaults>();

	Config::instanceSetup(argc, argv);
	auto& conf = *Config::instance();
	if (conf.help()) {
		return 0;
	}
	auto scen = conf.generateScenario();
	auto inst = conf.generateInstance();
	scen->load();
	if (!conf.saveScenario().empty()) {
		scen->saveFiltered(conf.saveScenario());
	}
	if (!scen->run(*inst, conf.output()))
		return 1;

	return 0;
}
