#include <rsapp/scen/DynamicSingleTarget.hpp>
#include <rsapp/run/InstanceController.hpp>
#include <rsapp/conf/Config.hpp>

namespace rsapp::scen
{

void DynamicSingleTarget::load(const archiver& archive)
{
	const auto& conf = ::rsapp::conf::Config::ref();
	if (conf.type() == ::rsapp::conf::ScenarioType::DynamicMultiSingleTarget) {
		m_benchmarkMulti = std::make_shared<benchmark_multi_type>();
		m_benchmarkMulti->load(archive);
		if (m_filter) {
			m_benchmarkMulti->applyFilter(*m_filter);
		}
	} else {
		m_benchmark = std::make_shared<benchmark_type>();
		m_benchmark->load(archive);
		if (m_filter) {
			m_benchmark->applyFilter(*m_filter);
		}
	}
}

void DynamicSingleTarget::saveFiltered(std::ostream& scenario)
{
	const auto& conf = ::rsapp::conf::Config::ref();
	if (conf.type() == ::rsapp::conf::ScenarioType::DynamicMultiSingleTarget) {
		if (m_benchmarkMulti == nullptr)
			throw std::runtime_error("benchmark not loaded");
		auto filteredBenchmark = m_benchmarkMulti->refineBenchmarkByAppliedFilter();
		filteredBenchmark->save(scenario);
	} else {
		if (m_benchmark == nullptr)
			throw std::runtime_error("benchmark not loaded");
		auto filteredBenchmark = m_benchmark->refineBenchmarkByAppliedFilter();
		filteredBenchmark->save(scenario);
	}
}

bool DynamicSingleTarget::run(run::InstanceController& inst, std::ostream& results)
{
	const auto& conf = ::rsapp::conf::Config::ref();
	if (conf.type() == ::rsapp::conf::ScenarioType::DynamicMultiSingleTarget) {
		return inst.runBenchmark(std::make_any<benchmark_multi_ptr>(m_benchmarkMulti), results);
	} else {
		return inst.runBenchmark(std::make_any<benchmark_ptr>(m_benchmark), results);
	}
}

} // namespace rsapp::scen
