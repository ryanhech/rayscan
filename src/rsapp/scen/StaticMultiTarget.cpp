#include <rsapp/scen/StaticMultiTarget.hpp>
#include <rsapp/run/InstanceController.hpp>

namespace rsapp::scen
{

void StaticMultiTarget::load(const archiver& archive)
{
	m_benchmark = std::make_shared<benchmark_type>();
	m_benchmark->load(archive);
	if (m_filter) {
		m_benchmark->applyFilter(*m_filter);
	}
}

void StaticMultiTarget::saveFiltered(std::ostream& scenario)
{
	if (m_benchmark == nullptr)
		throw std::runtime_error("benchmark not loaded");
	auto filteredBenchmark = m_benchmark->refineBenchmarkByAppliedFilter();
	filteredBenchmark->save(scenario);
}

bool StaticMultiTarget::run(run::InstanceController& inst, std::ostream& results)
{
	return inst.runBenchmark(std::make_any<benchmark_ptr>(m_benchmark), results);
}

} // namespace rsapp::scen
