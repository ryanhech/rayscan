#include <rsapp/scen/Scenario.hpp>
#include <fstream>
#include <rsapp/conf/Config.hpp>

namespace rsapp::scen
{

void Scenario::load()
{
	auto* conf = conf::Config::instance();
	if (!conf)
		throw std::runtime_error("config not configured");
	load(conf->scenario());
}
void Scenario::load(const std::filesystem::path& filename)
{
	archiver archive;
	archive.load(filename);
	load(archive);
}
void Scenario::load(std::istream& scenario, const std::filesystem::path& filename)
{
	archiver archive;
	archive.load(scenario, filename);
	load(archive);
}

void Scenario::saveFiltered(const std::filesystem::path& scenario)
{
	auto* conf = conf::Config::instance();
	if (!conf)
		throw std::runtime_error("config not configured");
	std::ofstream sout(scenario.string(), std::ios::out | std::ios::trunc);
	if (!sout)
		throw std::runtime_error("file failed to open");
	saveFiltered(sout);
}

void Scenario::filter(std::string_view F)
{
	if (F.empty())
		m_filter = std::nullopt;
	else
		m_filter = F;
}

void Scenario::clearFilter()
{
	m_filter.reset();
}

void Scenario::repeat(size_t rep)
{
	m_repeat = rep;
}

} // namespace rsapp::scen
