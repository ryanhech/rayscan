#include <rsapp/conf/Config.hpp>
#include <boost/program_options.hpp>
#include <iostream>
#include <boost/lexical_cast.hpp>
#include <sstream>

namespace rsapp::conf
{

std::unique_ptr<Config> Config::s_instance;

Config::Config()
{ }

Config::~Config()
{ }

void Config::instanceCreate()
{
	setInstance_(std::make_unique<Config>());
}

void Config::instanceSetup(int argc, const char *const *argv)
{
	s_instance->initConfig(argc, argv);
}

void Config::setInstance_(std::unique_ptr<Config>&& inst)
{
	s_instance = std::move(inst);
}

void Config::setupProgramOptions()
{
	using namespace boost::program_options;
	m_help.add_options()
		("help", "display program command line");
	std::ostringstream scenType, rayscanType;
	std::vector<std::string_view> types;
	auto&& print_types = [](std::ostringstream& oss, std::vector<std::string_view>& list) {
		std::sort(list.begin(), list.end());
		list.erase(std::unique(list.begin(), list.end()), list.end());
		bool first = true;
		for (auto s : list) {
			if (first) first = false;
			else oss << std::string_view(", ");
			oss << s;
		}
	};
	// scenType description
	scenType << "type of scenario to load; values [";
	types.clear();
	for (auto& scen : m_registeredScenarios) {
		types.emplace_back(std::string_view(scen.first).substr(scen.first.find_first_of('#')+1));
	}
	print_types(scenType, types);
	scenType << ']';
	//rayscanType description
	rayscanType << "rayscan type to run; values [";
	types.clear();
	for (auto& scen : m_registeredInstances) {
		types.emplace_back(scen.first);
	}
	print_types(rayscanType, types);
	rayscanType << ']';

	m_scenarios.add_options()
		("type,t", value<std::string>()->required()->notifier([this] (const std::string& s) { type_(s); }), "RayScan type: st (single target), mst (multi single target), dst (dynamic st), mt (multi target), dmt (dynamic multi target)")
		("scenario-type,T", value<std::string>()->default_value("static")->notifier([this] (const std::string& s) { scenarioType_(s); }), scenType.str().c_str())
		("scenario-file,S", value<std::string>()->required()->notifier([this] (const std::string& s) { scenario_(s); }), "scenario filename")
		("save-scenario", value<std::string>()->notifier([this] (const std::string& s) { saveScenario_(s); }), "scenario filename to save filtered scenario to")
		("rayscan,r", value<std::string>()->default_value("default")->notifier([this] (const std::string& s) { rayscan_(s); }), rayscanType.str().c_str())
		("filter,f", value<std::string>()->default_value("")->implicit_value("")->notifier([this] (const std::string& s) { filter_(s); }), "scenario filter to apply")
		("repeat", value<std::string>()->default_value("1")->notifier([this] (const std::string& s) { repeat_(s); }), "how many times to repeat search for averaged time")
		("output,o", value<std::string>()->notifier([this] (const std::string& s) { output_(s); }), "file to output results, will default to stdout if not attached")
		("params,p", value<std::string>()->notifier([this] (const std::string& s) { param_(s); }), "additional parameters supplied to the searcher")
		;
	m_positional.add("scenario-file", 1);
}

void Config::initConfig(int argc, const char *const *argv)
{
	using namespace boost::program_options;
	setupProgramOptions();
	boost::program_options::options_description root;
	root.add(m_help);
	command_line_parser cmd(argc, argv);
	cmd.allow_unregistered();
	cmd.options(root);
	variables_map vm;
	store(cmd.run(), vm);
	if (!vm["help"].empty()) {
		if (vm.size() != 1)
			throw std::runtime_error("too many arguments for help");
		m_options.help = true;
		boost::program_options::options_description help;
		help.add(m_scenarios).add(m_help);
		std::cout << help;
	} else {
		vm.clear();
		command_line_parser cmd2(argc, argv);
		cmd2.options(m_scenarios);
		cmd2.positional(m_positional);
		root.add(m_scenarios);
		store(cmd2.run(), vm);
		notify(vm);
	}
}

void Config::type_(const std::string& type)
{
	if (type == "st") {
		m_options.type = ScenarioType::SingleTarget;
	} else if (type == "dst") {
		m_options.type = ScenarioType::DynamicSingleTarget;
	} else if (type == "mt") {
		m_options.type = ScenarioType::MultiTarget;
	} else if (type == "mst") {
		m_options.type = ScenarioType::MultiSingleTarget;
	} else if (type == "dmt") {
		m_options.type = ScenarioType::DynamicMultiTarget;
	} else if (type == "dmst") {
		m_options.type = ScenarioType::DynamicMultiSingleTarget;
	} else
		throw std::runtime_error("invalid type");
}

void Config::scenario_(const std::string& scenario)
{
	m_options.scenario.assign(scenario);
	if (!std::filesystem::is_regular_file(m_options.scenario))
		throw std::runtime_error("invalid file");
}

void Config::saveScenario_(const std::string& saveScenario)
{
	m_options.saveScenario.assign(saveScenario);
	if (auto stat = std::filesystem::status(m_options.saveScenario); std::filesystem::exists(stat) && !std::filesystem::is_regular_file(stat))
		throw std::runtime_error("invalid save file");
}

void Config::scenarioType_(const std::string& scenarioType)
{
	m_options.scenarioType = scenarioType;
}

void Config::rayscan_(const std::string& rayscan)
{
	m_options.rayscan = rayscan;
}

void Config::filter_(const std::string& filter)
{
	m_options.filter = filter;
}

void Config::repeat_(const std::string& repeat)
{
	size_t r = boost::lexical_cast<size_t>(repeat);
	if (r == 0 || r > 16)
		std::runtime_error("repeat value is invalid, must be between 1 and 16");
	m_options.repeat = r;
}

void Config::output_(const std::string& output)
{
	if (m_outputFile.is_open())
		throw std::runtime_error("output already opened, can only be set once");
	m_outputFile.open(output);
	if (!m_outputFile.is_open())
		throw std::runtime_error("failed to open output file");
	m_options.output = &m_outputFile;
}
std::ostream& Config::output() const noexcept
{
	if (m_options.output == nullptr)
		return std::cout;
	else
		return *m_options.output;
}

void Config::param_(const std::string& param)
{
	for (auto it = param.begin(), ite = param.end(); it != ite; ) {
		std::string pname, pvalue;
		if (!std::isalpha(*it))
			throw std::runtime_error("invalid param name");
		do {
			pname.push_back(*it);
		} while (++it != ite && std::isalnum(*it));
		if (it != ite && *it != ',') {
			if (*it != '=' || ++it == ite)
				throw std::runtime_error("invalid parameter value");
			while (it != ite) {
				if (*it == ',')
					break;
				if (!std::isgraph(*it))
					throw std::runtime_error("invalid parameter value");
				pvalue.push_back(*it++);
			}
		}
		if (!m_options.param.try_emplace(std::move(pname), std::move(pvalue)).second)
			throw std::runtime_error("double param");
		if (it != ite) {
			if (*it != ',')
				throw std::runtime_error("invalid param seperator");
			++it;
		}
	}
}
std::optional<std::string_view> Config::param(const std::string& param) const noexcept
{
	if (auto it = m_options.param.find(param); it != m_options.param.end()) {
		return std::string_view(it->second);
	} else {
		return std::nullopt;
	}
}

void Config::registerScenario(ScenarioType type, std::string_view name, const std::function<std::unique_ptr<scen::Scenario>()>& generator)
{
	if (!m_registeredScenarios.try_emplace(scenJoin(type, name), generator).second)
		throw std::logic_error("scenario already registered");
}
std::unique_ptr<scen::Scenario> Config::generateScenario(ScenarioType type, std::string_view name)
{
	if (auto it = m_registeredScenarios.find(scenJoin(type, name)); it != m_registeredScenarios.end()) {
		auto ptr = it->second();
		if (!ptr)
			throw std::runtime_error("scenario invalid");
		ptr->filter(filter());
		ptr->repeat(repeat());
		return ptr;
	} else {
		throw std::runtime_error("scenario invalid");
	}
}
std::unique_ptr<scen::Scenario> Config::generateScenario(std::string_view name)
{
	return generateScenario(type(), name);
}
std::unique_ptr<scen::Scenario> Config::generateScenario()
{
	return generateScenario(type(), std::string_view(scenarioType()));
}

void Config::registerInstance(std::string_view name, const std::function<std::unique_ptr<run::InstanceController>()>& generator)
{
	if (!m_registeredInstances.try_emplace(std::string(name), generator).second)
		throw std::logic_error("instance already registered");
}
std::unique_ptr<run::InstanceController> Config::generateInstance(std::string_view name)
{
	if (auto it = m_registeredInstances.find(std::string(name)); it != m_registeredInstances.end()) {
		return it->second();
	} else {
		throw std::runtime_error("instance invalid");
	}
}
std::unique_ptr<run::InstanceController> Config::generateInstance()
{
	return generateInstance(rayscan());
}

std::string Config::scenJoin(ScenarioType type, std::string_view name)
{
	using namespace std::string_view_literals;
	std::string_view hash;
	switch (type) {
	case ScenarioType::SingleTarget:
		hash = "st#"sv;
		break;
	case ScenarioType::MultiSingleTarget:
		hash = "mst#"sv;
		break;
	case ScenarioType::DynamicSingleTarget:
		hash = "dst#"sv;
		break;
	case ScenarioType::MultiTarget:
		hash = "mt#"sv;
		break;
	case ScenarioType::DynamicMultiTarget:
		hash = "dmt#"sv;
		break;
	case ScenarioType::DynamicMultiSingleTarget:
		hash = "dmst#"sv;
		break;
	}
	std::string out;
	out.reserve(hash.size() + name.size());
	out.append(hash.begin(), hash.end());
	out.append(name);
	return out;
}

} // namespace rsapp::conf
