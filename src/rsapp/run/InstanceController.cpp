#include <rsapp/run/InstanceController.hpp>
#include <rsapp/run/InstanceRunner.hpp>
#include <rsapp/conf/Config.hpp>
#include <rayscanlib/utils/StatAverager.hpp>

namespace rsapp::run
{

bool InstanceController::runBenchmark(const std::any& benchmark, std::ostream& results)
{
	auto& conf = ::rsapp::conf::Config::ref();
	auto& inst = m_runSuites.at(conf.type());
	return inst->run(benchmark, results);
}

InstanceController::InstanceController()
{ }
InstanceController::~InstanceController()
{ }

} // namespace rsapp::run
