#include <rsapp/conf/Config.hpp>
#include <rayscan/Loaders.hpp>
#include <rayscan/Defaults.hpp>
#include <rayscan/ImprovedScanners.hpp>
#include <rayscan/SubModules.hpp>
#include <rayscan/MultiH.hpp>
#include <rayscan/RTree.hpp>
#include <rayscan/Skipper.hpp>
#include <rayscan/CachedRayScan.hpp>

namespace {
template <typename Reg>
void registerConfig()
{
	Reg reg;
	reg.registerConfig();
}
}

int main(int argc, char* argv[])
{
	using namespace rayscan;
	using namespace rsapp::conf;
	Config::instanceCreate();
	registerConfig<Loaders>();
	registerConfig<Defaults>();
	registerConfig<ImprovedScanners>();
	registerConfig<SubModules>();
	registerConfig<MultiH>();
	registerConfig<RTree>();
	registerConfig<CachedRayScan>();
	registerConfig<Skipper>();

	Config::instanceSetup(argc, argv);
	auto& conf = *Config::instance();
	if (conf.help()) {
		return 0;
	}
	auto scen = conf.generateScenario();
	auto inst = conf.generateInstance();
	scen->load();
	if (!conf.saveScenario().empty()) {
		scen->saveFiltered(conf.saveScenario());
	}
	if (!scen->run(*inst, conf.output()))
		return 1;

	return 0;
}
