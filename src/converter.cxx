#include <conv/Config.hpp>
#include <conv/Loader.hpp>
#include <conv/MeshLoader.hpp>
#include <conv/MeshV2Loader.hpp>
#include <conv/ObstacleLoader.hpp>
#include <conv/MovingAiLoader.hpp>
#include <conv/ScenLoader.hpp>
#include <iostream>

int main(int argc, char* argv[])
{
	using namespace conv;
	Config::instanceCreate();
	
	Loader loader;
	loader.emplace<MeshLoader>("mesh");
	loader.emplace<MeshV2Loader>("mesh2");
	loader.emplace<ObstacleLoader>("obst");
	loader.emplace<MovingAiLoader>("mai");
	loader.emplace<MovingAiScenarioLoader>("mai-scen");
	loader.emplace<ScenLoader>("scen");

	Config::instanceSetup(argc, argv);
	auto& conf = *Config::instance();

	if (conf.help()) {
		conf.printHelp(std::cout);
		return 0;
	}
	
	loader.load();
	loader.save();

	return 0;
}
