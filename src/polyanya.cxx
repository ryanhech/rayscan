#include <polyanyalib/Config.hpp>
#include <polyanya/Loaders.hpp>
#include <polyanya/Defaults.hpp>

namespace {
template <typename Reg>
void registerConfig()
{
	Reg reg;
	reg.registerConfig();
}
}

int main(int argc, char* argv[])
{
	using polyanyalib::Config;
	Config::instanceCreate();
	registerConfig<polyanya::Loaders>();
	registerConfig<polyanya::Defaults>();
	
	Config::instanceSetup(argc, argv);
	auto& conf = *Config::instance();
	if (conf.help()) {
		return 0;
	}
	auto scen = conf.generateScenario();
	auto inst = conf.generateInstance();
	scen->load();
	if (!conf.saveScenario().empty()) {
		scen->saveFiltered(conf.saveScenario());
	}
	if (!scen->run(*inst, conf.output()))
		return 1;

	return 0;
}
