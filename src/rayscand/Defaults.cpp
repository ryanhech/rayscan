#include <rayscand/Defaults.hpp>
// base rayscan
#include <rayscanlib/search/RayScanSingleTarget.hpp>
#include <rayscanlib/search/RayScanMultiSingleTarget.hpp>
// expansions
#include <rayscanlib/search/expansion/BasicExpansion.hpp>
#include <rayscanlib/search/expansion/SectorExpansion.hpp>
#include <rayscanlib/search/scan/RefinedScanner.hpp>
#include <rayscanlib/search/scan/ConvexScanner.hpp>
// heuristic
#include <rayscanlib/search/heuristic/EuclideanHeuristic.hpp>
// rayscan
#include <rayscanlib/search/raycast/BresenhamDblRayCast.hpp>

namespace rayscand
{

void Defaults::registerConfig()
{
	using namespace rayscanlib::search;
	using namespace rayscanlib::search::modules;
	using namespace rsapp::conf::reg;

	using rayscan_st_default = RayScanSingleTarget<false, double,
		BasicStartExpansionModule, BasicExpansionModule, ConvexScannerModule<0>, // scan and expander
		EuclideanHeuristicModule, // heuristic
		BresenhamDblRayCastModule<false>
	>;

	registerInstance<ST<rayscan_st_default>, DST<rayscan_st_default>>("default");
}

} // namespace rayscand
