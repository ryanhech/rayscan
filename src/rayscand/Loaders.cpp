#include <rayscand/Loaders.hpp>
#include <rsapp/scen/StaticSingleTarget.hpp>
#include <rsapp/scen/DynamicSingleTarget.hpp>

namespace rayscand
{

void Loaders::registerConfig()
{
	registerScenario<rsapp::scen::StaticSingleTarget>(rsapp::conf::ScenarioType::SingleTarget, "static");
	registerScenario<rsapp::scen::DynamicSingleTarget>(rsapp::conf::ScenarioType::DynamicSingleTarget, "static");
}

} // namespace rayscand
