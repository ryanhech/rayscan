# RayScan 

Repository codebase for RayScan.

Scenarios were generated from the MovingAI Repository [https://movingai.com/benchmarks/](https://movingai.com/benchmarks/).

Polyanya code was modify from repository [https://bitbucket.org/dharabor/pathfinding](https://bitbucket.org/dharabor/pathfinding).

Meshes for Polyanya were generated using Fade2D [https://www.geom.at/fade2d/html/](https://www.geom.at/fade2d/html/).
Fade2D is not included in this repository nor the code we used to generate the meshes, the meshes themselves are included.

This project makes use of some Boost libraries [https://www.boost.org/](https://www.boost.org/).
Only ProgramOptions needs a compiled version in this project.

## How to use

Project is compiled using CMake [https://cmake.org/](https://cmake.org/).
Three programs are compiled, `rayscan`, `rayscand` and `polyanya`.

### RayScan

Run RayScan, parameters seen below.

    rayscan -t[st|mt|dst|dmt] -S $scenario -r $type

Program `rayscan` takes the `-t` argument as the type of scenario:

1. `st`: single-target benchmark, given with `.st.pfarc` extension
2. `st`: multi-target benchmark, given with `.mt.pfarc` extension
3. `dst`: dynamic-single-target benchmark, given with `.dst.pfarc` extension
4. `dmt`: dynamic-multi-target benchmark, given with `.dmt.pfarc` extension

The `-S` parameter is the scenario file, scenarios found undo `scenario/` directory.

The `-r` parameter is the RayScan type to run, some as seen below:

- `default`: RayScan
- `convex`: RayScan w/ convex scanner
- `oracle`: RayScan oracle
- `block`: RayScan w/ convex scanner + blocking extension
- `skip`: RayScan w/ convex scanner + skip extension
- `bypass`: RayScan w/ convex scanner + bypass extension
- `cache`: RayScan w/ cache extension
- `hnear`: Multi-target RayScan w/ blocking and nearest target heuristic
- `rtree`: Multi-target RayScan w/ blocking and nearest target heuristic and r-tree

Use `--help` for more options.

Program `rayscand` is also RayScan but support double-precision points, whereas `rayscan` only supports 16-bit integers.

### Polyanya

Polyanya works the same as `rayscan`, with differing options.

The `-r` parameter for polyanya is seen below:

- `default`: Polyanya using mesh-walking for point location
- `static`: Polyanya using an interval-structure for point location, does not work with dynamic benchmarks
- `naive`: Polyanya using a naive point location method
- `interval`: Polyanya using interval-heuristic

Polyanya takes a `-p` options as well, where `-pmerge=simple` will merge polygon faces on the mesh.

Dyanmic polyanya benchmarks do not all work in this version, as they want to re-generate the meshes during the benchmark but no mesh generation is included here.
